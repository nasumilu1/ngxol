import {Component} from '@angular/core';
import {RouterLink, RouterLinkActive, RouterOutlet} from "@angular/router";
import {MatMenu, MatMenuItem, MatMenuTrigger} from "@angular/material/menu";
import {MatButton} from "@angular/material/button";
import {MatToolbar} from "@angular/material/toolbar";
import {MatIcon} from "@angular/material/icon";
import {NgOptimizedImage} from "@angular/common";
import {MatTooltip} from "@angular/material/tooltip";

@Component({
    selector: 'app-root',
  imports: [
    RouterOutlet, RouterLink, RouterLinkActive,
    MatMenu, MatMenuItem, MatMenuTrigger, MatButton,
    MatToolbar, MatIcon, NgOptimizedImage, MatTooltip
  ],
    templateUrl: './app.component.html',
    styleUrl: './app.component.css'
})
export class AppComponent {

}
