import {ApplicationConfig, provideExperimentalZonelessChangeDetection} from '@angular/core';
import {provideRouter} from '@angular/router';

import {routes} from './app.routes';
import {provideAnimationsAsync} from '@angular/platform-browser/animations/async';
import {provideExamples, provideMapTilerAPIKey} from '@nasumilu/ngx-ol-examples';
import {provideProj4, withMapTilerApi} from "@nasumilu/ngx-ol-map";
import {provideHttpClient, withFetch} from "@angular/common/http";
import {environment} from "../environments/environment";
import {provideFeatureLoader, withHttpClient } from '@nasumilu/ngx-ol-source';

export const appConfig: ApplicationConfig = {
  providers: [
    provideHttpClient(withFetch()),
    provideFeatureLoader(withHttpClient()),
    provideMapTilerAPIKey(environment.mapTilerApiKey),
    provideProj4(
      withMapTilerApi(environment.mapTilerApiKey),
      {
        code: 'EPSG:4759',
        proj4: '+proj=longlat +ellps=GRS80 +no_defs +type=crs'
      },
      {
        code: 'EPSG:2238',
        proj4: '+proj=lcc +lat_0=29 +lon_0=-84.5 +lat_1=30.75 +lat_2=29.5833333333333 +x_0=600000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=us-ft +no_defs +type=crs'
      }
    ),
    provideExperimentalZonelessChangeDetection(),
    provideRouter(routes),
    provideAnimationsAsync(),
    provideExamples("public/examples")
  ]
};
