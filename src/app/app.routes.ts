import {Routes} from '@angular/router';
import {
  BasicMapComponent,
  BasicOverlayMapComponent,
  FullScreenMapComponent,
  OverviewMapComponent,
  RotateMapComponent,
  ScaleLineMapComponent,
  ZoomToExtentComponent,
  MousePositionMapComponent,
  KeyboardInteractionComponent,
  MouseInteractionComponent,
  DragInteractionComponent,
  ProjectionMapComponent,
  LayerGroupComponent,
  GraticuleMapComponent,
  ImageLayerComponent,
  HeatmapComponent,
  VectorTileMapComponent,
  WebglLayerMapComponent,
  EsriFeatureMapComponent,
  TopoJSONLayerMapComponent
} from '@nasumilu/ngx-ol-examples';

export const routes: Routes = [
  {
    path: 'basic-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Basic Map',
    data: {
      component: BasicMapComponent,
      example: 'basic-map'
    }
  },
  {
    path: 'overview-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Overview Map Control',
    data: {
      component: OverviewMapComponent,
      example: 'overview-map'
    }
  },
  {
    path: 'mouse-position-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Mouse Position Control',
    data: {
      component: MousePositionMapComponent,
      example: 'mouse-position-map'
    }
  },
  {
    path: 'scale-line-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Scale Line Control',
    data: {
      component: ScaleLineMapComponent,
      example: 'scale-line-map'
    }
  },
  {
    path: 'basic-overlay-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Basic Overlay (Popup)',
    data: {
      component: BasicOverlayMapComponent,
      example: 'basic-overlay-map'
    }
  },
  {
    path: 'full-screen-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Full Screen Map',
    data: {
      component: FullScreenMapComponent,
      example: 'full-screen-map'
    }
  },
  {
    path: 'zoom-to-extent',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Zoom To Extent',
    data: {
      component: ZoomToExtentComponent,
      example: 'zoom-to-extent'
    }
  },
  {
    path: 'rotate-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Rotate Control Example',
    data: {
      component: RotateMapComponent,
      example: 'rotate-map'
    }
  },
  {
    path: 'keyboard-interaction',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Keyboard Interaction',
    data: {
      component: KeyboardInteractionComponent,
      example: 'keyboard-interaction'
    }
  },
  {
    path: 'mouse-interaction',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Mouse Interaction',
    data: {
      component: MouseInteractionComponent,
      example: 'mouse-interaction'
    }
  },
  {
    path: 'drag-interaction',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Mouse & Keyboard Interaction',
    data: {
      component: DragInteractionComponent,
      example: 'drag-interaction'
    }
  },
  {
    path: 'projection-service',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Projection Service',
    data: {
      component: ProjectionMapComponent,
      example: 'projection-map'
    }
  },
  {
    path: 'layer-group',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Layer Group',
    data: {
      component: LayerGroupComponent,
      example: 'layer-group'
    }
  },
  {
    path: 'graticule-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Graticule Map',
    data: {
      component: GraticuleMapComponent,
      example: 'graticule-map'
    }
  },
  {
    path: 'image-layer',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Image Layer',
    data: {
      component: ImageLayerComponent,
      example: 'image-layer'
    }
  },
  {
    path: 'heatmap',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Heatmap',
    data: {
      component: HeatmapComponent,
      example: 'heatmap'
    }
  },
  {
    path: 'vector-tile-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'Vector Title Map',
    data: {
      component: VectorTileMapComponent,
      example: 'vector-tile-map'
    }
  },
  {
    path: 'webgl-layer-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'WebGL Map',
    data: {
      component: WebglLayerMapComponent,
      example: 'webgl-layer-map'
    }
  },
  {
    path: 'esri-feature-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'ArcGIS REST Feature Service',
    data: {
      component: EsriFeatureMapComponent,
      example: 'esri-feature-map'
    }
  },
  {
    path: 'topojson-layer-map',
    loadComponent: () => import('@nasumilu/ngx-ol-examples').then(m => m.ExampleComponent),
    title: 'TopoJSON',
    data: {
      component: TopoJSONLayerMapComponent,
      example: 'topojson-layer-map'
    }
  }
];
