import { InjectionToken, OnDestroy, OnInit, Provider } from '@angular/core';
import { OlBaseVectorLayerDirective } from "@nasumilu/ngx-ol-layer";
import Collection from "ol/Collection";
import { Feature } from "ol";
import { Geometry, Point } from "ol/geom";
import { OlSourceDirective } from "./ol-source.directive";
import { LoadingStrategy, Options } from "ol/source/Vector";
import { ProjectionLike } from "ol/proj";
import { FeatureLoader, FeatureUrlFunction } from "ol/featureloader";
import { Observable } from "rxjs";
import { FeatureLike } from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import FeatureFormat from "ol/format/Feature";
import { HttpClient } from "@angular/common/http";
import * as i0 from "@angular/core";
/**
 * Represents an event related to a vector source.
 */
export type VectorSourceEvent = {
    layer: OlBaseVectorLayerDirective<VectorLayer>;
    feature?: FeatureLike[];
};
/**
 * A type definition for a function that handles the successful loading of features.
 *
 * This function is invoked when a set of features with associated geometry is successfully loaded.
 */
export type LoadSuccessFn = (features: Feature<Geometry>[]) => void;
/**
 * Defines a function type used to handle load failures.
 *
 * This function is invoked when an error occurs during a loading process.
 * It takes a single argument, an Error object, which provides details about the failure.
 */
export type LoadFailureFn = () => void;
export type FeatureLoaderFactory = (url: string | FeatureUrlFunction, format: FeatureFormat) => FeatureLoader;
export type ClusterFn = (point: Point, features: Feature<Geometry>[]) => Feature<Point>;
export declare const OL_FEATURE_LOADER_FACTORY: InjectionToken<FeatureLoaderFactory>;
export declare function provideFeatureLoader(loaderFactory: (httpClient: HttpClient) => FeatureLoaderFactory): Provider;
export declare function withHttpClient(): (httpClient: HttpClient) => FeatureLoaderFactory;
export declare abstract class OlVectorSourceDirective extends OlSourceDirective implements OnInit, OnDestroy {
    #private;
    protected readonly olLayer: OlBaseVectorLayerDirective<any>;
    readonly overlaps: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly useSpatialIndex: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly strategy: import("@angular/core").InputSignal<LoadingStrategy | undefined>;
    readonly featureAdded: import("@angular/core").OutputEmitterRef<VectorSourceEvent>;
    readonly featureRemoved: import("@angular/core").OutputEmitterRef<VectorSourceEvent>;
    readonly featureLoadStarted: import("@angular/core").OutputEmitterRef<VectorSourceEvent>;
    readonly featureLoadEnded: import("@angular/core").OutputEmitterRef<VectorSourceEvent>;
    readonly featureLoadError: import("@angular/core").OutputEmitterRef<VectorSourceEvent>;
    readonly cluster: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly clusterDistance: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly minDistance: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly createCluster: import("@angular/core").InputSignal<ClusterFn>;
    protected options$(): Observable<Options>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlVectorSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlVectorSourceDirective, never, never, { "overlaps": { "alias": "overlaps"; "required": false; "isSignal": true; }; "useSpatialIndex": { "alias": "useSpatialIndex"; "required": false; "isSignal": true; }; "strategy": { "alias": "strategy"; "required": false; "isSignal": true; }; "cluster": { "alias": "cluster"; "required": false; "isSignal": true; }; "clusterDistance": { "alias": "clusterDistance"; "required": false; "isSignal": true; }; "minDistance": { "alias": "minDistance"; "required": false; "isSignal": true; }; "createCluster": { "alias": "createCluster"; "required": false; "isSignal": true; }; }, { "featureAdded": "featureAdded"; "featureRemoved": "featureRemoved"; "featureLoadStarted": "featureLoadStarted"; "featureLoadEnded": "featureLoadEnded"; "featureLoadError": "featureLoadError"; }, never, never, true, never>;
}
export declare class OlFeatureSourceDirective extends OlVectorSourceDirective {
    readonly features: import("@angular/core").InputSignal<Feature<Geometry>[] | Collection<Feature<Geometry>>>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlFeatureSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlFeatureSourceDirective, "[features]", never, { "features": { "alias": "features"; "required": true; "isSignal": true; }; }, {}, never, never, true, never>;
}
export declare class OlWktSourceDirective extends OlVectorSourceDirective {
    readonly splitCollection: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly dataProjection: import("@angular/core").InputSignal<ProjectionLike>;
    readonly featureProjection: import("@angular/core").InputSignal<ProjectionLike>;
    readonly wkt: import("@angular/core").InputSignal<string>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlWktSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlWktSourceDirective, "[wkt]", never, { "splitCollection": { "alias": "splitCollection"; "required": false; "isSignal": true; }; "dataProjection": { "alias": "dataProjection"; "required": false; "isSignal": true; }; "featureProjection": { "alias": "featureProjection"; "required": false; "isSignal": true; }; "wkt": { "alias": "wkt"; "required": true; "isSignal": true; }; }, {}, never, never, true, never>;
}
export declare class OlEsriJsonSourceDirective extends OlVectorSourceDirective {
    readonly geometryName: import("@angular/core").InputSignal<string | undefined>;
    readonly esrijson: import("@angular/core").InputSignal<string | FeatureUrlFunction>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlEsriJsonSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlEsriJsonSourceDirective, "[esrijson]", never, { "geometryName": { "alias": "geometryName"; "required": false; "isSignal": true; }; "esrijson": { "alias": "esrijson"; "required": true; "isSignal": true; }; }, {}, never, never, true, never>;
}
export declare class OlGeoJsonSourceDirective extends OlVectorSourceDirective {
    readonly dataProjection: import("@angular/core").InputSignal<ProjectionLike>;
    readonly geojson: import("@angular/core").InputSignal<string | FeatureUrlFunction>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlGeoJsonSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlGeoJsonSourceDirective, "[geojson]", never, { "dataProjection": { "alias": "dataProjection"; "required": false; "isSignal": true; }; "geojson": { "alias": "geojson"; "required": true; "isSignal": true; }; }, {}, never, never, true, never>;
}
export declare class OlKmlSourceDirective extends OlVectorSourceDirective {
    readonly showPointNames: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly extractStyles: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly kml: import("@angular/core").InputSignal<string | FeatureUrlFunction>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlKmlSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlKmlSourceDirective, "[kml]", never, { "showPointNames": { "alias": "showPointNames"; "required": false; "isSignal": true; }; "extractStyles": { "alias": "extractStyles"; "required": false; "isSignal": true; }; "kml": { "alias": "kml"; "required": true; "isSignal": true; }; }, {}, never, never, true, never>;
}
export declare class OlTopoJsonSource extends OlVectorSourceDirective {
    readonly dataProjection: import("@angular/core").InputSignal<ProjectionLike>;
    readonly layerName: import("@angular/core").InputSignal<string | undefined>;
    readonly layers: import("@angular/core").InputSignal<string[] | undefined>;
    readonly topojson: import("@angular/core").InputSignal<string | FeatureUrlFunction>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlTopoJsonSource, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlTopoJsonSource, "[topojson]", never, { "dataProjection": { "alias": "dataProjection"; "required": false; "isSignal": true; }; "layerName": { "alias": "layerName"; "required": false; "isSignal": true; }; "layers": { "alias": "layers"; "required": false; "isSignal": true; }; "topojson": { "alias": "topojson"; "required": true; "isSignal": true; }; }, {}, never, never, true, never>;
}
export type GMLVersion = '2.0' | '3.0' | '3.2';
export declare class OlGMLSourceDirective extends OlVectorSourceDirective {
    #private;
    readonly featureNS: import("@angular/core").InputSignal<string | {
        [ns: string]: string;
    } | undefined>;
    readonly featureType: import("@angular/core").InputSignal<string | string[] | undefined>;
    readonly srsName: import("@angular/core").InputSignal<string | undefined>;
    readonly surface: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly curve: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly multiSurface: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly multiCurve: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly hasZ: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly schemaLocation: import("@angular/core").InputSignal<string | undefined>;
    readonly gml: import("@angular/core").InputSignal<string>;
    readonly version: import("@angular/core").InputSignal<GMLVersion>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlGMLSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlGMLSourceDirective, "[gml]", never, { "featureNS": { "alias": "featureNS"; "required": false; "isSignal": true; }; "featureType": { "alias": "featureType"; "required": false; "isSignal": true; }; "srsName": { "alias": "srsName"; "required": false; "isSignal": true; }; "surface": { "alias": "surface"; "required": false; "isSignal": true; }; "curve": { "alias": "curve"; "required": false; "isSignal": true; }; "multiSurface": { "alias": "multiSurface"; "required": false; "isSignal": true; }; "multiCurve": { "alias": "multiCurve"; "required": false; "isSignal": true; }; "hasZ": { "alias": "hasZ"; "required": false; "isSignal": true; }; "schemaLocation": { "alias": "schemaLocation"; "required": false; "isSignal": true; }; "gml": { "alias": "gml"; "required": true; "isSignal": true; }; "version": { "alias": "version"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
export type WFSVersion = '1.0.0' | '1.1.0' | '2.0.0';
export declare class OlWFSSourceDirective extends OlVectorSourceDirective {
    readonly layer: import("@angular/core").InputSignal<string>;
    readonly wfs: import("@angular/core").InputSignal<string>;
    readonly gml: import("@angular/core").InputSignal<GMLVersion | undefined>;
    readonly version: import("@angular/core").InputSignal<WFSVersion>;
    private url;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlWFSSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlWFSSourceDirective, "[wfs]", never, { "layer": { "alias": "layer"; "required": true; "isSignal": true; }; "wfs": { "alias": "wfs"; "required": true; "isSignal": true; }; "gml": { "alias": "gml"; "required": false; "isSignal": true; }; "version": { "alias": "version"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-vector-source.directive.d.ts.map