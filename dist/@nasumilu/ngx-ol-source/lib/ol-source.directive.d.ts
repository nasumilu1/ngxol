import { AttributionLike, Options } from "ol/source/Source";
import { ProjectionLike } from "ol/proj";
import { Proj4Service } from "@nasumilu/ngx-ol-map";
import { Observable } from "rxjs";
import * as i0 from "@angular/core";
export declare class OlSourceDirective {
    #private;
    protected readonly proj4Service: Proj4Service | null;
    readonly attributions: import("@angular/core").ModelSignal<AttributionLike | undefined>;
    readonly attributionsCollapsible: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly wrapX: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly projection: import("@angular/core").InputSignal<ProjectionLike>;
    readonly interpolate: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    protected options$(): Observable<Options>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlSourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlSourceDirective, never, never, { "attributions": { "alias": "attributions"; "required": false; "isSignal": true; }; "attributionsCollapsible": { "alias": "attributionsCollapsible"; "required": false; "isSignal": true; }; "wrapX": { "alias": "wrapX"; "required": false; "isSignal": true; }; "projection": { "alias": "projection"; "required": false; "isSignal": true; }; "interpolate": { "alias": "interpolate"; "required": false; "isSignal": true; }; }, { "attributions": "attributionsChange"; }, never, never, true, never>;
}
//# sourceMappingURL=ol-source.directive.d.ts.map