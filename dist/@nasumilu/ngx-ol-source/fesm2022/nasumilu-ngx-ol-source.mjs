import * as i0 from '@angular/core';
import { inject, model, input, booleanAttribute, Directive, InjectionToken, output, numberAttribute } from '@angular/core';
import { Proj4Service } from '@nasumilu/ngx-ol-map';
import { of, map, catchError, first, tap, forkJoin, switchMap } from 'rxjs';
import { OlBaseVectorLayerDirective } from '@nasumilu/ngx-ol-layer';
import Collection from 'ol/Collection';
import { Feature } from 'ol';
import VectorSource from 'ol/source/Vector';
import EsriJSON from 'ol/format/EsriJSON';
import { WKT, GeoJSON, KML, TopoJSON, WFS } from 'ol/format';
import GML2 from 'ol/format/GML2';
import GML3 from 'ol/format/GML3';
import GML32 from 'ol/format/GML32';
import { unByKey } from 'ol/Observable';
import { HttpClient } from '@angular/common/http';
import { Cluster } from 'ol/source';
import { tile } from 'ol/loadingstrategy';
import { createXYZ } from 'ol/tilegrid';
import { toFixed } from 'ol/math';

class OlSourceDirective {
    constructor() {
        this.proj4Service = inject(Proj4Service, { optional: true });
        this.attributions = model();
        this.attributionsCollapsible = input(true, { transform: booleanAttribute });
        this.wrapX = input(true, { transform: booleanAttribute });
        this.projection = input();
        this.interpolate = input(false, { transform: booleanAttribute });
        this.#options = (this.proj4Service?.getProjection(this.projection()) ?? of(this.projection()))
            .pipe(map(projection => ({
            projection,
            attributions: this.attributions(),
            attributionsCollapsible: this.attributionsCollapsible(),
            wrapX: this.wrapX(),
            interpolate: this.interpolate()
        })));
    }
    #options;
    options$() {
        return this.#options;
    }
    static { this.ɵfac = function OlSourceDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlSourceDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlSourceDirective, inputs: { attributions: [1, "attributions"], attributionsCollapsible: [1, "attributionsCollapsible"], wrapX: [1, "wrapX"], projection: [1, "projection"], interpolate: [1, "interpolate"] }, outputs: { attributions: "attributionsChange" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlSourceDirective, [{
        type: Directive
    }], null, null); })();

const OL_FEATURE_LOADER_FACTORY = new InjectionToken('FeatureLoaderFactoryToken');
function provideFeatureLoader(loaderFactory) {
    return { provide: OL_FEATURE_LOADER_FACTORY, useFactory: loaderFactory, deps: [HttpClient] };
}
function withHttpClient() {
    return function (httpClient) {
        return function (url, format) {
            return function (extent, resolution, projection, success, failure) {
                const url_ = typeof url === 'function' ? url(extent, resolution, projection) : url;
                const responseType = format.getType() === 'arraybuffer' ? 'arraybuffer' : 'text';
                // @ts-ignore
                httpClient.get(url_, { responseType }).pipe(catchError(error => {
                    // @ts-ignore
                    this.removeLoadedExtent(extent);
                    failure?.();
                    return of(undefined);
                })).subscribe(response => {
                    if (response != undefined) {
                        const features = format.readFeatures(response, { extent: extent, featureProjection: projection });
                        // @ts-ignore
                        this.addFeatures(features);
                        success?.(features);
                    }
                });
            };
        };
    };
}
const DEFAULT_CLUSTER_FN = (point, features) => {
    return new Feature({
        geometry: point,
        features: features,
        size: features.length > 1 ? `${features.length}` : ''
    });
};
class OlVectorSourceDirective extends OlSourceDirective {
    constructor() {
        super(...arguments);
        this.#loader = inject(OL_FEATURE_LOADER_FACTORY, { optional: true });
        this.#eventKeys = [];
        this.olLayer = inject(OlBaseVectorLayerDirective);
        this.overlaps = input(true, { transform: booleanAttribute });
        this.useSpatialIndex = input(true, { transform: booleanAttribute });
        this.strategy = input();
        this.featureAdded = output();
        this.featureRemoved = output();
        this.featureLoadStarted = output();
        this.featureLoadEnded = output();
        this.featureLoadError = output();
        // cluster
        this.cluster = input(false, { transform: booleanAttribute });
        this.clusterDistance = input(20, { transform: numberAttribute });
        this.minDistance = input(0, { transform: numberAttribute });
        this.createCluster = input(DEFAULT_CLUSTER_FN);
    }
    #loader;
    #eventKeys;
    options$() {
        return super.options$().pipe(map(options => Object.assign(options, {
            overlaps: this.overlaps(),
            useSpatialIndex: this.useSpatialIndex(),
            strategy: this.strategy()
        })));
    }
    #source$() {
        return this.options$().pipe(map(options => {
            if (options.url && options.format && this.#loader) {
                options = Object.assign(options, { loader: this.#loader(options.url, options.format) });
            }
            return options;
        }), map(options => new VectorSource(options)), map(source => {
            if (this.cluster()) {
                source = new Cluster({
                    source,
                    distance: this.clusterDistance(),
                    minDistance: this.minDistance(),
                    createCluster: this.createCluster()
                });
            }
            return source;
        }));
    }
    ngOnInit() {
        this.#source$()
            .pipe(first(), tap(source => {
            this.#eventKeys.push(source.on('addfeature', evt => this.featureAdded.emit({
                layer: this.olLayer,
                feature: evt.feature ? [evt.feature] : undefined
            })), source.on('removefeature', evt => this.featureRemoved.emit({
                layer: this.olLayer,
                feature: evt.feature ? [evt.feature] : undefined
            })), source.on('featuresloadstart', evt => this.featureLoadStarted.emit({
                layer: this.olLayer,
                feature: evt.features
            })), source.on('featuresloadend', evt => this.featureLoadEnded.emit({
                layer: this.olLayer,
                feature: evt.features
            })), source.on('featuresloaderror', evt => this.featureLoadError.emit({
                layer: this.olLayer
            })));
        })).subscribe(source => this.olLayer.layer().setSource(source));
    }
    ngOnDestroy() {
        unByKey(this.#eventKeys);
        this.#eventKeys = [];
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlVectorSourceDirective_BaseFactory; return function OlVectorSourceDirective_Factory(__ngFactoryType__) { return (ɵOlVectorSourceDirective_BaseFactory || (ɵOlVectorSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlVectorSourceDirective)))(__ngFactoryType__ || OlVectorSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlVectorSourceDirective, inputs: { overlaps: [1, "overlaps"], useSpatialIndex: [1, "useSpatialIndex"], strategy: [1, "strategy"], cluster: [1, "cluster"], clusterDistance: [1, "clusterDistance"], minDistance: [1, "minDistance"], createCluster: [1, "createCluster"] }, outputs: { featureAdded: "featureAdded", featureRemoved: "featureRemoved", featureLoadStarted: "featureLoadStarted", featureLoadEnded: "featureLoadEnded", featureLoadError: "featureLoadError" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlVectorSourceDirective, [{
        type: Directive
    }], null, null); })();
class OlFeatureSourceDirective extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.features = input.required();
    }
    options$() {
        let features = this.features();
        if (Array.isArray(features)) {
            features = new Collection(features);
        }
        return super.options$().pipe(map(options => Object.assign(options, { features: features })));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlFeatureSourceDirective_BaseFactory; return function OlFeatureSourceDirective_Factory(__ngFactoryType__) { return (ɵOlFeatureSourceDirective_BaseFactory || (ɵOlFeatureSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlFeatureSourceDirective)))(__ngFactoryType__ || OlFeatureSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlFeatureSourceDirective, selectors: [["", "features", ""]], inputs: { features: [1, "features"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlFeatureSourceDirective, [{
        type: Directive,
        args: [{
                selector: '[features]'
            }]
    }], null, null); })();
class OlWktSourceDirective extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.splitCollection = input(false, { transform: booleanAttribute });
        this.dataProjection = input('EPSG:4326');
        this.featureProjection = input('EPSG:3857');
        this.wkt = input.required();
    }
    options$() {
        const format = new WKT({ splitCollection: this.splitCollection() });
        const projections$ = forkJoin([
            (this.proj4Service?.getProjection(this.featureProjection()) ?? of(this.featureProjection())),
            (this.proj4Service?.getProjection(this.dataProjection()) ?? of(this.dataProjection())),
        ]).pipe(map(([featureProjection, dataProjection]) => ({ dataProjection, featureProjection })));
        return super.options$().pipe(switchMap(options => projections$.pipe(map(projection => Object.assign(options, { features: new Collection(format.readFeatures(this.wkt(), projection)) })))));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlWktSourceDirective_BaseFactory; return function OlWktSourceDirective_Factory(__ngFactoryType__) { return (ɵOlWktSourceDirective_BaseFactory || (ɵOlWktSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlWktSourceDirective)))(__ngFactoryType__ || OlWktSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlWktSourceDirective, selectors: [["", "wkt", ""]], inputs: { splitCollection: [1, "splitCollection"], dataProjection: [1, "dataProjection"], featureProjection: [1, "featureProjection"], wkt: [1, "wkt"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlWktSourceDirective, [{
        type: Directive,
        args: [{
                selector: '[wkt]'
            }]
    }], null, null); })();
class OlEsriJsonSourceDirective extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.geometryName = input();
        this.esrijson = input.required();
    }
    options$() {
        return super.options$().pipe(map(options => Object.assign(options, {
            format: new EsriJSON({ geometryName: this.geometryName() }),
            url: this.esrijson()
        })));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlEsriJsonSourceDirective_BaseFactory; return function OlEsriJsonSourceDirective_Factory(__ngFactoryType__) { return (ɵOlEsriJsonSourceDirective_BaseFactory || (ɵOlEsriJsonSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlEsriJsonSourceDirective)))(__ngFactoryType__ || OlEsriJsonSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlEsriJsonSourceDirective, selectors: [["", "esrijson", ""]], inputs: { geometryName: [1, "geometryName"], esrijson: [1, "esrijson"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlEsriJsonSourceDirective, [{
        type: Directive,
        args: [{
                selector: '[esrijson]'
            }]
    }], null, null); })();
class OlGeoJsonSourceDirective extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.dataProjection = input('EPSG:4326');
        this.geojson = input.required();
    }
    options$() {
        return super.options$().pipe(switchMap(options => (this.proj4Service?.getProjection(this.dataProjection()) ?? of(this.dataProjection()))
            .pipe(map(dataProjection => Object.assign(options, {
            url: this.geojson(),
            format: new GeoJSON({
                dataProjection: dataProjection
            })
        })))));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlGeoJsonSourceDirective_BaseFactory; return function OlGeoJsonSourceDirective_Factory(__ngFactoryType__) { return (ɵOlGeoJsonSourceDirective_BaseFactory || (ɵOlGeoJsonSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlGeoJsonSourceDirective)))(__ngFactoryType__ || OlGeoJsonSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlGeoJsonSourceDirective, selectors: [["", "geojson", ""]], inputs: { dataProjection: [1, "dataProjection"], geojson: [1, "geojson"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlGeoJsonSourceDirective, [{
        type: Directive,
        args: [{
                selector: '[geojson]'
            }]
    }], null, null); })();
class OlKmlSourceDirective extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.showPointNames = input(true, { transform: booleanAttribute });
        this.extractStyles = input(true, { transform: booleanAttribute });
        this.kml = input.required();
    }
    options$() {
        return super.options$().pipe(map(options => Object.assign(options, {
            format: new KML({
                extractStyles: this.extractStyles(),
                showPointNames: this.showPointNames()
            }),
            url: this.kml()
        })));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlKmlSourceDirective_BaseFactory; return function OlKmlSourceDirective_Factory(__ngFactoryType__) { return (ɵOlKmlSourceDirective_BaseFactory || (ɵOlKmlSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlKmlSourceDirective)))(__ngFactoryType__ || OlKmlSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlKmlSourceDirective, selectors: [["", "kml", ""]], inputs: { showPointNames: [1, "showPointNames"], extractStyles: [1, "extractStyles"], kml: [1, "kml"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlKmlSourceDirective, [{
        type: Directive,
        args: [{
                selector: '[kml]'
            }]
    }], null, null); })();
class OlTopoJsonSource extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.dataProjection = input('EPSG:4326');
        this.layerName = input();
        this.layers = input();
        this.topojson = input.required();
    }
    options$() {
        return super.options$().pipe(switchMap(options => (this.proj4Service?.getProjection(this.dataProjection()) ?? of(this.dataProjection()))
            .pipe(map(dataProjection => Object.assign(options, {
            url: this.topojson(),
            format: new TopoJSON({
                dataProjection: dataProjection,
                layerName: this.layerName(),
                layers: this.layers()
            })
        })))));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlTopoJsonSource_BaseFactory; return function OlTopoJsonSource_Factory(__ngFactoryType__) { return (ɵOlTopoJsonSource_BaseFactory || (ɵOlTopoJsonSource_BaseFactory = i0.ɵɵgetInheritedFactory(OlTopoJsonSource)))(__ngFactoryType__ || OlTopoJsonSource); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlTopoJsonSource, selectors: [["", "topojson", ""]], inputs: { dataProjection: [1, "dataProjection"], layerName: [1, "layerName"], layers: [1, "layers"], topojson: [1, "topojson"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlTopoJsonSource, [{
        type: Directive,
        args: [{
                selector: '[topojson]'
            }]
    }], null, null); })();
const GML_VERSION = {
    '1.0.0': GML2,
    '1.1.0': GML3,
    '2.0': GML2,
    '2.0.0': GML32,
    '3.0': GML3,
    '3.2': GML32
};
class OlGMLSourceDirective extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.featureNS = input();
        this.featureType = input();
        this.srsName = input();
        this.surface = input(false, { transform: booleanAttribute });
        this.curve = input(false, { transform: booleanAttribute });
        this.multiSurface = input(true, { transform: booleanAttribute });
        this.multiCurve = input(true, { transform: booleanAttribute });
        this.hasZ = input(false, { transform: booleanAttribute });
        this.schemaLocation = input();
        this.gml = input.required();
        this.version = input('2.0');
    }
    #gmlFormat() {
        const formatOptions = {
            featureNS: this.featureNS(),
            featureType: this.featureType(),
            srsName: this.srsName(),
            surface: this.surface(),
            curve: this.curve(),
            multiSurface: this.multiSurface(),
            multiCurve: this.multiCurve(),
            hasZ: this.hasZ(),
            schemaLocation: this.schemaLocation()
        };
        const version = this.version();
        let format;
        if (version == '2.0') {
            format = new GML2(formatOptions);
        }
        else if (version == '3.0') {
            format = new GML3(formatOptions);
        }
        else {
            format = new GML32(formatOptions);
        }
        return format;
    }
    options$() {
        return super.options$().pipe(map(options => ({
            ...options,
            format: this.#gmlFormat(),
            url: this.gml()
        })));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlGMLSourceDirective_BaseFactory; return function OlGMLSourceDirective_Factory(__ngFactoryType__) { return (ɵOlGMLSourceDirective_BaseFactory || (ɵOlGMLSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlGMLSourceDirective)))(__ngFactoryType__ || OlGMLSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlGMLSourceDirective, selectors: [["", "gml", ""]], inputs: { featureNS: [1, "featureNS"], featureType: [1, "featureType"], srsName: [1, "srsName"], surface: [1, "surface"], curve: [1, "curve"], multiSurface: [1, "multiSurface"], multiCurve: [1, "multiCurve"], hasZ: [1, "hasZ"], schemaLocation: [1, "schemaLocation"], gml: [1, "gml"], version: [1, "version"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlGMLSourceDirective, [{
        type: Directive,
        args: [{
                selector: '[gml]'
            }]
    }], null, null); })();
class OlWFSSourceDirective extends OlVectorSourceDirective {
    constructor() {
        super(...arguments);
        this.layer = input.required();
        this.wfs = input.required();
        this.gml = input();
        this.version = input('1.1.0');
    }
    url(extent, resolution, projection) {
        const version = this.version();
        const params = {
            service: 'WFS',
            version,
            request: 'GetFeature',
            typeName: this.layer(),
            bbox: `${extent.map(value => toFixed(value, 6)).join(',')},${projection.getCode()}`,
            outputFormat: GML_VERSION[this.gml() ?? version].name,
            srsName: projection.getCode()
        };
        return `${this.wfs()}?${new URLSearchParams(params).toString()}`;
    }
    options$() {
        return super.options$().pipe(map(options => Object.assign(options, {
            format: new WFS({ version: this.version(), gmlFormat: new GML_VERSION[this.gml() ?? this.version()]() }),
            strategy: tile(createXYZ({ tileSize: 512 })),
            url: OlWFSSourceDirective.prototype.url.bind(this)
        })));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlWFSSourceDirective_BaseFactory; return function OlWFSSourceDirective_Factory(__ngFactoryType__) { return (ɵOlWFSSourceDirective_BaseFactory || (ɵOlWFSSourceDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlWFSSourceDirective)))(__ngFactoryType__ || OlWFSSourceDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlWFSSourceDirective, selectors: [["", "wfs", ""]], inputs: { layer: [1, "layer"], wfs: [1, "wfs"], gml: [1, "gml"], version: [1, "version"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlWFSSourceDirective, [{
        type: Directive,
        args: [{
                selector: '[wfs]'
            }]
    }], null, null); })();

/*
 * Public API Surface of ngx-ol-source
 */

/**
 * Generated bundle index. Do not edit.
 */

export { OL_FEATURE_LOADER_FACTORY, OlEsriJsonSourceDirective, OlFeatureSourceDirective, OlGMLSourceDirective, OlGeoJsonSourceDirective, OlKmlSourceDirective, OlSourceDirective, OlTopoJsonSource, OlVectorSourceDirective, OlWFSSourceDirective, OlWktSourceDirective, provideFeatureLoader, withHttpClient };
//# sourceMappingURL=nasumilu-ngx-ol-source.mjs.map
