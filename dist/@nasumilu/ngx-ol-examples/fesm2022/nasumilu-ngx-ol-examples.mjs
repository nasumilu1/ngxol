import * as i0 from '@angular/core';
import { InjectionToken, inject, Injectable, model, viewChild, ViewContainerRef, Component, ChangeDetectionStrategy, ElementRef, computed, signal, linkedSignal, Pipe, input } from '@angular/core';
import { provideHttpClient, withFetch, HttpClient } from '@angular/common/http';
import { MatTab, MatTabGroup } from '@angular/material/tabs';
import { DomSanitizer } from '@angular/platform-browser';
import { of, switchMap, forkJoin, map, tap, first } from 'rxjs';
import hljs from 'highlight.js';
import typescript from 'highlight.js/lib/languages/typescript';
import { ActivatedRoute } from '@angular/router';
import { OlViewDirective, OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlCoordinatePipe, OlOverlay, OlOverlaysDirective, OlTransformPipe, Proj4Service } from '@nasumilu/ngx-ol-map';
import { OlDefaultControlDirective, OlOverviewMapDirective, OlMousePositionDirective, OlScaleLineDirective, OlAttributionDirective, OlZoomControlDirective, OlFullScreenDirective, OlZoomToExtentDirective, OlRotateDirective } from '@nasumilu/ngx-ol-control';
import { OlDefaultInteractionDirective, OlMouseWheelZoomDirective, OlDragRotateDirective, OlKeyboardZoomDirective, OlKeyboardPanDirective, OlDblClickZoomDirective, OlDragPanDirective, OlDragZoomDirective, DragBoxDirective } from '@nasumilu/ngx-ol-interaction';
import { OlTileLayerDirective, SourceDirective, OlVectorLayerDirective, VisibleDirective, OlGroupLayerDirective, MinMaxLayerZoomDirective, OlGraticleLayerDirective, OlImageLayerDirective, ExtentDirective, OlHeatmapLayerDirective, OlVectorTileLayerDirective, OlWebGLTileLayerDirective, ZIndexDirective, MinMaxResolutionDirective } from '@nasumilu/ngx-ol-layer';
import { OSM, ImageWMS, OGCVectorTile } from 'ol/source';
import { OlWktSourceDirective, OlFeatureSourceDirective, OlGeoJsonSourceDirective, OlWFSSourceDirective, OlKmlSourceDirective, OlEsriJsonSourceDirective, OlTopoJsonSource } from '@nasumilu/ngx-ol-source';
import { toStringHDMS, format } from 'ol/coordinate';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { MatSelect } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { Overlay, Feature } from 'ol';
import { MatSlider, MatSliderThumb, MatSliderRangeThumb } from '@angular/material/slider';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { Point } from 'ol/geom';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { Style, Fill } from 'ol/style';
import Stroke from 'ol/style/Stroke';
import { getCenter } from 'ol/extent';
import { MVT } from 'ol/format';
import ImageTile from 'ol/source/ImageTile';
import { MatButtonToggleGroup, MatButtonToggle } from '@angular/material/button-toggle';
import { fromLonLat } from 'ol/proj';
import { createXYZ } from 'ol/tilegrid';
import { tile } from 'ol/loadingstrategy';
import { DecimalPipe } from '@angular/common';

const MAP_TILER_API_KEY = new InjectionToken('MapTilerAPIKey');
const EXAMPLES_BASE_PATH = new InjectionToken('ExamplesBasePath');
function provideExamples(basePath) {
    return [
        { provide: EXAMPLES_BASE_PATH, useValue: basePath },
        provideHttpClient(withFetch())
    ];
}
function provideMapTilerAPIKey(apiKey) {
    return { provide: MAP_TILER_API_KEY, useValue: apiKey };
}

hljs.registerLanguage('typescript', typescript);
class ExampleService {
    constructor() {
        this.basePath = of(inject(EXAMPLES_BASE_PATH));
        this.httpClient = inject(HttpClient);
        this.sanitizer = inject(DomSanitizer);
    }
    code(name) {
        return this.basePath.pipe(switchMap(basePath => this.httpClient.get(`${basePath}/${name}/${name}.component.ts`, { responseType: 'text' })));
    }
    markup(name) {
        return this.basePath.pipe(switchMap(basePath => this.httpClient.get(`${basePath}/${name}/${name}.component.html`, { responseType: 'text' })));
    }
    style(name) {
        return this.basePath.pipe(switchMap(basePath => this.httpClient.get(`${basePath}/${name}/${name}.component.css`, { responseType: 'text' })));
    }
    example(name) {
        return forkJoin([this.code(name), this.markup(name), this.style(name)]).pipe(map(([code, markup, style]) => ({
            code: this.sanitizer.bypassSecurityTrustHtml(hljs.highlight(code, { language: 'typescript' }).value),
            markup: this.sanitizer.bypassSecurityTrustHtml(hljs.highlight(markup, { language: 'html' }).value),
            style: this.sanitizer.bypassSecurityTrustHtml(hljs.highlight(style, { language: 'css' }).value)
        })));
    }
    static { this.ɵfac = function ExampleService_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || ExampleService)(); }; }
    static { this.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: ExampleService, factory: ExampleService.ɵfac, providedIn: 'root' }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ExampleService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], null, null); })();

const _c0$5 = ["exampleContainer"];
function ExampleComponent_ng_template_2_Template(rf, ctx) { }
function ExampleComponent_Conditional_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-tab-group")(1, "mat-tab", 1)(2, "pre");
    i0.ɵɵelement(3, "code", 2);
    i0.ɵɵelementEnd()();
    i0.ɵɵelementStart(4, "mat-tab", 3)(5, "pre");
    i0.ɵɵelement(6, "code", 4);
    i0.ɵɵelementEnd()();
    i0.ɵɵelementStart(7, "mat-tab", 5)(8, "pre");
    i0.ɵɵelement(9, "code", 6);
    i0.ɵɵelementEnd()()();
} if (rf & 2) {
    let tmp_2_0;
    let tmp_3_0;
    let tmp_4_0;
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("innerHTML", (tmp_2_0 = ctx_r0.codeExample()) == null ? null : tmp_2_0.code, i0.ɵɵsanitizeHtml);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("innerHTML", (tmp_3_0 = ctx_r0.codeExample()) == null ? null : tmp_3_0.markup, i0.ɵɵsanitizeHtml);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("innerHTML", (tmp_4_0 = ctx_r0.codeExample()) == null ? null : tmp_4_0.style, i0.ɵɵsanitizeHtml);
} }
class ExampleComponent {
    constructor() {
        this.title = model();
        this.exampleContainer = viewChild('exampleContainer', { read: ViewContainerRef });
        this.codeExample = model();
        this.exampleService = inject(ExampleService);
        this.route = inject(ActivatedRoute);
    }
    ngOnInit() {
        this.route.title.subscribe(title => this.title.set(title));
        this.route.data.pipe(tap(data => this.exampleContainer()?.createComponent(data['component'])), switchMap(data => this.exampleService.example(data['example']))).subscribe(example => this.codeExample.set(example));
    }
    static { this.ɵfac = function ExampleComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || ExampleComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: ExampleComponent, selectors: [["example"]], viewQuery: function ExampleComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.exampleContainer, _c0$5, 5, ViewContainerRef);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, inputs: { title: [1, "title"], codeExample: [1, "codeExample"] }, outputs: { title: "titleChange", codeExample: "codeExampleChange" }, decls: 5, vars: 2, consts: [["exampleContainer", ""], ["label", "TypeScript"], [1, "language-javascript", 3, "innerHTML"], ["label", "Markup"], [1, "language-html", 3, "innerHTML"], ["label", "Style"], [1, "language-css", 3, "innerHTML"]], template: function ExampleComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "h2");
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(2, ExampleComponent_ng_template_2_Template, 0, 0, "ng-template", null, 0, i0.ɵɵtemplateRefExtractor)(4, ExampleComponent_Conditional_4_Template, 10, 3, "mat-tab-group");
        } if (rf & 2) {
            let tmp_1_0;
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate((tmp_1_0 = ctx.title()) !== null && tmp_1_0 !== undefined ? tmp_1_0 : "Example");
            i0.ɵɵadvance(3);
            i0.ɵɵconditional(ctx.codeExample() != undefined ? 4 : -1);
        } }, dependencies: [MatTab,
            MatTabGroup], styles: ["h2[_ngcontent-%COMP%]{margin-left:1em}mat-tab-group[_ngcontent-%COMP%]{width:60%;margin:auto}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ExampleComponent, [{
        type: Component,
        args: [{ selector: 'example', imports: [
                    MatTab,
                    MatTabGroup
                ], template: "<h2>{{ title() ?? 'Example' }}</h2>\n\n<ng-template #exampleContainer/>\n\n@if (this.codeExample() != undefined) {\n  <mat-tab-group>\n    <mat-tab label=\"TypeScript\">\n      <pre><code class=\"language-javascript\" [innerHTML]=\"this.codeExample()?.code\"></code></pre>\n    </mat-tab>\n    <mat-tab label=\"Markup\">\n      <pre><code class=\"language-html\" [innerHTML]=\"this.codeExample()?.markup\"></code></pre>\n    </mat-tab>\n    <mat-tab label=\"Style\">\n      <pre><code class=\"language-css\" [innerHTML]=\"this.codeExample()?.style\"></code></pre>\n    </mat-tab>\n  </mat-tab-group>\n}\n\n", styles: ["h2{margin-left:1em}mat-tab-group{width:60%;margin:auto}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(ExampleComponent, { className: "ExampleComponent", filePath: "lib/example/example.component.ts", lineNumber: 16 }); })();

class BasicMapComponent {
    constructor() {
        this.wkt = 'Polygon ((31.1 -30.1, 8.1 -26.2, -0.4 -44.8, 42.8 -46.8, 31.1 -30.1))';
        this.source = new OSM();
        this.viewDirective = viewChild(OlViewDirective);
    }
    static { this.ɵfac = function BasicMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || BasicMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: BasicMapComponent, selectors: [["basic-map"]], viewQuery: function BasicMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.viewDirective, OlViewDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, decls: 10, vars: 7, consts: [[3, "source"], [3, "wkt"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "settings"]], template: function BasicMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0)(3, "ol-vector-layer", 1);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(4, "ol-controls", 2)(5, "ol-interactions", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 4)(7, "div");
            i0.ɵɵtext(8);
            i0.ɵɵpipe(9, "coordinate");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            let tmp_2_0;
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance();
            i0.ɵɵproperty("wkt", ctx.wkt);
            i0.ɵɵadvance(5);
            i0.ɵɵtextInterpolate1("Center: ", i0.ɵɵpipeBind3(9, 3, (tmp_2_0 = ctx.viewDirective()) == null ? null : tmp_2_0.center(), "hdms", 3), "");
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlTileLayerDirective,
            OlDefaultInteractionDirective, OlDefaultControlDirective, OlCoordinatePipe, SourceDirective,
            OlVectorLayerDirective, OlWktSourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{display:flex;justify-content:center;padding:.25em}"], changeDetection: 0 }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(BasicMapComponent, [{
        type: Component,
        args: [{ selector: 'basic-map', imports: [
                    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlTileLayerDirective,
                    OlDefaultInteractionDirective, OlDefaultControlDirective, OlCoordinatePipe, SourceDirective,
                    OlVectorLayerDirective, OlWktSourceDirective
                ], changeDetection: ChangeDetectionStrategy.OnPush, template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n    <ol-vector-layer [wkt]=\"wkt\" />\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n<div class=\"settings\">\n  <div>Center: {{ viewDirective()?.center() | coordinate:'hdms':3}}</div>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{display:flex;justify-content:center;padding:.25em}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(BasicMapComponent, { className: "BasicMapComponent", filePath: "lib/basic-map/basic-map.component.ts", lineNumber: 24 }); })();

class OverviewMapComponent {
    constructor() {
        this.source = new OSM();
    }
    static { this.ɵfac = function OverviewMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OverviewMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: OverviewMapComponent, selectors: [["overview-map"]], decls: 8, vars: 2, consts: [[3, "source"], ["defaultControls", ""], ["collapsible", "true", "collapsed", "false"], ["defaultInteractions", ""]], template: function OverviewMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls", 1)(4, "ol-overview-map", 2)(5, "ol-layers");
            i0.ɵɵelement(6, "ol-tile-layer", 0);
            i0.ɵɵelementEnd()()();
            i0.ɵɵelement(7, "ol-interactions", 3);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("source", ctx.source);
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective,
            OlOverviewMapDirective, OlTileLayerDirective, OlInteractionsDirective,
            OlDefaultInteractionDirective, OlDefaultControlDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OverviewMapComponent, [{
        type: Component,
        args: [{ selector: 'overview-map', imports: [OlMapComponent, OlLayersDirective, OlControlsDirective,
                    OlOverviewMapDirective, OlTileLayerDirective, OlInteractionsDirective,
                    OlDefaultInteractionDirective, OlDefaultControlDirective, SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls defaultControls>\n    <ol-overview-map collapsible=\"true\" collapsed=\"false\">\n      <ol-layers>\n        <ol-tile-layer [source]=\"source\" />\n      </ol-layers>\n    </ol-overview-map>\n  </ol-controls>\n\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n", styles: ["ol-map{display:block;width:100%;height:350px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(OverviewMapComponent, { className: "OverviewMapComponent", filePath: "lib/overview-map/overview-map.component.ts", lineNumber: 21 }); })();

const _forTrack0 = ($index, $item) => $item.srid;
function MousePositionMapComponent_For_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 4);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const projection_r1 = ctx.$implicit;
    i0.ɵɵproperty("value", projection_r1);
    i0.ɵɵadvance();
    i0.ɵɵtextInterpolate(projection_r1.label);
} }
class MousePositionMapComponent {
    constructor() {
        this.projections = [
            {
                srid: 'EPSG:4326',
                label: 'WGS 84',
                format: "Lat: {y}, Lng: {x}",
                fractionDigits: 6
            },
            {
                srid: 'EPSG:3857',
                label: 'WGS 84 / Pseudo-Mercator',
                format: 'x: {x}, y: {y}',
                fractionDigits: 1
            },
            {
                srid: 'EPSG:32716',
                label: 'WGS84 / UTM zone 16S',
                format: coordinate => coordinate ? toStringHDMS(coordinate, 2) : ''
            }
        ];
        this.source = new OSM();
        this.position = viewChild(OlMousePositionDirective);
        this.projection = model(this.projections[0]);
    }
    format(coordinate) {
        const projection = this.projection();
        const format$1 = projection.format;
        const precision = projection.fractionDigits;
        if (typeof format$1 === 'string') {
            return coordinate ? format(coordinate, format$1, precision) : '';
        }
        return format$1 ? format$1(coordinate) : '';
    }
    ngAfterContentInit() {
        this.projection.subscribe(projection => {
            const position = this.position();
            position?.projection.set(projection.srid);
        });
    }
    static { this.ɵfac = function MousePositionMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || MousePositionMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: MousePositionMapComponent, selectors: [["mouse-position-map"]], viewQuery: function MousePositionMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.position, OlMousePositionDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, inputs: { projection: [1, "projection"] }, outputs: { projection: "projectionChange" }, decls: 14, vars: 4, consts: [[3, "source"], [3, "projection", "coordinateFormat"], [1, "settings"], [3, "valueChange", "value"], [3, "value"]], template: function MousePositionMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls");
            i0.ɵɵelement(4, "ol-mouse-position", 1);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "ol-interactions");
            i0.ɵɵelement(6, "ol-mouse-wheel-zoom");
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(7, "div", 2)(8, "mat-form-field")(9, "mat-label");
            i0.ɵɵtext(10, "Projection");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(11, "mat-select", 3);
            i0.ɵɵtwoWayListener("valueChange", function MousePositionMapComponent_Template_mat_select_valueChange_11_listener($event) { i0.ɵɵtwoWayBindingSet(ctx.projection, $event) || (ctx.projection = $event); return $event; });
            i0.ɵɵrepeaterCreate(12, MousePositionMapComponent_For_13_Template, 2, 2, "mat-option", 4, _forTrack0);
            i0.ɵɵelementEnd()()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("projection", ctx.projection().srid)("coordinateFormat", ctx.format.bind(ctx));
            i0.ɵɵadvance(7);
            i0.ɵɵtwoWayProperty("value", ctx.projection);
            i0.ɵɵadvance();
            i0.ɵɵrepeater(ctx.projections);
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective,
            OlTileLayerDirective, OlMousePositionDirective, OlMouseWheelZoomDirective,
            MatFormField, MatLabel, MatSelect, MatOption, OlInteractionsDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}div.settings[_ngcontent-%COMP%]{padding:.25em;display:flex;justify-content:center;width:100%}mat-form-field[_ngcontent-%COMP%]{width:300px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(MousePositionMapComponent, [{
        type: Component,
        args: [{ selector: 'mouse-position-map', imports: [
                    OlMapComponent, OlLayersDirective, OlControlsDirective,
                    OlTileLayerDirective, OlMousePositionDirective, OlMouseWheelZoomDirective,
                    MatFormField, MatLabel, MatSelect, MatOption, OlInteractionsDirective, SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls>\n    <ol-mouse-position [projection]=\"projection().srid\" [coordinateFormat]=\"this.format.bind(this)\"/>\n  </ol-controls>\n\n  <ol-interactions>\n    <ol-mouse-wheel-zoom/>\n  </ol-interactions>\n\n</ol-map>\n\n<div class=\"settings\">\n  <mat-form-field>\n    <mat-label>Projection</mat-label>\n    <mat-select [(value)]=\"projection\">\n      @for (projection of projections; track projection.srid) {\n        <mat-option [value]=\"projection\">{{ projection.label }}</mat-option>\n      }\n    </mat-select>\n  </mat-form-field>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}div.settings{padding:.25em;display:flex;justify-content:center;width:100%}mat-form-field{width:300px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(MousePositionMapComponent, { className: "MousePositionMapComponent", filePath: "lib/mouse-position-map/mouse-position-map.component.ts", lineNumber: 29 }); })();

class ScaleLineMapComponent {
    constructor() {
        this.source = new OSM();
        this.scale = viewChild(OlScaleLineDirective);
        this.unit = model('metric');
    }
    ngAfterContentInit() {
        this.unit.subscribe(unit => this.scale()?.units.set(unit));
    }
    static { this.ɵfac = function ScaleLineMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || ScaleLineMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: ScaleLineMapComponent, selectors: [["scale-line-map"]], viewQuery: function ScaleLineMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.scale, OlScaleLineDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, inputs: { unit: [1, "unit"] }, outputs: { unit: "unitChange" }, decls: 21, vars: 3, consts: [[3, "source"], ["defaultControls", ""], ["bar", "true", 3, "unitsChange", "units"], ["defaultInteractions", ""], [1, "settings"], [3, "valueChange", "value"], ["value", "degrees"], ["value", "imperial"], ["value", "nautical"], ["value", "metric"], ["value", "us"]], template: function ScaleLineMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls", 1)(4, "ol-scale-line", 2);
            i0.ɵɵtwoWayListener("unitsChange", function ScaleLineMapComponent_Template_ol_scale_line_unitsChange_4_listener($event) { i0.ɵɵtwoWayBindingSet(ctx.unit, $event) || (ctx.unit = $event); return $event; });
            i0.ɵɵelementEnd()();
            i0.ɵɵelement(5, "ol-interactions", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 4)(7, "mat-form-field")(8, "mat-label");
            i0.ɵɵtext(9, "Units");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(10, "mat-select", 5);
            i0.ɵɵtwoWayListener("valueChange", function ScaleLineMapComponent_Template_mat_select_valueChange_10_listener($event) { i0.ɵɵtwoWayBindingSet(ctx.unit, $event) || (ctx.unit = $event); return $event; });
            i0.ɵɵelementStart(11, "mat-option", 6);
            i0.ɵɵtext(12, "Degrees");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(13, "mat-option", 7);
            i0.ɵɵtext(14, "Imperial");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(15, "mat-option", 8);
            i0.ɵɵtext(16, "Nautical");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(17, "mat-option", 9);
            i0.ɵɵtext(18, "Metric");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(19, "mat-option", 10);
            i0.ɵɵtext(20, "US");
            i0.ɵɵelementEnd()()()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(2);
            i0.ɵɵtwoWayProperty("units", ctx.unit);
            i0.ɵɵadvance(6);
            i0.ɵɵtwoWayProperty("value", ctx.unit);
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective,
            OlTileLayerDirective, OlScaleLineDirective, OlDefaultInteractionDirective, OlDefaultControlDirective,
            MatFormField, MatSelect, MatLabel, MatOption, OlInteractionsDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}div.settings[_ngcontent-%COMP%]{padding:.25em;display:flex;justify-content:center;width:100%}"], changeDetection: 0 }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ScaleLineMapComponent, [{
        type: Component,
        args: [{ selector: 'scale-line-map', imports: [
                    OlMapComponent, OlLayersDirective, OlControlsDirective,
                    OlTileLayerDirective, OlScaleLineDirective, OlDefaultInteractionDirective, OlDefaultControlDirective,
                    MatFormField, MatSelect, MatLabel, MatOption, OlInteractionsDirective, SourceDirective
                ], changeDetection: ChangeDetectionStrategy.OnPush, template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls defaultControls>\n    <ol-scale-line [(units)]=\"unit\" bar=\"true\"/>\n  </ol-controls>\n\n  <ol-interactions defaultInteractions />\n\n</ol-map>\n<div class=\"settings\">\n  <mat-form-field>\n    <mat-label>Units</mat-label>\n    <mat-select [(value)]=\"unit\">\n      <mat-option value=\"degrees\">Degrees</mat-option>\n      <mat-option value=\"imperial\">Imperial</mat-option>\n      <mat-option value=\"nautical\">Nautical</mat-option>\n      <mat-option value=\"metric\">Metric</mat-option>\n      <mat-option value=\"us\">US</mat-option>\n    </mat-select>\n  </mat-form-field>\n</div>\n\n", styles: ["ol-map{display:block;width:100%;height:350px}div.settings{padding:.25em;display:flex;justify-content:center;width:100%}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(ScaleLineMapComponent, { className: "ScaleLineMapComponent", filePath: "lib/scale-line-map/scale-line-map.component.ts", lineNumber: 23 }); })();

// Angular version of https://openlayers.org/en/latest/examples/popup.html
class SimplePopupComponent extends OlOverlay {
    constructor() {
        super(...arguments);
        this.ele = inject(ElementRef);
        this.overlay = computed(() => new Overlay({
            element: this.ele.nativeElement,
            autoPan: {
                animation: {
                    duration: 250
                }
            }
        }));
        this.content = signal('Unknown');
    }
    close(evt) {
        this.overlay().setPosition(undefined);
        evt.target.blur();
        return false;
    }
    show(coordinate) {
        this.content.set(toStringHDMS(coordinate));
        this.overlay().setPosition(coordinate);
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵSimplePopupComponent_BaseFactory; return function SimplePopupComponent_Factory(__ngFactoryType__) { return (ɵSimplePopupComponent_BaseFactory || (ɵSimplePopupComponent_BaseFactory = i0.ɵɵgetInheritedFactory(SimplePopupComponent)))(__ngFactoryType__ || SimplePopupComponent); }; })(); }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: SimplePopupComponent, selectors: [["simple-popup"]], features: [i0.ɵɵProvidersFeature([{ provide: OlOverlay, useExisting: SimplePopupComponent }]), i0.ɵɵInheritDefinitionFeature], decls: 4, vars: 1, consts: [["id", "popup", 1, "ol-popup"], ["href", "#", "id", "popup-closer", 1, "ol-popup-closer", 3, "click"], ["id", "popup-content"]], template: function SimplePopupComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0)(1, "a", 1);
            i0.ɵɵlistener("click", function SimplePopupComponent_Template_a_click_1_listener($event) { return ctx.close($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(2, "div", 2);
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate1(" ", ctx.content(), " ");
        } }, styles: [".ol-popup[_ngcontent-%COMP%]{position:absolute;background-color:#fff;box-shadow:0 1px 4px #0003;padding:15px;border-radius:10px;border:1px solid #cccccc;bottom:12px;left:-50px;min-width:280px}.ol-popup[_ngcontent-%COMP%]:after, .ol-popup[_ngcontent-%COMP%]:before{top:100%;border:solid transparent;content:\" \";height:0;width:0;position:absolute;pointer-events:none}.ol-popup[_ngcontent-%COMP%]:after{border-top-color:#fff;border-width:10px;left:48px;margin-left:-10px}.ol-popup[_ngcontent-%COMP%]:before{border-top-color:#ccc;border-width:11px;left:48px;margin-left:-11px}.ol-popup-closer[_ngcontent-%COMP%]{text-decoration:none;position:absolute;top:2px;right:8px}.ol-popup-closer[_ngcontent-%COMP%]:after{content:\"\\2716\"}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(SimplePopupComponent, [{
        type: Component,
        args: [{ selector: 'simple-popup', providers: [{ provide: OlOverlay, useExisting: SimplePopupComponent }], template: `
    <div id="popup" class="ol-popup">
      <a href="#" id="popup-closer" (click)="close($event)" class="ol-popup-closer"></a>
      <div id="popup-content">
        {{ content() }}
      </div>
    </div>`, styles: [".ol-popup{position:absolute;background-color:#fff;box-shadow:0 1px 4px #0003;padding:15px;border-radius:10px;border:1px solid #cccccc;bottom:12px;left:-50px;min-width:280px}.ol-popup:after,.ol-popup:before{top:100%;border:solid transparent;content:\" \";height:0;width:0;position:absolute;pointer-events:none}.ol-popup:after{border-top-color:#fff;border-width:10px;left:48px;margin-left:-10px}.ol-popup:before{border-top-color:#ccc;border-width:11px;left:48px;margin-left:-11px}.ol-popup-closer{text-decoration:none;position:absolute;top:2px;right:8px}.ol-popup-closer:after{content:\"\\2716\"}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(SimplePopupComponent, { className: "SimplePopupComponent", filePath: "lib/basic-overlay-map/basic-overlay-map.component.ts", lineNumber: 71 }); })();
// Map Component
class BasicOverlayMapComponent {
    constructor() {
        this.source = new OSM();
        this.map = viewChild(OlMapComponent);
        this.popup = viewChild(SimplePopupComponent);
    }
    static { this.ɵfac = function BasicOverlayMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || BasicOverlayMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: BasicOverlayMapComponent, selectors: [["basic-overlay-map"]], viewQuery: function BasicOverlayMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.map, OlMapComponent, 5);
            i0.ɵɵviewQuerySignal(ctx.popup, SimplePopupComponent, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(2);
        } }, decls: 7, vars: 1, consts: [[3, "mapSingleClick"], [3, "source"], ["defaultControls", ""], ["defaultInteractions", ""]], template: function BasicOverlayMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map", 0);
            i0.ɵɵlistener("mapSingleClick", function BasicOverlayMapComponent_Template_ol_map_mapSingleClick_0_listener($event) { let tmp_0_0; return (tmp_0_0 = ctx.popup()) == null ? null : tmp_0_0.show($event.coordinate); });
            i0.ɵɵelementStart(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 1);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(3, "ol-controls", 2)(4, "ol-interactions", 3);
            i0.ɵɵelementStart(5, "ol-overlays");
            i0.ɵɵelement(6, "simple-popup");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
        } }, dependencies: [OlControlsDirective, OlLayersDirective, OlMapComponent, OlInteractionsDirective, OlDefaultInteractionDirective,
            OlTileLayerDirective, OlOverlaysDirective, SimplePopupComponent, OlDefaultControlDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(BasicOverlayMapComponent, [{
        type: Component,
        args: [{ selector: 'basic-overlay-map', imports: [
                    OlControlsDirective, OlLayersDirective, OlMapComponent, OlInteractionsDirective, OlDefaultInteractionDirective,
                    OlTileLayerDirective, OlOverlaysDirective, SimplePopupComponent, OlDefaultControlDirective, SourceDirective
                ], template: "<ol-map (mapSingleClick)=\"popup()?.show($event.coordinate)\">\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls defaultControls />\n  <ol-interactions defaultInteractions />\n\n  <ol-overlays>\n    <simple-popup />\n  </ol-overlays>\n\n</ol-map>\n", styles: ["ol-map{display:block;width:100%;height:350px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(BasicOverlayMapComponent, { className: "BasicOverlayMapComponent", filePath: "lib/basic-overlay-map/basic-overlay-map.component.ts", lineNumber: 108 }); })();

class FullScreenMapComponent {
    constructor() {
        this.source = new OSM();
    }
    handleFullScreenChange(isFullScreen) {
        alert(`Full screen changed to ${isFullScreen}`);
    }
    static { this.ɵfac = function FullScreenMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || FullScreenMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: FullScreenMapComponent, selectors: [["full-screen-map"]], decls: 9, vars: 1, consts: [[3, "source"], ["tipLabel", "Full Screen Mode", 3, "fullScreenChange"]], template: function FullScreenMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls");
            i0.ɵɵelement(4, "ol-zoom-control")(5, "ol-attribution");
            i0.ɵɵelementStart(6, "ol-full-screen", 1);
            i0.ɵɵlistener("fullScreenChange", function FullScreenMapComponent_Template_ol_full_screen_fullScreenChange_6_listener($event) { return ctx.handleFullScreenChange($event); });
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(7, "ol-interactions");
            i0.ɵɵelement(8, "ol-mouse-wheel-zoom");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
        } }, dependencies: [OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
            OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
            OlFullScreenDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(FullScreenMapComponent, [{
        type: Component,
        args: [{ selector: 'full-screen-map', imports: [
                    OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
                    OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
                    OlFullScreenDirective, SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls>\n    <ol-zoom-control  />\n    <ol-attribution />\n    <ol-full-screen (fullScreenChange)=\"handleFullScreenChange($event)\" tipLabel=\"Full Screen Mode\"/>\n  </ol-controls>\n\n  <ol-interactions>\n    <ol-mouse-wheel-zoom/>\n  </ol-interactions>\n\n</ol-map>\n\n", styles: ["ol-map{display:block;width:100%;height:350px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(FullScreenMapComponent, { className: "FullScreenMapComponent", filePath: "lib/full-screen-map/full-screen-map.component.ts", lineNumber: 18 }); })();

class ZoomToExtentComponent {
    constructor() {
        this.source = new OSM();
        this.extent = [-14815256, 2011249, -6547035, 6679622];
    }
    static { this.ɵfac = function ZoomToExtentComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || ZoomToExtentComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: ZoomToExtentComponent, selectors: [["zoom-to-extent"]], decls: 9, vars: 2, consts: [[3, "source"], ["tipLabel", "United States", "label", "\uD83D\uDD16", 3, "extent"]], template: function ZoomToExtentComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls");
            i0.ɵɵelement(4, "ol-zoom-control")(5, "ol-attribution")(6, "ol-zoom-to-extent", 1);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "ol-interactions");
            i0.ɵɵelement(8, "ol-mouse-wheel-zoom");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("extent", ctx.extent);
        } }, dependencies: [OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
            OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
            OlZoomToExtentDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ZoomToExtentComponent, [{
        type: Component,
        args: [{ selector: 'zoom-to-extent', imports: [
                    OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
                    OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
                    OlZoomToExtentDirective, SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls>\n    <ol-zoom-control  />\n    <ol-attribution />\n    <ol-zoom-to-extent [extent]=\"extent\" tipLabel=\"United States\" label=\"\uD83D\uDD16\"/>\n  </ol-controls>\n\n  <ol-interactions>\n    <ol-mouse-wheel-zoom/>\n  </ol-interactions>\n\n</ol-map>\n", styles: ["ol-map{display:block;width:100%;height:350px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(ZoomToExtentComponent, { className: "ZoomToExtentComponent", filePath: "lib/zoom-to-extent/zoom-to-extent.component.ts", lineNumber: 18 }); })();

class RotateMapComponent {
    constructor() {
        this.source = new OSM();
        this.viewDirective = viewChild(OlViewDirective);
        this.degrees = linkedSignal(() => {
            const radians = this.viewDirective()?.rotation() ?? 0;
            return radians * 180 / Math.PI % 360;
        });
    }
    rotationLabel(value) {
        return `${value}°`;
    }
    handleRotation(value) {
        const degrees = value * Math.PI / 180;
        this.viewDirective()?.rotation.set(degrees);
    }
    static { this.ɵfac = function RotateMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || RotateMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: RotateMapComponent, selectors: [["rotate-map"]], viewQuery: function RotateMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.viewDirective, OlViewDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, decls: 15, vars: 3, consts: [[3, "source"], ["autoHide", "false"], [1, "setting"], ["min", "0", "max", "360", "step", "1", "discrete", "", "showTickMarks", "", 3, "displayWith"], ["matSliderThumb", "", 3, "valueChange", "value"]], template: function RotateMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls");
            i0.ɵɵelement(4, "ol-zoom-control")(5, "ol-attribution")(6, "ol-rotate", 1);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "ol-interactions");
            i0.ɵɵelement(8, "ol-drag-rotate")(9, "ol-mouse-wheel-zoom");
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(10, "div", 2)(11, "label");
            i0.ɵɵtext(12, "Rotation");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(13, "mat-slider", 3)(14, "input", 4);
            i0.ɵɵtwoWayListener("valueChange", function RotateMapComponent_Template_input_valueChange_14_listener($event) { i0.ɵɵtwoWayBindingSet(ctx.degrees, $event) || (ctx.degrees = $event); return $event; });
            i0.ɵɵlistener("valueChange", function RotateMapComponent_Template_input_valueChange_14_listener($event) { return ctx.handleRotation($event); });
            i0.ɵɵelementEnd()()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(11);
            i0.ɵɵproperty("displayWith", ctx.rotationLabel);
            i0.ɵɵadvance();
            i0.ɵɵtwoWayProperty("value", ctx.degrees);
        } }, dependencies: [OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
            OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
            OlRotateDirective, MatSlider, MatSliderThumb, OlDragRotateDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.setting[_ngcontent-%COMP%]{display:flex;justify-content:center;align-items:center;gap:.25em}mat-slider[_ngcontent-%COMP%]{min-width:250px}"], changeDetection: 0 }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(RotateMapComponent, [{
        type: Component,
        args: [{ selector: 'rotate-map', imports: [
                    OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
                    OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
                    OlRotateDirective, MatSlider, MatSliderThumb, OlDragRotateDirective, SourceDirective
                ], changeDetection: ChangeDetectionStrategy.OnPush, template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls>\n    <ol-zoom-control/>\n    <ol-attribution/>\n    <ol-rotate autoHide=\"false\"/>\n  </ol-controls>\n\n  <ol-interactions>\n    <ol-drag-rotate/>\n    <ol-mouse-wheel-zoom/>\n  </ol-interactions>\n\n</ol-map>\n  <div class=\"setting\">\n    <label>Rotation</label>\n    <mat-slider min=\"0\" max=\"360\" step=\"1\" discrete showTickMarks [displayWith]=\"rotationLabel\">\n      <input matSliderThumb [(value)]=\"degrees\" (valueChange)=\"handleRotation($event)\"/>\n    </mat-slider>\n  </div>\n\n", styles: ["ol-map{display:block;width:100%;height:350px}.setting{display:flex;justify-content:center;align-items:center;gap:.25em}mat-slider{min-width:250px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(RotateMapComponent, { className: "RotateMapComponent", filePath: "lib/rotate-map/rotate-map.component.ts", lineNumber: 20 }); })();

class IsActivePipe {
    transform(value) {
        return value ? 'Active' : 'Inactive';
    }
    static { this.ɵfac = function IsActivePipe_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || IsActivePipe)(); }; }
    static { this.ɵpipe = /*@__PURE__*/ i0.ɵɵdefinePipe({ name: "isActive", type: IsActivePipe, pure: true }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(IsActivePipe, [{
        type: Pipe,
        args: [{ name: 'isActive' }]
    }], null, null); })();
class KeyboardInteractionComponent {
    constructor() {
        this.keyboardZoom = viewChild(OlKeyboardZoomDirective);
        this.keyboardPan = viewChild(OlKeyboardPanDirective);
        this.source = new OSM();
        this.keyboardZoomActive = linkedSignal(() => this.keyboardZoom()?.active());
        this.keyboardPanActive = linkedSignal(() => this.keyboardPan()?.active());
    }
    toggleKeyboardZoom() {
        this.keyboardZoom()?.active.update(value => !value);
    }
    toggleKeyboardPan() {
        this.keyboardPan()?.active.update(value => !value);
    }
    static { this.ɵfac = function KeyboardInteractionComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || KeyboardInteractionComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: KeyboardInteractionComponent, selectors: [["keyboard-zoom"]], viewQuery: function KeyboardInteractionComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.keyboardZoom, OlKeyboardZoomDirective, 5);
            i0.ɵɵviewQuerySignal(ctx.keyboardPan, OlKeyboardPanDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(2);
        } }, decls: 16, vars: 9, consts: [[3, "source"], [1, "settings"], [3, "toggleChange", "checked"]], template: function KeyboardInteractionComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls");
            i0.ɵɵelement(4, "ol-zoom-control")(5, "ol-attribution");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "ol-interactions");
            i0.ɵɵelement(7, "ol-keyboard-pan")(8, "ol-keyboard-zoom");
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(9, "div", 1)(10, "mat-slide-toggle", 2);
            i0.ɵɵlistener("toggleChange", function KeyboardInteractionComponent_Template_mat_slide_toggle_toggleChange_10_listener() { return ctx.toggleKeyboardZoom(); });
            i0.ɵɵtext(11);
            i0.ɵɵpipe(12, "isActive");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(13, "mat-slide-toggle", 2);
            i0.ɵɵlistener("toggleChange", function KeyboardInteractionComponent_Template_mat_slide_toggle_toggleChange_13_listener() { return ctx.toggleKeyboardPan(); });
            i0.ɵɵtext(14);
            i0.ɵɵpipe(15, "isActive");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(8);
            i0.ɵɵproperty("checked", ctx.keyboardZoomActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Keyboard Zoom ", i0.ɵɵpipeBind1(12, 5, ctx.keyboardZoomActive()), "");
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("checked", ctx.keyboardPanActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Keyboard Pan ", i0.ɵɵpipeBind1(15, 7, ctx.keyboardPanActive()), "");
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlKeyboardPanDirective,
            OlKeyboardZoomDirective, OlZoomControlDirective, OlAttributionDirective, OlTileLayerDirective, MatSlideToggle, IsActivePipe, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{gap:2em;padding:.5em;display:flex;justify-content:center}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(KeyboardInteractionComponent, [{
        type: Component,
        args: [{ selector: 'keyboard-zoom', imports: [
                    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlKeyboardPanDirective,
                    OlKeyboardZoomDirective, OlZoomControlDirective, OlAttributionDirective, OlTileLayerDirective, MatSlideToggle,
                    IsActivePipe, SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls>\n    <ol-zoom-control  />\n    <ol-attribution />\n  </ol-controls>\n\n  <ol-interactions>\n    <ol-keyboard-pan/>\n    <ol-keyboard-zoom />\n  </ol-interactions>\n\n</ol-map>\n<div class=\"settings\">\n  <mat-slide-toggle [checked]=\"keyboardZoomActive\" (toggleChange)=\"toggleKeyboardZoom()\">Keyboard Zoom {{ keyboardZoomActive() | isActive }}</mat-slide-toggle>\n  <mat-slide-toggle [checked]=\"keyboardPanActive\" (toggleChange)=\"toggleKeyboardPan()\">Keyboard Pan {{ keyboardPanActive() | isActive }}</mat-slide-toggle>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{gap:2em;padding:.5em;display:flex;justify-content:center}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(KeyboardInteractionComponent, { className: "KeyboardInteractionComponent", filePath: "lib/keyboard-interaction/keyboard-interaction.component.ts", lineNumber: 27 }); })();

class MouseInteractionComponent {
    constructor() {
        this.mouseWheelZoom = viewChild(OlMouseWheelZoomDirective);
        this.dblClickZoom = viewChild(OlDblClickZoomDirective);
        this.dragPan = viewChild(OlDragPanDirective);
        this.source = new OSM();
        this.mouseWheelZoomActive = linkedSignal(() => this.mouseWheelZoom()?.active());
        this.dblClickZoomActive = linkedSignal(() => this.dblClickZoom()?.active());
        this.dragPanActive = linkedSignal(() => this.dragPan()?.active());
    }
    toggleMouseWheelZoom() {
        this.mouseWheelZoom()?.active.update(value => !value);
    }
    toggleDblClickZoom() {
        this.dblClickZoom()?.active.update(value => !value);
    }
    toggleDragPan() {
        this.dragPan()?.active.update(value => !value);
    }
    static { this.ɵfac = function MouseInteractionComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || MouseInteractionComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: MouseInteractionComponent, selectors: [["mouse-interaction"]], viewQuery: function MouseInteractionComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.mouseWheelZoom, OlMouseWheelZoomDirective, 5);
            i0.ɵɵviewQuerySignal(ctx.dblClickZoom, OlDblClickZoomDirective, 5);
            i0.ɵɵviewQuerySignal(ctx.dragPan, OlDragPanDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(3);
        } }, decls: 20, vars: 13, consts: [[3, "source"], [1, "settings"], [3, "toggleChange", "checked"]], template: function MouseInteractionComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls");
            i0.ɵɵelement(4, "ol-zoom-control")(5, "ol-attribution");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "ol-interactions");
            i0.ɵɵelement(7, "ol-dbl-click-zoom")(8, "ol-mouse-wheel-zoom")(9, "ol-drag-pan");
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(10, "div", 1)(11, "mat-slide-toggle", 2);
            i0.ɵɵlistener("toggleChange", function MouseInteractionComponent_Template_mat_slide_toggle_toggleChange_11_listener() { return ctx.toggleMouseWheelZoom(); });
            i0.ɵɵtext(12);
            i0.ɵɵpipe(13, "isActive");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(14, "mat-slide-toggle", 2);
            i0.ɵɵlistener("toggleChange", function MouseInteractionComponent_Template_mat_slide_toggle_toggleChange_14_listener() { return ctx.toggleDblClickZoom(); });
            i0.ɵɵtext(15);
            i0.ɵɵpipe(16, "isActive");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(17, "mat-slide-toggle", 2);
            i0.ɵɵlistener("toggleChange", function MouseInteractionComponent_Template_mat_slide_toggle_toggleChange_17_listener() { return ctx.toggleDragPan(); });
            i0.ɵɵtext(18);
            i0.ɵɵpipe(19, "isActive");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(9);
            i0.ɵɵproperty("checked", ctx.mouseWheelZoomActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Mouse Wheel Zoom ", i0.ɵɵpipeBind1(13, 7, ctx.mouseWheelZoomActive()), "");
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("checked", ctx.dblClickZoomActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Double Click Zoom ", i0.ɵɵpipeBind1(16, 9, ctx.dblClickZoomActive()), "");
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("checked", ctx.dragPanActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Drag Pan ", i0.ɵɵpipeBind1(19, 11, ctx.dragPanActive()), "");
        } }, dependencies: [IsActivePipe, MatSlideToggle, OlAttributionDirective, OlControlsDirective, OlInteractionsDirective,
            OlLayersDirective, OlMapComponent, OlZoomControlDirective, OlTileLayerDirective,
            OlMouseWheelZoomDirective, OlDblClickZoomDirective, OlDragPanDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{gap:2em;padding:.5em;display:flex;justify-content:center}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(MouseInteractionComponent, [{
        type: Component,
        args: [{ selector: 'mouse-interaction', imports: [IsActivePipe, MatSlideToggle, OlAttributionDirective, OlControlsDirective, OlInteractionsDirective,
                    OlLayersDirective, OlMapComponent, OlZoomControlDirective, OlTileLayerDirective,
                    OlMouseWheelZoomDirective, OlDblClickZoomDirective, OlDragPanDirective, SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls>\n    <ol-zoom-control  />\n    <ol-attribution />\n  </ol-controls>\n\n  <ol-interactions>\n    <ol-dbl-click-zoom />\n    <ol-mouse-wheel-zoom />\n    <ol-drag-pan />\n  </ol-interactions>\n\n</ol-map>\n<div class=\"settings\">\n  <mat-slide-toggle [checked]=\"mouseWheelZoomActive\" (toggleChange)=\"toggleMouseWheelZoom()\">Mouse Wheel Zoom {{ mouseWheelZoomActive() | isActive }}</mat-slide-toggle>\n  <mat-slide-toggle [checked]=\"dblClickZoomActive\" (toggleChange)=\"toggleDblClickZoom()\">Double Click Zoom {{ dblClickZoomActive() | isActive }}</mat-slide-toggle>\n  <mat-slide-toggle [checked]=\"dragPanActive\" (toggleChange)=\"toggleDragPan()\">Drag Pan {{ dragPanActive() | isActive }}</mat-slide-toggle>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{gap:2em;padding:.5em;display:flex;justify-content:center}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(MouseInteractionComponent, { className: "MouseInteractionComponent", filePath: "lib/mouse-interaction/mouse-interaction.component.ts", lineNumber: 22 }); })();

class DragInteractionComponent {
    constructor() {
        this.dragZoom = viewChild(OlDragZoomDirective);
        this.dragPan = viewChild(OlDragPanDirective);
        this.dragRotate = viewChild(OlDragRotateDirective);
        this.dragBox = viewChild(DragBoxDirective);
        this.source = new OSM();
        this.dragZoomActive = linkedSignal(() => this.dragZoom()?.active());
        this.dragPanActive = linkedSignal(() => this.dragPan()?.active());
        this.dragRotateActive = linkedSignal(() => this.dragRotate()?.active());
        this.boxStart = signal(undefined);
        this.boxEnd = signal(undefined);
    }
    toggleDragZoom() {
        this.dragZoom()?.active.update(value => !value);
    }
    toggleDragPan() {
        this.dragPan()?.active.update(value => !value);
    }
    toggleDragRotate() {
        this.dragRotate()?.active.update(value => !value);
    }
    ngOnInit() {
        this.dragBox()?.boxStarted.subscribe(coordinate => {
            this.boxEnd.set(undefined);
            this.boxStart.set(coordinate);
        });
        this.dragBox()?.boxEnded.subscribe(coordinate => this.boxEnd.set(coordinate));
    }
    static { this.ɵfac = function DragInteractionComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || DragInteractionComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: DragInteractionComponent, selectors: [["drag-interaction"]], viewQuery: function DragInteractionComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.dragZoom, OlDragZoomDirective, 5);
            i0.ɵɵviewQuerySignal(ctx.dragPan, OlDragPanDirective, 5);
            i0.ɵɵviewQuerySignal(ctx.dragRotate, OlDragRotateDirective, 5);
            i0.ɵɵviewQuerySignal(ctx.dragBox, DragBoxDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(4);
        } }, decls: 23, vars: 15, consts: [[3, "source"], ["zoomInLabel", "\u2795", "zoomOutLabel", "\u2796", "zoomOutTipLabel", "Zoom Out", "zoomInTipLabel", "Zoom In"], ["autoHide", "false", "label", "\u2949"], ["collapsible", "true", "collapseLabel", "\u2AA2", "label", "\u24D8"], ["defaultInteractions", ""], [1, "settings"], [3, "toggleChange", "checked"]], template: function DragInteractionComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ol-controls");
            i0.ɵɵelement(4, "ol-zoom-control", 1)(5, "ol-rotate", 2)(6, "ol-attribution", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(7, "ol-interactions", 4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "div", 5)(9, "mat-slide-toggle", 6);
            i0.ɵɵlistener("toggleChange", function DragInteractionComponent_Template_mat_slide_toggle_toggleChange_9_listener() { return ctx.toggleDragZoom(); });
            i0.ɵɵtext(10);
            i0.ɵɵpipe(11, "isActive");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(12, "mat-slide-toggle", 6);
            i0.ɵɵlistener("toggleChange", function DragInteractionComponent_Template_mat_slide_toggle_toggleChange_12_listener() { return ctx.toggleDragPan(); });
            i0.ɵɵtext(13);
            i0.ɵɵpipe(14, "isActive");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(15, "mat-slide-toggle", 6);
            i0.ɵɵlistener("toggleChange", function DragInteractionComponent_Template_mat_slide_toggle_toggleChange_15_listener() { return ctx.toggleDragRotate(); });
            i0.ɵɵtext(16);
            i0.ɵɵpipe(17, "isActive");
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(18, "div", 5)(19, "div");
            i0.ɵɵtext(20);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(21, "div");
            i0.ɵɵtext(22);
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance(7);
            i0.ɵɵproperty("checked", ctx.dragZoomActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Drag Zoom ", i0.ɵɵpipeBind1(11, 9, ctx.dragZoomActive()), "");
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("checked", ctx.dragPanActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Drag Pan ", i0.ɵɵpipeBind1(14, 11, ctx.dragPanActive()), "");
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("checked", ctx.dragRotateActive);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Drag Rotate ", i0.ɵɵpipeBind1(17, 13, ctx.dragRotateActive()), "");
            i0.ɵɵadvance(4);
            i0.ɵɵtextInterpolate1("Start: ", ctx.boxStart(), "");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate1("End: ", ctx.boxEnd(), "");
        } }, dependencies: [IsActivePipe, MatSlideToggle, OlAttributionDirective, OlControlsDirective,
            OlInteractionsDirective, OlLayersDirective, OlMapComponent, OlZoomControlDirective,
            OlTileLayerDirective, OlRotateDirective, OlDefaultInteractionDirective, SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{gap:2em;padding:.5em;display:flex;justify-content:center}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DragInteractionComponent, [{
        type: Component,
        args: [{ selector: 'drag-interaction', imports: [
                    IsActivePipe, MatSlideToggle, OlAttributionDirective, OlControlsDirective,
                    OlInteractionsDirective, OlLayersDirective, OlMapComponent, OlZoomControlDirective,
                    OlTileLayerDirective, OlRotateDirective, OlDefaultInteractionDirective, SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls>\n    <ol-zoom-control zoomInLabel=\"&#10133;\" zoomOutLabel=\"&#10134;\" zoomOutTipLabel=\"Zoom Out\" zoomInTipLabel=\"Zoom In\" />\n    <ol-rotate autoHide=\"false\" label=\"\u2949\"/>\n    <ol-attribution collapsible=\"true\" collapseLabel=\"&#10914;\" label=\"&#x24D8;\"/>\n  </ol-controls>\n\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n<div class=\"settings\">\n  <mat-slide-toggle [checked]=\"dragZoomActive\" (toggleChange)=\"toggleDragZoom()\">Drag Zoom {{ dragZoomActive() | isActive }}</mat-slide-toggle>\n  <mat-slide-toggle [checked]=\"dragPanActive\" (toggleChange)=\"toggleDragPan()\">Drag Pan {{ dragPanActive() | isActive }}</mat-slide-toggle>\n  <mat-slide-toggle [checked]=\"dragRotateActive\" (toggleChange)=\"toggleDragRotate()\">Drag Rotate {{ dragRotateActive() | isActive }}</mat-slide-toggle>\n</div>\n<div class=\"settings\">\n  <div>Start: {{ boxStart() }}</div>\n  <div>End: {{ boxEnd() }}</div>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{gap:2em;padding:.5em;display:flex;justify-content:center}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(DragInteractionComponent, { className: "DragInteractionComponent", filePath: "lib/drag-interaction/drag-interaction.component.ts", lineNumber: 36 }); })();

const _c0$4 = ["*"];
const TransformCoordinateComponent_Defer_5_DepsFn = () => [OlTransformPipe, OlCoordinatePipe, import('@angular/common').then(m => m.AsyncPipe)];
function TransformCoordinateComponent_Defer_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "transform");
    i0.ɵɵpipe(3, "async");
    i0.ɵɵpipe(4, "coordinate");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance();
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind4(4, 7, i0.ɵɵpipeBind1(3, 5, i0.ɵɵpipeBind3(2, 1, ctx_r0.center(), ctx_r0.sourceCrs(), ctx_r0.destinationCrs())), "format", ctx_r0.factionDigits(), ctx_r0.tpl()));
} }
function TransformCoordinateComponent_DeferPlaceholder_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-spinner", 2);
} }
const _c1$1 = () => [2375184, 492342];
/**
 * TransformCoordinateComponent is responsible for transforming geographic coordinates from one projection system
 * to another and displaying the transformed coordinates in the specified format. The component loads the necessary
 * projections and transforms the coordinates asynchronously, providing a loading spinner indicator while the
 * transformation is in progress.
 *
 * Inputs:
 * - `source`: The source projection defined as a required input (ProjectionLike). This is the projection in which
 *   the input coordinates are provided.
 * - `destination`: The destination projection defined as a required input (ProjectionLike). This is the projection
 *   to which the coordinates will be transformed.
 * - `center`: The input coordinate model, which is required and can be either a Coordinate, undefined, or null.
 *   This represents the geographic coordinate to be transformed.
 * - `tpl`: The string template used to format the transformed coordinate. Defaults to '{x}, {y}'.
 * - `factionDigits`: The number of decimal places to use when formatting the transformed coordinate. Defaults to 3.
 */
class TransformCoordinateComponent {
    constructor() {
        this.#projService = inject(Proj4Service);
        this.loaded = signal(false);
        this.sourceCrs = input.required();
        this.destinationCrs = input.required();
        this.center = model.required();
        this.tpl = input('{x}, {y}');
        this.factionDigits = input(3);
    }
    #projService;
    ngOnInit() {
        forkJoin([this.#projService.getProjection(this.sourceCrs()), this.#projService.getProjection(this.destinationCrs())])
            .pipe(first())
            .subscribe(() => this.loaded.set(true));
    }
    static { this.ɵfac = function TransformCoordinateComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || TransformCoordinateComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: TransformCoordinateComponent, selectors: [["transform-coordinate"]], inputs: { sourceCrs: [1, "sourceCrs"], destinationCrs: [1, "destinationCrs"], center: [1, "center"], tpl: [1, "tpl"], factionDigits: [1, "factionDigits"] }, outputs: { center: "centerChange" }, ngContentSelectors: _c0$4, decls: 7, vars: 1, consts: [[500], [1, "transform-value"], ["diameter", "15"]], template: function TransformCoordinateComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵprojectionDef();
            i0.ɵɵelementStart(0, "div", 1)(1, "div");
            i0.ɵɵprojection(2);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(3, TransformCoordinateComponent_Defer_3_Template, 5, 12)(4, TransformCoordinateComponent_DeferPlaceholder_4_Template, 1, 0);
            i0.ɵɵdefer(5, 3, TransformCoordinateComponent_Defer_5_DepsFn, null, 4, null, null, 0, i0.ɵɵdeferEnableTimerScheduling);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(5);
            i0.ɵɵdeferWhen(ctx.loaded());
        } }, dependencies: [MatProgressSpinner], styles: [".transform-value[_ngcontent-%COMP%]{display:flex;justify-content:space-between;gap:.25em}.transform-value[_ngcontent-%COMP%]:first-child{text-align:end}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadataAsync(TransformCoordinateComponent, () => [import('@angular/common').then(m => m.AsyncPipe)], AsyncPipe => { i0.ɵsetClassMetadata(TransformCoordinateComponent, [{
        type: Component,
        args: [{ selector: 'transform-coordinate', imports: [
                    OlTransformPipe, OlCoordinatePipe,
                    AsyncPipe,
                    MatProgressSpinner
                ], template: `
    <div class="transform-value ">
      <div>
        <ng-content/>
      </div>
      @defer (when loaded()) {
        <div>{{ center() | transform:sourceCrs():destinationCrs() | async | coordinate:'format':factionDigits():tpl() }}</div>
      } @placeholder (minimum 500ms) {
        <mat-spinner diameter="15"/>
      }
    </div>
  `, styles: [".transform-value{display:flex;justify-content:space-between;gap:.25em}.transform-value:first-child{text-align:end}\n"] }]
    }], null, null); }); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(TransformCoordinateComponent, { className: "TransformCoordinateComponent", filePath: "lib/projection-map/projection-map.component.ts", lineNumber: 76 }); })();
/**
 * ProjectionMapComponent is an Angular component designed for rendering and managing
 * a basic map interface using OpenLayers. This component integrates configurations such
 * as layers, controls, interactions, and coordinate transformations to establish a functional
 * map. It also supports integration with custom point-of-interest data for specific use cases.
 */
class ProjectionMapComponent {
    constructor() {
        this.source = new OSM();
        this.features = [
            new Feature({ geometry: new Point([2508336, 339113]) }),
            new Feature({ geometry: new Point([1596539, 527761]) })
        ];
        this.viewDirective = viewChild(OlViewDirective);
    }
    static { this.ɵfac = function ProjectionMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || ProjectionMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: ProjectionMapComponent, selectors: [["basic-map"]], viewQuery: function ProjectionMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.viewDirective, OlViewDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, decls: 17, vars: 10, consts: [["projection", "EPSG:2238", "extent", "-1151161.826,-352124.584, 6828738.708, 1775132.109", 3, "zoom", "center"], [3, "source"], [3, "features"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "transform-values"], ["sourceCrs", "EPSG:2238", "destinationCrs", "EPSG:2238", 3, "center"], ["sourceCrs", "EPSG:2238", "destinationCrs", "EPSG:4326", 3, "center"], ["sourceCrs", "EPSG:2238", "destinationCrs", "EPSG:3086", 3, "center"], ["sourceCrs", "EPSG:2238", "destinationCrs", "EPSG:3857", 3, "center"], ["sourceCrs", "EPSG:2238", "destinationCrs", "EPSG:3580", 3, "center"]], template: function ProjectionMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map", 0)(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 1)(3, "ol-vector-layer", 2);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(4, "ol-controls", 3)(5, "ol-interactions", 4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 5)(7, "transform-coordinate", 6);
            i0.ɵɵtext(8, " NAD83 / Florida North (ftUS): ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "transform-coordinate", 7);
            i0.ɵɵtext(10, " World Geodetic System 1984: ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(11, "transform-coordinate", 8);
            i0.ɵɵtext(12, " NAD83 / Florida GDL Albers: ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(13, "transform-coordinate", 9);
            i0.ɵɵtext(14, " Spherical Mercator: ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(15, "transform-coordinate", 10);
            i0.ɵɵtext(16, " NAD83 / NWT Lambert: ");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            let tmp_4_0;
            let tmp_5_0;
            let tmp_6_0;
            let tmp_7_0;
            let tmp_8_0;
            i0.ɵɵproperty("zoom", 8)("center", i0.ɵɵpureFunction0(9, _c1$1));
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance();
            i0.ɵɵproperty("features", ctx.features);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("center", (tmp_4_0 = ctx.viewDirective()) == null ? null : tmp_4_0.center());
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("center", (tmp_5_0 = ctx.viewDirective()) == null ? null : tmp_5_0.center());
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("center", (tmp_6_0 = ctx.viewDirective()) == null ? null : tmp_6_0.center());
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("center", (tmp_7_0 = ctx.viewDirective()) == null ? null : tmp_7_0.center());
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("center", (tmp_8_0 = ctx.viewDirective()) == null ? null : tmp_8_0.center());
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlTileLayerDirective,
            OlDefaultInteractionDirective, OlDefaultControlDirective, TransformCoordinateComponent, OlVectorLayerDirective,
            SourceDirective, OlFeatureSourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.transform-values[_ngcontent-%COMP%]{display:flex;flex-direction:column;gap:.25em;max-width:30%;margin:auto}"], changeDetection: 0 }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ProjectionMapComponent, [{
        type: Component,
        args: [{ selector: 'basic-map', imports: [
                    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlTileLayerDirective,
                    OlDefaultInteractionDirective, OlDefaultControlDirective, TransformCoordinateComponent, OlVectorLayerDirective,
                    SourceDirective, OlFeatureSourceDirective
                ], changeDetection: ChangeDetectionStrategy.OnPush, template: "<ol-map projection=\"EPSG:2238\"\n        [zoom]=\"8\"\n        [center]=\"[2375184,492342]\"\n        extent=\"-1151161.826,-352124.584, 6828738.708, 1775132.109\">\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n    <ol-vector-layer [features]=\"features\" />\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n<div class=\"transform-values\">\n  <transform-coordinate [center]=\"viewDirective()?.center()\" sourceCrs=\"EPSG:2238\" destinationCrs=\"EPSG:2238\">\n    NAD83 / Florida North (ftUS):\n  </transform-coordinate>\n\n  <transform-coordinate [center]=\"viewDirective()?.center()\" sourceCrs=\"EPSG:2238\" destinationCrs=\"EPSG:4326\">\n    World Geodetic System 1984:\n  </transform-coordinate>\n\n  <transform-coordinate [center]=\"viewDirective()?.center()\" sourceCrs=\"EPSG:2238\" destinationCrs=\"EPSG:3086\">\n    NAD83 / Florida GDL Albers:\n  </transform-coordinate>\n\n  <transform-coordinate [center]=\"viewDirective()?.center()\" sourceCrs=\"EPSG:2238\" destinationCrs=\"EPSG:3857\">\n    Spherical Mercator:\n  </transform-coordinate>\n\n  <transform-coordinate [center]=\"viewDirective()?.center()\" sourceCrs=\"EPSG:2238\" destinationCrs=\"EPSG:3580\">\n    NAD83 / NWT Lambert:\n  </transform-coordinate>\n\n</div>\n\n\n", styles: ["ol-map{display:block;width:100%;height:350px}.transform-values{display:flex;flex-direction:column;gap:.25em;max-width:30%;margin:auto}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(ProjectionMapComponent, { className: "ProjectionMapComponent", filePath: "lib/projection-map/projection-map.component.ts", lineNumber: 112 }); })();

const _c0$3 = ["testGroup"];
class LayerGroupComponent {
    constructor() {
        this.basePath = inject(EXAMPLES_BASE_PATH);
        this.visibleDirective = viewChild('testGroup', { read: VisibleDirective });
        this.visible = linkedSignal(() => this.visibleDirective()?.visible() ?? false);
        this.osmSource = new OSM();
        this.pointStyle = {
            'circle-radius': 6,
            'circle-fill-color': '#3366997F',
            'circle-stroke-color': '#333',
            'circle-stroke-width': 1.25
        };
        this.randomPointUrl = `${this.basePath}/data/random-point.geojson`;
        this.lineStyle = feature => new Style({
            stroke: new Stroke({ width: 2, color: feature.get('color') })
        });
        this.randomLineUrl = `${this.basePath}/data/random-lines.geojson`;
    }
    toggleGroupVisibility() {
        this.visibleDirective()?.visible.update(value => !value);
    }
    static { this.ɵfac = function LayerGroupComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || LayerGroupComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: LayerGroupComponent, selectors: [["layer-group"]], viewQuery: function LayerGroupComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.visibleDirective, _c0$3, 5, VisibleDirective);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, decls: 13, vars: 13, consts: [["testGroup", ""], [3, "source"], [3, "visible", "maxZoom"], [3, "geojson"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "settings"], [3, "toggleChange", "checked"]], template: function LayerGroupComponent_Template(rf, ctx) { if (rf & 1) {
            const _r1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 1);
            i0.ɵɵelementStart(3, "ol-group-layer", 2, 0);
            i0.ɵɵelement(5, "ol-vector-layer", 3)(6, "ol-vector-layer", 3);
            i0.ɵɵelementEnd()();
            i0.ɵɵelement(7, "ol-controls", 4)(8, "ol-interactions", 5);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "div", 6)(10, "mat-slide-toggle", 7);
            i0.ɵɵlistener("toggleChange", function LayerGroupComponent_Template_mat_slide_toggle_toggleChange_10_listener() { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.toggleGroupVisibility()); });
            i0.ɵɵtext(11);
            i0.ɵɵpipe(12, "isActive");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.osmSource);
            i0.ɵɵadvance();
            i0.ɵɵproperty("visible", ctx.visible())("maxZoom", 10);
            i0.ɵɵadvance(2);
            i0.ɵɵstyleMap(ctx.pointStyle);
            i0.ɵɵproperty("geojson", ctx.randomPointUrl);
            i0.ɵɵadvance();
            i0.ɵɵstyleMap(ctx.lineStyle);
            i0.ɵɵproperty("geojson", ctx.randomLineUrl);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("checked", ctx.visible);
            i0.ɵɵadvance();
            i0.ɵɵtextInterpolate1("Toggle Layer Group ", i0.ɵɵpipeBind1(12, 11, ctx.visible()), " ");
        } }, dependencies: [OlMapComponent, OlLayersDirective,
            OlTileLayerDirective, OlVectorLayerDirective, OlGroupLayerDirective,
            OlControlsDirective, OlDefaultControlDirective,
            OlInteractionsDirective, OlDefaultInteractionDirective,
            MatSlideToggle, IsActivePipe, SourceDirective,
            OlGeoJsonSourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{display:flex;justify-content:center;padding:.25em}"], changeDetection: 0 }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(LayerGroupComponent, [{
        type: Component,
        args: [{ selector: 'layer-group', imports: [
                    OlMapComponent, OlLayersDirective,
                    OlTileLayerDirective, OlVectorLayerDirective, OlGroupLayerDirective,
                    OlControlsDirective, OlDefaultControlDirective,
                    OlInteractionsDirective, OlDefaultInteractionDirective,
                    MatSlideToggle, IsActivePipe, SourceDirective,
                    OlGeoJsonSourceDirective
                ], changeDetection: ChangeDetectionStrategy.OnPush, template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"osmSource\" />\n    <ol-group-layer #testGroup [visible]=\"visible()\" [maxZoom]=\"10\">\n      <ol-vector-layer [geojson]=\"randomPointUrl\" [style]=\"pointStyle\"/>\n      <ol-vector-layer [geojson]=\"randomLineUrl\" [style]=\"lineStyle\" />\n    </ol-group-layer>\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n<div class=\"settings\">\n  <mat-slide-toggle [checked]=\"visible\" (toggleChange)=\"toggleGroupVisibility()\">Toggle Layer Group {{ visible() | isActive }} </mat-slide-toggle>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{display:flex;justify-content:center;padding:.25em}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(LayerGroupComponent, { className: "LayerGroupComponent", filePath: "lib/layer-group/layer-group.component.ts", lineNumber: 40 }); })();

const _c0$2 = ["riv_gauges_new"];
class GraticuleMapComponent {
    constructor() {
        this.minMaxZoomDirective = viewChild('riv_gauges_new', { read: MinMaxLayerZoomDirective });
        this.viewDirective = viewChild(OlViewDirective);
        this.minZoom = linkedSignal(() => this.minMaxZoomDirective()?.minZoom());
        this.maxZoom = linkedSignal(() => this.minMaxZoomDirective()?.maxZoom());
        this.extent = [-14229084, 2643357, -5512723, 6859294];
        this.center = getCenter(this.extent);
        this.style = {
            "circle-radius": 8,
            "circle-fill-color": 'rgba(51,102,153,0.77)',
            "circle-stroke-color": "rgba(0,0,0,0.6)",
            "circle-stroke-width": 1.25,
            "text-value": ['get', 'size'],
            "text-fill-color": "rgb(255,255,255)"
        };
        this.osmSource = new OSM();
    }
    updateMinZoom(value) {
        this.minMaxZoomDirective()?.minZoom.set(value);
    }
    updateMaxZoom(value) {
        this.minMaxZoomDirective()?.maxZoom.set(value);
    }
    static { this.ɵfac = function GraticuleMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || GraticuleMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: GraticuleMapComponent, selectors: [["graticule-map"]], viewQuery: function GraticuleMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.minMaxZoomDirective, _c0$2, 5, MinMaxLayerZoomDirective);
            i0.ɵɵviewQuerySignal(ctx.viewDirective, OlViewDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(2);
        } }, decls: 21, vars: 12, consts: [["riv_gauges_new", ""], [3, "center", "zoom", "maxZoom"], [3, "source"], ["latLabelPosition", "1.0"], ["cluster", "true", "wfs", "https://mapservices.weather.noaa.gov/eventdriven/services/water/riv_gauges/MapServer/WFSServer", "layer", "riv_gauges_new:Observed_River_Stages", 3, "extent", "minZoom", "maxZoom"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "settings"], ["for", "minZoom"], ["min", "3", "max", "14", "discrete", "", "showTickMarks", ""], ["id", "minZoom", "matSliderStartThumb", "", 3, "valueChange", "value"], ["id", "maxZoom", "matSliderEndThumb", "", 3, "valueChange", "value"], ["for", "maxZoom"]], template: function GraticuleMapComponent_Template(rf, ctx) { if (rf & 1) {
            const _r1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "ol-map", 1)(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 2)(3, "ol-graticle-layer", 3)(4, "ol-vector-layer", 4, 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(6, "ol-controls", 5)(7, "ol-interactions", 6);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "div", 7)(9, "label", 8);
            i0.ɵɵtext(10, "Minimum Zoom");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(11, "mat-slider", 9)(12, "input", 10);
            i0.ɵɵtwoWayListener("valueChange", function GraticuleMapComponent_Template_input_valueChange_12_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.minZoom, $event) || (ctx.minZoom = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function GraticuleMapComponent_Template_input_valueChange_12_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateMinZoom($event)); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(13, "input", 11);
            i0.ɵɵtwoWayListener("valueChange", function GraticuleMapComponent_Template_input_valueChange_13_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.maxZoom, $event) || (ctx.maxZoom = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function GraticuleMapComponent_Template_input_valueChange_13_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateMaxZoom($event)); });
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(14, "label", 12);
            i0.ɵɵtext(15, "Maximum Zoom");
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(16, "div", 7)(17, "label");
            i0.ɵɵtext(18, "Current Zoom:");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(19, "p");
            i0.ɵɵtext(20);
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            let tmp_11_0;
            i0.ɵɵproperty("center", ctx.center)("zoom", 3)("maxZoom", 14);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.osmSource);
            i0.ɵɵadvance(2);
            i0.ɵɵstyleMap(ctx.style);
            i0.ɵɵproperty("extent", ctx.extent)("minZoom", 8)("maxZoom", 14);
            i0.ɵɵadvance(8);
            i0.ɵɵtwoWayProperty("value", ctx.minZoom);
            i0.ɵɵadvance();
            i0.ɵɵtwoWayProperty("value", ctx.maxZoom);
            i0.ɵɵadvance(7);
            i0.ɵɵtextInterpolate((tmp_11_0 = ctx.viewDirective()) == null ? null : tmp_11_0.zoom());
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlDefaultInteractionDirective,
            OlDefaultControlDirective, OlTileLayerDirective, OlGraticleLayerDirective, SourceDirective, OlWFSSourceDirective,
            OlVectorLayerDirective, MatSlider, MatSliderRangeThumb], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{display:flex;justify-content:center;align-items:center;gap:.5em;padding:.25em}mat-slider[_ngcontent-%COMP%]{min-width:25%}"], changeDetection: 0 }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(GraticuleMapComponent, [{
        type: Component,
        args: [{ selector: 'graticule-map', imports: [
                    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlDefaultInteractionDirective,
                    OlDefaultControlDirective, OlTileLayerDirective, OlGraticleLayerDirective, SourceDirective, OlWFSSourceDirective,
                    OlVectorLayerDirective, MatSlider, MatSliderRangeThumb
                ], changeDetection: ChangeDetectionStrategy.OnPush, template: "<ol-map [center]=\"center\" [zoom]=\"3\" [maxZoom]=\"14\">\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"osmSource\"/>\n    <ol-graticle-layer latLabelPosition=\"1.0\"/>\n    <ol-vector-layer #riv_gauges_new [extent]=\"extent\" [style]=\"style\" [minZoom]=\"8\" [maxZoom]=\"14\" cluster=\"true\"\n                     wfs=\"https://mapservices.weather.noaa.gov/eventdriven/services/water/riv_gauges/MapServer/WFSServer\"\n                     layer=\"riv_gauges_new:Observed_River_Stages\"/>\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n<div class=\"settings\">\n  <label for=\"minZoom\">Minimum Zoom</label>\n  <mat-slider min=\"3\" max=\"14\" discrete showTickMarks>\n    <input id=\"minZoom\" [(value)]=\"minZoom\" matSliderStartThumb (valueChange)=\"updateMinZoom($event)\">\n    <input id=\"maxZoom\" [(value)]=\"maxZoom\" matSliderEndThumb (valueChange)=\"updateMaxZoom($event)\">\n  </mat-slider>\n  <label for=\"maxZoom\">Maximum Zoom</label>\n</div>\n<div class=\"settings\">\n  <label>Current Zoom:</label>\n  <p>{{ viewDirective()?.zoom() }}</p>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{display:flex;justify-content:center;align-items:center;gap:.5em;padding:.25em}mat-slider{min-width:25%}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(GraticuleMapComponent, { className: "GraticuleMapComponent", filePath: "lib/graticule-map/graticule-map.component.ts", lineNumber: 32 }); })();

const ImageLayerComponent_Defer_11_DepsFn = () => [import('@angular/material/icon').then(m => m.MatIcon)];
function ImageLayerComponent_Defer_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 7);
    i0.ɵɵtext(1, "check_circle");
    i0.ɵɵelementEnd();
} }
function ImageLayerComponent_DeferPlaceholder_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-spinner", 8);
} }
class ImageLayerComponent {
    constructor() {
        this.imageLayer = viewChild(OlImageLayerDirective);
        this.extent = [-13884991, 2870341, -7455066, 6338219];
        this.center = getCenter(this.extent);
        this.loaded = signal(false);
        this.osmSource = new OSM();
        this.imageSource = new ImageWMS({
            url: 'https://ahocevar.com/geoserver/wms',
            params: { 'LAYERS': 'topp:states' },
            ratio: 1,
            serverType: 'geoserver',
        });
    }
    ngAfterViewInit() {
        this.imageLayer()?.postRender.subscribe(() => this.loaded.set(true));
    }
    static { this.ɵfac = function ImageLayerComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || ImageLayerComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: ImageLayerComponent, selectors: [["image-layer"]], viewQuery: function ImageLayerComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.imageLayer, OlImageLayerDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, decls: 13, vars: 6, consts: [[500], [3, "center", "zoom"], [3, "source"], [3, "source", "extent"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "settings"], [1, "loaded-icon"], ["diameter", "16"]], template: function ImageLayerComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map", 1)(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 2)(3, "ol-image-layer", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(4, "ol-controls", 4)(5, "ol-interactions", 5);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 6)(7, "p");
            i0.ɵɵtext(8, "Image Layer: ");
            i0.ɵɵtemplate(9, ImageLayerComponent_Defer_9_Template, 2, 0)(10, ImageLayerComponent_DeferPlaceholder_10_Template, 1, 0);
            i0.ɵɵdefer(11, 9, ImageLayerComponent_Defer_11_DepsFn, null, 10, null, null, 0, i0.ɵɵdeferEnableTimerScheduling);
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            i0.ɵɵproperty("center", ctx.center)("zoom", 4);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.osmSource);
            i0.ɵɵadvance();
            i0.ɵɵproperty("source", ctx.imageSource)("extent", ctx.extent);
            i0.ɵɵadvance(8);
            i0.ɵɵdeferWhen(ctx.loaded());
        } }, dependencies: [OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective,
            OlDefaultInteractionDirective, OlDefaultControlDirective,
            OlImageLayerDirective, OlTileLayerDirective, SourceDirective,
            MatProgressSpinner], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{display:flex;justify-content:center;padding:.25em}.settings[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{display:flex;align-items:center;gap:1em}.loaded-icon[_ngcontent-%COMP%]{color:#08b836}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadataAsync(ImageLayerComponent, () => [import('@angular/material/icon').then(m => m.MatIcon)], MatIcon => { i0.ɵsetClassMetadata(ImageLayerComponent, [{
        type: Component,
        args: [{ selector: 'image-layer', imports: [
                    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective,
                    OlDefaultInteractionDirective, OlDefaultControlDirective,
                    OlImageLayerDirective, OlTileLayerDirective, SourceDirective, ExtentDirective,
                    MatProgressSpinner,
                    MatIcon
                ], template: "<ol-map [center]=\"center\" [zoom]=\"4\">\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"osmSource\"/>\n    <ol-image-layer [source]=\"imageSource\" [extent]=\"extent\"/>\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n\n<div class=\"settings\">\n  <p>Image Layer:\n    @defer (when loaded()) {\n      <mat-icon class=\"loaded-icon\">check_circle</mat-icon>\n    } @placeholder (minimum 500ms) {\n      <mat-spinner diameter=\"16\"/>\n    }\n  </p>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{display:flex;justify-content:center;padding:.25em}.settings p{display:flex;align-items:center;gap:1em}.loaded-icon{color:#08b836}\n"] }]
    }], null, null); }); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(ImageLayerComponent, { className: "ImageLayerComponent", filePath: "lib/image-layer/image-layer.component.ts", lineNumber: 22 }); })();

class HeatmapComponent {
    constructor() {
        this.heatmapLayer = viewChild(OlHeatmapLayerDirective);
        this.radius = linkedSignal(() => this.heatmapLayer()?.radius() ?? 10);
        this.blur = linkedSignal(() => this.heatmapLayer()?.blur() ?? 10);
        this.osmSource = new OSM();
    }
    handleRadius(value) {
        this.heatmapLayer()?.radius.set(value);
    }
    handleBlur(value) {
        this.heatmapLayer()?.blur.set(value);
    }
    static { this.ɵfac = function HeatmapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || HeatmapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: HeatmapComponent, selectors: [["heatmap"]], viewQuery: function HeatmapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.heatmapLayer, OlHeatmapLayerDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, decls: 16, vars: 3, consts: [[3, "source"], ["kml", "https://openlayers.org/en/latest/examples/data/kml/2012_Earthquakes_Mag5.kml"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "setting"], ["min", "3", "max", "25", "step", "1", "discrete", "", "showTickMarks", ""], ["matSliderThumb", "", 3, "valueChange", "value"]], template: function HeatmapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 0)(3, "ol-heatmap-layer", 1);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(4, "ol-controls", 2)(5, "ol-interactions", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 4)(7, "label");
            i0.ɵɵtext(8, "Radius");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "mat-slider", 5)(10, "input", 6);
            i0.ɵɵtwoWayListener("valueChange", function HeatmapComponent_Template_input_valueChange_10_listener($event) { i0.ɵɵtwoWayBindingSet(ctx.radius, $event) || (ctx.radius = $event); return $event; });
            i0.ɵɵlistener("valueChange", function HeatmapComponent_Template_input_valueChange_10_listener($event) { return ctx.handleRadius($event); });
            i0.ɵɵelementEnd()()();
            i0.ɵɵelementStart(11, "div", 4)(12, "label");
            i0.ɵɵtext(13, "Blur");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(14, "mat-slider", 5)(15, "input", 6);
            i0.ɵɵtwoWayListener("valueChange", function HeatmapComponent_Template_input_valueChange_15_listener($event) { i0.ɵɵtwoWayBindingSet(ctx.blur, $event) || (ctx.blur = $event); return $event; });
            i0.ɵɵlistener("valueChange", function HeatmapComponent_Template_input_valueChange_15_listener($event) { return ctx.handleBlur($event); });
            i0.ɵɵelementEnd()()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.osmSource);
            i0.ɵɵadvance(8);
            i0.ɵɵtwoWayProperty("value", ctx.radius);
            i0.ɵɵadvance(5);
            i0.ɵɵtwoWayProperty("value", ctx.blur);
        } }, dependencies: [OlControlsDirective,
            OlDefaultControlDirective,
            OlDefaultInteractionDirective,
            OlInteractionsDirective,
            OlLayersDirective,
            OlMapComponent,
            OlTileLayerDirective,
            OlHeatmapLayerDirective,
            SourceDirective,
            MatSlider,
            MatSliderThumb,
            OlKmlSourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.setting[_ngcontent-%COMP%]{display:flex;justify-content:center;align-items:center;gap:.25em}mat-slider[_ngcontent-%COMP%]{min-width:250px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(HeatmapComponent, [{
        type: Component,
        args: [{ selector: 'heatmap', imports: [
                    OlControlsDirective,
                    OlDefaultControlDirective,
                    OlDefaultInteractionDirective,
                    OlInteractionsDirective,
                    OlLayersDirective,
                    OlMapComponent,
                    OlTileLayerDirective,
                    OlHeatmapLayerDirective,
                    SourceDirective,
                    MatSlider,
                    MatSliderThumb,
                    OlKmlSourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"osmSource\" />\n    <ol-heatmap-layer kml=\"https://openlayers.org/en/latest/examples/data/kml/2012_Earthquakes_Mag5.kml\" />\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n\n<div class=\"setting\">\n  <label>Radius</label>\n  <mat-slider min=\"3\" max=\"25\" step=\"1\" discrete showTickMarks>\n    <input matSliderThumb [(value)]=\"radius\" (valueChange)=\"handleRadius($event)\"/>\n  </mat-slider>\n</div>\n\n<div class=\"setting\">\n  <label>Blur</label>\n  <mat-slider min=\"3\" max=\"25\" step=\"1\" discrete showTickMarks>\n    <input matSliderThumb [(value)]=\"blur\" (valueChange)=\"handleBlur($event)\"/>\n  </mat-slider>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.setting{display:flex;justify-content:center;align-items:center;gap:.25em}mat-slider{min-width:250px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(HeatmapComponent, { className: "HeatmapComponent", filePath: "lib/heatmap/heatmap.component.ts", lineNumber: 29 }); })();

class VectorTileMapComponent {
    constructor() {
        this.source = new OGCVectorTile({
            url: 'https://maps.gnosis.earth/ogcapi/collections/NaturalEarth:cultural:ne_10m_admin_0_countries/tiles/WebMercatorQuad',
            format: new MVT(),
        });
    }
    static { this.ɵfac = function VectorTileMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || VectorTileMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: VectorTileMapComponent, selectors: [["vector-tile-map"]], decls: 5, vars: 1, consts: [[3, "source"], ["defaultControls", ""], ["defaultInteractions", ""]], template: function VectorTileMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-vector-tile-layer", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(3, "ol-controls", 1)(4, "ol-interactions", 2);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
        } }, dependencies: [OlControlsDirective,
            OlDefaultControlDirective,
            OlDefaultInteractionDirective,
            OlInteractionsDirective,
            OlLayersDirective,
            OlMapComponent,
            OlVectorTileLayerDirective,
            SourceDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(VectorTileMapComponent, [{
        type: Component,
        args: [{ selector: 'vector-tile-map', imports: [
                    OlControlsDirective,
                    OlDefaultControlDirective,
                    OlDefaultInteractionDirective,
                    OlInteractionsDirective,
                    OlLayersDirective,
                    OlMapComponent,
                    OlVectorTileLayerDirective,
                    SourceDirective
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-vector-tile-layer [source]=\"source\" />\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n", styles: ["ol-map{display:block;width:100%;height:350px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(VectorTileMapComponent, { className: "VectorTileMapComponent", filePath: "lib/vector-tile-map/vector-tile-map.component.ts", lineNumber: 24 }); })();

const _c0$1 = ["randomPoints"];
class WebglLayerMapComponent {
    constructor() {
        this.#apiKey = inject(MAP_TILER_API_KEY);
        this.layer = viewChild(OlWebGLTileLayerDirective);
        this.zIndexDirective = viewChild('randomPoints', { read: ZIndexDirective });
        this.basePath = inject(EXAMPLES_BASE_PATH);
        this.#attributions = [
            '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> ' +
                '<a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
        ];
        this.randomPointUrl = `${this.basePath}/data/random-point.geojson`;
        this.kmlUrl = `${this.basePath}/data/random-polygon.kml`;
        this.polygonStyle = {
            'stroke-width': 1.25,
            'stroke-color': '#FFF'
        };
        this.source = new ImageTile({
            attributions: this.#attributions,
            url: `https://api.maptiler.com/maps/satellite/{z}/{x}/{y}.jpg?key=${this.#apiKey}`,
            tileSize: 512,
            maxZoom: 20,
        });
        this.zIndex = linkedSignal(() => this.zIndexDirective()?.zIndex() ?? 1);
        this.contrast = linkedSignal(() => this.layer()?.contrast() ?? 0);
        this.exposure = linkedSignal(() => this.layer()?.exposure() ?? 0);
        this.saturation = linkedSignal(() => this.layer()?.saturation() ?? 0);
        this.brightness = linkedSignal(() => this.layer()?.brightness() ?? 0);
        this.gamma = linkedSignal(() => this.layer()?.gamma() ?? 1);
    }
    #apiKey;
    #attributions;
    updateZIndex(evt) {
        this.zIndexDirective()?.zIndex.set(evt.value);
    }
    updateStyle(value, key) {
        const layer = this.layer();
        let styleSignal = layer?.contrast;
        switch (key) {
            case 'contrast':
                break;
            case 'exposure':
                styleSignal = layer?.exposure;
                break;
            case 'saturation':
                styleSignal = layer?.saturation;
                break;
            case 'brightness':
                styleSignal = layer?.brightness;
                break;
            case 'gamma':
                styleSignal = layer?.gamma;
                break;
        }
        styleSignal?.set(value);
    }
    static { this.ɵfac = function WebglLayerMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || WebglLayerMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: WebglLayerMapComponent, selectors: [["webgl-layer-map"]], viewQuery: function WebglLayerMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.layer, OlWebGLTileLayerDirective, 5);
            i0.ɵɵviewQuerySignal(ctx.zIndexDirective, _c0$1, 5, ZIndexDirective);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(2);
        } }, decls: 41, vars: 12, consts: [["randomPoints", ""], [3, "zIndexChange", "geojson", "zIndex"], [3, "source", "contrast"], ["extractStyles", "false", 3, "kml"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "settings"], ["name", "vector-layer-order", "aria-label", "Vector Layer Order", 3, "change"], ["value", "1", "checked", ""], ["value", "0"], ["min", "-1.0", "max", "1.0", "step", "0.1", "discrete", ""], ["matSliderThumb", "", 3, "valueChange", "value"], ["min", "0", "max", "100", "step", "0.1", "discrete", ""]], template: function WebglLayerMapComponent_Template(rf, ctx) { if (rf & 1) {
            const _r1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers")(2, "ol-vector-layer", 1, 0);
            i0.ɵɵtwoWayListener("zIndexChange", function WebglLayerMapComponent_Template_ol_vector_layer_zIndexChange_2_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.zIndex, $event) || (ctx.zIndex = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelement(4, "ol-webgl-tile-layer", 2)(5, "ol-vector-layer", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(6, "ol-controls", 4)(7, "ol-interactions", 5);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "div", 6)(9, "div");
            i0.ɵɵtext(10, " Random Points ZIndex:\u00A0 ");
            i0.ɵɵelementStart(11, "mat-button-toggle-group", 7);
            i0.ɵɵlistener("change", function WebglLayerMapComponent_Template_mat_button_toggle_group_change_11_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateZIndex($event)); });
            i0.ɵɵelementStart(12, "mat-button-toggle", 8);
            i0.ɵɵtext(13, "Front");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(14, "mat-button-toggle", 9);
            i0.ɵɵtext(15, "Back");
            i0.ɵɵelementEnd()()();
            i0.ɵɵelementStart(16, "div")(17, "label");
            i0.ɵɵtext(18, "Contrast:");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(19, "mat-slider", 10)(20, "input", 11);
            i0.ɵɵtwoWayListener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_20_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.contrast, $event) || (ctx.contrast = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_20_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateStyle($event, "contrast")); });
            i0.ɵɵelementEnd()()();
            i0.ɵɵelementStart(21, "div")(22, "label");
            i0.ɵɵtext(23, "Exposure:");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(24, "mat-slider", 10)(25, "input", 11);
            i0.ɵɵtwoWayListener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_25_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.exposure, $event) || (ctx.exposure = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_25_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateStyle($event, "exposure")); });
            i0.ɵɵelementEnd()()();
            i0.ɵɵelementStart(26, "div")(27, "label");
            i0.ɵɵtext(28, "Saturation:");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(29, "mat-slider", 10)(30, "input", 11);
            i0.ɵɵtwoWayListener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_30_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.saturation, $event) || (ctx.saturation = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_30_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateStyle($event, "saturation")); });
            i0.ɵɵelementEnd()()();
            i0.ɵɵelementStart(31, "div")(32, "label");
            i0.ɵɵtext(33, "Brightness:");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(34, "mat-slider", 10)(35, "input", 11);
            i0.ɵɵtwoWayListener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_35_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.brightness, $event) || (ctx.brightness = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_35_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateStyle($event, "brightness")); });
            i0.ɵɵelementEnd()()();
            i0.ɵɵelementStart(36, "div")(37, "label");
            i0.ɵɵtext(38, "Gamma:");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(39, "mat-slider", 12)(40, "input", 11);
            i0.ɵɵtwoWayListener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_40_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.gamma, $event) || (ctx.gamma = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function WebglLayerMapComponent_Template_input_valueChange_40_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateStyle($event, "gamma")); });
            i0.ɵɵelementEnd()()()();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("geojson", ctx.randomPointUrl);
            i0.ɵɵtwoWayProperty("zIndex", ctx.zIndex);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source)("contrast", ctx.contrast());
            i0.ɵɵadvance();
            i0.ɵɵstyleMap(ctx.polygonStyle);
            i0.ɵɵproperty("kml", ctx.kmlUrl);
            i0.ɵɵadvance(15);
            i0.ɵɵtwoWayProperty("value", ctx.contrast);
            i0.ɵɵadvance(5);
            i0.ɵɵtwoWayProperty("value", ctx.exposure);
            i0.ɵɵadvance(5);
            i0.ɵɵtwoWayProperty("value", ctx.saturation);
            i0.ɵɵadvance(5);
            i0.ɵɵtwoWayProperty("value", ctx.brightness);
            i0.ɵɵadvance(5);
            i0.ɵɵtwoWayProperty("value", ctx.gamma);
        } }, dependencies: [OlControlsDirective,
            OlDefaultControlDirective,
            OlDefaultInteractionDirective,
            OlInteractionsDirective,
            OlLayersDirective,
            OlMapComponent,
            OlWebGLTileLayerDirective,
            OlVectorLayerDirective,
            OlGeoJsonSourceDirective,
            OlKmlSourceDirective,
            SourceDirective,
            MatButtonToggleGroup,
            MatButtonToggle,
            MatSlider,
            MatSliderThumb], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{display:flex;margin:.5em;max-width:90%;justify-content:space-evenly;gap:.5em}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(WebglLayerMapComponent, [{
        type: Component,
        args: [{ selector: 'webgl-layer-map', imports: [
                    OlControlsDirective,
                    OlDefaultControlDirective,
                    OlDefaultInteractionDirective,
                    OlInteractionsDirective,
                    OlLayersDirective,
                    OlMapComponent,
                    OlWebGLTileLayerDirective,
                    OlVectorLayerDirective,
                    OlGeoJsonSourceDirective,
                    OlKmlSourceDirective,
                    SourceDirective,
                    MatButtonToggleGroup,
                    MatButtonToggle,
                    MatSlider,
                    MatSliderThumb
                ], template: "<ol-map>\n\n    <ol-layers>\n        <ol-vector-layer #randomPoints [geojson]=\"randomPointUrl\" [(zIndex)]=\"zIndex\"/>\n        <ol-webgl-tile-layer [source]=\"source\" [contrast]=\"contrast()\"/>\n        <ol-vector-layer [kml]=\"kmlUrl\" extractStyles=\"false\" [style]=\"polygonStyle\"/>\n    </ol-layers>\n\n    <ol-controls defaultControls/>\n    <ol-interactions defaultInteractions/>\n\n</ol-map>\n\n<div class=\"settings\">\n\n    <div>\n        Random Points ZIndex:&nbsp;\n        <mat-button-toggle-group name=\"vector-layer-order\" aria-label=\"Vector Layer Order\"\n                                 (change)=\"updateZIndex($event)\">\n            <mat-button-toggle value=\"1\" checked>Front</mat-button-toggle>\n            <mat-button-toggle value=\"0\">Back</mat-button-toggle>\n        </mat-button-toggle-group>\n\n    </div>\n\n    <div>\n        <label>Contrast:</label>\n        <mat-slider min=\"-1.0\" max=\"1.0\" step=\"0.1\" discrete>\n            <input matSliderThumb [(value)]=\"contrast\" (valueChange)=\"updateStyle($event, 'contrast')\" />\n        </mat-slider>\n    </div>\n\n    <div>\n        <label>Exposure:</label>\n        <mat-slider min=\"-1.0\" max=\"1.0\" step=\"0.1\" discrete>\n            <input matSliderThumb [(value)]=\"exposure\" (valueChange)=\"updateStyle($event, 'exposure')\" />\n        </mat-slider>\n    </div>\n\n    <div>\n        <label>Saturation:</label>\n        <mat-slider min=\"-1.0\" max=\"1.0\" step=\"0.1\" discrete>\n            <input matSliderThumb [(value)]=\"saturation\" (valueChange)=\"updateStyle($event, 'saturation')\" />\n        </mat-slider>\n    </div>\n\n    <div>\n        <label>Brightness:</label>\n        <mat-slider min=\"-1.0\" max=\"1.0\" step=\"0.1\" discrete>\n            <input matSliderThumb [(value)]=\"brightness\" (valueChange)=\"updateStyle($event, 'brightness')\" />\n        </mat-slider>\n    </div>\n\n    <div>\n        <label>Gamma:</label>\n        <mat-slider min=\"0\" max=\"100\" step=\"0.1\" discrete>\n            <input matSliderThumb [(value)]=\"gamma\" (valueChange)=\"updateStyle($event, 'gamma')\" />\n        </mat-slider>\n    </div>\n\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{display:flex;margin:.5em;max-width:90%;justify-content:space-evenly;gap:.5em}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(WebglLayerMapComponent, { className: "WebglLayerMapComponent", filePath: "lib/webgl-layer-map/webgl-layer-map.component.ts", lineNumber: 37 }); })();

class EsriFeatureMapComponent {
    constructor() {
        this.source = new OSM();
        this.center = fromLonLat([1.72, 52.4]);
        this.baseUrl = 'https://services-eu1.arcgis.com/NPIbx47lsIiu2pqz/ArcGIS/rest/services/' +
            'Neptune_Coastline_Campaign_Open_Data_Land_Use_2014/FeatureServer/';
        this.fillColors = {
            'Lost To Sea Since 1965': [0, 0, 0, 1],
            'Urban/Built-up': [104, 104, 104, 1],
            'Shacks': [115, 76, 0, 1],
            'Industry': [230, 0, 0, 1],
            'Wasteland': [230, 0, 0, 1],
            'Caravans': [0, 112, 255, 0.5],
            'Defence': [230, 152, 0, 0.5],
            'Transport': [230, 152, 0, 1],
            'Open Countryside': [255, 255, 115, 1],
            'Woodland': [38, 115, 0, 1],
            'Managed Recreation/Sport': [85, 255, 0, 1],
            'Amenity Water': [0, 112, 255, 1],
            'Inland Water': [0, 38, 115, 1],
        };
        this.style = new Style({
            fill: new Fill(),
            stroke: new Stroke({
                color: [0, 0, 0, 1],
                width: 0.5,
            }),
        });
        this.strategy = tile(createXYZ({
            tileSize: 512,
        }));
        this.attributions = 'University of Leicester (commissioned by the ' +
            '<a href="https://www.arcgis.com/home/item.html?id=' +
            'd5f05b1dc3dd4d76906c421bc1727805">National Trust</a>)';
        this.serviceUrl = EsriFeatureMapComponent.prototype._serviceUrl.bind(this);
        this.styleFn = EsriFeatureMapComponent.prototype._styleFn.bind(this);
    }
    _serviceUrl(extent, resolution, projection) {
        const srid = projection
            .getCode()
            .split(/:(?=\d+$)/)
            .pop();
        return this.baseUrl +
            '0/query/?f=json&' +
            'returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry=' +
            encodeURIComponent('{"xmin":' +
                extent[0] +
                ',"ymin":' +
                extent[1] +
                ',"xmax":' +
                extent[2] +
                ',"ymax":' +
                extent[3] +
                ',"spatialReference":{"wkid":' +
                srid +
                '}}') +
            '&geometryType=esriGeometryEnvelope&inSR=' +
            srid +
            '&outFields=*' +
            '&outSR=' +
            srid;
    }
    _styleFn(feature, resolution) {
        const classify = feature.get('LU_2014');
        const color = this.fillColors[classify] || [0, 0, 0, 0];
        this.style.getFill()?.setColor(color);
        return this.style;
    }
    static { this.ɵfac = function EsriFeatureMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || EsriFeatureMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: EsriFeatureMapComponent, selectors: [["esri-feature-map"]], decls: 6, vars: 8, consts: [[3, "center", "zoom"], [3, "source"], [3, "esrijson", "strategy", "attributions"], ["defaultControls", ""], ["defaultInteractions", ""]], template: function EsriFeatureMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "ol-map", 0)(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 1)(3, "ol-vector-layer", 2);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(4, "ol-controls", 3)(5, "ol-interactions", 4);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵproperty("center", ctx.center)("zoom", 10);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.source);
            i0.ɵɵadvance();
            i0.ɵɵstyleMap(ctx.styleFn);
            i0.ɵɵproperty("esrijson", ctx.serviceUrl)("strategy", ctx.strategy)("attributions", ctx.attributions);
        } }, dependencies: [OlControlsDirective,
            OlDefaultControlDirective,
            OlDefaultInteractionDirective,
            OlInteractionsDirective,
            OlLayersDirective,
            OlMapComponent,
            OlTileLayerDirective,
            SourceDirective,
            OlEsriJsonSourceDirective,
            OlVectorLayerDirective], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(EsriFeatureMapComponent, [{
        type: Component,
        args: [{ selector: 'esri-feature-map', imports: [
                    OlControlsDirective,
                    OlDefaultControlDirective,
                    OlDefaultInteractionDirective,
                    OlInteractionsDirective,
                    OlLayersDirective,
                    OlMapComponent,
                    OlTileLayerDirective,
                    SourceDirective,
                    OlEsriJsonSourceDirective,
                    OlVectorLayerDirective
                ], template: "<ol-map [center]=\"center\" [zoom]=\"10\">\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"source\" />\n    <ol-vector-layer [esrijson]=\"serviceUrl\" [strategy]=\"strategy\" [style]=\"styleFn\" [attributions]=\"attributions\"/>\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n", styles: ["ol-map{display:block;width:100%;height:350px}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(EsriFeatureMapComponent, { className: "EsriFeatureMapComponent", filePath: "lib/esri-feature-map/esri-feature-map.component.ts", lineNumber: 33 }); })();

const _c0 = ["topoJsonLayer"];
const _c1 = () => ["countries"];
class TopoJSONLayerMapComponent {
    constructor() {
        this.minMaxResolution = viewChild('topoJsonLayer', { read: MinMaxResolutionDirective });
        this.viewDirective = viewChild(OlViewDirective);
        this.minResolution = linkedSignal(() => this.minMaxResolution()?.minResolution());
        this.maxResolution = linkedSignal(() => this.minMaxResolution()?.maxResolution());
        this.#apiKey = inject(MAP_TILER_API_KEY);
        this.attributions = '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> ' +
            '<a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>';
        this.datavizDarSource = new ImageTile({
            attributions: this.attributions,
            url: `https://api.maptiler.com/maps/dataviz-dark/{z}/{x}/{y}.png?key=${this.#apiKey}`,
            tileSize: 512
        });
        this.style = {
            'stroke-color': 'white',
            'stroke-width': 1.5,
        };
    }
    #apiKey;
    updateMinResolution(value) {
        this.minMaxResolution()?.minResolution.set(value);
    }
    updateMaxResolution(value) {
        this.minMaxResolution()?.maxResolution.set(value);
    }
    static { this.ɵfac = function TopoJSONLayerMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || TopoJSONLayerMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: TopoJSONLayerMapComponent, selectors: [["topojson-layer-map"]], viewQuery: function TopoJSONLayerMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.minMaxResolution, _c0, 5, MinMaxResolutionDirective);
            i0.ɵɵviewQuerySignal(ctx.viewDirective, OlViewDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(2);
        } }, decls: 21, vars: 13, consts: [["topoJsonLayer", ""], [3, "source"], ["topojson", "https://openlayers.org/en/latest/examples/data/topojson/world-110m.json", "overlaps", "false", 3, "layers", "minResolution", "maxResolution"], ["defaultControls", ""], ["defaultInteractions", ""], [1, "settings"], ["for", "minResolution"], ["min", "100", "max", "25000", "step", "100", "discrete", ""], ["id", "minResolution", "matSliderStartThumb", "", 3, "valueChange", "value"], ["id", "maxResolution", "matSliderEndThumb", "", 3, "valueChange", "value"], ["for", "maxResolution"]], template: function TopoJSONLayerMapComponent_Template(rf, ctx) { if (rf & 1) {
            const _r1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "ol-map")(1, "ol-layers");
            i0.ɵɵelement(2, "ol-tile-layer", 1)(3, "ol-vector-layer", 2, 0);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(5, "ol-controls", 3)(6, "ol-interactions", 4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "div", 5)(8, "label", 6);
            i0.ɵɵtext(9, "Minimum Resolution");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(10, "mat-slider", 7)(11, "input", 8);
            i0.ɵɵtwoWayListener("valueChange", function TopoJSONLayerMapComponent_Template_input_valueChange_11_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.minResolution, $event) || (ctx.minResolution = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function TopoJSONLayerMapComponent_Template_input_valueChange_11_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateMinResolution($event)); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(12, "input", 9);
            i0.ɵɵtwoWayListener("valueChange", function TopoJSONLayerMapComponent_Template_input_valueChange_12_listener($event) { i0.ɵɵrestoreView(_r1); i0.ɵɵtwoWayBindingSet(ctx.maxResolution, $event) || (ctx.maxResolution = $event); return i0.ɵɵresetView($event); });
            i0.ɵɵlistener("valueChange", function TopoJSONLayerMapComponent_Template_input_valueChange_12_listener($event) { i0.ɵɵrestoreView(_r1); return i0.ɵɵresetView(ctx.updateMaxResolution($event)); });
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(13, "label", 10);
            i0.ɵɵtext(14, "Maximum Resolution");
            i0.ɵɵelementEnd()();
            i0.ɵɵelementStart(15, "div", 5)(16, "label");
            i0.ɵɵtext(17, "Current Resolution:");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(18, "p");
            i0.ɵɵtext(19);
            i0.ɵɵpipe(20, "number");
            i0.ɵɵelementEnd()();
        } if (rf & 2) {
            let tmp_8_0;
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("source", ctx.datavizDarSource);
            i0.ɵɵadvance();
            i0.ɵɵstyleMap(ctx.style);
            i0.ɵɵproperty("layers", i0.ɵɵpureFunction0(12, _c1))("minResolution", 5000)("maxResolution", 20000);
            i0.ɵɵadvance(8);
            i0.ɵɵtwoWayProperty("value", ctx.minResolution);
            i0.ɵɵadvance();
            i0.ɵɵtwoWayProperty("value", ctx.maxResolution);
            i0.ɵɵadvance(7);
            i0.ɵɵtextInterpolate(i0.ɵɵpipeBind2(20, 9, (tmp_8_0 = ctx.viewDirective()) == null ? null : tmp_8_0.resolution(), "5.1-1"));
        } }, dependencies: [OlControlsDirective,
            OlDefaultControlDirective,
            OlDefaultInteractionDirective,
            OlInteractionsDirective,
            OlLayersDirective,
            OlMapComponent,
            OlTileLayerDirective,
            SourceDirective,
            OlTopoJsonSource,
            OlVectorLayerDirective,
            MatSlider,
            MatSliderRangeThumb,
            DecimalPipe], styles: ["ol-map[_ngcontent-%COMP%]{display:block;width:100%;height:350px}.settings[_ngcontent-%COMP%]{display:flex;justify-content:center;align-items:center;gap:.5em;padding:.25em}mat-slider[_ngcontent-%COMP%]{min-width:25%}"] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(TopoJSONLayerMapComponent, [{
        type: Component,
        args: [{ selector: 'topojson-layer-map', imports: [
                    OlControlsDirective,
                    OlDefaultControlDirective,
                    OlDefaultInteractionDirective,
                    OlInteractionsDirective,
                    OlLayersDirective,
                    OlMapComponent,
                    OlTileLayerDirective,
                    SourceDirective,
                    OlTopoJsonSource,
                    OlVectorLayerDirective,
                    MatSlider,
                    MatSliderRangeThumb,
                    DecimalPipe
                ], template: "<ol-map>\n\n  <ol-layers>\n    <ol-tile-layer [source]=\"datavizDarSource\" />\n    <ol-vector-layer #topoJsonLayer topojson=\"https://openlayers.org/en/latest/examples/data/topojson/world-110m.json\"\n      overlaps=\"false\" [layers]=\"['countries']\" [style]=\"style\" [minResolution]=\"5000\" [maxResolution]=\"20000\"/>\n  </ol-layers>\n\n  <ol-controls defaultControls/>\n  <ol-interactions defaultInteractions/>\n\n</ol-map>\n\n<div class=\"settings\">\n  <label for=\"minResolution\">Minimum Resolution</label>\n  <mat-slider min=\"100\" max=\"25000\" step=\"100\" discrete>\n    <input id=\"minResolution\" [(value)]=\"minResolution\" matSliderStartThumb (valueChange)=\"updateMinResolution($event)\">\n    <input id=\"maxResolution\" [(value)]=\"maxResolution\" matSliderEndThumb  (valueChange)=\"updateMaxResolution($event)\">\n  </mat-slider>\n  <label for=\"maxResolution\">Maximum Resolution</label>\n</div>\n<div class=\"settings\">\n  <label>Current Resolution:</label>\n  <p>{{ viewDirective()?.resolution() | number : '5.1-1'}}</p>\n</div>\n", styles: ["ol-map{display:block;width:100%;height:350px}.settings{display:flex;justify-content:center;align-items:center;gap:.5em;padding:.25em}mat-slider{min-width:25%}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(TopoJSONLayerMapComponent, { className: "TopoJSONLayerMapComponent", filePath: "lib/topojson-layer-map/topojson-layer-map.component.ts", lineNumber: 39 }); })();

/*
 * Public API Surface of ngx-ol-examples
 */

/**
 * Generated bundle index. Do not edit.
 */

export { BasicMapComponent, BasicOverlayMapComponent, DragInteractionComponent, EXAMPLES_BASE_PATH, EsriFeatureMapComponent, ExampleComponent, ExampleService, FullScreenMapComponent, GraticuleMapComponent, HeatmapComponent, ImageLayerComponent, IsActivePipe, KeyboardInteractionComponent, LayerGroupComponent, MAP_TILER_API_KEY, MouseInteractionComponent, MousePositionMapComponent, OverviewMapComponent, ProjectionMapComponent, RotateMapComponent, ScaleLineMapComponent, SimplePopupComponent, TopoJSONLayerMapComponent, TransformCoordinateComponent, VectorTileMapComponent, WebglLayerMapComponent, ZoomToExtentComponent, provideExamples, provideMapTilerAPIKey };
//# sourceMappingURL=nasumilu-ngx-ol-examples.mjs.map
