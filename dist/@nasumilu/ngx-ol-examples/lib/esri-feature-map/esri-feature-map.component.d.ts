import { OSM } from "ol/source";
import { Style } from "ol/style";
import { Projection } from "ol/proj";
import { Extent } from "ol/extent";
import { FeatureLike } from "ol/Feature";
import * as i0 from "@angular/core";
export declare class EsriFeatureMapComponent {
    protected readonly source: OSM;
    protected center: import("ol/coordinate").Coordinate;
    private readonly baseUrl;
    private fillColors;
    protected style: Style;
    protected strategy: (arg0: import("ol/extent").Extent, arg1: number, arg2: import("ol/proj").Projection) => Array<import("ol/extent").Extent>;
    protected attributions: string;
    protected serviceUrl: (extent: Extent, resolution: number, projection: Projection) => string;
    protected styleFn: (feature: FeatureLike, resolution: number) => Style;
    private _serviceUrl;
    private _styleFn;
    static ɵfac: i0.ɵɵFactoryDeclaration<EsriFeatureMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<EsriFeatureMapComponent, "esri-feature-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=esri-feature-map.component.d.ts.map