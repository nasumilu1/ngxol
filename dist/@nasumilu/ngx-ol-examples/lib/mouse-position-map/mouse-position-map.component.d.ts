import { AfterContentInit } from '@angular/core';
import { OlMousePositionDirective } from "@nasumilu/ngx-ol-control";
import { Coordinate, CoordinateFormat } from "ol/coordinate";
import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export type PositionModel = {
    srid: string;
    label: string;
    format: string | CoordinateFormat;
    fractionDigits?: number;
};
export declare class MousePositionMapComponent implements AfterContentInit {
    protected readonly projections: PositionModel[];
    protected readonly source: OSM;
    protected readonly position: import("@angular/core").Signal<OlMousePositionDirective | undefined>;
    readonly projection: import("@angular/core").ModelSignal<PositionModel>;
    format(coordinate: Coordinate | undefined): string;
    ngAfterContentInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MousePositionMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MousePositionMapComponent, "mouse-position-map", never, { "projection": { "alias": "projection"; "required": false; "isSignal": true; }; }, { "projection": "projectionChange"; }, never, never, true, never>;
}
//# sourceMappingURL=mouse-position-map.component.d.ts.map