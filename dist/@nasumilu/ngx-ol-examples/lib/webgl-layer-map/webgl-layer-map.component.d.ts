import ImageTile from 'ol/source/ImageTile';
import { MatButtonToggleChange } from "@angular/material/button-toggle";
import { FlatStroke } from "ol/style/flat";
import * as i0 from "@angular/core";
export declare class WebglLayerMapComponent {
    #private;
    private readonly layer;
    private readonly zIndexDirective;
    private readonly basePath;
    protected readonly randomPointUrl: string;
    protected readonly kmlUrl: string;
    protected readonly polygonStyle: FlatStroke;
    protected readonly source: ImageTile;
    protected zIndex: import("@angular/core").WritableSignal<number>;
    protected readonly contrast: import("@angular/core").WritableSignal<number>;
    protected readonly exposure: import("@angular/core").WritableSignal<number>;
    protected readonly saturation: import("@angular/core").WritableSignal<number>;
    protected readonly brightness: import("@angular/core").WritableSignal<number>;
    protected readonly gamma: import("@angular/core").WritableSignal<number>;
    updateZIndex(evt: MatButtonToggleChange): void;
    updateStyle(value: number, key: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<WebglLayerMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<WebglLayerMapComponent, "webgl-layer-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=webgl-layer-map.component.d.ts.map