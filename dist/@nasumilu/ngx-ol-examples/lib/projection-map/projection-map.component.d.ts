import { OnInit } from '@angular/core';
import { OlViewDirective } from '@nasumilu/ngx-ol-map';
import { Feature } from "ol";
import { Point } from "ol/geom";
import { Coordinate } from "ol/coordinate";
import { ProjectionLike } from "ol/proj";
import { OSM } from "ol/source";
import * as i0 from "@angular/core";
/**
 * TransformCoordinateComponent is responsible for transforming geographic coordinates from one projection system
 * to another and displaying the transformed coordinates in the specified format. The component loads the necessary
 * projections and transforms the coordinates asynchronously, providing a loading spinner indicator while the
 * transformation is in progress.
 *
 * Inputs:
 * - `source`: The source projection defined as a required input (ProjectionLike). This is the projection in which
 *   the input coordinates are provided.
 * - `destination`: The destination projection defined as a required input (ProjectionLike). This is the projection
 *   to which the coordinates will be transformed.
 * - `center`: The input coordinate model, which is required and can be either a Coordinate, undefined, or null.
 *   This represents the geographic coordinate to be transformed.
 * - `tpl`: The string template used to format the transformed coordinate. Defaults to '{x}, {y}'.
 * - `factionDigits`: The number of decimal places to use when formatting the transformed coordinate. Defaults to 3.
 */
export declare class TransformCoordinateComponent implements OnInit {
    #private;
    protected readonly loaded: import("@angular/core").WritableSignal<boolean>;
    readonly sourceCrs: import("@angular/core").InputSignal<ProjectionLike>;
    readonly destinationCrs: import("@angular/core").InputSignal<ProjectionLike>;
    readonly center: import("@angular/core").ModelSignal<Coordinate | null | undefined>;
    readonly tpl: import("@angular/core").InputSignal<string>;
    readonly factionDigits: import("@angular/core").InputSignal<number>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TransformCoordinateComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TransformCoordinateComponent, "transform-coordinate", never, { "sourceCrs": { "alias": "sourceCrs"; "required": true; "isSignal": true; }; "destinationCrs": { "alias": "destinationCrs"; "required": true; "isSignal": true; }; "center": { "alias": "center"; "required": true; "isSignal": true; }; "tpl": { "alias": "tpl"; "required": false; "isSignal": true; }; "factionDigits": { "alias": "factionDigits"; "required": false; "isSignal": true; }; }, { "center": "centerChange"; }, never, ["*"], true, never>;
}
/**
 * ProjectionMapComponent is an Angular component designed for rendering and managing
 * a basic map interface using OpenLayers. This component integrates configurations such
 * as layers, controls, interactions, and coordinate transformations to establish a functional
 * map. It also supports integration with custom point-of-interest data for specific use cases.
 */
export declare class ProjectionMapComponent {
    protected readonly source: OSM;
    protected readonly features: Feature<Point>[];
    protected readonly viewDirective: import("@angular/core").Signal<OlViewDirective | undefined>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ProjectionMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ProjectionMapComponent, "basic-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=projection-map.component.d.ts.map