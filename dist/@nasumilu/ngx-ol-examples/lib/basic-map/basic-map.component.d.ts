import { OlViewDirective } from '@nasumilu/ngx-ol-map';
import { OSM } from 'ol/source';
import * as i0 from "@angular/core";
export declare class BasicMapComponent {
    protected readonly wkt = "Polygon ((31.1 -30.1, 8.1 -26.2, -0.4 -44.8, 42.8 -46.8, 31.1 -30.1))";
    protected readonly source: OSM;
    protected readonly viewDirective: import("@angular/core").Signal<OlViewDirective | undefined>;
    static ɵfac: i0.ɵɵFactoryDeclaration<BasicMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<BasicMapComponent, "basic-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=basic-map.component.d.ts.map