import { OlViewDirective } from "@nasumilu/ngx-ol-map";
import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class RotateMapComponent {
    protected readonly source: OSM;
    protected readonly viewDirective: import("@angular/core").Signal<OlViewDirective | undefined>;
    protected readonly degrees: import("@angular/core").WritableSignal<number>;
    rotationLabel(value: number): string;
    handleRotation(value: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<RotateMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<RotateMapComponent, "rotate-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=rotate-map.component.d.ts.map