import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class FullScreenMapComponent {
    protected readonly source: OSM;
    handleFullScreenChange(isFullScreen: boolean): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<FullScreenMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<FullScreenMapComponent, "full-screen-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=full-screen-map.component.d.ts.map