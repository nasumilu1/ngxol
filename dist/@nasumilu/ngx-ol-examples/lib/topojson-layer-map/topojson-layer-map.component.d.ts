import ImageTile from "ol/source/ImageTile";
import { OlViewDirective } from "@nasumilu/ngx-ol-map";
import { FlatStroke } from "ol/style/flat";
import * as i0 from "@angular/core";
export declare class TopoJSONLayerMapComponent {
    #private;
    private readonly minMaxResolution;
    protected readonly viewDirective: import("@angular/core").Signal<OlViewDirective | undefined>;
    protected readonly minResolution: import("@angular/core").WritableSignal<number | undefined>;
    protected readonly maxResolution: import("@angular/core").WritableSignal<number | undefined>;
    protected readonly attributions: string;
    protected readonly datavizDarSource: ImageTile;
    protected style: FlatStroke;
    updateMinResolution(value: number): void;
    updateMaxResolution(value: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TopoJSONLayerMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TopoJSONLayerMapComponent, "topojson-layer-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=topojson-layer-map.component.d.ts.map