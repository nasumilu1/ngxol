import { OGCVectorTile } from "ol/source";
import * as i0 from "@angular/core";
export declare class VectorTileMapComponent {
    protected readonly source: OGCVectorTile<import("ol/render/Feature").default>;
    static ɵfac: i0.ɵɵFactoryDeclaration<VectorTileMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<VectorTileMapComponent, "vector-tile-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=vector-tile-map.component.d.ts.map