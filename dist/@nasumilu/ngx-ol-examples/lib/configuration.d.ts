import { InjectionToken } from "@angular/core";
export declare const MAP_TILER_API_KEY: InjectionToken<string>;
export declare const EXAMPLES_BASE_PATH: InjectionToken<string>;
export declare function provideExamples(basePath: string): (import("@angular/core").EnvironmentProviders | {
    provide: InjectionToken<string>;
    useValue: string;
})[];
export declare function provideMapTilerAPIKey(apiKey: string): {
    provide: InjectionToken<string>;
    useValue: string;
};
//# sourceMappingURL=configuration.d.ts.map