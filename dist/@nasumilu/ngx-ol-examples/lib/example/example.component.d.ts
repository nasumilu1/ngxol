import { OnInit } from '@angular/core';
import { CodeExample } from "../example.service";
import * as i0 from "@angular/core";
export declare class ExampleComponent implements OnInit {
    protected title: import("@angular/core").ModelSignal<string | undefined>;
    private readonly exampleContainer;
    protected readonly codeExample: import("@angular/core").ModelSignal<CodeExample | undefined>;
    private readonly exampleService;
    private readonly route;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ExampleComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ExampleComponent, "example", never, { "title": { "alias": "title"; "required": false; "isSignal": true; }; "codeExample": { "alias": "codeExample"; "required": false; "isSignal": true; }; }, { "title": "titleChange"; "codeExample": "codeExampleChange"; }, never, never, true, never>;
}
//# sourceMappingURL=example.component.d.ts.map