import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class HeatmapComponent {
    private readonly heatmapLayer;
    protected radius: import("@angular/core").WritableSignal<number>;
    protected blur: import("@angular/core").WritableSignal<number>;
    protected readonly osmSource: OSM;
    handleRadius(value: number): void;
    handleBlur(value: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<HeatmapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<HeatmapComponent, "heatmap", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=heatmap.component.d.ts.map