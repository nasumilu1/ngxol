import { AfterContentInit } from '@angular/core';
import { OlScaleLineDirective } from "@nasumilu/ngx-ol-control";
import { Units } from "ol/control/ScaleLine";
import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class ScaleLineMapComponent implements AfterContentInit {
    protected readonly source: OSM;
    protected readonly scale: import("@angular/core").Signal<OlScaleLineDirective | undefined>;
    protected readonly unit: import("@angular/core").ModelSignal<Units>;
    ngAfterContentInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ScaleLineMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ScaleLineMapComponent, "scale-line-map", never, { "unit": { "alias": "unit"; "required": false; "isSignal": true; }; }, { "unit": "unitChange"; }, never, never, true, never>;
}
//# sourceMappingURL=scale-line-map.component.d.ts.map