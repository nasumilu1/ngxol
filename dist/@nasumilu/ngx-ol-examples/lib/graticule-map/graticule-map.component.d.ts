import { OlViewDirective } from '@nasumilu/ngx-ol-map';
import { MinMaxLayerZoomDirective } from '@nasumilu/ngx-ol-layer';
import { OSM } from "ol/source";
import { Extent } from "ol/extent";
import { FlatStyle } from "ol/style/flat";
import * as i0 from "@angular/core";
export declare class GraticuleMapComponent {
    protected readonly minMaxZoomDirective: import("@angular/core").Signal<MinMaxLayerZoomDirective | undefined>;
    protected readonly viewDirective: import("@angular/core").Signal<OlViewDirective | undefined>;
    protected readonly minZoom: import("@angular/core").WritableSignal<number | undefined>;
    protected readonly maxZoom: import("@angular/core").WritableSignal<number | undefined>;
    protected readonly extent: Extent;
    protected readonly center: import("ol/coordinate").Coordinate;
    protected readonly style: FlatStyle;
    protected readonly osmSource: OSM;
    updateMinZoom(value: number): void;
    updateMaxZoom(value: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<GraticuleMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<GraticuleMapComponent, "graticule-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=graticule-map.component.d.ts.map