import { AfterViewInit } from '@angular/core';
import { ImageWMS, OSM } from "ol/source";
import { Extent } from "ol/extent";
import * as i0 from "@angular/core";
export declare class ImageLayerComponent implements AfterViewInit {
    private readonly imageLayer;
    protected readonly extent: Extent;
    protected readonly center: import("ol/coordinate").Coordinate;
    protected readonly loaded: import("@angular/core").WritableSignal<boolean>;
    protected readonly osmSource: OSM;
    protected readonly imageSource: ImageWMS;
    ngAfterViewInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ImageLayerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ImageLayerComponent, "image-layer", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=image-layer.component.d.ts.map