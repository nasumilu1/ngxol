import { OSM } from "ol/source";
import { FlatCircle } from "ol/style/flat";
import { StyleFunction } from "ol/style/Style";
import * as i0 from "@angular/core";
export declare class LayerGroupComponent {
    private readonly basePath;
    private readonly visibleDirective;
    readonly visible: import("@angular/core").WritableSignal<boolean>;
    protected readonly osmSource: OSM;
    protected readonly pointStyle: FlatCircle;
    protected readonly randomPointUrl: string;
    protected readonly lineStyle: StyleFunction;
    protected readonly randomLineUrl: string;
    toggleGroupVisibility(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<LayerGroupComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<LayerGroupComponent, "layer-group", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=layer-group.component.d.ts.map