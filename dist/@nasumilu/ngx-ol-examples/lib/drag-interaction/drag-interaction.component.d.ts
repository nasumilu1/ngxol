import { OnInit } from '@angular/core';
import { Coordinate } from "ol/coordinate";
import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class DragInteractionComponent implements OnInit {
    private readonly dragZoom;
    private readonly dragPan;
    private readonly dragRotate;
    private readonly dragBox;
    protected readonly source: OSM;
    protected readonly dragZoomActive: import("@angular/core").WritableSignal<boolean | undefined>;
    protected readonly dragPanActive: import("@angular/core").WritableSignal<boolean | undefined>;
    protected readonly dragRotateActive: import("@angular/core").WritableSignal<boolean | undefined>;
    protected readonly boxStart: import("@angular/core").WritableSignal<Coordinate | undefined>;
    protected readonly boxEnd: import("@angular/core").WritableSignal<Coordinate | undefined>;
    toggleDragZoom(): void;
    toggleDragPan(): void;
    toggleDragRotate(): void;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DragInteractionComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DragInteractionComponent, "drag-interaction", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=drag-interaction.component.d.ts.map