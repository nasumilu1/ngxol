import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class ZoomToExtentComponent {
    protected readonly source: OSM;
    protected readonly extent: number[];
    static ɵfac: i0.ɵɵFactoryDeclaration<ZoomToExtentComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ZoomToExtentComponent, "zoom-to-extent", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=zoom-to-extent.component.d.ts.map