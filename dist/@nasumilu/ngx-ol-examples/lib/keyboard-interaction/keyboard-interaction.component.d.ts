import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class IsActivePipe {
    transform(value: boolean | undefined | unknown): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<IsActivePipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<IsActivePipe, "isActive", true>;
}
export declare class KeyboardInteractionComponent {
    private readonly keyboardZoom;
    private readonly keyboardPan;
    protected readonly source: OSM;
    protected readonly keyboardZoomActive: import("@angular/core").WritableSignal<boolean | undefined>;
    protected readonly keyboardPanActive: import("@angular/core").WritableSignal<boolean | undefined>;
    toggleKeyboardZoom(): void;
    toggleKeyboardPan(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<KeyboardInteractionComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<KeyboardInteractionComponent, "keyboard-zoom", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=keyboard-interaction.component.d.ts.map