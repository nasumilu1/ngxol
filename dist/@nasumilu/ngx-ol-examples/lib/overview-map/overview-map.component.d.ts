import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class OverviewMapComponent {
    protected readonly source: OSM;
    static ɵfac: i0.ɵɵFactoryDeclaration<OverviewMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<OverviewMapComponent, "overview-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=overview-map.component.d.ts.map