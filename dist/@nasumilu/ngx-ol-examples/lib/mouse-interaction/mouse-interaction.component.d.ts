import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class MouseInteractionComponent {
    private readonly mouseWheelZoom;
    private readonly dblClickZoom;
    private readonly dragPan;
    protected readonly source: OSM;
    protected readonly mouseWheelZoomActive: import("@angular/core").WritableSignal<boolean | undefined>;
    protected readonly dblClickZoomActive: import("@angular/core").WritableSignal<boolean | undefined>;
    protected readonly dragPanActive: import("@angular/core").WritableSignal<boolean | undefined>;
    toggleMouseWheelZoom(): void;
    toggleDblClickZoom(): void;
    toggleDragPan(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MouseInteractionComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MouseInteractionComponent, "mouse-interaction", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=mouse-interaction.component.d.ts.map