import { SafeHtml } from '@angular/platform-browser';
import { Observable } from "rxjs";
import * as i0 from "@angular/core";
export type CodeExample = {
    code: SafeHtml;
    markup: SafeHtml;
    style: SafeHtml;
};
export declare class ExampleService {
    private readonly basePath;
    private readonly httpClient;
    private readonly sanitizer;
    code(name: string): Observable<string>;
    markup(name: string): Observable<string>;
    style(name: string): Observable<string>;
    example(name: string): Observable<CodeExample>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ExampleService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<ExampleService>;
}
//# sourceMappingURL=example.service.d.ts.map