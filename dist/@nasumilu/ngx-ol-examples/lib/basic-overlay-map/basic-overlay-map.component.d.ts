import { OlMapComponent, OlOverlay } from '@nasumilu/ngx-ol-map';
import { Overlay } from "ol";
import { Coordinate } from "ol/coordinate";
import { OSM } from "ol/source";
import * as i0 from "@angular/core";
export declare class SimplePopupComponent extends OlOverlay {
    private readonly ele;
    readonly overlay: import("@angular/core").Signal<Overlay>;
    protected readonly content: import("@angular/core").WritableSignal<string>;
    close(evt: MouseEvent): boolean;
    show(coordinate: Coordinate): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SimplePopupComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<SimplePopupComponent, "simple-popup", never, {}, {}, never, never, true, never>;
}
export declare class BasicOverlayMapComponent {
    protected readonly source: OSM;
    protected readonly map: import("@angular/core").Signal<OlMapComponent | undefined>;
    protected readonly popup: import("@angular/core").Signal<SimplePopupComponent | undefined>;
    static ɵfac: i0.ɵɵFactoryDeclaration<BasicOverlayMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<BasicOverlayMapComponent, "basic-overlay-map", never, {}, {}, never, never, true, never>;
}
//# sourceMappingURL=basic-overlay-map.component.d.ts.map