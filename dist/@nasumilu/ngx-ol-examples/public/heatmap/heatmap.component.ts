import {Component, linkedSignal, viewChild} from '@angular/core';
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {OlHeatmapLayerDirective, OlTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {OSM} from "ol/source";
import {MatSlider, MatSliderThumb} from "@angular/material/slider";
import { OlKmlSourceDirective } from '@nasumilu/ngx-ol-source';

@Component({
  selector: 'heatmap',
  imports: [
    OlControlsDirective,
    OlDefaultControlDirective,
    OlDefaultInteractionDirective,
    OlInteractionsDirective,
    OlLayersDirective,
    OlMapComponent,
    OlTileLayerDirective,
    OlHeatmapLayerDirective,
    SourceDirective,
    MatSlider,
    MatSliderThumb,
    OlKmlSourceDirective
  ],
  templateUrl: './heatmap.component.html',
  styleUrl: './heatmap.component.css'
})
export class HeatmapComponent {

  private readonly heatmapLayer = viewChild(OlHeatmapLayerDirective);

  protected radius = linkedSignal(() => this.heatmapLayer()?.radius() ?? 10);
  protected blur = linkedSignal(() => this.heatmapLayer()?.blur() ?? 10);

  protected readonly osmSource = new OSM();

  handleRadius(value: number) {
    this.heatmapLayer()?.radius.set(value);
  }

  handleBlur(value: number) {
    this.heatmapLayer()?.blur.set(value);
  }

}
