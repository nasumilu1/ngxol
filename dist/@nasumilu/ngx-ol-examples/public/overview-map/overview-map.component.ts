import {Component} from '@angular/core';
import {OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective} from '@nasumilu/ngx-ol-map';
import {
  OlOverviewMapDirective,
  OlDefaultControlDirective
} from "@nasumilu/ngx-ol-control";
import {OlTileLayerDirective, SourceDirective} from '@nasumilu/ngx-ol-layer';
import {OlDefaultInteractionDirective} from '@nasumilu/ngx-ol-interaction';
import {OSM} from "ol/source";


@Component({
  selector: 'overview-map',
  imports: [OlMapComponent, OlLayersDirective, OlControlsDirective,
    OlOverviewMapDirective, OlTileLayerDirective, OlInteractionsDirective,
    OlDefaultInteractionDirective, OlDefaultControlDirective, SourceDirective
  ],
  templateUrl: './overview-map.component.html',
  styleUrl: './overview-map.component.css'
})
export class OverviewMapComponent {

  protected readonly source = new OSM();

}
