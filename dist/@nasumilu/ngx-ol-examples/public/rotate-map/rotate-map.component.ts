import {ChangeDetectionStrategy, Component, linkedSignal, viewChild} from '@angular/core';
import {OlAttributionDirective, OlRotateDirective, OlZoomControlDirective} from "@nasumilu/ngx-ol-control";
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent, OlViewDirective} from "@nasumilu/ngx-ol-map";
import {OlMouseWheelZoomDirective, OlDragRotateDirective} from "@nasumilu/ngx-ol-interaction";
import {OlTileLayerDirective, SourceDirective} from '@nasumilu/ngx-ol-layer';
import {MatSlider, MatSliderThumb} from "@angular/material/slider";
import {OSM} from "ol/source";

@Component({
  selector: 'rotate-map',
  imports: [
    OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
    OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
    OlRotateDirective, MatSlider, MatSliderThumb, OlDragRotateDirective, SourceDirective
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './rotate-map.component.html',
  styleUrl: './rotate-map.component.css'
})
export class RotateMapComponent {

  protected readonly source = new OSM();
  protected readonly viewDirective = viewChild(OlViewDirective);

  protected readonly degrees = linkedSignal(() => {
    const radians = this.viewDirective()?.rotation() ?? 0;
    return radians * 180 / Math.PI % 360;
  });

  rotationLabel(value: number): string {
    return `${value}°`;
  }

  handleRotation(value: number): void {
    const degrees = value * Math.PI / 180;
    this.viewDirective()?.rotation.set(degrees);
  }

}
