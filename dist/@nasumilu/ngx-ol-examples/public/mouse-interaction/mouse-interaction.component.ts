import {Component, linkedSignal, viewChild} from '@angular/core';
import {IsActivePipe} from "../keyboard-interaction/keyboard-interaction.component";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {OlAttributionDirective, OlZoomControlDirective} from "@nasumilu/ngx-ol-control";
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {
  OlDblClickZoomDirective, OlDragPanDirective,
  OlMouseWheelZoomDirective
} from "@nasumilu/ngx-ol-interaction";
import {OlTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {OSM} from "ol/source";

@Component({
  selector: 'mouse-interaction',
  imports: [IsActivePipe, MatSlideToggle, OlAttributionDirective, OlControlsDirective, OlInteractionsDirective,
    OlLayersDirective, OlMapComponent, OlZoomControlDirective, OlTileLayerDirective,
    OlMouseWheelZoomDirective, OlDblClickZoomDirective, OlDragPanDirective, SourceDirective
  ],
  templateUrl: './mouse-interaction.component.html',
  styleUrl: './mouse-interaction.component.css'
})
export class MouseInteractionComponent {

  private readonly mouseWheelZoom = viewChild(OlMouseWheelZoomDirective);
  private readonly dblClickZoom = viewChild(OlDblClickZoomDirective);
  private readonly dragPan = viewChild(OlDragPanDirective);

  protected readonly source = new OSM();
  protected readonly mouseWheelZoomActive = linkedSignal(() => this.mouseWheelZoom()?.active());
  protected readonly dblClickZoomActive = linkedSignal(() => this.dblClickZoom()?.active());
  protected readonly dragPanActive = linkedSignal(() => this.dragPan()?.active());

  toggleMouseWheelZoom(): void {
    this.mouseWheelZoom()?.active.update(value => !value);
  }

  toggleDblClickZoom(): void {
    this.dblClickZoom()?.active.update(value => !value);
  }

  toggleDragPan(): void {
    this.dragPan()?.active.update(value => !value);
  }

}
