import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
  model, OnInit,
  signal,
  viewChild
} from '@angular/core';
import {
  OlControlsDirective, OlInteractionsDirective,
  OlLayersDirective, OlMapComponent,
  OlViewDirective, OlCoordinatePipe,
  OlTransformPipe,
  Proj4Service
} from '@nasumilu/ngx-ol-map';
import {OlDefaultControlDirective} from '@nasumilu/ngx-ol-control';
import {OlDefaultInteractionDirective,} from '@nasumilu/ngx-ol-interaction';
import {OlTileLayerDirective, OlVectorLayerDirective, SourceDirective} from '@nasumilu/ngx-ol-layer';
import VectorSource from "ol/source/Vector";
import {Feature} from "ol";
import {Point} from "ol/geom";
import {AsyncPipe} from "@angular/common";
import {Coordinate} from "ol/coordinate";
import {ProjectionLike} from "ol/proj";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {first, forkJoin} from "rxjs";
import {OSM} from "ol/source";
import { OlFeatureSourceDirective } from '@nasumilu/ngx-ol-source';

/**
 * TransformCoordinateComponent is responsible for transforming geographic coordinates from one projection system
 * to another and displaying the transformed coordinates in the specified format. The component loads the necessary
 * projections and transforms the coordinates asynchronously, providing a loading spinner indicator while the
 * transformation is in progress.
 *
 * Inputs:
 * - `source`: The source projection defined as a required input (ProjectionLike). This is the projection in which
 *   the input coordinates are provided.
 * - `destination`: The destination projection defined as a required input (ProjectionLike). This is the projection
 *   to which the coordinates will be transformed.
 * - `center`: The input coordinate model, which is required and can be either a Coordinate, undefined, or null.
 *   This represents the geographic coordinate to be transformed.
 * - `tpl`: The string template used to format the transformed coordinate. Defaults to '{x}, {y}'.
 * - `factionDigits`: The number of decimal places to use when formatting the transformed coordinate. Defaults to 3.
 */
@Component({
  selector: 'transform-coordinate',
  imports: [
    OlTransformPipe, OlCoordinatePipe, AsyncPipe, MatProgressSpinner
  ],
  template: `
    <div class="transform-value ">
      <div>
        <ng-content/>
      </div>
      @defer (when loaded()) {
        <div>{{ center() | transform:sourceCrs():destinationCrs() | async | coordinate:'format':factionDigits():tpl() }}</div>
      } @placeholder (minimum 500ms) {
        <mat-spinner diameter="15"/>
      }
    </div>
  `,
  styles: `
    .transform-value {
      display: flex;
      justify-content: space-between;
      gap: 0.25em;
    }

    .transform-value:first-child {
      text-align: end;
    }
  `
})
export class TransformCoordinateComponent implements OnInit {

  readonly #projService = inject(Proj4Service);
  protected readonly loaded = signal(false);

  public readonly sourceCrs = input.required<ProjectionLike>();
  public readonly destinationCrs = input.required<ProjectionLike>();
  public readonly center = model.required<Coordinate | undefined | null>();
  public readonly tpl = input<string>('{x}, {y}');
  public readonly factionDigits = input<number>(3);

  ngOnInit() {
    forkJoin([this.#projService.getProjection(this.sourceCrs()), this.#projService.getProjection(this.destinationCrs())])
      .pipe(first())
      .subscribe(() => this.loaded.set(true));
  }
}


/**
 * ProjectionMapComponent is an Angular component designed for rendering and managing
 * a basic map interface using OpenLayers. This component integrates configurations such
 * as layers, controls, interactions, and coordinate transformations to establish a functional
 * map. It also supports integration with custom point-of-interest data for specific use cases.
 */
@Component({
  selector: 'basic-map',
  imports: [
    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlTileLayerDirective,
    OlDefaultInteractionDirective, OlDefaultControlDirective, TransformCoordinateComponent, OlVectorLayerDirective,
    SourceDirective, OlFeatureSourceDirective
  ],
  templateUrl: 'projection-map.component.html',
  styleUrl: 'projection-map.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectionMapComponent {

  protected readonly source = new OSM();
  protected readonly features = [
      new Feature({geometry: new Point([2508336,339113])}),
      new Feature({geometry: new Point([1596539,527761])})
];

  protected readonly viewDirective = viewChild(OlViewDirective);

}
