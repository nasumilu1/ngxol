import {Component, linkedSignal, Pipe, viewChild} from '@angular/core';
import {OlKeyboardPanDirective, OlKeyboardZoomDirective} from "@nasumilu/ngx-ol-interaction";
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent } from '@nasumilu/ngx-ol-map';
import {OlAttributionDirective, OlZoomControlDirective } from '@nasumilu/ngx-ol-control';
import {OlTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {OSM} from "ol/source";

@Pipe({name: 'isActive'})
export class IsActivePipe {

  transform(value: boolean | undefined | unknown): string {
    return value ? 'Active' : 'Inactive';
  }
}

@Component({
  selector: 'keyboard-zoom',
  imports: [
    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlKeyboardPanDirective,
    OlKeyboardZoomDirective, OlZoomControlDirective, OlAttributionDirective, OlTileLayerDirective, MatSlideToggle,
    IsActivePipe, SourceDirective
  ],
  templateUrl: './keyboard-interaction.component.html',
  styleUrl: './keyboard-interaction.component.css'
})
export class KeyboardInteractionComponent {

  private readonly keyboardZoom = viewChild(OlKeyboardZoomDirective);
  private readonly keyboardPan = viewChild(OlKeyboardPanDirective);

  protected readonly source = new OSM();
  protected readonly keyboardZoomActive = linkedSignal(() => this.keyboardZoom()?.active());
  protected readonly keyboardPanActive = linkedSignal(() => this.keyboardPan()?.active());

  toggleKeyboardZoom(): void {
    this.keyboardZoom()?.active.update(value => !value);
  }

  toggleKeyboardPan(): void {
    this.keyboardPan()?.active.update(value => !value);
  }

}
