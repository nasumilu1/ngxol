import {AfterContentInit, ChangeDetectionStrategy, Component, model, viewChild} from '@angular/core';
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from '@nasumilu/ngx-ol-map';
import {OlScaleLineDirective, OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlTileLayerDirective, SourceDirective} from '@nasumilu/ngx-ol-layer';
import {OlDefaultInteractionDirective} from '@nasumilu/ngx-ol-interaction';
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatOption} from "@angular/material/core";
import {MatSelect} from "@angular/material/select";
import {Units} from "ol/control/ScaleLine";
import {OSM} from "ol/source";

@Component({
  selector: 'scale-line-map',
  imports: [
    OlMapComponent, OlLayersDirective, OlControlsDirective,
    OlTileLayerDirective, OlScaleLineDirective, OlDefaultInteractionDirective, OlDefaultControlDirective,
    MatFormField, MatSelect, MatLabel, MatOption, OlInteractionsDirective, SourceDirective
  ],
  templateUrl: './scale-line-map.component.html',
  styleUrl: './scale-line-map.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScaleLineMapComponent implements AfterContentInit {

  protected readonly source = new OSM();
  protected readonly scale = viewChild(OlScaleLineDirective);
  protected readonly unit = model<Units>('metric');

  ngAfterContentInit() {
    this.unit.subscribe(unit => this.scale()?.units.set(unit));
  }


}
