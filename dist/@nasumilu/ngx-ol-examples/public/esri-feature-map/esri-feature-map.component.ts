import {Component} from '@angular/core';
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {OlTileLayerDirective, OlVectorLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {OlEsriJsonSourceDirective} from "@nasumilu/ngx-ol-source";
import {OSM} from "ol/source";
import {Fill, Style} from "ol/style";
import Stroke from "ol/style/Stroke";
import {fromLonLat, Projection} from "ol/proj";
import {Extent} from "ol/extent";
import {createXYZ} from "ol/tilegrid";
import {tile as tileStrategy} from 'ol/loadingstrategy';
import {FeatureLike} from "ol/Feature";

@Component({
  selector: 'esri-feature-map',
  imports: [
    OlControlsDirective,
    OlDefaultControlDirective,
    OlDefaultInteractionDirective,
    OlInteractionsDirective,
    OlLayersDirective,
    OlMapComponent,
    OlTileLayerDirective,
    SourceDirective,
    OlEsriJsonSourceDirective,
    OlVectorLayerDirective
  ],
  templateUrl: './esri-feature-map.component.html',
  styleUrl: './esri-feature-map.component.css'
})
export class EsriFeatureMapComponent {

  protected readonly source = new OSM();

  protected center = fromLonLat([1.72, 52.4]);

  private readonly baseUrl =
    'https://services-eu1.arcgis.com/NPIbx47lsIiu2pqz/ArcGIS/rest/services/' +
    'Neptune_Coastline_Campaign_Open_Data_Land_Use_2014/FeatureServer/';

  private fillColors: {[key: string]: number[]} = {
    'Lost To Sea Since 1965': [0, 0, 0, 1],
    'Urban/Built-up': [104, 104, 104, 1],
    'Shacks': [115, 76, 0, 1],
    'Industry': [230, 0, 0, 1],
    'Wasteland': [230, 0, 0, 1],
    'Caravans': [0, 112, 255, 0.5],
    'Defence': [230, 152, 0, 0.5],
    'Transport': [230, 152, 0, 1],
    'Open Countryside': [255, 255, 115, 1],
    'Woodland': [38, 115, 0, 1],
    'Managed Recreation/Sport': [85, 255, 0, 1],
    'Amenity Water': [0, 112, 255, 1],
    'Inland Water': [0, 38, 115, 1],
  };

  protected style = new Style({
    fill: new Fill(),
    stroke: new Stroke({
      color: [0, 0, 0, 1],
      width: 0.5,
    }),
  });

  protected strategy = tileStrategy(
    createXYZ({
      tileSize: 512,
    })
  );

  protected attributions = 'University of Leicester (commissioned by the ' +
    '<a href="https://www.arcgis.com/home/item.html?id=' +
    'd5f05b1dc3dd4d76906c421bc1727805">National Trust</a>)';

  protected serviceUrl = EsriFeatureMapComponent.prototype._serviceUrl.bind(this);
  protected styleFn = EsriFeatureMapComponent.prototype._styleFn.bind(this);
  private _serviceUrl(extent: Extent, resolution: number, projection: Projection) {
    const srid = projection
      .getCode()
      .split(/:(?=\d+$)/)
      .pop();

    return this.baseUrl +
      '0/query/?f=json&' +
      'returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry=' +
      encodeURIComponent(
        '{"xmin":' +
        extent[0] +
        ',"ymin":' +
        extent[1] +
        ',"xmax":' +
        extent[2] +
        ',"ymax":' +
        extent[3] +
        ',"spatialReference":{"wkid":' +
        srid +
        '}}',
      ) +
      '&geometryType=esriGeometryEnvelope&inSR=' +
      srid +
      '&outFields=*' +
      '&outSR=' +
      srid;
  }

  private _styleFn(feature: FeatureLike, resolution: number): Style {
      const classify = feature.get('LU_2014');
      const color = this.fillColors[classify] || [0, 0, 0, 0];
      this.style.getFill()?.setColor(color);
      return this.style;
  }

}
