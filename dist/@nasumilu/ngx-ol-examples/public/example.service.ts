import {inject, Injectable} from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {HttpClient} from "@angular/common/http";
import {forkJoin, map, Observable, of, switchMap} from "rxjs";
import {EXAMPLES_BASE_PATH} from "./configuration";
import hljs from 'highlight.js';
import typescript from 'highlight.js/lib/languages/typescript';

hljs.registerLanguage('typescript', typescript);


export type CodeExample = {
  code: SafeHtml;
  markup: SafeHtml;
  style: SafeHtml;
};

@Injectable({
  providedIn: 'root'
})
export class ExampleService {

  private readonly basePath = of(inject(EXAMPLES_BASE_PATH));
  private readonly httpClient = inject(HttpClient);
  private readonly sanitizer = inject(DomSanitizer);

  code(name: string): Observable<string> {
    return this.basePath.pipe(
      switchMap(basePath =>
        this.httpClient.get(`${basePath}/${name}/${name}.component.ts`, {responseType: 'text'}))
    );
  }

  markup(name: string): Observable<string> {
    return this.basePath.pipe(
      switchMap(basePath =>
        this.httpClient.get(`${basePath}/${name}/${name}.component.html`, {responseType: 'text'}))
    )
  }

  style(name: string): Observable<string> {
    return this.basePath.pipe(
      switchMap(basePath =>
        this.httpClient.get(`${basePath}/${name}/${name}.component.css`, {responseType: 'text'})
      )
    );
  }

  example(name: string): Observable<CodeExample> {
    return forkJoin([this.code(name), this.markup(name), this.style(name)]).pipe(
      map(([code, markup, style]) => ({
        code: this.sanitizer.bypassSecurityTrustHtml(hljs.highlight(code, {language: 'typescript'}).value),
        markup: this.sanitizer.bypassSecurityTrustHtml(hljs.highlight(markup, {language: 'html'}).value),
        style: this.sanitizer.bypassSecurityTrustHtml(hljs.highlight(style, {language: 'css'}).value)
      }))
    );
  }

}
