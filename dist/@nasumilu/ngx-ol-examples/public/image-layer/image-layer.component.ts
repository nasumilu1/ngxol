import {AfterViewInit, Component, signal, viewChild} from '@angular/core';
import {ImageWMS, OSM} from "ol/source";
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {ExtentDirective, OlImageLayerDirective, OlTileLayerDirective, SourceDirective} from '@nasumilu/ngx-ol-layer';
import {Extent, getCenter} from "ol/extent";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {MatIcon} from "@angular/material/icon";

@Component({
  selector: 'image-layer',
  imports: [
    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective,
    OlDefaultInteractionDirective, OlDefaultControlDirective,
    OlImageLayerDirective, OlTileLayerDirective, SourceDirective, ExtentDirective,
    MatProgressSpinner, MatIcon
  ],
  templateUrl: './image-layer.component.html',
  styleUrl: './image-layer.component.css'
})
export class ImageLayerComponent implements AfterViewInit {

  private readonly imageLayer = viewChild(OlImageLayerDirective);
  protected readonly extent: Extent = [-13884991, 2870341, -7455066, 6338219];
  protected readonly center = getCenter(this.extent);

  protected readonly loaded = signal(false);

  protected readonly osmSource = new OSM();
  protected readonly imageSource = new ImageWMS({
    url: 'https://ahocevar.com/geoserver/wms',
    params: {'LAYERS': 'topp:states'},
    ratio: 1,
    serverType: 'geoserver',
  });

  ngAfterViewInit() {
    this.imageLayer()?.postRender.subscribe(() => this.loaded.set(true));
  }

}
