import {Component, inject, model, OnInit, viewChild, ViewContainerRef} from '@angular/core';
import {MatTab, MatTabGroup} from "@angular/material/tabs";
import {CodeExample, ExampleService} from "../example.service";
import {ActivatedRoute} from "@angular/router";
import {switchMap, tap} from "rxjs";

@Component({
  selector: 'example',
    imports: [
        MatTab,
        MatTabGroup
    ],
  templateUrl: './example.component.html',
  styleUrl: './example.component.css'
})
export class ExampleComponent implements OnInit {

  protected title = model<string>();
  private readonly exampleContainer = viewChild('exampleContainer', {read: ViewContainerRef});
  protected readonly codeExample = model<CodeExample>();
  private readonly exampleService = inject(ExampleService);
  private readonly route = inject(ActivatedRoute);

  ngOnInit() {
    this.route.title.subscribe(title => this.title.set(title));
    this.route.data.pipe(
      tap(data => this.exampleContainer()?.createComponent(data['component'])),
      switchMap(data => this.exampleService.example(data['example']))
    ).subscribe(example => this.codeExample.set(example));
  }
}
