import {
  ChangeDetectionStrategy,
  Component,
  inject,
  linkedSignal,
  viewChild
} from '@angular/core';
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from '@nasumilu/ngx-ol-map';
import {OSM} from "ol/source";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {OlGeoJsonSourceDirective} from "@nasumilu/ngx-ol-source";
import {
  OlGroupLayerDirective, OlTileLayerDirective,
  OlVectorLayerDirective, VisibleDirective,
  SourceDirective
} from '@nasumilu/ngx-ol-layer';
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {IsActivePipe} from "../keyboard-interaction/keyboard-interaction.component";
import {EXAMPLES_BASE_PATH} from "../configuration";
import {FlatCircle} from "ol/style/flat";
import {Style} from "ol/style";
import Stroke from "ol/style/Stroke";
import {StyleFunction} from "ol/style/Style";

@Component({
  selector: 'layer-group',
  imports: [
    OlMapComponent, OlLayersDirective,
    OlTileLayerDirective, OlVectorLayerDirective, OlGroupLayerDirective,
    OlControlsDirective, OlDefaultControlDirective,
    OlInteractionsDirective, OlDefaultInteractionDirective,
    MatSlideToggle, IsActivePipe, SourceDirective,
    OlGeoJsonSourceDirective
  ],
  templateUrl: './layer-group.component.html',
  styleUrl: './layer-group.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayerGroupComponent {

  private readonly basePath = inject(EXAMPLES_BASE_PATH);
  private readonly visibleDirective = viewChild('testGroup', {read: VisibleDirective});

  public readonly visible = linkedSignal(() => this.visibleDirective()?.visible() ?? false);

  protected readonly osmSource = new OSM();

  protected readonly pointStyle: FlatCircle = {
    'circle-radius': 6,
    'circle-fill-color': '#3366997F',
    'circle-stroke-color': '#333',
    'circle-stroke-width': 1.25
  };

  protected readonly randomPointUrl = `${this.basePath}/data/random-point.geojson`;

  protected readonly lineStyle: StyleFunction = feature => new Style({
    stroke: new Stroke({width: 2, color: feature.get('color')})
  });

  protected readonly randomLineUrl = `${this.basePath}/data/random-lines.geojson`;

  toggleGroupVisibility(): void {
    this.visibleDirective()?.visible.update(value => !value);
  }

}
