import {AfterViewInit, Component, inject, linkedSignal, viewChild} from '@angular/core';
import {MAP_TILER_API_KEY} from "../configuration";
import ImageTile from "ol/source/ImageTile";
import {
  OlControlsDirective,
  OlInteractionsDirective,
  OlLayersDirective,
  OlMapComponent,
  OlViewDirective
} from "@nasumilu/ngx-ol-map";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {OlTileLayerDirective, OlVectorLayerDirective, MinMaxResolutionDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import { OlTopoJsonSource } from '@nasumilu/ngx-ol-source';
import {FlatStroke} from "ol/style/flat";
import {MatSlider, MatSliderRangeThumb} from "@angular/material/slider";
import {DecimalPipe} from "@angular/common";

@Component({
  selector: 'topojson-layer-map',
  imports: [
    OlControlsDirective,
    OlDefaultControlDirective,
    OlDefaultInteractionDirective,
    OlInteractionsDirective,
    OlLayersDirective,
    OlMapComponent,
    OlTileLayerDirective,
    SourceDirective,
    OlTopoJsonSource,
    OlVectorLayerDirective,
    MatSlider,
    MatSliderRangeThumb,
    DecimalPipe
  ],
  templateUrl: './topojson-layer-map.component.html',
  styleUrl: './topojson-layer-map.component.css'
})
export class TopoJSONLayerMapComponent {

  private readonly minMaxResolution = viewChild('topoJsonLayer', {read: MinMaxResolutionDirective});
  protected readonly viewDirective = viewChild(OlViewDirective);

  protected readonly minResolution = linkedSignal(() => this.minMaxResolution()?.minResolution());
  protected readonly maxResolution = linkedSignal(() => this.minMaxResolution()?.maxResolution());

  readonly #apiKey = inject(MAP_TILER_API_KEY);
  protected readonly attributions =
    '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> ' +
    '<a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>';

  protected readonly datavizDarSource = new ImageTile({
    attributions: this.attributions,
    url: `https://api.maptiler.com/maps/dataviz-dark/{z}/{x}/{y}.png?key=${this.#apiKey}`,
    tileSize: 512
  });

  protected style: FlatStroke = {
    'stroke-color': 'white',
    'stroke-width': 1.5,
  };

  updateMinResolution(value: number) {
    this.minMaxResolution()?.minResolution.set(value);
  }

  updateMaxResolution(value: number) {
    this.minMaxResolution()?.maxResolution.set(value);
  }

}
