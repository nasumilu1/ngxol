import { OnDestroy, OnInit } from '@angular/core';
import ScaleLine, { Units } from "ol/control/ScaleLine";
import { OlControl } from '@nasumilu/ngx-ol-map';
import * as i0 from "@angular/core";
/**
 * Directive for integrating OpenLayers' ScaleLine control in Angular applications.
 * This directive enables seamless configuration and interaction with the ScaleLine control,
 * which adds a scale bar to a map for representing distances.
 *
 * Extends:
 * - OlControl<ScaleLine>: Provides interaction with OpenLayers' ScaleLine control.
 * - Angular lifecycle hooks OnInit and OnDestroy.
 *
 * Selector:
 * - ol-scale-line: Use this selector in templates to add the scale line control to a map.
 *
 * Dependencies:
 * - OpenLayers' ScaleLine class.
 *
 * Inputs:
 * - className: A string representing the CSS class name for the control.
 * - minWidth: A number defining the minimum width of the scale line in pixels. Default is 64.
 * - maxWidth: An optional number defining the maximum width of the scale line in pixels.
 * - units: Sets the units for measurement (e.g., metric, imperial). Default is 'metric'.
 * - bar: A boolean indicating whether to use the bar representation of the scale. Default is false.
 * - steps: A number determining the number of steps in the bar scale. Default is 4.
 * - text: A boolean indicating whether to display a textual representation of the scale. Default is false.
 * - dpi: An optional number specifying the dots per inch (DPI) for rendering accuracy.
 *
 * Outputs:
 * - binds to `units` via model changes to synchronize units between the directive and the ScaleLine control.
 *
 * Lifecycle Methods:
 * - ngOnInit(): Initializes the OpenLayers ScaleLine control and binds the `units` property for bidirectional synchronization.
 * - ngOnDestroy(): Cleans up event listeners attached to the ScaleLine control's `change:units` event.
 *
 * Computed Properties:
 * - control: A reactive property that creates and updates the ScaleLine control instance in response to changes in input properties.
 *
 * Notes:
 * - The directive automatically synchronizes the `units` input and updates the ScaleLine control's units when necessary.
 * - The OpenLayers `unByKey` function is used during destruction to unregister events attached to the control.
 */
export declare class OlScaleLineDirective extends OlControl<ScaleLine> implements OnInit, OnDestroy {
    #private;
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly minWidth: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly maxWidth: import("@angular/core").InputSignalWithTransform<number | undefined, unknown>;
    readonly units: import("@angular/core").ModelSignal<Units>;
    readonly bar: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly steps: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly text: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly dpi: import("@angular/core").InputSignalWithTransform<number | undefined, unknown>;
    readonly control: import("@angular/core").Signal<ScaleLine>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlScaleLineDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlScaleLineDirective, "ol-scale-line", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "minWidth": { "alias": "minWidth"; "required": false; "isSignal": true; }; "maxWidth": { "alias": "maxWidth"; "required": false; "isSignal": true; }; "units": { "alias": "units"; "required": false; "isSignal": true; }; "bar": { "alias": "bar"; "required": false; "isSignal": true; }; "steps": { "alias": "steps"; "required": false; "isSignal": true; }; "text": { "alias": "text"; "required": false; "isSignal": true; }; "dpi": { "alias": "dpi"; "required": false; "isSignal": true; }; }, { "units": "unitsChange"; }, never, never, true, never>;
}
//# sourceMappingURL=ol-scale-line.directive.d.ts.map