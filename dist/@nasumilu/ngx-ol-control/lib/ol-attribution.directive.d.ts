import Attribution from 'ol/control/Attribution';
import { OlControl } from '@nasumilu/ngx-ol-map';
import * as i0 from "@angular/core";
/**
 * Directive representing an OpenLayers Attribution control for Angular applications.
 * This class extends the OlControl class, encapsulating the functionality of the OpenLayers Attribution control.
 *
 * The directive provides an interface to configure and manipulate the Attribution control with attributes for
 * class names, labels, collapsibility, and other related options.
 *
 * Selector:
 * - `ol-attribution`: Use this selector to include the directive in an Angular component's template.
 *
 * Attributes:
 * - `className`: Defines the CSS class name for the control. Accepts a string value.
 * - `label`: Specifies the label for the Attribution control. Accepts a string value.
 * - `tipLabel`: Provides the tooltip text displayed upon hovering over the attribution control. Accepts a string value.
 * - `collapsed`: Determines whether the attribution control should initially be rendered in a collapsed state. Accepts a boolean value.
 * - `collapsible`: Indicates if the attribution control is collapsible by the user. Accepts a boolean value.
 * - `collapseLabel`: Specifies the label shown in the control when it is collapsed. Accepts a string or HTMLElement value.
 * - `expandClassName`: The CSS class name applied when the control is expanded. Accepts a string value.
 * - `collapseClassName`: The CSS class name applied when the control is collapsed. Accepts a string value.
 *
 * Computed Properties:
 * - `control`: Represents the underlying OpenLayers Attribution control instance configured with the provided inputs.
 *
 * This directive is typically used to add Attribution control in OpenLayers-enabled Angular applications,
 * allowing users to easily toggle and customize map attribution information.
 */
export declare class OlAttributionDirective extends OlControl<Attribution> {
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly label: import("@angular/core").InputSignal<string | undefined>;
    readonly tipLabel: import("@angular/core").InputSignal<string | undefined>;
    readonly collapsed: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly collapsible: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly collapseLabel: import("@angular/core").InputSignal<string | HTMLElement>;
    readonly expandClassName: import("@angular/core").InputSignal<string>;
    readonly collapseClassName: import("@angular/core").InputSignal<string>;
    readonly control: import("@angular/core").Signal<Attribution>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlAttributionDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlAttributionDirective, "ol-attribution", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "label": { "alias": "label"; "required": false; "isSignal": true; }; "tipLabel": { "alias": "tipLabel"; "required": false; "isSignal": true; }; "collapsed": { "alias": "collapsed"; "required": false; "isSignal": true; }; "collapsible": { "alias": "collapsible"; "required": false; "isSignal": true; }; "collapseLabel": { "alias": "collapseLabel"; "required": false; "isSignal": true; }; "expandClassName": { "alias": "expandClassName"; "required": false; "isSignal": true; }; "collapseClassName": { "alias": "collapseClassName"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-attribution.directive.d.ts.map