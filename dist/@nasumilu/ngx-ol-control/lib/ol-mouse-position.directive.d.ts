import { OnDestroy, OnInit } from '@angular/core';
import { CoordinateFormat } from "ol/coordinate";
import { ProjectionLike } from "ol/proj";
import MousePosition from "ol/control/MousePosition";
import { OlControl } from '@nasumilu/ngx-ol-map';
import * as i0 from "@angular/core";
/**
 * A directive that wraps the OpenLayers MousePosition control, allowing integration with Angular templates and bindings.
 * This directive provides options to control the format and projection of mouse position display, as well as other configurations.
 *
 * It extends the OlControl base class and binds inputs and models to control properties.
 *
 * Inputs:
 * - `className`: String input to define the CSS class name applied to the control's container element.
 * - `coordinateFormat`: A model input to define the formatting function for coordinates.
 * - `projection`: A model input to specify the projection used to transform coordinates.
 * - `placeholder`: String input to specify the placeholder text displayed when no position is available.
 * - `wrapX`: Boolean input that determines whether the world is wrapped horizontally.
 *
 * Lifecycle Methods:
 * - `ngOnInit`: Initializes the MousePosition control, subscribes to model updates, and updates the control state whenever bound properties change.
 * - `ngOnDestroy`: Cleans up event listeners and other resources when the directive is destroyed.
 */
export declare class OlMousePositionDirective extends OlControl<MousePosition> implements OnInit, OnDestroy {
    #private;
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly coordinateFormat: import("@angular/core").ModelSignal<CoordinateFormat | undefined>;
    readonly projection: import("@angular/core").ModelSignal<ProjectionLike>;
    readonly placeholder: import("@angular/core").InputSignal<string | undefined>;
    readonly wrapX: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly control: import("@angular/core").Signal<MousePosition>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlMousePositionDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlMousePositionDirective, "ol-mouse-position", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "coordinateFormat": { "alias": "coordinateFormat"; "required": false; "isSignal": true; }; "projection": { "alias": "projection"; "required": false; "isSignal": true; }; "placeholder": { "alias": "placeholder"; "required": false; "isSignal": true; }; "wrapX": { "alias": "wrapX"; "required": false; "isSignal": true; }; }, { "coordinateFormat": "coordinateFormatChange"; "projection": "projectionChange"; }, never, never, true, never>;
}
//# sourceMappingURL=ol-mouse-position.directive.d.ts.map