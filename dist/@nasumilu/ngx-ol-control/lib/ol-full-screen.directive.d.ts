import { OnDestroy, OnInit } from '@angular/core';
import { OlControl } from "@nasumilu/ngx-ol-map";
import FullScreen from "ol/control/FullScreen";
import * as i0 from "@angular/core";
/**
 * Directive for integrating OpenLayers FullScreen control.
 *
 * This directive provides a convenient way to add and configure the FullScreen control in an OpenLayers map.
 * It allows customization of control properties such as class names, labels, and key interactions for the fullscreen toggle functionality.
 *
 * Features:
 * - Configurable class names for active/inactive states.
 * - Customizable labels for the FullScreen button.
 * - Emits events on entering and leaving fullscreen mode.
 * - Optional keyboard key support for toggling fullscreen mode.
 *
 * Inputs:
 * - `className`: Defines the CSS class name applied to the FullScreen element.
 * - `label`: Text or content used as the label for the control (inactive state).
 * - `tipLabel`: Tooltip text for the control.
 * - `labelActive`: Text or content used as the label for the control (active state).
 * - `activeClassName`: CSS class applied when fullscreen mode is active.
 * - `inactiveClassName`: CSS class applied when fullscreen mode is inactive.
 * - `keys`: Boolean defining whether keyboard shortcuts should be enabled for toggling fullscreen.
 *
 * Outputs:
 * - `fullScreenChange`: Event emitted whenever the fullscreen mode changes.
 *   Emits `true` when entering fullscreen and `false` when leaving.
 *
 * Lifecycle Hooks:
 * - Initializes and sets up event listeners for FullScreen control on `ngOnInit`.
 * - Cleans up event listeners on `ngOnDestroy`.
 */
export declare class OlFullScreenDirective extends OlControl<FullScreen> implements OnInit, OnDestroy {
    #private;
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly label: import("@angular/core").InputSignal<string | undefined>;
    readonly tipLabel: import("@angular/core").InputSignal<string | undefined>;
    readonly labelActive: import("@angular/core").InputSignal<string | HTMLElement | Text | undefined>;
    readonly activeClassName: import("@angular/core").InputSignal<string | undefined>;
    readonly inactiveClassName: import("@angular/core").InputSignal<string | undefined>;
    readonly keys: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly fullScreenChange: import("@angular/core").OutputEmitterRef<boolean>;
    readonly control: import("@angular/core").Signal<FullScreen>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlFullScreenDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlFullScreenDirective, "ol-full-screen", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "label": { "alias": "label"; "required": false; "isSignal": true; }; "tipLabel": { "alias": "tipLabel"; "required": false; "isSignal": true; }; "labelActive": { "alias": "labelActive"; "required": false; "isSignal": true; }; "activeClassName": { "alias": "activeClassName"; "required": false; "isSignal": true; }; "inactiveClassName": { "alias": "inactiveClassName"; "required": false; "isSignal": true; }; "keys": { "alias": "keys"; "required": false; "isSignal": true; }; }, { "fullScreenChange": "fullScreenChange"; }, never, never, true, never>;
}
//# sourceMappingURL=ol-full-screen.directive.d.ts.map