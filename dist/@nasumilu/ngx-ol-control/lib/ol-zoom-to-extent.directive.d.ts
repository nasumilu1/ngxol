import { OlControl } from "@nasumilu/ngx-ol-map";
import ZoomToExtent from "ol/control/ZoomToExtent";
import { Extent } from "ol/extent";
import * as i0 from "@angular/core";
/**
 * Directive that integrates OpenLayers ZoomToExtent control into Angular components.
 * This directive provides functionality to create and manage a ZoomToExtent control in
 * an OpenLayers map within an Angular application.
 *
 * The ZoomToExtent control provides a button to zoom to a predefined extent.
 *
 * Inputs:
 * - `className`: The CSS class name to apply to the control's container.
 * - `label`: The label to display on the control's button.
 * - `tipLabel`: The tooltip text to display when hovering over the control.
 * - `extent`: The extent (bounding box) to zoom to when the control is activated.
 *
 * This directive extends the `OlControl` base class to provide integration with other OpenLayers controls.
 */
export declare class OlZoomToExtentDirective extends OlControl<ZoomToExtent> {
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly label: import("@angular/core").InputSignal<string | undefined>;
    readonly tipLabel: import("@angular/core").InputSignal<string | undefined>;
    readonly extent: import("@angular/core").InputSignal<Extent | undefined>;
    readonly control: import("@angular/core").Signal<ZoomToExtent>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlZoomToExtentDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlZoomToExtentDirective, "ol-zoom-to-extent", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "label": { "alias": "label"; "required": false; "isSignal": true; }; "tipLabel": { "alias": "tipLabel"; "required": false; "isSignal": true; }; "extent": { "alias": "extent"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-zoom-to-extent.directive.d.ts.map