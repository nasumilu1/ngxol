import Zoom from "ol/control/Zoom";
import { OlControl } from '@nasumilu/ngx-ol-map';
import * as i0 from "@angular/core";
/**
 * A directive that provides a configurable OpenLayers zoom control.
 *
 * This directive extends the base `OlControl` for OpenLayers and integrates
 * a zoom control, allowing customization of various parameters such as animation
 * duration, button class names, labels, and zoom delta values.
 *
 * Directive Selector:
 * - `ol-zoom-control`
 *
 * Host Directives:
 * - `ClassNameDirective`: Allows setting a custom CSS class name for the control.
 *
 * Providers:
 * - Provides `OlControl` token, using itself (`OlZoomControlDirective`) as the implementation.
 *
 * Inputs:
 * - `duration`: The duration (in milliseconds) for zoom animations. Default is `250`.
 * - `zoomInClassName`: Custom class name for the zoom-in button. Default is `'ol-zoom-in'`.
 * - `zoomOutClassName`: Custom class name for the zoom-out button. Default is `'ol-zoom-out'`.
 * - `zoomInLabel`: Label or content for the zoom-in button. Default is `'+'`.
 * - `zoomOutLabel`: Label or content for the zoom-out button. Default is `'-'`.
 * - `zoomInTipLabel`: Tooltip label for the zoom-in button. Default is `'Zoom in'`.
 * - `zoomOutTipLabel`: Tooltip label for the zoom-out button. Default is `'Zoom out'`.
 * - `delta`: The zoom delta value, determining the zoom level increment or decrement. Default is `1`.
 *
 * Computed Properties:
 * - `control`: Generates a new `Zoom` instance configured with the provided inputs, applying
 *   all specified properties like class names, labels, and animation duration.
 */
export declare class OlZoomControlDirective extends OlControl<Zoom> {
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly zoomInClassName: import("@angular/core").InputSignal<string>;
    readonly zoomOutClassName: import("@angular/core").InputSignal<string>;
    readonly zoomInLabel: import("@angular/core").InputSignal<string>;
    readonly zoomOutLabel: import("@angular/core").InputSignal<string>;
    readonly zoomInTipLabel: import("@angular/core").InputSignal<string>;
    readonly zoomOutTipLabel: import("@angular/core").InputSignal<string>;
    readonly delta: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly control: import("@angular/core").Signal<Zoom>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlZoomControlDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlZoomControlDirective, "ol-zoom-control", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "zoomInClassName": { "alias": "zoomInClassName"; "required": false; "isSignal": true; }; "zoomOutClassName": { "alias": "zoomOutClassName"; "required": false; "isSignal": true; }; "zoomInLabel": { "alias": "zoomInLabel"; "required": false; "isSignal": true; }; "zoomOutLabel": { "alias": "zoomOutLabel"; "required": false; "isSignal": true; }; "zoomInTipLabel": { "alias": "zoomInTipLabel"; "required": false; "isSignal": true; }; "zoomOutTipLabel": { "alias": "zoomOutTipLabel"; "required": false; "isSignal": true; }; "delta": { "alias": "delta"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-zoom-control.directive.d.ts.map