import { OlControl } from '@nasumilu/ngx-ol-map';
import OverviewMap from "ol/control/OverviewMap";
import * as i0 from "@angular/core";
/**
 * Directive for integrating an OpenLayers OverviewMap control as an Angular component.
 * This directive allows you to configure and use the OverviewMap control within an Angular application.
 * The control provides a small overview map that shows the current view extent of the larger map.
 *
 * Extends:
 * - OlControl<OverviewMap>
 *
 * Implements:
 * - AfterContentInit
 *
 * Features:
 * - Configurable properties such as class name, labels, collapse behavior, and layering.
 * - Supports dynamic addition and removal of layers via `OlLayersDirective`.
 * - Automatically updates the control based on the provided inputs.
 *
 * Inputs:
 * - `className`: Sets a custom class name for the OverviewMap control's container.
 * - `label`: Sets the label for the expand button.
 * - `tipLabel`: Sets the tooltip label for the expand button.
 * - `collapsed`: Controls whether the overview map is collapsed by default (default is `true`).
 * - `collapsible`: Enables or disables the collapsibility of the overview map (default is `false`).
 * - `collapseLabel`: Defines the label for the collapse button (default is `›`).
 * - `rotateWithView`: Toggles whether the overview map should rotate with the main view (default is `false`).
 *
 * Outputs:
 * - Integrates with `OlLayersDirective` to handle layer additions and removals dynamically.
 *
 * Lifecycle:
 * - `ngAfterContentInit`: Subscribes to layer changes from `OlLayersDirective` to update the internal layer collection of the OverviewMap control.
 */
export declare class OlOverviewMapDirective extends OlControl<OverviewMap> {
    private readonly layerDirective;
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly label: import("@angular/core").InputSignal<string | undefined>;
    readonly tipLabel: import("@angular/core").InputSignal<string | undefined>;
    readonly collapsed: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly collapsible: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly collapseLabel: import("@angular/core").InputSignal<string | HTMLElement>;
    readonly rotateWithView: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly control: import("@angular/core").Signal<OverviewMap>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlOverviewMapDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlOverviewMapDirective, "ol-overview-map", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "label": { "alias": "label"; "required": false; "isSignal": true; }; "tipLabel": { "alias": "tipLabel"; "required": false; "isSignal": true; }; "collapsed": { "alias": "collapsed"; "required": false; "isSignal": true; }; "collapsible": { "alias": "collapsible"; "required": false; "isSignal": true; }; "collapseLabel": { "alias": "collapseLabel"; "required": false; "isSignal": true; }; "rotateWithView": { "alias": "rotateWithView"; "required": false; "isSignal": true; }; }, {}, ["layerDirective"], never, true, never>;
}
//# sourceMappingURL=ol-overview-map.directive.d.ts.map