import { OlControl } from "@nasumilu/ngx-ol-map";
import Rotate from "ol/control/Rotate";
import * as i0 from "@angular/core";
/**
 * Directive to integrate OpenLayers Rotate control into Angular components.
 *
 * Extends the OlControl base class to provide bindings for the Rotate control in OpenLayers.
 * This directive allows customizing various options for the Rotate control using Angular's
 * reactive input system.
 *
 * Selector: `ol-rotate`
 *
 * Inputs:
 * - `className` (string): CSS class name to set to the rotate control element.
 * - `label` (string): The label to display on the rotate control.
 * - `compassClassName` (string): CSS class name for the compass element within the control.
 * - `tipLabel` (string): Displayed tooltip text for the button.
 * - `duration` (number): The animation duration in milliseconds for the rotation reset.
 * - `autoHide` (boolean): If true, the control is hidden when the map rotation is 0.
 *
 * Control:
 * - Computed `control` property creates a new Rotate instance with the given configuration.
 */
export declare class OlRotateDirective extends OlControl<Rotate> {
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly label: import("@angular/core").InputSignal<string | undefined>;
    readonly compassClassName: import("@angular/core").InputSignal<string | undefined>;
    readonly tipLabel: import("@angular/core").InputSignal<string | undefined>;
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly autoHide: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly control: import("@angular/core").Signal<Rotate>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlRotateDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlRotateDirective, "ol-rotate", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "label": { "alias": "label"; "required": false; "isSignal": true; }; "compassClassName": { "alias": "compassClassName"; "required": false; "isSignal": true; }; "tipLabel": { "alias": "tipLabel"; "required": false; "isSignal": true; }; "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "autoHide": { "alias": "autoHide"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-rotate.directive.d.ts.map