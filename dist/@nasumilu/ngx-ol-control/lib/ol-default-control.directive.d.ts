import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./ol-zoom-control.directive";
import * as i2 from "./ol-rotate.directive";
import * as i3 from "./ol-attribution.directive";
/**
 * A directive that provides a set of default controls for OpenLayers maps.
 * This directive automatically adds the specified controls to the parent
 * `OlControlsDirective` when initialized.
 *
 * By default, the following controls are included:
 * - Zoom control
 * - Rotate control
 * - Attribution control
 *
 * The directive relies on the framework's dependency injection mechanism
 * to obtain references to the requisite control directives.
 *
 * This directive should be used as an attribute on a container element,
 * and it will automatically synchronize the default controls with the map.
 */
export declare class OlDefaultControlDirective implements OnInit {
    #private;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlDefaultControlDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlDefaultControlDirective, "[defaultControls]", never, {}, {}, never, never, true, [{ directive: typeof i1.OlZoomControlDirective; inputs: {}; outputs: {}; }, { directive: typeof i2.OlRotateDirective; inputs: {}; outputs: {}; }, { directive: typeof i3.OlAttributionDirective; inputs: {}; outputs: {}; }]>;
}
//# sourceMappingURL=ol-default-control.directive.d.ts.map