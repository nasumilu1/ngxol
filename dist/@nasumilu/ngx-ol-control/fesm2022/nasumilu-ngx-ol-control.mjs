import * as i0 from '@angular/core';
import { input, numberAttribute, computed, Directive, booleanAttribute, contentChild, model, output, inject } from '@angular/core';
import Zoom from 'ol/control/Zoom';
import { OlControl, OlLayersDirective, OlControlsDirective } from '@nasumilu/ngx-ol-map';
import Attribution from 'ol/control/Attribution';
import OverviewMap from 'ol/control/OverviewMap';
import { get } from 'ol/proj';
import MousePosition from 'ol/control/MousePosition';
import { unByKey } from 'ol/Observable';
import ScaleLine from 'ol/control/ScaleLine';
import FullScreen from 'ol/control/FullScreen';
import ZoomToExtent from 'ol/control/ZoomToExtent';
import Rotate from 'ol/control/Rotate';

/**
 * A directive that provides a configurable OpenLayers zoom control.
 *
 * This directive extends the base `OlControl` for OpenLayers and integrates
 * a zoom control, allowing customization of various parameters such as animation
 * duration, button class names, labels, and zoom delta values.
 *
 * Directive Selector:
 * - `ol-zoom-control`
 *
 * Host Directives:
 * - `ClassNameDirective`: Allows setting a custom CSS class name for the control.
 *
 * Providers:
 * - Provides `OlControl` token, using itself (`OlZoomControlDirective`) as the implementation.
 *
 * Inputs:
 * - `duration`: The duration (in milliseconds) for zoom animations. Default is `250`.
 * - `zoomInClassName`: Custom class name for the zoom-in button. Default is `'ol-zoom-in'`.
 * - `zoomOutClassName`: Custom class name for the zoom-out button. Default is `'ol-zoom-out'`.
 * - `zoomInLabel`: Label or content for the zoom-in button. Default is `'+'`.
 * - `zoomOutLabel`: Label or content for the zoom-out button. Default is `'-'`.
 * - `zoomInTipLabel`: Tooltip label for the zoom-in button. Default is `'Zoom in'`.
 * - `zoomOutTipLabel`: Tooltip label for the zoom-out button. Default is `'Zoom out'`.
 * - `delta`: The zoom delta value, determining the zoom level increment or decrement. Default is `1`.
 *
 * Computed Properties:
 * - `control`: Generates a new `Zoom` instance configured with the provided inputs, applying
 *   all specified properties like class names, labels, and animation duration.
 */
class OlZoomControlDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.className = input();
        this.duration = input(250, { transform: numberAttribute });
        this.zoomInClassName = input('ol-zoom-in');
        this.zoomOutClassName = input('ol-zoom-out');
        this.zoomInLabel = input('+');
        this.zoomOutLabel = input('-');
        this.zoomInTipLabel = input('Zoom in');
        this.zoomOutTipLabel = input('Zoom out');
        this.delta = input(1, { transform: numberAttribute });
        this.control = computed(() => new Zoom({
            className: this.className(),
            duration: this.duration(),
            zoomInClassName: this.zoomInClassName(),
            zoomOutClassName: this.zoomOutClassName(),
            zoomInLabel: this.zoomInLabel(),
            zoomOutLabel: this.zoomOutLabel(),
            zoomInTipLabel: this.zoomInTipLabel(),
            zoomOutTipLabel: this.zoomOutTipLabel(),
            delta: this.delta()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlZoomControlDirective_BaseFactory; return function OlZoomControlDirective_Factory(__ngFactoryType__) { return (ɵOlZoomControlDirective_BaseFactory || (ɵOlZoomControlDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlZoomControlDirective)))(__ngFactoryType__ || OlZoomControlDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlZoomControlDirective, selectors: [["ol-zoom-control"]], inputs: { className: [1, "className"], duration: [1, "duration"], zoomInClassName: [1, "zoomInClassName"], zoomOutClassName: [1, "zoomOutClassName"], zoomInLabel: [1, "zoomInLabel"], zoomOutLabel: [1, "zoomOutLabel"], zoomInTipLabel: [1, "zoomInTipLabel"], zoomOutTipLabel: [1, "zoomOutTipLabel"], delta: [1, "delta"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlZoomControlDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlZoomControlDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-zoom-control',
                providers: [{ provide: OlControl, useExisting: OlZoomControlDirective }]
            }]
    }], null, null); })();

/**
 * Directive representing an OpenLayers Attribution control for Angular applications.
 * This class extends the OlControl class, encapsulating the functionality of the OpenLayers Attribution control.
 *
 * The directive provides an interface to configure and manipulate the Attribution control with attributes for
 * class names, labels, collapsibility, and other related options.
 *
 * Selector:
 * - `ol-attribution`: Use this selector to include the directive in an Angular component's template.
 *
 * Attributes:
 * - `className`: Defines the CSS class name for the control. Accepts a string value.
 * - `label`: Specifies the label for the Attribution control. Accepts a string value.
 * - `tipLabel`: Provides the tooltip text displayed upon hovering over the attribution control. Accepts a string value.
 * - `collapsed`: Determines whether the attribution control should initially be rendered in a collapsed state. Accepts a boolean value.
 * - `collapsible`: Indicates if the attribution control is collapsible by the user. Accepts a boolean value.
 * - `collapseLabel`: Specifies the label shown in the control when it is collapsed. Accepts a string or HTMLElement value.
 * - `expandClassName`: The CSS class name applied when the control is expanded. Accepts a string value.
 * - `collapseClassName`: The CSS class name applied when the control is collapsed. Accepts a string value.
 *
 * Computed Properties:
 * - `control`: Represents the underlying OpenLayers Attribution control instance configured with the provided inputs.
 *
 * This directive is typically used to add Attribution control in OpenLayers-enabled Angular applications,
 * allowing users to easily toggle and customize map attribution information.
 */
class OlAttributionDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.className = input();
        this.label = input();
        this.tipLabel = input();
        this.collapsed = input(true, { transform: booleanAttribute });
        this.collapsible = input(false, { transform: booleanAttribute });
        this.collapseLabel = input('›');
        this.expandClassName = input('ol-attribution-expand');
        this.collapseClassName = input('ol-attribution-collapse');
        this.control = computed(() => new Attribution({
            className: this.className(),
            label: this.label(),
            tipLabel: this.tipLabel(),
            collapsed: this.collapsed(),
            collapsible: this.collapsible(),
            collapseLabel: this.collapseLabel(),
            expandClassName: this.expandClassName(),
            collapseClassName: this.collapseClassName(),
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlAttributionDirective_BaseFactory; return function OlAttributionDirective_Factory(__ngFactoryType__) { return (ɵOlAttributionDirective_BaseFactory || (ɵOlAttributionDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlAttributionDirective)))(__ngFactoryType__ || OlAttributionDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlAttributionDirective, selectors: [["ol-attribution"]], inputs: { className: [1, "className"], label: [1, "label"], tipLabel: [1, "tipLabel"], collapsed: [1, "collapsed"], collapsible: [1, "collapsible"], collapseLabel: [1, "collapseLabel"], expandClassName: [1, "expandClassName"], collapseClassName: [1, "collapseClassName"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlAttributionDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlAttributionDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-attribution',
                providers: [{ provide: OlControl, useExisting: OlAttributionDirective }]
            }]
    }], null, null); })();

/**
 * Directive for integrating an OpenLayers OverviewMap control as an Angular component.
 * This directive allows you to configure and use the OverviewMap control within an Angular application.
 * The control provides a small overview map that shows the current view extent of the larger map.
 *
 * Extends:
 * - OlControl<OverviewMap>
 *
 * Implements:
 * - AfterContentInit
 *
 * Features:
 * - Configurable properties such as class name, labels, collapse behavior, and layering.
 * - Supports dynamic addition and removal of layers via `OlLayersDirective`.
 * - Automatically updates the control based on the provided inputs.
 *
 * Inputs:
 * - `className`: Sets a custom class name for the OverviewMap control's container.
 * - `label`: Sets the label for the expand button.
 * - `tipLabel`: Sets the tooltip label for the expand button.
 * - `collapsed`: Controls whether the overview map is collapsed by default (default is `true`).
 * - `collapsible`: Enables or disables the collapsibility of the overview map (default is `false`).
 * - `collapseLabel`: Defines the label for the collapse button (default is `›`).
 * - `rotateWithView`: Toggles whether the overview map should rotate with the main view (default is `false`).
 *
 * Outputs:
 * - Integrates with `OlLayersDirective` to handle layer additions and removals dynamically.
 *
 * Lifecycle:
 * - `ngAfterContentInit`: Subscribes to layer changes from `OlLayersDirective` to update the internal layer collection of the OverviewMap control.
 */
class OlOverviewMapDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.layerDirective = contentChild(OlLayersDirective);
        this.className = input();
        this.label = input();
        this.tipLabel = input();
        this.collapsed = input(true, { transform: booleanAttribute });
        this.collapsible = input(false, { transform: booleanAttribute });
        this.collapseLabel = input('›');
        this.rotateWithView = input(false, { transform: booleanAttribute });
        this.control = computed(() => new OverviewMap({
            className: this.className(),
            label: this.label(),
            tipLabel: this.tipLabel(),
            collapsible: this.collapsible(),
            collapsed: this.collapsed(),
            collapseLabel: this.collapseLabel(),
            layers: this.layerDirective()?.layers.map(olLayer => olLayer.layer()),
            rotateWithView: this.rotateWithView()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlOverviewMapDirective_BaseFactory; return function OlOverviewMapDirective_Factory(__ngFactoryType__) { return (ɵOlOverviewMapDirective_BaseFactory || (ɵOlOverviewMapDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlOverviewMapDirective)))(__ngFactoryType__ || OlOverviewMapDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlOverviewMapDirective, selectors: [["ol-overview-map"]], contentQueries: function OlOverviewMapDirective_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.layerDirective, OlLayersDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, inputs: { className: [1, "className"], label: [1, "label"], tipLabel: [1, "tipLabel"], collapsed: [1, "collapsed"], collapsible: [1, "collapsible"], collapseLabel: [1, "collapseLabel"], rotateWithView: [1, "rotateWithView"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlOverviewMapDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlOverviewMapDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-overview-map',
                providers: [{ provide: OlControl, useExisting: OlOverviewMapDirective }]
            }]
    }], null, null); })();

/**
 * A directive that wraps the OpenLayers MousePosition control, allowing integration with Angular templates and bindings.
 * This directive provides options to control the format and projection of mouse position display, as well as other configurations.
 *
 * It extends the OlControl base class and binds inputs and models to control properties.
 *
 * Inputs:
 * - `className`: String input to define the CSS class name applied to the control's container element.
 * - `coordinateFormat`: A model input to define the formatting function for coordinates.
 * - `projection`: A model input to specify the projection used to transform coordinates.
 * - `placeholder`: String input to specify the placeholder text displayed when no position is available.
 * - `wrapX`: Boolean input that determines whether the world is wrapped horizontally.
 *
 * Lifecycle Methods:
 * - `ngOnInit`: Initializes the MousePosition control, subscribes to model updates, and updates the control state whenever bound properties change.
 * - `ngOnDestroy`: Cleans up event listeners and other resources when the directive is destroyed.
 */
class OlMousePositionDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.className = input();
        this.coordinateFormat = model();
        this.projection = model();
        this.placeholder = input();
        this.wrapX = input(true, { transform: booleanAttribute });
        this.control = computed(() => new MousePosition({
            className: this.className(),
            coordinateFormat: this.coordinateFormat(),
            projection: this.projection(),
            placeholder: this.placeholder(),
            wrapX: this.wrapX()
        }));
        this.#eventKeys = [];
    }
    #eventKeys;
    ngOnInit() {
        const control = this.control();
        this.projection.subscribe(value => {
            const _value = get(value);
            const projection = control.getProjection();
            if (_value?.getCode() != projection?.getCode()) {
                control.setProjection(value);
            }
        });
        this.coordinateFormat.subscribe(value => {
            const format = control.getCoordinateFormat();
            if (value != undefined && format !== value) {
                control.setCoordinateFormat(value);
            }
        });
        this.#eventKeys.push(control.on('change:projection', () => {
            const projection = control.getProjection();
            if (get(this.projection())?.getCode() !== projection?.getCode()) {
                this.projection.set(projection);
            }
        }));
        this.#eventKeys.push(control.on('change:coordinateFormat', () => {
            const format = control.getCoordinateFormat();
            if (this.coordinateFormat() !== format) {
                this.coordinateFormat.set(format);
            }
        }));
    }
    ngOnDestroy() {
        unByKey(this.#eventKeys);
        this.#eventKeys = [];
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlMousePositionDirective_BaseFactory; return function OlMousePositionDirective_Factory(__ngFactoryType__) { return (ɵOlMousePositionDirective_BaseFactory || (ɵOlMousePositionDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlMousePositionDirective)))(__ngFactoryType__ || OlMousePositionDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlMousePositionDirective, selectors: [["ol-mouse-position"]], inputs: { className: [1, "className"], coordinateFormat: [1, "coordinateFormat"], projection: [1, "projection"], placeholder: [1, "placeholder"], wrapX: [1, "wrapX"] }, outputs: { coordinateFormat: "coordinateFormatChange", projection: "projectionChange" }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlMousePositionDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlMousePositionDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-mouse-position',
                providers: [{ provide: OlControl, useExisting: OlMousePositionDirective }]
            }]
    }], null, null); })();

/**
 * Directive for integrating OpenLayers' ScaleLine control in Angular applications.
 * This directive enables seamless configuration and interaction with the ScaleLine control,
 * which adds a scale bar to a map for representing distances.
 *
 * Extends:
 * - OlControl<ScaleLine>: Provides interaction with OpenLayers' ScaleLine control.
 * - Angular lifecycle hooks OnInit and OnDestroy.
 *
 * Selector:
 * - ol-scale-line: Use this selector in templates to add the scale line control to a map.
 *
 * Dependencies:
 * - OpenLayers' ScaleLine class.
 *
 * Inputs:
 * - className: A string representing the CSS class name for the control.
 * - minWidth: A number defining the minimum width of the scale line in pixels. Default is 64.
 * - maxWidth: An optional number defining the maximum width of the scale line in pixels.
 * - units: Sets the units for measurement (e.g., metric, imperial). Default is 'metric'.
 * - bar: A boolean indicating whether to use the bar representation of the scale. Default is false.
 * - steps: A number determining the number of steps in the bar scale. Default is 4.
 * - text: A boolean indicating whether to display a textual representation of the scale. Default is false.
 * - dpi: An optional number specifying the dots per inch (DPI) for rendering accuracy.
 *
 * Outputs:
 * - binds to `units` via model changes to synchronize units between the directive and the ScaleLine control.
 *
 * Lifecycle Methods:
 * - ngOnInit(): Initializes the OpenLayers ScaleLine control and binds the `units` property for bidirectional synchronization.
 * - ngOnDestroy(): Cleans up event listeners attached to the ScaleLine control's `change:units` event.
 *
 * Computed Properties:
 * - control: A reactive property that creates and updates the ScaleLine control instance in response to changes in input properties.
 *
 * Notes:
 * - The directive automatically synchronizes the `units` input and updates the ScaleLine control's units when necessary.
 * - The OpenLayers `unByKey` function is used during destruction to unregister events attached to the control.
 */
class OlScaleLineDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.className = input();
        this.minWidth = input(64, { transform: numberAttribute });
        this.maxWidth = input(undefined, { transform: numberAttribute });
        this.units = model('metric');
        this.bar = input(false, { transform: booleanAttribute });
        this.steps = input(4, { transform: numberAttribute });
        this.text = input(false, { transform: booleanAttribute });
        this.dpi = input(undefined, { transform: numberAttribute });
        this.control = computed(() => new ScaleLine({
            className: this.className(),
            minWidth: this.minWidth(),
            maxWidth: this.maxWidth(),
            units: this.units(),
            bar: this.bar(),
            steps: this.steps(),
            text: this.text(),
            dpi: this.dpi()
        }));
    }
    #eventKey;
    ngOnInit() {
        const control = this.control();
        this.units.subscribe(value => {
            const units = control.getUnits();
            if (units !== value) {
                control.setUnits(value);
            }
        });
        this.#eventKey = control.on('change:units', () => {
            const units = control.getUnits();
            if (this.units() !== units) {
                this.units.set(units);
            }
        });
    }
    ngOnDestroy() {
        if (this.#eventKey) {
            unByKey(this.#eventKey);
            this.#eventKey = undefined;
        }
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlScaleLineDirective_BaseFactory; return function OlScaleLineDirective_Factory(__ngFactoryType__) { return (ɵOlScaleLineDirective_BaseFactory || (ɵOlScaleLineDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlScaleLineDirective)))(__ngFactoryType__ || OlScaleLineDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlScaleLineDirective, selectors: [["ol-scale-line"]], inputs: { className: [1, "className"], minWidth: [1, "minWidth"], maxWidth: [1, "maxWidth"], units: [1, "units"], bar: [1, "bar"], steps: [1, "steps"], text: [1, "text"], dpi: [1, "dpi"] }, outputs: { units: "unitsChange" }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlScaleLineDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlScaleLineDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-scale-line',
                providers: [{ provide: OlControl, useExisting: OlScaleLineDirective }]
            }]
    }], null, null); })();

/**
 * Directive for integrating OpenLayers FullScreen control.
 *
 * This directive provides a convenient way to add and configure the FullScreen control in an OpenLayers map.
 * It allows customization of control properties such as class names, labels, and key interactions for the fullscreen toggle functionality.
 *
 * Features:
 * - Configurable class names for active/inactive states.
 * - Customizable labels for the FullScreen button.
 * - Emits events on entering and leaving fullscreen mode.
 * - Optional keyboard key support for toggling fullscreen mode.
 *
 * Inputs:
 * - `className`: Defines the CSS class name applied to the FullScreen element.
 * - `label`: Text or content used as the label for the control (inactive state).
 * - `tipLabel`: Tooltip text for the control.
 * - `labelActive`: Text or content used as the label for the control (active state).
 * - `activeClassName`: CSS class applied when fullscreen mode is active.
 * - `inactiveClassName`: CSS class applied when fullscreen mode is inactive.
 * - `keys`: Boolean defining whether keyboard shortcuts should be enabled for toggling fullscreen.
 *
 * Outputs:
 * - `fullScreenChange`: Event emitted whenever the fullscreen mode changes.
 *   Emits `true` when entering fullscreen and `false` when leaving.
 *
 * Lifecycle Hooks:
 * - Initializes and sets up event listeners for FullScreen control on `ngOnInit`.
 * - Cleans up event listeners on `ngOnDestroy`.
 */
class OlFullScreenDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.className = input();
        this.label = input();
        this.tipLabel = input();
        this.labelActive = input();
        this.activeClassName = input();
        this.inactiveClassName = input();
        this.keys = input(false, { transform: booleanAttribute });
        this.fullScreenChange = output();
        this.control = computed(() => new FullScreen({
            className: this.className(),
            label: this.label(),
            tipLabel: this.tipLabel(),
            labelActive: this.labelActive(),
            activeClassName: this.activeClassName(),
            inactiveClassName: this.inactiveClassName(),
            keys: this.keys(),
        }));
        this.#eventKeys = [];
    }
    #eventKeys;
    ngOnInit() {
        this.#eventKeys.push(this.control().on('enterfullscreen', () => this.fullScreenChange.emit(true)));
        this.#eventKeys.push(this.control().on('leavefullscreen', () => this.fullScreenChange.emit(false)));
    }
    ngOnDestroy() {
        unByKey(this.#eventKeys);
        this.#eventKeys = [];
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlFullScreenDirective_BaseFactory; return function OlFullScreenDirective_Factory(__ngFactoryType__) { return (ɵOlFullScreenDirective_BaseFactory || (ɵOlFullScreenDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlFullScreenDirective)))(__ngFactoryType__ || OlFullScreenDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlFullScreenDirective, selectors: [["ol-full-screen"]], inputs: { className: [1, "className"], label: [1, "label"], tipLabel: [1, "tipLabel"], labelActive: [1, "labelActive"], activeClassName: [1, "activeClassName"], inactiveClassName: [1, "inactiveClassName"], keys: [1, "keys"] }, outputs: { fullScreenChange: "fullScreenChange" }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlFullScreenDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlFullScreenDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-full-screen',
                providers: [{ provide: OlControl, useExisting: OlFullScreenDirective }]
            }]
    }], null, null); })();

/**
 * Directive that integrates OpenLayers ZoomToExtent control into Angular components.
 * This directive provides functionality to create and manage a ZoomToExtent control in
 * an OpenLayers map within an Angular application.
 *
 * The ZoomToExtent control provides a button to zoom to a predefined extent.
 *
 * Inputs:
 * - `className`: The CSS class name to apply to the control's container.
 * - `label`: The label to display on the control's button.
 * - `tipLabel`: The tooltip text to display when hovering over the control.
 * - `extent`: The extent (bounding box) to zoom to when the control is activated.
 *
 * This directive extends the `OlControl` base class to provide integration with other OpenLayers controls.
 */
class OlZoomToExtentDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.className = input();
        this.label = input();
        this.tipLabel = input();
        this.extent = input();
        this.control = computed(() => new ZoomToExtent({
            className: this.className(),
            label: this.label(),
            tipLabel: this.tipLabel(),
            extent: this.extent()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlZoomToExtentDirective_BaseFactory; return function OlZoomToExtentDirective_Factory(__ngFactoryType__) { return (ɵOlZoomToExtentDirective_BaseFactory || (ɵOlZoomToExtentDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlZoomToExtentDirective)))(__ngFactoryType__ || OlZoomToExtentDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlZoomToExtentDirective, selectors: [["ol-zoom-to-extent"]], inputs: { className: [1, "className"], label: [1, "label"], tipLabel: [1, "tipLabel"], extent: [1, "extent"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlZoomToExtentDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlZoomToExtentDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-zoom-to-extent',
                providers: [{ provide: OlControl, useExisting: OlZoomToExtentDirective }]
            }]
    }], null, null); })();

/**
 * Directive to integrate OpenLayers Rotate control into Angular components.
 *
 * Extends the OlControl base class to provide bindings for the Rotate control in OpenLayers.
 * This directive allows customizing various options for the Rotate control using Angular's
 * reactive input system.
 *
 * Selector: `ol-rotate`
 *
 * Inputs:
 * - `className` (string): CSS class name to set to the rotate control element.
 * - `label` (string): The label to display on the rotate control.
 * - `compassClassName` (string): CSS class name for the compass element within the control.
 * - `tipLabel` (string): Displayed tooltip text for the button.
 * - `duration` (number): The animation duration in milliseconds for the rotation reset.
 * - `autoHide` (boolean): If true, the control is hidden when the map rotation is 0.
 *
 * Control:
 * - Computed `control` property creates a new Rotate instance with the given configuration.
 */
class OlRotateDirective extends OlControl {
    constructor() {
        super(...arguments);
        this.className = input();
        this.label = input();
        this.compassClassName = input();
        this.tipLabel = input();
        this.duration = input(250, { transform: numberAttribute });
        this.autoHide = input(true, { transform: booleanAttribute });
        this.control = computed(() => new Rotate({
            className: this.className(),
            label: this.label(),
            tipLabel: this.tipLabel(),
            compassClassName: this.compassClassName(),
            duration: this.duration(),
            autoHide: this.autoHide()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlRotateDirective_BaseFactory; return function OlRotateDirective_Factory(__ngFactoryType__) { return (ɵOlRotateDirective_BaseFactory || (ɵOlRotateDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlRotateDirective)))(__ngFactoryType__ || OlRotateDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlRotateDirective, selectors: [["ol-rotate"]], inputs: { className: [1, "className"], label: [1, "label"], compassClassName: [1, "compassClassName"], tipLabel: [1, "tipLabel"], duration: [1, "duration"], autoHide: [1, "autoHide"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlControl, useExisting: OlRotateDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlRotateDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-rotate',
                providers: [{ provide: OlControl, useExisting: OlRotateDirective }]
            }]
    }], null, null); })();

/**
 * A directive that provides a set of default controls for OpenLayers maps.
 * This directive automatically adds the specified controls to the parent
 * `OlControlsDirective` when initialized.
 *
 * By default, the following controls are included:
 * - Zoom control
 * - Rotate control
 * - Attribution control
 *
 * The directive relies on the framework's dependency injection mechanism
 * to obtain references to the requisite control directives.
 *
 * This directive should be used as an attribute on a container element,
 * and it will automatically synchronize the default controls with the map.
 */
class OlDefaultControlDirective {
    #controls = inject(OlControlsDirective);
    #zoomControl = inject(OlZoomControlDirective);
    #rotateControl = inject(OlRotateDirective);
    #attributionControl = inject(OlAttributionDirective);
    ngOnInit() {
        this.#controls.addControl(this.#zoomControl);
        this.#controls.addControl(this.#rotateControl);
        this.#controls.addControl(this.#attributionControl);
    }
    static { this.ɵfac = function OlDefaultControlDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlDefaultControlDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlDefaultControlDirective, selectors: [["", "defaultControls", ""]], features: [i0.ɵɵHostDirectivesFeature([OlZoomControlDirective, OlRotateDirective, OlAttributionDirective])] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlDefaultControlDirective, [{
        type: Directive,
        args: [{
                selector: '[defaultControls]',
                hostDirectives: [
                    OlZoomControlDirective,
                    OlRotateDirective,
                    OlAttributionDirective
                ]
            }]
    }], null, null); })();

/*
 * Public API Surface of ngx-ol-control
 */

/**
 * Generated bundle index. Do not edit.
 */

export { OlAttributionDirective, OlDefaultControlDirective, OlFullScreenDirective, OlMousePositionDirective, OlOverviewMapDirective, OlRotateDirective, OlScaleLineDirective, OlZoomControlDirective, OlZoomToExtentDirective };
//# sourceMappingURL=nasumilu-ngx-ol-control.mjs.map
