export * from './lib/ol-base-layer.directive';
export * from './lib/ol-vector-layer.directive';
export * from './lib/ol-tile-layer.directive';
export * from './lib/ol-image-layer.directive';
//# sourceMappingURL=public-api.d.ts.map