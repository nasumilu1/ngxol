import * as i0 from '@angular/core';
import { inject, Directive, model, input, computed, contentChildren, output, booleanAttribute, numberAttribute } from '@angular/core';
import { OlLayer } from '@nasumilu/ngx-ol-map';
import { unByKey } from 'ol/Observable';
import LayerGroup from 'ol/layer/Group';
import Collection from 'ol/Collection';
import VectorLayer from 'ol/layer/Vector';
import Graticule from 'ol/layer/Graticule';
import VectorImageLayer from 'ol/layer/VectorImage';
import Heatmap from 'ol/layer/Heatmap';
import VectorTileLayer from 'ol/layer/VectorTile';
import TileLayer from 'ol/layer/Tile';
import WebGLTileLayer from 'ol/layer/WebGLTile';
import ImageLayer from 'ol/layer/Image';

class AbstractLayerDirective {
    constructor() {
        this.layerDirective = inject(OlLayer);
        this.eventKeys = [];
    }
    ngOnDestroy() {
        unByKey(this.eventKeys);
        this.eventKeys = [];
    }
    static { this.ɵfac = function AbstractLayerDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || AbstractLayerDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: AbstractLayerDirective }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AbstractLayerDirective, [{
        type: Directive
    }], null, null); })();
class OpacityDirective extends AbstractLayerDirective {
    constructor() {
        super(...arguments);
        this.opacity = model(1.0);
    }
    ngOnInit() {
        const layer = this.layerDirective.layer();
        layer.setOpacity(this.opacity());
        this.opacity.subscribe(value => {
            const opacity = layer.getOpacity();
            if (opacity !== value) {
                layer.setOpacity(value);
            }
        });
        this.eventKeys.push(layer.on('change:opacity', () => {
            const opacity = layer.getOpacity();
            if (this.opacity() !== opacity) {
                this.opacity.set(opacity);
            }
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOpacityDirective_BaseFactory; return function OpacityDirective_Factory(__ngFactoryType__) { return (ɵOpacityDirective_BaseFactory || (ɵOpacityDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OpacityDirective)))(__ngFactoryType__ || OpacityDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OpacityDirective, inputs: { opacity: [1, "opacity"] }, outputs: { opacity: "opacityChange" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OpacityDirective, [{
        type: Directive
    }], null, null); })();
class VisibleDirective extends AbstractLayerDirective {
    constructor() {
        super(...arguments);
        this.visible = model(true);
    }
    ngOnInit() {
        const layer = this.layerDirective.layer();
        layer.setVisible(this.visible());
        this.visible.subscribe(value => {
            const visible = layer.getVisible();
            if (visible !== value) {
                layer.setVisible(value);
            }
        });
        this.eventKeys.push(layer.on('change:visible', () => {
            const visible = layer.getVisible();
            if (this.visible() !== visible) {
                this.visible.set(visible);
            }
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵVisibleDirective_BaseFactory; return function VisibleDirective_Factory(__ngFactoryType__) { return (ɵVisibleDirective_BaseFactory || (ɵVisibleDirective_BaseFactory = i0.ɵɵgetInheritedFactory(VisibleDirective)))(__ngFactoryType__ || VisibleDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: VisibleDirective, inputs: { visible: [1, "visible"] }, outputs: { visible: "visibleChange" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(VisibleDirective, [{
        type: Directive
    }], null, null); })();
/**
 * The ExtentDirective is an Angular directive that manages the synchronization
 * of an OpenLayers layer's extent with a reactive model property.
 *
 * This directive is used to dynamically manage the extent of a map layer in response
 * to changes in an observable model property or changes within the map itself.
 *
 * The directive's primary responsibilities include:
 * - Setting the initial extent of the OpenLayers layer based on the reactive model.
 * - Subscribing to changes in the model property and updating the layer's extent accordingly.
 * - Listening for changes in the layer's extent and updating the reactive model property.
 *
 * Lifecycle Hooks:
 * - `ngOnInit`: Initializes the synchronization between the extent model property and
 *   the OpenLayers layer's extent.
 * - `ngOnDestroy`: Cleans up event listeners and other resources associated with the directive.
 */
class ExtentDirective extends AbstractLayerDirective {
    constructor() {
        super(...arguments);
        this.extent = model();
    }
    ngOnInit() {
        const layer = this.layerDirective.layer();
        layer.setExtent(this.extent());
        this.extent.subscribe(value => {
            const extent = layer.getExtent();
            if (extent !== value) {
                layer.setExtent(value);
            }
        });
        this.eventKeys.push(layer.on('change:extent', () => {
            const extent = layer.getExtent();
            if (this.extent() !== extent) {
                this.extent.set(extent);
            }
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵExtentDirective_BaseFactory; return function ExtentDirective_Factory(__ngFactoryType__) { return (ɵExtentDirective_BaseFactory || (ɵExtentDirective_BaseFactory = i0.ɵɵgetInheritedFactory(ExtentDirective)))(__ngFactoryType__ || ExtentDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: ExtentDirective, inputs: { extent: [1, "extent"] }, outputs: { extent: "extentChange" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ExtentDirective, [{
        type: Directive
    }], null, null); })();
class ZIndexDirective extends AbstractLayerDirective {
    constructor() {
        super(...arguments);
        this.zIndex = model();
    }
    ngOnInit() {
        const layer = this.layerDirective.layer();
        layer.setZIndex(this.zIndex());
        this.zIndex.subscribe(value => {
            const zIndex = layer.getZIndex();
            if (zIndex !== value) {
                layer.setZIndex(value);
            }
        });
        this.eventKeys.push(layer.on('change:zIndex', () => {
            const zIndex = layer.getZIndex();
            if (this.zIndex() !== zIndex) {
                this.zIndex.set(zIndex);
            }
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵZIndexDirective_BaseFactory; return function ZIndexDirective_Factory(__ngFactoryType__) { return (ɵZIndexDirective_BaseFactory || (ɵZIndexDirective_BaseFactory = i0.ɵɵgetInheritedFactory(ZIndexDirective)))(__ngFactoryType__ || ZIndexDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: ZIndexDirective, inputs: { zIndex: [1, "zIndex"] }, outputs: { zIndex: "zIndexChange" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ZIndexDirective, [{
        type: Directive
    }], null, null); })();
class MinMaxLayerZoomDirective extends AbstractLayerDirective {
    constructor() {
        super(...arguments);
        this.minZoom = model(-Infinity);
        this.maxZoom = model(Infinity);
    }
    ngOnInit() {
        const layer = this.layerDirective.layer();
        layer.setMinZoom(this.minZoom());
        layer.setMaxZoom(this.maxZoom());
        this.minZoom.subscribe(value => {
            const minZoom = layer.getMinZoom();
            if (minZoom !== value) {
                layer.setMinZoom(value);
            }
        });
        this.maxZoom.subscribe(value => {
            const maxZoom = layer.getMaxZoom();
            if (maxZoom !== value) {
                layer.setMaxZoom(value);
            }
        });
        this.eventKeys.push(layer.on('change:minZoom', () => {
            const minZoom = layer.getMinZoom();
            if (this.minZoom() !== minZoom) {
                this.minZoom.set(minZoom);
            }
        }));
        this.eventKeys.push(layer.on('change:maxZoom', () => {
            const maxZoom = layer.getMaxZoom();
            if (this.maxZoom() !== maxZoom) {
                this.maxZoom.set(maxZoom);
            }
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵMinMaxLayerZoomDirective_BaseFactory; return function MinMaxLayerZoomDirective_Factory(__ngFactoryType__) { return (ɵMinMaxLayerZoomDirective_BaseFactory || (ɵMinMaxLayerZoomDirective_BaseFactory = i0.ɵɵgetInheritedFactory(MinMaxLayerZoomDirective)))(__ngFactoryType__ || MinMaxLayerZoomDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: MinMaxLayerZoomDirective, inputs: { minZoom: [1, "minZoom"], maxZoom: [1, "maxZoom"] }, outputs: { minZoom: "minZoomChange", maxZoom: "maxZoomChange" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(MinMaxLayerZoomDirective, [{
        type: Directive
    }], null, null); })();
class MinMaxResolutionDirective extends AbstractLayerDirective {
    constructor() {
        super(...arguments);
        this.minResolution = model(0);
        this.maxResolution = model(Infinity);
    }
    ngOnInit() {
        const layer = this.layerDirective.layer();
        layer.setMinResolution(this.minResolution());
        layer.setMaxResolution(this.maxResolution());
        this.minResolution.subscribe(value => {
            const minResolution = layer.getMinResolution();
            if (minResolution !== value) {
                layer.setMinResolution(value);
            }
        });
        this.maxResolution.subscribe(value => {
            const maxResolution = layer.getMaxResolution();
            if (maxResolution !== value) {
                layer.setMaxResolution(value);
            }
        });
        this.eventKeys.push(layer.on('change:minResolution', () => {
            const minResolution = layer.getMinResolution();
            if (this.minResolution() !== minResolution) {
                this.minResolution.set(minResolution);
            }
        }));
        this.eventKeys.push(layer.on('change:maxResolution', () => {
            const maxResolution = layer.getMaxResolution();
            if (this.maxResolution() !== maxResolution) {
                this.maxResolution.set(maxResolution);
            }
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵMinMaxResolutionDirective_BaseFactory; return function MinMaxResolutionDirective_Factory(__ngFactoryType__) { return (ɵMinMaxResolutionDirective_BaseFactory || (ɵMinMaxResolutionDirective_BaseFactory = i0.ɵɵgetInheritedFactory(MinMaxResolutionDirective)))(__ngFactoryType__ || MinMaxResolutionDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: MinMaxResolutionDirective, inputs: { minResolution: [1, "minResolution"], maxResolution: [1, "maxResolution"] }, outputs: { minResolution: "minResolutionChange", maxResolution: "maxResolutionChange" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(MinMaxResolutionDirective, [{
        type: Directive
    }], null, null); })();
/**
 * Abstract base directive for OpenLayers layers, providing common functionality
 * for managing layer configuration and lifecycle. This class is designed to
 * simplify the integration of OpenLayers layers within an Angular application.
 *
 * @template L The type of the OpenLayers BaseLayer that this directive manages.
 */
class OlBaseLayerDirective extends OlLayer {
    constructor() {
        super(...arguments);
        this.background = input();
        this.layer = computed(() => this.initLayer({ background: this.background() }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlBaseLayerDirective_BaseFactory; return function OlBaseLayerDirective_Factory(__ngFactoryType__) { return (ɵOlBaseLayerDirective_BaseFactory || (ɵOlBaseLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlBaseLayerDirective)))(__ngFactoryType__ || OlBaseLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlBaseLayerDirective, inputs: { background: [1, "background"] }, features: [i0.ɵɵHostDirectivesFeature([{ directive: OpacityDirective, inputs: ["opacity", "opacity"], outputs: ["opacityChange", "opacityChange"] }, { directive: VisibleDirective, inputs: ["visible", "visible"], outputs: ["visibleChange", "visibleChange"] }, { directive: ExtentDirective, inputs: ["extent", "extent"], outputs: ["extentChange", "extentChange"] }, { directive: ZIndexDirective, inputs: ["zIndex", "zIndex"], outputs: ["zIndexChange", "zIndexChange"] }, { directive: MinMaxLayerZoomDirective, inputs: ["minZoom", "minZoom", "maxZoom", "maxZoom"], outputs: ["minZoomChange", "minZoomChange", "maxZoomChange", "maxZoomChange"] }, { directive: MinMaxResolutionDirective, inputs: ["minResolution", "minResolution", "maxResolution", "maxResolution"], outputs: ["minResolutionChange", "minResolutionChange", "maxResolutionChange", "maxResolutionChange"] }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlBaseLayerDirective, [{
        type: Directive,
        args: [{
                hostDirectives: [
                    { directive: OpacityDirective, inputs: ['opacity'], outputs: ['opacityChange'] },
                    { directive: VisibleDirective, inputs: ['visible'], outputs: ['visibleChange'] },
                    { directive: ExtentDirective, inputs: ['extent'], outputs: ['extentChange'] },
                    { directive: ZIndexDirective, inputs: ['zIndex'], outputs: ['zIndexChange'] },
                    { directive: MinMaxLayerZoomDirective, inputs: ['minZoom', 'maxZoom'], outputs: ['minZoomChange', 'maxZoomChange'] },
                    { directive: MinMaxResolutionDirective, inputs: ['minResolution', 'maxResolution'], outputs: ['minResolutionChange', 'maxResolutionChange'] },
                ]
            }]
    }], null, null); })();
/**
 * Directive to manage OpenLayers LayerGroup components in Angular applications.
 * Acts as a wrapper for LayerGroup functionalities and integrates with Angular's lifecycle hooks.
 *
 * This directive is used to create and manage a group of layers (LayerGroup) in an OpenLayers map,
 * and provides capabilities to synchronize Angular component layers within the group.
 * It supports interaction with the OlLayerDirective to aggregate individual layers into a Collection.
 *
 * - Automatically initializes a LayerGroup instance with the specified options.
 * - Collects individual layers provided in the template using content children and adds them to the group.
 * - Ensures synchronization of Angular component layers with the underlying OpenLayers LayerGroup.
 *
 * Implements the AfterContentInit interface to manage component's content initialization and layer aggregation.
 */
class OlGroupLayerDirective extends OlBaseLayerDirective {
    constructor() {
        super(...arguments);
        this.#olLayers = new Collection();
        this.#layers = new Collection();
        this.#eventKey = [];
        this.layers = contentChildren(OlLayer);
        this.layerAdded = output();
        this.layerRemoved = output();
    }
    #olLayers;
    #layers;
    #eventKey;
    initLayer(options) {
        options.layers = this.#layers;
        return new LayerGroup(options);
    }
    getLayers() {
        return this.#olLayers.getArray();
    }
    find(predicate) {
        return this.getLayers().find(predicate);
    }
    filter(predicate) {
        return this.getLayers().filter(predicate);
    }
    addLayer(layer) {
        this.#olLayers.push(layer);
    }
    removeLayer(layer) {
        this.#olLayers.remove(layer);
    }
    ngOnInit() {
        this.#eventKey.push(this.#olLayers.on('add', evt => {
            this.#layers.push(evt.element.layer());
            this.layerAdded.emit(evt.element);
        }));
        this.#eventKey.push(this.#olLayers.on('remove', evt => {
            this.#layers.remove(evt.element.layer());
            this.layerRemoved.emit(evt.element);
        }));
    }
    ngAfterContentInit() {
        this.layers().forEach(OlGroupLayerDirective.prototype.addLayer.bind(this));
    }
    ngOnDestroy() {
        unByKey(this.#eventKey);
        this.#eventKey = [];
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlGroupLayerDirective_BaseFactory; return function OlGroupLayerDirective_Factory(__ngFactoryType__) { return (ɵOlGroupLayerDirective_BaseFactory || (ɵOlGroupLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlGroupLayerDirective)))(__ngFactoryType__ || OlGroupLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlGroupLayerDirective, selectors: [["ol-group-layer"]], contentQueries: function OlGroupLayerDirective_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.layers, OlLayer, 4);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, outputs: { layerAdded: "layerAdded", layerRemoved: "layerRemoved" }, features: [i0.ɵɵProvidersFeature([{ provide: OlLayer, useExisting: OlGroupLayerDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlGroupLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-group-layer',
                providers: [{ provide: OlLayer, useExisting: OlGroupLayerDirective }]
            }]
    }], null, null); })();
/**
 * Abstract class representing a directive for an OpenLayers (OL) layer. This class provides base functionality
 * for managing an OpenLayers Layer instance, including its source and associated lifecycle events.
 * It extends the functionality of `OlBaseLayerDirective` and implements the `OnInit` and `OnDestroy` Angular lifecycle hooks.
 *
 * @template L The type of the OpenLayers layer, extending the base Layer class.
 */
class OlLayerDirective extends OlBaseLayerDirective {
    constructor() {
        super(...arguments);
        this.eventKeys = [];
        this.postRender = output();
        this.preRender = output();
    }
    ngOnInit() {
        const layer = this.layer();
        this.eventKeys.push(layer.on('postrender', (event) => {
            if (event.frameState) {
                this.postRender.emit(event.frameState);
            }
        }));
        this.eventKeys.push(layer.on('prerender', (event) => {
            if (event.frameState) {
                this.preRender.emit(event.frameState);
            }
        }));
    }
    ngOnDestroy() {
        unByKey(this.eventKeys);
        this.eventKeys = [];
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlLayerDirective_BaseFactory; return function OlLayerDirective_Factory(__ngFactoryType__) { return (ɵOlLayerDirective_BaseFactory || (ɵOlLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlLayerDirective)))(__ngFactoryType__ || OlLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlLayerDirective, outputs: { postRender: "postRender", preRender: "preRender" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlLayerDirective, [{
        type: Directive
    }], null, null); })();
/**
 * The `SourceDirective` is an Angular directive that manages the OpenLayers source of a layer.
 * It binds the source of an OpenLayers layer to a reactive model, ensuring synchronization
 * between the layer's source and the underlying model. The directive is meant to be used with
 * the `OlLayerDirective`.
 *
 * Features:
 * - Initializes the layer's source based on the value of the reactive `source` model.
 * - Synchronizes changes between the layer's source and the `source` model.
 * - Cleans up any registered events during destruction.
 *
 * Lifecycle:
 * - In `ngOnInit`, binds the given source to the layer and listens for source changes.
 * - In `ngOnDestroy`, cleans up event listeners to prevent memory leaks.
 */
class SourceDirective {
    constructor() {
        this.#layerDirective = inject(OlLayerDirective);
        this.source = model(null);
    }
    #layerDirective;
    #eventKey;
    ngOnInit() {
        const layer = this.#layerDirective.layer();
        layer.setSource(this.source() ?? null);
        this.source.subscribe(value => {
            const source = layer.getSource();
            if (source !== value) {
                layer.setSource(value);
            }
        });
        this.#eventKey = layer.on('change:source', () => {
            const source = layer.getSource();
            if (this.source() !== source) {
                this.source.set(source);
            }
        });
    }
    ngOnDestroy() {
        if (this.#eventKey) {
            unByKey(this.#eventKey);
        }
    }
    static { this.ɵfac = function SourceDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || SourceDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: SourceDirective, selectors: [["", "source", ""]], inputs: { source: [1, "source"] }, outputs: { source: "sourceChange" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(SourceDirective, [{
        type: Directive,
        args: [{
                selector: '[source]'
            }]
    }], null, null); })();

/**
 * OlBaseVectorLayerDirective serves as a base class for creating directives that
 * manage OpenLayers vector layers.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `style`: The layer's StyleLike | FlatStyleLike
 *
 */
class OlBaseVectorLayerDirective extends OlLayerDirective {
    constructor() {
        super(...arguments);
        this.className = input();
        this.updateWhileAnimating = input(false, { transform: booleanAttribute });
        this.updateWhileInteracting = input(false, { transform: booleanAttribute });
        this.declutter = input();
        this.style = model();
        this.layer = computed(() => this.initLayer({
            className: this.className(),
            style: this.style(),
            updateWhileAnimating: this.updateWhileAnimating(),
            updateWhileInteracting: this.updateWhileInteracting(),
            declutter: this.declutter(),
            background: this.background()
        }));
    }
    ngOnInit() {
        super.ngOnInit();
        const layer = this.layer();
        this.style.subscribe(value => {
            layer.setStyle(value);
        });
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlBaseVectorLayerDirective_BaseFactory; return function OlBaseVectorLayerDirective_Factory(__ngFactoryType__) { return (ɵOlBaseVectorLayerDirective_BaseFactory || (ɵOlBaseVectorLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlBaseVectorLayerDirective)))(__ngFactoryType__ || OlBaseVectorLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlBaseVectorLayerDirective, inputs: { className: [1, "className"], updateWhileAnimating: [1, "updateWhileAnimating"], updateWhileInteracting: [1, "updateWhileInteracting"], declutter: [1, "declutter"], style: [1, "style"] }, outputs: { style: "styleChange" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlBaseVectorLayerDirective, [{
        type: Directive
    }], null, null); })();
/**
 * Directive representing an OpenLayers vector layer.
 *
 * This directive is used to integrate vector layers with Angular applications, allowing
 * seamless interaction with the OpenLayers library. Through dependency injection, it provides
 * the base layer functionality and reuses `OlBaseVectorLayerDirective`.
 *
 * The directive is associated with the `ol-vector-layer` selector and provides itself
 * as both `OlLayer` and `OlBaseVectorLayerDirective` for other components or classes to
 * depend on.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 */
class OlVectorLayerDirective extends OlBaseVectorLayerDirective {
    initLayer(options) {
        return new VectorLayer(options);
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlVectorLayerDirective_BaseFactory; return function OlVectorLayerDirective_Factory(__ngFactoryType__) { return (ɵOlVectorLayerDirective_BaseFactory || (ɵOlVectorLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlVectorLayerDirective)))(__ngFactoryType__ || OlVectorLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlVectorLayerDirective, selectors: [["ol-vector-layer"]], features: [i0.ɵɵProvidersFeature([
                { provide: OlLayer, useExisting: OlVectorLayerDirective },
                { provide: OlLayerDirective, useExisting: OlVectorLayerDirective },
                { provide: OlBaseVectorLayerDirective, useExisting: OlVectorLayerDirective }
            ]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlVectorLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-vector-layer',
                providers: [
                    { provide: OlLayer, useExisting: OlVectorLayerDirective },
                    { provide: OlLayerDirective, useExisting: OlVectorLayerDirective },
                    { provide: OlBaseVectorLayerDirective, useExisting: OlVectorLayerDirective }
                ]
            }]
    }], null, null); })();
/**
 * Converts a string of comma-separated numbers into an array of numbers
 * or returns the input array of numbers as is.
 *
 * @param {number[]|string} value - The input value, which can be either an
 * array of numbers or a string of comma-separated numbers.
 * @return {number[]} An array of numbers parsed from the input string or
 * the input array if already provided as numbers.
 */
function intervalAttribute(value) {
    if (typeof value === 'string') {
        return value.split(',').map(parseFloat);
    }
    return value;
}
/**
 * A directive that represents an OpenLayers graticule layer in an Angular application.
 * The graticule layer renders a grid representing meridians (longitude) and parallels (latitude)
 * on a map. This directive extends the base vector layer directive and provides additional
 * configuration options specific to graticule rendering.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - maxLines: Defines the maximum number of grid lines to render.
 * - targetSize: Determines the target size for grid spacing.
 * - showLabels: Toggles the visibility of the longitude and latitude labels.
 * - lonLabelFormatter: Custom function to format longitude labels.
 * - latLabelFormatter: Custom function to format latitude labels.
 * - lonlatPosition: Specifies the longitude label position relative to the grid lines.
 * - latlonPosition: Specifies the latitude label position relative to the grid lines.
 * - strokeStyle: Configures the styling of the grid lines using a `Stroke` object.
 * - lonLabelStyle: Specifies the text style for longitude labels using a `Text` object.
 * - latLabelStyle: Specifies the text style for latitude labels using a `Text` object.
 * - intervals: Array of intervals to determine the spacing of grid lines, defined in degrees.
 * - wrapX: Determines whether the grid wraps horizontally across the world.
 *
 * The directive automatically integrates with OpenLayers and Angular dependency injection
 * to act as a vector layer, allowing for interaction with additional layers and features.
 */
class OlGraticleLayerDirective extends OlBaseVectorLayerDirective {
    constructor() {
        super(...arguments);
        this.maxLines = input(100, { transform: numberAttribute });
        this.targetSize = input(100, { transform: numberAttribute });
        this.showLabels = input(true, { transform: booleanAttribute });
        this.lonLabelFormatter = input();
        this.latLabelFormatter = input();
        this.lonlatPosition = input(0, { transform: numberAttribute });
        this.latlonPosition = input(1, { transform: numberAttribute });
        this.strokeStyle = input();
        this.lonLabelStyle = input();
        this.latLabelStyle = input();
        this.intervals = input([
            90, 45, 30, 20, 10, 5, 2, 1, 30 / 60, 20 / 60, 10 / 60, 5 / 60, 2 / 60, 1 / 60, 30 / 3600, 20 / 3600, 10 / 3600, 5 / 3600, 2 / 3600, 1 / 3600
        ], { transform: intervalAttribute });
        this.wrapX = input(true, { transform: booleanAttribute });
    }
    initLayer(options) {
        options = Object.assign(options, {
            maxLines: this.maxLines(),
            targetSize: this.targetSize(),
            showLabels: this.showLabels(),
            lonLabelFormatter: this.lonLabelFormatter(),
            latlonLabelFormatter: this.latLabelFormatter(),
            lonlatPosition: this.lonlatPosition(),
            latlonPosition: this.latlonPosition(),
            strokeStyle: this.strokeStyle(),
            lonLabelStyle: this.lonLabelStyle(),
            latLabelStyle: this.latLabelStyle(),
            intervals: this.intervals(),
            wrapX: this.wrapX()
        });
        return new Graticule(options);
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlGraticleLayerDirective_BaseFactory; return function OlGraticleLayerDirective_Factory(__ngFactoryType__) { return (ɵOlGraticleLayerDirective_BaseFactory || (ɵOlGraticleLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlGraticleLayerDirective)))(__ngFactoryType__ || OlGraticleLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlGraticleLayerDirective, selectors: [["ol-graticle-layer"]], inputs: { maxLines: [1, "maxLines"], targetSize: [1, "targetSize"], showLabels: [1, "showLabels"], lonLabelFormatter: [1, "lonLabelFormatter"], latLabelFormatter: [1, "latLabelFormatter"], lonlatPosition: [1, "lonlatPosition"], latlonPosition: [1, "latlonPosition"], strokeStyle: [1, "strokeStyle"], lonLabelStyle: [1, "lonLabelStyle"], latLabelStyle: [1, "latLabelStyle"], intervals: [1, "intervals"], wrapX: [1, "wrapX"] }, features: [i0.ɵɵProvidersFeature([
                { provide: OlLayer, useExisting: OlGraticleLayerDirective },
                { provide: OlLayerDirective, useExisting: OlGraticleLayerDirective },
                { provide: OlBaseVectorLayerDirective, useExisting: OlGraticleLayerDirective }
            ]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlGraticleLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-graticle-layer',
                providers: [
                    { provide: OlLayer, useExisting: OlGraticleLayerDirective },
                    { provide: OlLayerDirective, useExisting: OlGraticleLayerDirective },
                    { provide: OlBaseVectorLayerDirective, useExisting: OlGraticleLayerDirective }
                ]
            }]
    }], null, null); })();
/**
 * Directive to configure and create an OpenLayers `VectorImageLayer` component.
 *
 * This class extends the `OlBaseVectorLayerDirective` to provide specific functionalities
 * for `VectorImageLayer` which supports rendering vector data with rasterization for better
 * performance in cases of large datasets or complex geometries.
 *
 * The `OlVectorImageLayerDirective` allows binding for specific configuration options
 * including the `imageRatio`, which determines the resolution of the raster images
 * used to draw vector data.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `imageRatio`: A readable input property defining the ratio between the physical and logical pixel size for the rasterization process.
 */
class OlVectorImageLayerDirective extends OlBaseVectorLayerDirective {
    constructor() {
        super(...arguments);
        this.imageRatio = input(1, { transform: numberAttribute });
    }
    initLayer(options) {
        options = Object.assign(options, { imageRatio: this.imageRatio() });
        return new VectorImageLayer(options);
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlVectorImageLayerDirective_BaseFactory; return function OlVectorImageLayerDirective_Factory(__ngFactoryType__) { return (ɵOlVectorImageLayerDirective_BaseFactory || (ɵOlVectorImageLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlVectorImageLayerDirective)))(__ngFactoryType__ || OlVectorImageLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlVectorImageLayerDirective, selectors: [["ol-vector-image-layer"]], inputs: { imageRatio: [1, "imageRatio"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlVectorImageLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-vector-image-layer'
            }]
    }], null, null); })();
/**
 * Directive to integrate OpenLayers Heatmap layer into Angular applications.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `blur`: Reactive property to manage the blur size of the heatmap layer.
 * - `radius`: Reactive property to control the size of the radius for each feature.
 * - `gradient`: Reactive property for setting the color gradient of the heatmap.
 *
 * Outputs:
 * - `blurChanged`:
 * - `radiusChanged`:
 * - `gradientChanged`:
 */
class OlHeatmapLayerDirective extends OlBaseVectorLayerDirective {
    constructor() {
        super(...arguments);
        this.blur = model(15);
        this.radius = model(8);
        this.gradient = model(['#00f', '#0ff', '#0f0', '#ff0', '#f00']);
    }
    initLayer(options) {
        options = Object.assign(options, {
            blur: this.blur(),
            radius: this.radius(),
            gradient: this.gradient()
        });
        return new Heatmap(options);
    }
    ngOnInit() {
        super.ngOnInit();
        const layer = this.layer();
        this.blur.subscribe(value => {
            const blur = layer.getBlur();
            if (blur !== value) {
                layer.setBlur(value);
            }
        });
        // @ts-ignore
        this.eventKeys.push(layer.on('change:blur', () => {
            const blur = layer.getBlur();
            if (this.blur() !== blur) {
                this.blur.set(blur);
            }
        }));
        this.radius.subscribe(value => {
            const radius = layer.getRadius();
            if (radius !== value) {
                layer.setRadius(value);
            }
        });
        // @ts-ignore
        this.eventKeys.push(layer.on('change:radius', () => {
            const radius = layer.getRadius();
            if (this.radius() !== radius) {
                this.radius.set(radius);
            }
        }));
        this.gradient.subscribe(value => {
            const gradient = layer.getGradient();
            if (gradient !== value) {
                layer.setGradient(value);
            }
        });
        // @ts-ignore
        this.eventKeys.push(layer.on('change:gradient', () => {
            const gradient = layer.getGradient();
            if (this.gradient() !== gradient) {
                this.gradient.set(gradient);
            }
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlHeatmapLayerDirective_BaseFactory; return function OlHeatmapLayerDirective_Factory(__ngFactoryType__) { return (ɵOlHeatmapLayerDirective_BaseFactory || (ɵOlHeatmapLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlHeatmapLayerDirective)))(__ngFactoryType__ || OlHeatmapLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlHeatmapLayerDirective, selectors: [["ol-heatmap-layer"]], inputs: { blur: [1, "blur"], radius: [1, "radius"], gradient: [1, "gradient"] }, outputs: { blur: "blurChange", radius: "radiusChange", gradient: "gradientChange" }, features: [i0.ɵɵProvidersFeature([
                { provide: OlLayer, useExisting: OlHeatmapLayerDirective },
                { provide: OlLayerDirective, useExisting: OlHeatmapLayerDirective },
                { provide: OlBaseVectorLayerDirective, useExisting: OlHeatmapLayerDirective }
            ]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlHeatmapLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-heatmap-layer',
                providers: [
                    { provide: OlLayer, useExisting: OlHeatmapLayerDirective },
                    { provide: OlLayerDirective, useExisting: OlHeatmapLayerDirective },
                    { provide: OlBaseVectorLayerDirective, useExisting: OlHeatmapLayerDirective }
                ]
            }]
    }], null, null); })();
/**
 * Directive for creating and configuring an OpenLayers vector tile layer.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `preload`: Sets the number of low-resolution parent tiles to preload. This helps improve perceived performance when the map is panned or zoomed.
 * - `renderMode`: Determines the rendering mode for vector tiles. Possible values are `'hybrid'`, `'vector'`, or `'image'`. The default value is `'hybrid'`.
 */
class OlVectorTileLayerDirective extends OlBaseVectorLayerDirective {
    constructor() {
        super(...arguments);
        this.preload = input(0, { transform: numberAttribute });
        this.renderMode = input('hybrid');
    }
    initLayer(options) {
        options = Object.assign(options, {
            preload: this.preload(),
            renderMode: this.renderMode()
        });
        return new VectorTileLayer(options);
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlVectorTileLayerDirective_BaseFactory; return function OlVectorTileLayerDirective_Factory(__ngFactoryType__) { return (ɵOlVectorTileLayerDirective_BaseFactory || (ɵOlVectorTileLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlVectorTileLayerDirective)))(__ngFactoryType__ || OlVectorTileLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlVectorTileLayerDirective, selectors: [["ol-vector-tile-layer"]], inputs: { preload: [1, "preload"], renderMode: [1, "renderMode"] }, features: [i0.ɵɵProvidersFeature([
                { provide: OlLayer, useExisting: OlVectorTileLayerDirective },
                { provide: OlLayerDirective, useExisting: OlVectorTileLayerDirective },
                { provide: OlBaseVectorLayerDirective, useExisting: OlVectorTileLayerDirective }
            ]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlVectorTileLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-vector-tile-layer',
                providers: [
                    { provide: OlLayer, useExisting: OlVectorTileLayerDirective },
                    { provide: OlLayerDirective, useExisting: OlVectorTileLayerDirective },
                    { provide: OlBaseVectorLayerDirective, useExisting: OlVectorTileLayerDirective }
                ]
            }]
    }], null, null); })();

/**
 * Represents an abstract base directive for OpenLayers tile layers, providing
 * common properties and configuration options for tile-based map layers.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
class OlBaseTileLayerDirective extends OlLayerDirective {
    constructor() {
        super(...arguments);
        this.className = input();
        this.preload = input(0, { transform: numberAttribute });
        this.useInterimTilesOnError = input(true, { transform: booleanAttribute });
        this.cacheSize = input(512, { transform: numberAttribute });
        this.layer = computed(() => this.initLayer({
            className: this.className(),
            preload: this.preload(),
            useInterimTilesOnError: this.useInterimTilesOnError(),
            cacheSize: this.cacheSize(),
            background: this.background()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlBaseTileLayerDirective_BaseFactory; return function OlBaseTileLayerDirective_Factory(__ngFactoryType__) { return (ɵOlBaseTileLayerDirective_BaseFactory || (ɵOlBaseTileLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlBaseTileLayerDirective)))(__ngFactoryType__ || OlBaseTileLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlBaseTileLayerDirective, inputs: { className: [1, "className"], preload: [1, "preload"], useInterimTilesOnError: [1, "useInterimTilesOnError"], cacheSize: [1, "cacheSize"] }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlBaseTileLayerDirective, [{
        type: Directive
    }], null, null); })();
/**
 * The OlTileLayerDirective is an Angular directive that represents an OpenLayers TileLayer.
 * The directive is registered with the selector 'ol-tile-layer' for use in component templates.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
class OlTileLayerDirective extends OlBaseTileLayerDirective {
    initLayer(options) {
        return new TileLayer(options);
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlTileLayerDirective_BaseFactory; return function OlTileLayerDirective_Factory(__ngFactoryType__) { return (ɵOlTileLayerDirective_BaseFactory || (ɵOlTileLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlTileLayerDirective)))(__ngFactoryType__ || OlTileLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlTileLayerDirective, selectors: [["ol-tile-layer"]], features: [i0.ɵɵProvidersFeature([
                { provide: OlLayer, useExisting: OlTileLayerDirective },
                { provide: OlLayerDirective, useExisting: OlTileLayerDirective },
                { provide: OlBaseTileLayerDirective, useExisting: OlTileLayerDirective }
            ]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlTileLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-tile-layer',
                providers: [
                    { provide: OlLayer, useExisting: OlTileLayerDirective },
                    { provide: OlLayerDirective, useExisting: OlTileLayerDirective },
                    { provide: OlBaseTileLayerDirective, useExisting: OlTileLayerDirective }
                ]
            }]
    }], null, null); })();
/**
 * Directive used to integrate an OpenLayers WebGL tile layer in declarative setups.
 * This directive acts as a wrapper for the OpenLayers `WebGLTileLayer`, allowing it
 * to be configured and used within an Angular application.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
class OlWebGLTileLayerDirective extends OlBaseTileLayerDirective {
    constructor() {
        super(...arguments);
        this.exposure = model(0);
        this.contrast = model(0);
        this.saturation = model(0);
        this.brightness = model(0);
        this.gamma = model(1);
        this.#variables = computed(() => ({
            exposure: this.exposure(),
            contrast: this.contrast(),
            saturation: this.saturation(),
            brightness: this.brightness(),
            gamma: this.gamma()
        }));
        this.#style = {
            exposure: ['var', 'exposure'],
            contrast: ['var', 'contrast'],
            saturation: ['var', 'saturation'],
            brightness: ['var', 'brightness'],
            gamma: ['var', 'gamma'],
            variables: this.#variables()
        };
    }
    #variables;
    #style;
    initLayer(options) {
        options.style = this.#style;
        return new WebGLTileLayer(options);
    }
    ngOnInit() {
        super.ngOnInit();
        const layer = this.layer();
        this.exposure.subscribe(() => layer.updateStyleVariables(this.#variables()));
        this.contrast.subscribe(() => layer.updateStyleVariables(this.#variables()));
        this.saturation.subscribe(() => layer.updateStyleVariables(this.#variables()));
        this.brightness.subscribe(() => layer.updateStyleVariables(this.#variables()));
        this.gamma.subscribe(() => layer.updateStyleVariables(this.#variables()));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlWebGLTileLayerDirective_BaseFactory; return function OlWebGLTileLayerDirective_Factory(__ngFactoryType__) { return (ɵOlWebGLTileLayerDirective_BaseFactory || (ɵOlWebGLTileLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlWebGLTileLayerDirective)))(__ngFactoryType__ || OlWebGLTileLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlWebGLTileLayerDirective, selectors: [["ol-webgl-tile-layer"]], inputs: { exposure: [1, "exposure"], contrast: [1, "contrast"], saturation: [1, "saturation"], brightness: [1, "brightness"], gamma: [1, "gamma"] }, outputs: { exposure: "exposureChange", contrast: "contrastChange", saturation: "saturationChange", brightness: "brightnessChange", gamma: "gammaChange" }, features: [i0.ɵɵProvidersFeature([
                { provide: OlLayer, useExisting: OlWebGLTileLayerDirective },
                { provide: OlLayerDirective, useExisting: OlWebGLTileLayerDirective },
                { provide: OlBaseTileLayerDirective, useExisting: OlWebGLTileLayerDirective }
            ]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlWebGLTileLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-webgl-tile-layer',
                providers: [
                    { provide: OlLayer, useExisting: OlWebGLTileLayerDirective },
                    { provide: OlLayerDirective, useExisting: OlWebGLTileLayerDirective },
                    { provide: OlBaseTileLayerDirective, useExisting: OlWebGLTileLayerDirective }
                ]
            }]
    }], null, null); })();

/**
 * Represents an abstract base class for OpenLayers image layer directives.
 * This directive extends the functionality of `OlLayerDirective` and provides
 * a foundation for working with OpenLayers image layers in Angular applications.
 *
 * @template L Extends the OpenLayers `BaseImageLayer` which uses `ImageSource` and `LayerRenderer`.
 */
class OlBaseImageLayerDirective extends OlLayerDirective {
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlBaseImageLayerDirective_BaseFactory; return function OlBaseImageLayerDirective_Factory(__ngFactoryType__) { return (ɵOlBaseImageLayerDirective_BaseFactory || (ɵOlBaseImageLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlBaseImageLayerDirective)))(__ngFactoryType__ || OlBaseImageLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlBaseImageLayerDirective, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlBaseImageLayerDirective, [{
        type: Directive
    }], null, null); })();
/**
 * A directive representing an OpenLayers Image Layer.
 *
 * The directive is associated with the 'ol-image-layer' selector and integrates
 * with Angular dependency injection by providing itself as an OpenLayers Layer
 * and Layer Directive.
 */
class OlImageLayerDirective extends OlBaseImageLayerDirective {
    initLayer(options) {
        return new ImageLayer(options);
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlImageLayerDirective_BaseFactory; return function OlImageLayerDirective_Factory(__ngFactoryType__) { return (ɵOlImageLayerDirective_BaseFactory || (ɵOlImageLayerDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlImageLayerDirective)))(__ngFactoryType__ || OlImageLayerDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlImageLayerDirective, selectors: [["ol-image-layer"]], features: [i0.ɵɵProvidersFeature([
                { provide: OlLayer, useExisting: OlImageLayerDirective },
                { provide: OlLayerDirective, useExisting: OlImageLayerDirective },
                { provide: OlBaseImageLayerDirective, useExisting: OlImageLayerDirective }
            ]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlImageLayerDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-image-layer',
                providers: [
                    { provide: OlLayer, useExisting: OlImageLayerDirective },
                    { provide: OlLayerDirective, useExisting: OlImageLayerDirective },
                    { provide: OlBaseImageLayerDirective, useExisting: OlImageLayerDirective }
                ]
            }]
    }], null, null); })();

/*
 * Public API Surface of ngx-ol-layer
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ExtentDirective, MinMaxLayerZoomDirective, MinMaxResolutionDirective, OlBaseImageLayerDirective, OlBaseLayerDirective, OlBaseTileLayerDirective, OlBaseVectorLayerDirective, OlGraticleLayerDirective, OlGroupLayerDirective, OlHeatmapLayerDirective, OlImageLayerDirective, OlLayerDirective, OlTileLayerDirective, OlVectorImageLayerDirective, OlVectorLayerDirective, OlVectorTileLayerDirective, OlWebGLTileLayerDirective, OpacityDirective, SourceDirective, VisibleDirective, ZIndexDirective, intervalAttribute };
//# sourceMappingURL=nasumilu-ngx-ol-layer.mjs.map
