import { OnInit } from '@angular/core';
import { OlLayerDirective } from "./ol-base-layer.directive";
import VectorLayer, { Options } from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import { FeatureLike } from "ol/Feature";
import VectorTile from "ol/source/VectorTile";
import BaseVectorLayer from "ol/layer/BaseVector";
import { Options as GraticuleOptions } from "ol/layer/Graticule";
import Text from 'ol/style/Text';
import Stroke from "ol/style/Stroke";
import CanvasVectorLayerRenderer from "ol/renderer/canvas/VectorLayer";
import CanvasVectorTileLayerRenderer from "ol/renderer/canvas/VectorTileLayer";
import CanvasVectorImageLayerRenderer from "ol/renderer/canvas/VectorImageLayer";
import WebGLPointsLayerRenderer from "ol/renderer/webgl/PointsLayer";
import VectorImageLayer, { Options as VectorImageOptions } from "ol/layer/VectorImage";
import Heatmap, { Options as HeatmapOptions } from "ol/layer/Heatmap";
import VectorTileLayer, { Options as VectorTitleOptions, VectorTileRenderType } from "ol/layer/VectorTile";
import { StyleLike } from "ol/style/Style";
import { FlatStyleLike } from "ol/style/flat";
import * as i0 from "@angular/core";
export type VectorSourceType = VectorSource<FeatureLike> | VectorTile<FeatureLike>;
export type VectorRenderType = CanvasVectorLayerRenderer | CanvasVectorTileLayerRenderer | CanvasVectorImageLayerRenderer | WebGLPointsLayerRenderer;
/**
 * OlBaseVectorLayerDirective serves as a base class for creating directives that
 * manage OpenLayers vector layers.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `style`: The layer's StyleLike | FlatStyleLike
 *
 */
export declare abstract class OlBaseVectorLayerDirective<L extends BaseVectorLayer<FeatureLike, VectorSourceType, VectorRenderType>> extends OlLayerDirective<L> {
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly updateWhileAnimating: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly updateWhileInteracting: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly declutter: import("@angular/core").InputSignal<string | boolean | undefined>;
    readonly style: import("@angular/core").ModelSignal<StyleLike | FlatStyleLike | undefined>;
    readonly layer: import("@angular/core").Signal<L>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlBaseVectorLayerDirective<any>, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlBaseVectorLayerDirective<any>, never, never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "updateWhileAnimating": { "alias": "updateWhileAnimating"; "required": false; "isSignal": true; }; "updateWhileInteracting": { "alias": "updateWhileInteracting"; "required": false; "isSignal": true; }; "declutter": { "alias": "declutter"; "required": false; "isSignal": true; }; "style": { "alias": "style"; "required": false; "isSignal": true; }; }, { "style": "styleChange"; }, never, never, true, never>;
}
/**
 * Directive representing an OpenLayers vector layer.
 *
 * This directive is used to integrate vector layers with Angular applications, allowing
 * seamless interaction with the OpenLayers library. Through dependency injection, it provides
 * the base layer functionality and reuses `OlBaseVectorLayerDirective`.
 *
 * The directive is associated with the `ol-vector-layer` selector and provides itself
 * as both `OlLayer` and `OlBaseVectorLayerDirective` for other components or classes to
 * depend on.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 */
export declare class OlVectorLayerDirective extends OlBaseVectorLayerDirective<VectorLayer<VectorSource<FeatureLike>, FeatureLike>> {
    protected initLayer(options: Options): VectorLayer<VectorSource<FeatureLike>, FeatureLike>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlVectorLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlVectorLayerDirective, "ol-vector-layer", never, {}, {}, never, never, true, never>;
}
/**
 * Converts a string of comma-separated numbers into an array of numbers
 * or returns the input array of numbers as is.
 *
 * @param {number[]|string} value - The input value, which can be either an
 * array of numbers or a string of comma-separated numbers.
 * @return {number[]} An array of numbers parsed from the input string or
 * the input array if already provided as numbers.
 */
export declare function intervalAttribute(value: number[] | string): number[];
/**
 * A directive that represents an OpenLayers graticule layer in an Angular application.
 * The graticule layer renders a grid representing meridians (longitude) and parallels (latitude)
 * on a map. This directive extends the base vector layer directive and provides additional
 * configuration options specific to graticule rendering.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - maxLines: Defines the maximum number of grid lines to render.
 * - targetSize: Determines the target size for grid spacing.
 * - showLabels: Toggles the visibility of the longitude and latitude labels.
 * - lonLabelFormatter: Custom function to format longitude labels.
 * - latLabelFormatter: Custom function to format latitude labels.
 * - lonlatPosition: Specifies the longitude label position relative to the grid lines.
 * - latlonPosition: Specifies the latitude label position relative to the grid lines.
 * - strokeStyle: Configures the styling of the grid lines using a `Stroke` object.
 * - lonLabelStyle: Specifies the text style for longitude labels using a `Text` object.
 * - latLabelStyle: Specifies the text style for latitude labels using a `Text` object.
 * - intervals: Array of intervals to determine the spacing of grid lines, defined in degrees.
 * - wrapX: Determines whether the grid wraps horizontally across the world.
 *
 * The directive automatically integrates with OpenLayers and Angular dependency injection
 * to act as a vector layer, allowing for interaction with additional layers and features.
 */
export declare class OlGraticleLayerDirective extends OlBaseVectorLayerDirective<VectorLayer<VectorSource<FeatureLike>, FeatureLike>> {
    readonly maxLines: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly targetSize: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly showLabels: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly lonLabelFormatter: import("@angular/core").InputSignal<((lon: number) => string) | undefined>;
    readonly latLabelFormatter: import("@angular/core").InputSignal<((lat: number) => string) | undefined>;
    readonly lonlatPosition: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly latlonPosition: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly strokeStyle: import("@angular/core").InputSignal<Stroke | undefined>;
    readonly lonLabelStyle: import("@angular/core").InputSignal<Text | undefined>;
    readonly latLabelStyle: import("@angular/core").InputSignal<Text | undefined>;
    readonly intervals: import("@angular/core").InputSignalWithTransform<number[], string | number[]>;
    readonly wrapX: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    protected initLayer(options: GraticuleOptions): VectorLayer<VectorSource<FeatureLike>, FeatureLike>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlGraticleLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlGraticleLayerDirective, "ol-graticle-layer", never, { "maxLines": { "alias": "maxLines"; "required": false; "isSignal": true; }; "targetSize": { "alias": "targetSize"; "required": false; "isSignal": true; }; "showLabels": { "alias": "showLabels"; "required": false; "isSignal": true; }; "lonLabelFormatter": { "alias": "lonLabelFormatter"; "required": false; "isSignal": true; }; "latLabelFormatter": { "alias": "latLabelFormatter"; "required": false; "isSignal": true; }; "lonlatPosition": { "alias": "lonlatPosition"; "required": false; "isSignal": true; }; "latlonPosition": { "alias": "latlonPosition"; "required": false; "isSignal": true; }; "strokeStyle": { "alias": "strokeStyle"; "required": false; "isSignal": true; }; "lonLabelStyle": { "alias": "lonLabelStyle"; "required": false; "isSignal": true; }; "latLabelStyle": { "alias": "latLabelStyle"; "required": false; "isSignal": true; }; "intervals": { "alias": "intervals"; "required": false; "isSignal": true; }; "wrapX": { "alias": "wrapX"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
/**
 * Directive to configure and create an OpenLayers `VectorImageLayer` component.
 *
 * This class extends the `OlBaseVectorLayerDirective` to provide specific functionalities
 * for `VectorImageLayer` which supports rendering vector data with rasterization for better
 * performance in cases of large datasets or complex geometries.
 *
 * The `OlVectorImageLayerDirective` allows binding for specific configuration options
 * including the `imageRatio`, which determines the resolution of the raster images
 * used to draw vector data.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `imageRatio`: A readable input property defining the ratio between the physical and logical pixel size for the rasterization process.
 */
export declare class OlVectorImageLayerDirective extends OlBaseVectorLayerDirective<VectorImageLayer<VectorSource<FeatureLike>, FeatureLike>> {
    readonly imageRatio: import("@angular/core").InputSignalWithTransform<number, unknown>;
    protected initLayer(options: VectorImageOptions): VectorImageLayer<VectorSource<FeatureLike>, FeatureLike>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlVectorImageLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlVectorImageLayerDirective, "ol-vector-image-layer", never, { "imageRatio": { "alias": "imageRatio"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
/**
 * Directive to integrate OpenLayers Heatmap layer into Angular applications.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `blur`: Reactive property to manage the blur size of the heatmap layer.
 * - `radius`: Reactive property to control the size of the radius for each feature.
 * - `gradient`: Reactive property for setting the color gradient of the heatmap.
 *
 * Outputs:
 * - `blurChanged`:
 * - `radiusChanged`:
 * - `gradientChanged`:
 */
export declare class OlHeatmapLayerDirective extends OlBaseVectorLayerDirective<Heatmap<FeatureLike, VectorSource<FeatureLike>>> implements OnInit {
    readonly blur: import("@angular/core").ModelSignal<number>;
    readonly radius: import("@angular/core").ModelSignal<number>;
    readonly gradient: import("@angular/core").ModelSignal<string[]>;
    protected initLayer(options: HeatmapOptions): Heatmap<FeatureLike, VectorSource<FeatureLike>>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlHeatmapLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlHeatmapLayerDirective, "ol-heatmap-layer", never, { "blur": { "alias": "blur"; "required": false; "isSignal": true; }; "radius": { "alias": "radius"; "required": false; "isSignal": true; }; "gradient": { "alias": "gradient"; "required": false; "isSignal": true; }; }, { "blur": "blurChange"; "radius": "radiusChange"; "gradient": "gradientChange"; }, never, never, true, never>;
}
/**
 * Directive for creating and configuring an OpenLayers vector tile layer.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `preload`: Sets the number of low-resolution parent tiles to preload. This helps improve perceived performance when the map is panned or zoomed.
 * - `renderMode`: Determines the rendering mode for vector tiles. Possible values are `'hybrid'`, `'vector'`, or `'image'`. The default value is `'hybrid'`.
 */
export declare class OlVectorTileLayerDirective extends OlBaseVectorLayerDirective<VectorTileLayer> {
    readonly preload: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly renderMode: import("@angular/core").InputSignal<VectorTileRenderType>;
    protected initLayer(options: VectorTitleOptions): VectorTileLayer;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlVectorTileLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlVectorTileLayerDirective, "ol-vector-tile-layer", never, { "preload": { "alias": "preload"; "required": false; "isSignal": true; }; "renderMode": { "alias": "renderMode"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-vector-layer.directive.d.ts.map