import { OnInit } from '@angular/core';
import TileLayer from "ol/layer/Tile";
import BaseTileLayer, { Options } from 'ol/layer/BaseTile';
import TileSource from "ol/source/Tile";
import LayerRenderer from "ol/renderer/Layer";
import { OlLayerDirective } from "./ol-base-layer.directive";
import WebGLTileLayer, { Options as WebGLTileLayerOptions } from "ol/layer/WebGLTile";
import * as i0 from "@angular/core";
/**
 * Represents an abstract base directive for OpenLayers tile layers, providing
 * common properties and configuration options for tile-based map layers.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
export declare abstract class OlBaseTileLayerDirective<L extends BaseTileLayer<TileSource, LayerRenderer<any>>> extends OlLayerDirective<L> {
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly preload: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly useInterimTilesOnError: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly cacheSize: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly layer: import("@angular/core").Signal<L>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlBaseTileLayerDirective<any>, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlBaseTileLayerDirective<any>, never, never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "preload": { "alias": "preload"; "required": false; "isSignal": true; }; "useInterimTilesOnError": { "alias": "useInterimTilesOnError"; "required": false; "isSignal": true; }; "cacheSize": { "alias": "cacheSize"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
/**
 * The OlTileLayerDirective is an Angular directive that represents an OpenLayers TileLayer.
 * The directive is registered with the selector 'ol-tile-layer' for use in component templates.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
export declare class OlTileLayerDirective extends OlBaseTileLayerDirective<TileLayer> {
    protected initLayer(options: Options<TileSource>): TileLayer;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlTileLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlTileLayerDirective, "ol-tile-layer", never, {}, {}, never, never, true, never>;
}
/**
 * Directive used to integrate an OpenLayers WebGL tile layer in declarative setups.
 * This directive acts as a wrapper for the OpenLayers `WebGLTileLayer`, allowing it
 * to be configured and used within an Angular application.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
export declare class OlWebGLTileLayerDirective extends OlBaseTileLayerDirective<WebGLTileLayer> implements OnInit {
    #private;
    readonly exposure: import("@angular/core").ModelSignal<number>;
    readonly contrast: import("@angular/core").ModelSignal<number>;
    readonly saturation: import("@angular/core").ModelSignal<number>;
    readonly brightness: import("@angular/core").ModelSignal<number>;
    readonly gamma: import("@angular/core").ModelSignal<number>;
    protected initLayer(options: WebGLTileLayerOptions): WebGLTileLayer;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlWebGLTileLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlWebGLTileLayerDirective, "ol-webgl-tile-layer", never, { "exposure": { "alias": "exposure"; "required": false; "isSignal": true; }; "contrast": { "alias": "contrast"; "required": false; "isSignal": true; }; "saturation": { "alias": "saturation"; "required": false; "isSignal": true; }; "brightness": { "alias": "brightness"; "required": false; "isSignal": true; }; "gamma": { "alias": "gamma"; "required": false; "isSignal": true; }; }, { "exposure": "exposureChange"; "contrast": "contrastChange"; "saturation": "saturationChange"; "brightness": "brightnessChange"; "gamma": "gammaChange"; }, never, never, true, never>;
}
//# sourceMappingURL=ol-tile-layer.directive.d.ts.map