import { AfterContentInit, OnDestroy, OnInit } from '@angular/core';
import BaseLayer, { BackgroundColor, Options } from "ol/layer/Base";
import { OlLayer } from '@nasumilu/ngx-ol-map';
import { EventsKey } from "ol/events";
import Layer from "ol/layer/Layer";
import LayerRenderer from 'ol/renderer/Layer';
import Source from "ol/source/Source";
import LayerGroup, { Options as LayerGroupOptions } from "ol/layer/Group";
import { Extent } from "ol/extent";
import { FrameState } from "ol/Map";
import * as i0 from "@angular/core";
declare abstract class AbstractLayerDirective implements OnDestroy {
    protected readonly layerDirective: OlLayer<any>;
    protected eventKeys: EventsKey[];
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<AbstractLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<AbstractLayerDirective, never, never, {}, {}, never, never, true, never>;
}
export declare class OpacityDirective extends AbstractLayerDirective implements OnInit {
    readonly opacity: import("@angular/core").ModelSignal<number>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OpacityDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OpacityDirective, never, never, { "opacity": { "alias": "opacity"; "required": false; "isSignal": true; }; }, { "opacity": "opacityChange"; }, never, never, true, never>;
}
export declare class VisibleDirective extends AbstractLayerDirective implements OnInit {
    readonly visible: import("@angular/core").ModelSignal<boolean>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<VisibleDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<VisibleDirective, never, never, { "visible": { "alias": "visible"; "required": false; "isSignal": true; }; }, { "visible": "visibleChange"; }, never, never, true, never>;
}
/**
 * The ExtentDirective is an Angular directive that manages the synchronization
 * of an OpenLayers layer's extent with a reactive model property.
 *
 * This directive is used to dynamically manage the extent of a map layer in response
 * to changes in an observable model property or changes within the map itself.
 *
 * The directive's primary responsibilities include:
 * - Setting the initial extent of the OpenLayers layer based on the reactive model.
 * - Subscribing to changes in the model property and updating the layer's extent accordingly.
 * - Listening for changes in the layer's extent and updating the reactive model property.
 *
 * Lifecycle Hooks:
 * - `ngOnInit`: Initializes the synchronization between the extent model property and
 *   the OpenLayers layer's extent.
 * - `ngOnDestroy`: Cleans up event listeners and other resources associated with the directive.
 */
export declare class ExtentDirective extends AbstractLayerDirective implements OnInit {
    readonly extent: import("@angular/core").ModelSignal<Extent | undefined>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ExtentDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<ExtentDirective, never, never, { "extent": { "alias": "extent"; "required": false; "isSignal": true; }; }, { "extent": "extentChange"; }, never, never, true, never>;
}
export declare class ZIndexDirective extends AbstractLayerDirective implements OnInit {
    readonly zIndex: import("@angular/core").ModelSignal<number | undefined>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ZIndexDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<ZIndexDirective, never, never, { "zIndex": { "alias": "zIndex"; "required": false; "isSignal": true; }; }, { "zIndex": "zIndexChange"; }, never, never, true, never>;
}
export declare class MinMaxLayerZoomDirective extends AbstractLayerDirective implements OnInit {
    readonly minZoom: import("@angular/core").ModelSignal<number>;
    readonly maxZoom: import("@angular/core").ModelSignal<number>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MinMaxLayerZoomDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<MinMaxLayerZoomDirective, never, never, { "minZoom": { "alias": "minZoom"; "required": false; "isSignal": true; }; "maxZoom": { "alias": "maxZoom"; "required": false; "isSignal": true; }; }, { "minZoom": "minZoomChange"; "maxZoom": "maxZoomChange"; }, never, never, true, never>;
}
export declare class MinMaxResolutionDirective extends AbstractLayerDirective implements OnInit {
    readonly minResolution: import("@angular/core").ModelSignal<number>;
    readonly maxResolution: import("@angular/core").ModelSignal<number>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MinMaxResolutionDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<MinMaxResolutionDirective, never, never, { "minResolution": { "alias": "minResolution"; "required": false; "isSignal": true; }; "maxResolution": { "alias": "maxResolution"; "required": false; "isSignal": true; }; }, { "minResolution": "minResolutionChange"; "maxResolution": "maxResolutionChange"; }, never, never, true, never>;
}
/**
 * Abstract base directive for OpenLayers layers, providing common functionality
 * for managing layer configuration and lifecycle. This class is designed to
 * simplify the integration of OpenLayers layers within an Angular application.
 *
 * @template L The type of the OpenLayers BaseLayer that this directive manages.
 */
export declare abstract class OlBaseLayerDirective<L extends BaseLayer> extends OlLayer<L> {
    readonly background: import("@angular/core").InputSignal<BackgroundColor | undefined>;
    readonly layer: import("@angular/core").Signal<L>;
    protected abstract initLayer<O extends Options>(options: O): L;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlBaseLayerDirective<any>, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlBaseLayerDirective<any>, never, never, { "background": { "alias": "background"; "required": false; "isSignal": true; }; }, {}, never, never, true, [{ directive: typeof OpacityDirective; inputs: { "opacity": "opacity"; }; outputs: { "opacityChange": "opacityChange"; }; }, { directive: typeof VisibleDirective; inputs: { "visible": "visible"; }; outputs: { "visibleChange": "visibleChange"; }; }, { directive: typeof ExtentDirective; inputs: { "extent": "extent"; }; outputs: { "extentChange": "extentChange"; }; }, { directive: typeof ZIndexDirective; inputs: { "zIndex": "zIndex"; }; outputs: { "zIndexChange": "zIndexChange"; }; }, { directive: typeof MinMaxLayerZoomDirective; inputs: { "minZoom": "minZoom"; "maxZoom": "maxZoom"; }; outputs: { "minZoomChange": "minZoomChange"; "maxZoomChange": "maxZoomChange"; }; }, { directive: typeof MinMaxResolutionDirective; inputs: { "minResolution": "minResolution"; "maxResolution": "maxResolution"; }; outputs: { "minResolutionChange": "minResolutionChange"; "maxResolutionChange": "maxResolutionChange"; }; }]>;
}
/**
 * Directive to manage OpenLayers LayerGroup components in Angular applications.
 * Acts as a wrapper for LayerGroup functionalities and integrates with Angular's lifecycle hooks.
 *
 * This directive is used to create and manage a group of layers (LayerGroup) in an OpenLayers map,
 * and provides capabilities to synchronize Angular component layers within the group.
 * It supports interaction with the OlLayerDirective to aggregate individual layers into a Collection.
 *
 * - Automatically initializes a LayerGroup instance with the specified options.
 * - Collects individual layers provided in the template using content children and adds them to the group.
 * - Ensures synchronization of Angular component layers with the underlying OpenLayers LayerGroup.
 *
 * Implements the AfterContentInit interface to manage component's content initialization and layer aggregation.
 */
export declare class OlGroupLayerDirective extends OlBaseLayerDirective<LayerGroup> implements OnInit, AfterContentInit, OnDestroy {
    #private;
    private readonly layers;
    readonly layerAdded: import("@angular/core").OutputEmitterRef<OlLayer<BaseLayer>>;
    readonly layerRemoved: import("@angular/core").OutputEmitterRef<OlLayer<BaseLayer>>;
    protected initLayer(options: LayerGroupOptions): LayerGroup;
    getLayers(): OlLayer<BaseLayer>[];
    find(predicate: (olLayer: OlLayer<BaseLayer>) => boolean): OlLayer<BaseLayer> | undefined;
    filter(predicate: (olLayer: OlLayer<BaseLayer>) => boolean): OlLayer<BaseLayer>[];
    addLayer<L extends BaseLayer>(layer: OlLayer<L>): void;
    removeLayer<L extends BaseLayer>(layer: OlLayer<L>): void;
    ngOnInit(): void;
    ngAfterContentInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlGroupLayerDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlGroupLayerDirective, "ol-group-layer", never, {}, { "layerAdded": "layerAdded"; "layerRemoved": "layerRemoved"; }, ["layers"], never, true, never>;
}
/**
 * Abstract class representing a directive for an OpenLayers (OL) layer. This class provides base functionality
 * for managing an OpenLayers Layer instance, including its source and associated lifecycle events.
 * It extends the functionality of `OlBaseLayerDirective` and implements the `OnInit` and `OnDestroy` Angular lifecycle hooks.
 *
 * @template L The type of the OpenLayers layer, extending the base Layer class.
 */
export declare abstract class OlLayerDirective<L extends Layer<Source, LayerRenderer<any>>> extends OlBaseLayerDirective<L> implements OnInit, OnDestroy {
    protected eventKeys: EventsKey[];
    readonly postRender: import("@angular/core").OutputEmitterRef<FrameState>;
    readonly preRender: import("@angular/core").OutputEmitterRef<FrameState>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlLayerDirective<any>, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlLayerDirective<any>, never, never, {}, { "postRender": "postRender"; "preRender": "preRender"; }, never, never, true, never>;
}
/**
 * The `SourceDirective` is an Angular directive that manages the OpenLayers source of a layer.
 * It binds the source of an OpenLayers layer to a reactive model, ensuring synchronization
 * between the layer's source and the underlying model. The directive is meant to be used with
 * the `OlLayerDirective`.
 *
 * Features:
 * - Initializes the layer's source based on the value of the reactive `source` model.
 * - Synchronizes changes between the layer's source and the `source` model.
 * - Cleans up any registered events during destruction.
 *
 * Lifecycle:
 * - In `ngOnInit`, binds the given source to the layer and listens for source changes.
 * - In `ngOnDestroy`, cleans up event listeners to prevent memory leaks.
 */
export declare class SourceDirective implements OnInit, OnDestroy {
    #private;
    readonly source: import("@angular/core").ModelSignal<Source | null>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SourceDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<SourceDirective, "[source]", never, { "source": { "alias": "source"; "required": false; "isSignal": true; }; }, { "source": "sourceChange"; }, never, never, true, never>;
}
export {};
//# sourceMappingURL=ol-base-layer.directive.d.ts.map