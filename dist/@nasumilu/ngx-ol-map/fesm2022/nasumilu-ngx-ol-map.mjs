import * as i0 from '@angular/core';
import { Directive, contentChildren, output, model, InjectionToken, inject, input, booleanAttribute, numberAttribute, contentChild, computed, viewChild, ElementRef, Component, ChangeDetectionStrategy, Pipe } from '@angular/core';
import Map from 'ol/Map';
import { unByKey } from 'ol/Observable';
import View from 'ol/View';
import { setEPSGLookup, fromEPSGCode, register } from 'ol/proj/proj4';
import { HttpClient } from '@angular/common/http';
import { of, switchMap, map, firstValueFrom, from, BehaviorSubject, forkJoin } from 'rxjs';
import proj4 from 'proj4';
import { Projection, addProjection, transform } from 'ol/proj';
import { format, toStringHDMS, toStringXY } from 'ol/coordinate';

class OlControl {
    static { this.ɵfac = function OlControl_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlControl)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlControl }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlControl, [{
        type: Directive
    }], null, null); })();
class OlControlsDirective {
    constructor() {
        this.#controls = new Set();
        this.olControls = contentChildren(OlControl);
        this.controlAdded = output();
        this.controlRemoved = output();
    }
    #controls;
    addControl(control) {
        if (!this.#controls.has(control)) {
            this.#controls.add(control);
            this.controlAdded.emit(control);
        }
    }
    removeControl(control) {
        if (this.#controls.delete(control)) {
            this.controlRemoved.emit(control);
        }
    }
    ngAfterContentInit() {
        this.olControls().forEach(OlControlsDirective.prototype.addControl.bind(this));
    }
    static { this.ɵfac = function OlControlsDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlControlsDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlControlsDirective, selectors: [["ol-controls"]], contentQueries: function OlControlsDirective_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.olControls, OlControl, 4);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, outputs: { controlAdded: "controlAdded", controlRemoved: "controlRemoved" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlControlsDirective, [{
        type: Directive,
        args: [{ selector: 'ol-controls' }]
    }], null, null); })();

class OlLayer {
}
class OlLayersDirective {
    constructor() {
        this.#layers = new Set();
        this.ngxOlLayers = contentChildren(OlLayer);
        this.layerAdded = output();
        this.layerRemoved = output();
    }
    #layers;
    get layers() {
        return Array.from(this.#layers.values());
    }
    addLayer(layer) {
        if (!this.#layers.has(layer)) {
            this.#layers.add(layer);
            this.layerAdded.emit(layer);
        }
    }
    removeLayer(layer) {
        if (this.#layers.delete(layer)) {
            this.layerRemoved.emit(layer);
        }
    }
    ngAfterContentInit() {
        this.ngxOlLayers().forEach(OlLayersDirective.prototype.addLayer.bind(this));
    }
    static { this.ɵfac = function OlLayersDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlLayersDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlLayersDirective, selectors: [["ol-layers"]], contentQueries: function OlLayersDirective_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.ngxOlLayers, OlLayer, 4);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, outputs: { layerAdded: "layerAdded", layerRemoved: "layerRemoved" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlLayersDirective, [{
        type: Directive,
        args: [{ selector: 'ol-layers' }]
    }], null, null); })();

class OlOverlay {
}
class OlOverlaysDirective {
    constructor() {
        this.#overlays = new Set();
        this.ngxOlOverlays = contentChildren(OlOverlay);
        this.overlayAdded = output();
        this.overlayRemoved = output();
    }
    #overlays;
    addOverlay(overlay) {
        if (!this.#overlays.has(overlay)) {
            this.#overlays.add(overlay);
            this.overlayAdded.emit(overlay);
        }
    }
    removeOverlay(overlay) {
        if (this.#overlays.delete(overlay)) {
            this.overlayRemoved.emit(overlay);
        }
    }
    ngAfterContentInit() {
        this.ngxOlOverlays().forEach(OlOverlaysDirective.prototype.addOverlay.bind(this));
    }
    static { this.ɵfac = function OlOverlaysDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlOverlaysDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlOverlaysDirective, selectors: [["ol-overlays"]], contentQueries: function OlOverlaysDirective_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.ngxOlOverlays, OlOverlay, 4);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, outputs: { overlayAdded: "overlayAdded", overlayRemoved: "overlayRemoved" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlOverlaysDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-overlays',
                standalone: true
            }]
    }], null, null); })();

/**
 * OlInteraction is an abstract class representing a directive to manage and synchronize the
 * active state of an OpenLayers interaction with Angular's reactive state management.
 *
 * This class provides functionality to:
 * - Synchronize the active state of an OpenLayers interaction with an Angular reactive signal.
 * - Automatically handle event subscriptions during the lifecycle of the directive.
 *
 * To use this class, extend it and implement the abstract `interaction` property to return
 * an OpenLayers interaction object.
 *
 * @template T Defines the type of OpenLayers Interaction to be used.
 */
class OlInteraction {
    constructor() {
        this.eventKeys = [];
        this.active = model(true);
    }
    ngOnInit() {
        const interaction = this.interaction();
        this.active.subscribe(value => {
            const active = interaction.getActive();
            if (value !== active) {
                interaction.setActive(value);
            }
        });
        this.eventKeys.push(this.interaction().on('change:active', () => {
            const active = interaction.getActive();
            if (this.active() !== active) {
                this.active.set(active);
            }
        }));
    }
    ngOnDestroy() {
        unByKey(this.eventKeys);
        this.eventKeys = [];
    }
    static { this.ɵfac = function OlInteraction_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlInteraction)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlInteraction, inputs: { active: [1, "active"] }, outputs: { active: "activeChange" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlInteraction, [{
        type: Directive
    }], null, null); })();
/**
 * The `OlInteractionsDirective` class is an Angular directive that manages a set of OpenLayers interactions.
 * This directive provides functionality to add or remove OpenLayers interactions and emits events when such actions occur.
 *
 * It works by maintaining an internal set of interactions and ensuring that interactions are added or removed consistently.
 *
 * Directive Selector:
 * - `ol-interactions`
 *
 * Implements:
 * - `AfterContentInit` lifecycle hook for initialization after content projection.
 *
 * Properties:
 * - `interactionAdded`: An event emitter that emits when a new interaction is added.
 * - `interactionRemoved`: An event emitter that emits when an interaction is removed.
 *
 * Methods:
 * - `addInteraction(interaction: OlInteraction<Interaction>)`: Adds a new interaction to the internal set if it is not already present, and emits the `interactionAdded` event.
 * - `removeInteraction(interaction: OlInteraction<Interaction>)`: Removes an interaction from the internal set if it is present, and emits the `interactionRemoved` event.
 */
class OlInteractionsDirective {
    constructor() {
        this.#interactions = new Set();
        this.interactions = contentChildren(OlInteraction);
        this.interactionAdded = output();
        this.interactionRemoved = output();
    }
    #interactions;
    addInteraction(interaction) {
        if (!this.#interactions.has(interaction)) {
            this.#interactions.add(interaction);
            this.interactionAdded.emit(interaction);
        }
    }
    removeInteraction(interaction) {
        if (this.#interactions.delete(interaction)) {
            this.interactionRemoved.emit(interaction);
        }
    }
    ngAfterContentInit() {
        this.interactions().forEach(OlInteractionsDirective.prototype.addInteraction.bind(this));
    }
    static { this.ɵfac = function OlInteractionsDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlInteractionsDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlInteractionsDirective, selectors: [["ol-interactions"]], contentQueries: function OlInteractionsDirective_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.interactions, OlInteraction, 4);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, outputs: { interactionAdded: "interactionAdded", interactionRemoved: "interactionRemoved" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlInteractionsDirective, [{
        type: Directive,
        args: [{ selector: 'ol-interactions' }]
    }], null, null); })();

const EPSG_LOOKUP = new InjectionToken('ProjectionLookupToken');
class ProjectionLookup {
}
class SpatialReferenceOrgLookup extends ProjectionLookup {
    #client = inject(HttpClient);
    #url = of('https://spatialreference.org/ref/epsg');
    lookup(code) {
        return this.#url.pipe(switchMap(url => this.#client.get(`${url}/${code}/proj4.txt`, { responseType: 'text' })));
    }
}
class MapTilerLookup extends ProjectionLookup {
    #client = inject(HttpClient);
    #key;
    #url = of(`https://api.maptiler.com/coordinates/search`);
    constructor(apiKey) {
        super();
        this.#key = apiKey;
    }
    lookup(code) {
        return this.#url.pipe(switchMap(url => this.#client.get(`${url}/${code}.json?transformations=true&exports=true&key=${this.#key}`)), map(response => {
            if (response.total === 0) {
                throw new Error(`No projection found for code ${code}`);
            }
            return response.results[0].exports.proj4;
        }));
    }
}
class Proj4Service {
    #epsgLookupFn = inject(EPSG_LOOKUP);
    constructor() {
        setEPSGLookup(Proj4Service.prototype.lookup.bind(this));
    }
    async lookup(code) {
        return await firstValueFrom(this.#epsgLookupFn.lookup(code));
    }
    getProjection(code) {
        if (undefined == code) {
            code = 3857;
        }
        return code instanceof Projection ? of(code) : from(fromEPSGCode(code));
    }
}
function withSpatialReferenceOrg() {
    return () => new SpatialReferenceOrgLookup();
}
function withMapTilerApi(apiKey) {
    return () => new MapTilerLookup(apiKey);
}
function provideProj4(provider, ...projections) {
    // register proj4
    projections.forEach(projection => {
        if (projection instanceof Projection) {
            addProjection(projection);
        }
        else {
            proj4.defs(`${projection.code}`, projection.proj4);
        }
    });
    register(proj4);
    return [
        { provide: Proj4Service, useClass: Proj4Service },
        { provide: EPSG_LOOKUP, useFactory: provider }
    ];
}

function extentAttribute(value) {
    if (typeof value === 'string') {
        value = value.split(',').map(parseFloat);
    }
    return value;
}
function constrainRotationAttribute(value) {
    if (typeof value === 'number') {
        return value;
    }
    if (typeof value === 'string') {
        const _value = parseInt(value);
        if (isNaN(_value)) {
            return value === 'true';
        }
        else {
            return _value;
        }
    }
    return false;
}
class OlViewDirective {
    constructor() {
        this.#proj4Service = inject(Proj4Service, { optional: true });
        this.#eventKeys = [];
        // center
        this.center = model([0, 0]);
        //zoom
        this.zoom = model(3);
        this.minZoom = model(0);
        this.maxZoom = model(28);
        // resolution
        this.resolution = model();
        //rotation
        this.rotation = model(0);
        // projection
        this.projection = input();
        this.multiWorld = input(false, { transform: booleanAttribute });
        this.showFullExtent = input(false, { transform: booleanAttribute });
        this.extent = input(undefined, { transform: extentAttribute });
        this.smoothResolutionConstraint = input(true, { transform: booleanAttribute });
        this.constrainRotation = input(true, { transform: constrainRotationAttribute });
        this.enableRotation = input(true, { transform: booleanAttribute });
        this.zoomFactor = input(2, { transform: numberAttribute });
        this.constrainOnlyCenter = input(false, { transform: booleanAttribute });
        this.view$ = new BehaviorSubject(this.#view);
    }
    #proj4Service;
    #view;
    #eventKeys;
    ngOnInit() {
        const projection$ = (this.#proj4Service?.getProjection(this.projection()) ?? of(this.projection()));
        projection$.subscribe(projection => {
            this.#view = new View({
                projection: projection,
                multiWorld: this.multiWorld(),
                showFullExtent: this.showFullExtent(),
                smoothResolutionConstraint: this.smoothResolutionConstraint(),
                constrainRotation: this.constrainRotation(),
                constrainOnlyCenter: this.constrainOnlyCenter(),
                enableRotation: this.enableRotation(),
                zoomFactor: this.zoomFactor(),
                extent: this.extent(),
                center: this.center(),
                zoom: this.zoom(),
                minZoom: this.minZoom(),
                maxZoom: this.maxZoom(),
                resolution: this.resolution(),
                rotation: this.rotation(),
            });
            this.view$.next(this.#view);
            this.center.subscribe(value => {
                const center = this.#view?.getCenter();
                if (center !== value) {
                    this.#view?.setCenter(center);
                }
            });
            this.#eventKeys.push(this.#view?.on('change:center', () => {
                const center = this.#view?.getCenter();
                if (undefined != center) {
                    this.center.set(center);
                }
            }));
            // zoom
            this.zoom.subscribe(value => {
                const zoom = this.#view?.getZoom();
                if (value && value !== zoom) {
                    this.#view?.setZoom(value);
                }
            });
            this.#eventKeys.push(this.#view?.on('change:resolution', () => {
                const zoom = this.#view?.getZoom();
                const resolution = this.#view?.getResolution();
                if (zoom != undefined && zoom != this.zoom()) {
                    this.zoom.set(zoom);
                }
                if (resolution != this.resolution()) {
                    this.resolution.set(resolution);
                }
            }));
            this.minZoom.subscribe(value => {
                const minZoom = this.#view?.getMinZoom();
                if (value !== minZoom) {
                    this.#view?.setMinZoom(value);
                }
            });
            this.maxZoom.subscribe(value => {
                const maxZoom = this.#view?.getMaxZoom();
                if (value !== maxZoom) {
                    this.#view?.setMaxZoom(value);
                }
            });
            //resolution
            this.resolution.subscribe(value => {
                const resolution = this.#view?.getResolution();
                if (value !== resolution) {
                    this.#view?.setResolution(value);
                }
            });
            // rotation
            this.rotation.subscribe(value => {
                const rotation = this.#view?.getRotation();
                if (value !== rotation) {
                    this.#view?.setRotation(value);
                }
            });
            this.#eventKeys.push(this.#view?.on('change:rotation', () => {
                const rotation = this.#view?.getRotation();
                if (rotation != this.rotation() && this.#view) {
                    this.rotation.set(this.#view.getRotation());
                }
            }));
        });
    }
    ngOnDestroy() {
        unByKey(this.#eventKeys);
        this.#eventKeys = [];
    }
    static { this.ɵfac = function OlViewDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlViewDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlViewDirective, inputs: { center: [1, "center"], zoom: [1, "zoom"], minZoom: [1, "minZoom"], maxZoom: [1, "maxZoom"], resolution: [1, "resolution"], rotation: [1, "rotation"], projection: [1, "projection"], multiWorld: [1, "multiWorld"], showFullExtent: [1, "showFullExtent"], extent: [1, "extent"], smoothResolutionConstraint: [1, "smoothResolutionConstraint"], constrainRotation: [1, "constrainRotation"], enableRotation: [1, "enableRotation"], zoomFactor: [1, "zoomFactor"], constrainOnlyCenter: [1, "constrainOnlyCenter"] }, outputs: { center: "centerChange", zoom: "zoomChange", minZoom: "minZoomChange", maxZoom: "maxZoomChange", resolution: "resolutionChange", rotation: "rotationChange" } }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlViewDirective, [{
        type: Directive
    }], null, null); })();

const _c0 = ["map"];
class OlMapComponent {
    constructor() {
        this.#eventKeys = [];
        this.olLayersDirective = contentChild(OlLayersDirective);
        this.olOverlaysDirective = contentChild(OlOverlaysDirective);
        this.olControlsDirective = contentChild(OlControlsDirective);
        this.olInteractionsDirective = contentChild(OlInteractionsDirective);
        this.#viewDirective = inject(OlViewDirective);
        this.#map = computed(() => new Map({
            layers: [],
            controls: [],
            overlays: [],
            interactions: []
        }));
        this.ele = viewChild('map', { read: ElementRef });
        this.name = input();
        // events
        // MapBrowserEvent
        this.mapClick = output();
        this.mapDblClick = output();
        this.mapSingleClick = output();
        this.pointerDrag = output();
        this.pointerMove = output();
        // MapEvent
        this.loadStart = output();
        this.loadEnd = output();
        this.moveEnd = output();
        this.moveStart = output();
    }
    #eventKeys;
    #viewDirective;
    #map;
    ngOnInit() {
        const map = this.#map();
        this.olLayersDirective()?.layerAdded.subscribe(olLayer => map.addLayer(olLayer.layer()));
        this.olLayersDirective()?.layerRemoved.subscribe(olLayer => map.removeLayer(olLayer.layer()));
        this.olControlsDirective()?.controlAdded.subscribe(olControl => map.addControl(olControl.control()));
        this.olControlsDirective()?.controlRemoved.subscribe(olControl => map.removeControl(olControl.control()));
        this.olInteractionsDirective()?.interactionAdded.subscribe(olInteraction => map.addInteraction(olInteraction.interaction()));
        this.olInteractionsDirective()?.interactionRemoved.subscribe(olInteraction => map.removeInteraction(olInteraction.interaction()));
        this.olOverlaysDirective()?.overlayAdded.subscribe(olOverlay => map.addOverlay(olOverlay.overlay()));
        this.olOverlaysDirective()?.overlayRemoved.subscribe(olOverlay => map.removeOverlay(olOverlay.overlay()));
        this.#eventKeys.push(
        //MapBrowserEvents
        map.on('click', evt => this.mapClick.emit({ coordinate: evt.coordinate, pixel: evt.pixel })), map.on('dblclick', evt => this.mapDblClick.emit({ coordinate: evt.coordinate, pixel: evt.pixel })), map.on('singleclick', evt => this.mapSingleClick.emit({ coordinate: evt.coordinate, pixel: evt.pixel })), map.on('pointerdrag', evt => this.pointerDrag.emit({ coordinate: evt.coordinate, pixel: evt.pixel })), map.on('pointermove', evt => this.pointerMove.emit({ coordinate: evt.coordinate, pixel: evt.pixel })), 
        // MapEvents
        map.on('movestart', evt => this.moveStart.emit(evt.frameState)), map.on('moveend', evt => this.moveEnd.emit(evt.frameState)));
        if (this.name()) {
            map.set('name', this.name());
        }
    }
    ngAfterViewInit() {
        this.#viewDirective.view$.subscribe(view => {
            if (view != undefined) {
                this.#map().setView(view);
            }
        });
        this.#map().setTarget(this.ele()?.nativeElement);
    }
    ngOnDestroy() {
        unByKey(this.#eventKeys);
        this.#eventKeys = [];
    }
    static { this.ɵfac = function OlMapComponent_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlMapComponent)(); }; }
    static { this.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: OlMapComponent, selectors: [["ol-map"]], contentQueries: function OlMapComponent_ContentQueries(rf, ctx, dirIndex) { if (rf & 1) {
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.olLayersDirective, OlLayersDirective, 5);
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.olOverlaysDirective, OlOverlaysDirective, 5);
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.olControlsDirective, OlControlsDirective, 5);
            i0.ɵɵcontentQuerySignal(dirIndex, ctx.olInteractionsDirective, OlInteractionsDirective, 5);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance(4);
        } }, viewQuery: function OlMapComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuerySignal(ctx.ele, _c0, 5, ElementRef);
        } if (rf & 2) {
            i0.ɵɵqueryAdvance();
        } }, inputs: { name: [1, "name"] }, outputs: { mapClick: "mapClick", mapDblClick: "mapDblClick", mapSingleClick: "mapSingleClick", pointerDrag: "pointerDrag", pointerMove: "pointerMove", loadStart: "loadStart", loadEnd: "loadEnd", moveEnd: "moveEnd", moveStart: "moveStart" }, features: [i0.ɵɵHostDirectivesFeature([{ directive: OlViewDirective, inputs: ["projection", "projection", "multiWorld", "multiWorld", "showFullExtent", "showFullExtent", "extent", "extent", "smoothResolutionConstraint", "smoothResolutionConstraint", "constrainRotation", "constrainRotation", "constrainOnlyCenter", "constrainOnlyCenter", "enableRotation", "enableRotation", "zoomFactor", "zoomFactor", "center", "center", "zoom", "zoom", "minZoom", "minZoom", "maxZoom", "maxZoom", "resolution", "resolution", "rotation", "rotation"], outputs: ["centerChange", "centerChange", "zoomChange", "zoomChange", "minZoomChange", "minZoomChange", "maxZoomChange", "maxZoomChange", "resolutionChange", "resolutionChange", "rotationChange", "rotationChange"] }])], decls: 2, vars: 0, consts: [["map", ""], ["tabindex", "0"]], template: function OlMapComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelement(0, "div", 1, 0);
        } }, styles: ["div[_ngcontent-%COMP%]{width:100%;height:100%}"], changeDetection: 0 }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlMapComponent, [{
        type: Component,
        args: [{ selector: 'ol-map', changeDetection: ChangeDetectionStrategy.OnPush, hostDirectives: [
                    {
                        directive: OlViewDirective,
                        inputs: [
                            'projection',
                            'multiWorld',
                            'showFullExtent',
                            'extent',
                            'smoothResolutionConstraint',
                            'constrainRotation',
                            'constrainOnlyCenter',
                            'enableRotation',
                            'zoomFactor',
                            'center',
                            'zoom',
                            'minZoom',
                            'maxZoom',
                            'resolution',
                            'rotation',
                        ],
                        outputs: [
                            'centerChange',
                            'zoomChange',
                            'minZoomChange',
                            'maxZoomChange',
                            'resolutionChange',
                            'rotationChange'
                        ]
                    },
                ], template: `<div #map tabindex="0"></div>`, styles: ["div{width:100%;height:100%}\n"] }]
    }], null, null); })();
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassDebugInfo(OlMapComponent, { className: "OlMapComponent", filePath: "lib/ol-map.component.ts", lineNumber: 59 }); })();

/**
 * A pipe for transforming OpenLayers `Coordinate` objects into string representations
 * based on the specified format and options.
 *
 * The `OlCoordinatePipe` can format a coordinate using a template, convert it into
 * HDMS (degrees, minutes, seconds) format, or represent it as an XY string.
 */
class OlCoordinatePipe {
    transform(value, format$1, fractionDigits, tpl) {
        if (undefined == value) {
            return value;
        }
        if (format$1 === 'format' && tpl) {
            return format(value, tpl, fractionDigits);
        }
        if (format$1 === 'hdms') {
            return toStringHDMS(value, fractionDigits);
        }
        return toStringXY(value, fractionDigits);
    }
    static { this.ɵfac = function OlCoordinatePipe_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlCoordinatePipe)(); }; }
    static { this.ɵpipe = /*@__PURE__*/ i0.ɵɵdefinePipe({ name: "coordinate", type: OlCoordinatePipe, pure: true }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlCoordinatePipe, [{
        type: Pipe,
        args: [{
                name: 'coordinate'
            }]
    }], null, null); })();
/**
 * Pipe that transforms geographical coordinates from a source projection to a destination projection.
 *
 * This pipe uses the Proj4Service to dynamically fetch projection definitions and performs
 * the transformation. If the Proj4Service is not available, it defaults to using the raw
 * source and destination projections provided.
 */
class OlTransformPipe {
    #projService = inject(Proj4Service, { optional: true });
    transform(coordinate, source, destination) {
        const source$ = this.#projService?.getProjection(source) ?? of(source);
        const destination$ = this.#projService?.getProjection(destination) ?? of(destination);
        return forkJoin([source$, destination$]).pipe(map(([source, destination]) => coordinate ? transform(coordinate, source, destination) : coordinate));
    }
    static { this.ɵfac = function OlTransformPipe_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlTransformPipe)(); }; }
    static { this.ɵpipe = /*@__PURE__*/ i0.ɵɵdefinePipe({ name: "transform", type: OlTransformPipe, pure: true }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlTransformPipe, [{
        type: Pipe,
        args: [{
                name: 'transform'
            }]
    }], null, null); })();

/*
 * Public API Surface of ngx-ol-map
 */

/**
 * Generated bundle index. Do not edit.
 */

export { EPSG_LOOKUP, OlControl, OlControlsDirective, OlCoordinatePipe, OlInteraction, OlInteractionsDirective, OlLayer, OlLayersDirective, OlMapComponent, OlOverlay, OlOverlaysDirective, OlTransformPipe, OlViewDirective, Proj4Service, ProjectionLookup, provideProj4, withMapTilerApi, withSpatialReferenceOrg };
//# sourceMappingURL=nasumilu-ngx-ol-map.mjs.map
