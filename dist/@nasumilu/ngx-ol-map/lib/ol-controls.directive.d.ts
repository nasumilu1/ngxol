import { AfterContentInit, Signal } from '@angular/core';
import Control from 'ol/control/Control';
import * as i0 from "@angular/core";
export declare abstract class OlControl<T extends Control> {
    abstract readonly control: Signal<T>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlControl<any>, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlControl<any>, never, never, {}, {}, never, never, true, never>;
}
export declare class OlControlsDirective implements AfterContentInit {
    #private;
    private readonly olControls;
    readonly controlAdded: import("@angular/core").OutputEmitterRef<OlControl<Control>>;
    readonly controlRemoved: import("@angular/core").OutputEmitterRef<OlControl<Control>>;
    addControl(control: OlControl<Control>): void;
    removeControl(control: OlControl<Control>): void;
    ngAfterContentInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlControlsDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlControlsDirective, "ol-controls", never, {}, { "controlAdded": "controlAdded"; "controlRemoved": "controlRemoved"; }, ["olControls"], never, true, never>;
}
//# sourceMappingURL=ol-controls.directive.d.ts.map