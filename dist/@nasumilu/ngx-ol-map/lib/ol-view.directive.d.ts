import { OnDestroy, OnInit } from '@angular/core';
import View from "ol/View";
import { Coordinate } from "ol/coordinate";
import { ProjectionLike } from "ol/proj";
import { Extent } from "ol/extent";
import { BehaviorSubject } from "rxjs";
import * as i0 from "@angular/core";
export declare class OlViewDirective implements OnInit, OnDestroy {
    #private;
    readonly center: import("@angular/core").ModelSignal<Coordinate>;
    readonly zoom: import("@angular/core").ModelSignal<number>;
    readonly minZoom: import("@angular/core").ModelSignal<number>;
    readonly maxZoom: import("@angular/core").ModelSignal<number>;
    readonly resolution: import("@angular/core").ModelSignal<number | undefined>;
    readonly rotation: import("@angular/core").ModelSignal<number>;
    readonly projection: import("@angular/core").InputSignal<ProjectionLike>;
    readonly multiWorld: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly showFullExtent: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly extent: import("@angular/core").InputSignalWithTransform<Extent | undefined, string | Extent | undefined>;
    readonly smoothResolutionConstraint: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly constrainRotation: import("@angular/core").InputSignalWithTransform<number | boolean, string | number | boolean>;
    readonly enableRotation: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly zoomFactor: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly constrainOnlyCenter: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly view$: BehaviorSubject<View | undefined>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlViewDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlViewDirective, never, never, { "center": { "alias": "center"; "required": false; "isSignal": true; }; "zoom": { "alias": "zoom"; "required": false; "isSignal": true; }; "minZoom": { "alias": "minZoom"; "required": false; "isSignal": true; }; "maxZoom": { "alias": "maxZoom"; "required": false; "isSignal": true; }; "resolution": { "alias": "resolution"; "required": false; "isSignal": true; }; "rotation": { "alias": "rotation"; "required": false; "isSignal": true; }; "projection": { "alias": "projection"; "required": false; "isSignal": true; }; "multiWorld": { "alias": "multiWorld"; "required": false; "isSignal": true; }; "showFullExtent": { "alias": "showFullExtent"; "required": false; "isSignal": true; }; "extent": { "alias": "extent"; "required": false; "isSignal": true; }; "smoothResolutionConstraint": { "alias": "smoothResolutionConstraint"; "required": false; "isSignal": true; }; "constrainRotation": { "alias": "constrainRotation"; "required": false; "isSignal": true; }; "enableRotation": { "alias": "enableRotation"; "required": false; "isSignal": true; }; "zoomFactor": { "alias": "zoomFactor"; "required": false; "isSignal": true; }; "constrainOnlyCenter": { "alias": "constrainOnlyCenter"; "required": false; "isSignal": true; }; }, { "center": "centerChange"; "zoom": "zoomChange"; "minZoom": "minZoomChange"; "maxZoom": "maxZoomChange"; "resolution": "resolutionChange"; "rotation": "rotationChange"; }, never, never, true, never>;
}
//# sourceMappingURL=ol-view.directive.d.ts.map