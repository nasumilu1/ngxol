import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { FrameState } from 'ol/Map';
import { Coordinate } from "ol/coordinate";
import { Pixel } from "ol/pixel";
import * as i0 from "@angular/core";
import * as i1 from "./ol-view.directive";
export interface MapCLickEvent {
    coordinate: Coordinate;
    pixel: Pixel;
}
export declare class OlMapComponent implements OnInit, AfterViewInit, OnDestroy {
    #private;
    private readonly olLayersDirective;
    private readonly olOverlaysDirective;
    private readonly olControlsDirective;
    private readonly olInteractionsDirective;
    private readonly ele;
    readonly name: import("@angular/core").InputSignal<string | undefined>;
    readonly mapClick: import("@angular/core").OutputEmitterRef<MapCLickEvent>;
    readonly mapDblClick: import("@angular/core").OutputEmitterRef<MapCLickEvent>;
    readonly mapSingleClick: import("@angular/core").OutputEmitterRef<MapCLickEvent>;
    readonly pointerDrag: import("@angular/core").OutputEmitterRef<MapCLickEvent>;
    readonly pointerMove: import("@angular/core").OutputEmitterRef<MapCLickEvent>;
    readonly loadStart: import("@angular/core").OutputEmitterRef<FrameState>;
    readonly loadEnd: import("@angular/core").OutputEmitterRef<FrameState>;
    readonly moveEnd: import("@angular/core").OutputEmitterRef<FrameState | null>;
    readonly moveStart: import("@angular/core").OutputEmitterRef<FrameState | null>;
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlMapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<OlMapComponent, "ol-map", never, { "name": { "alias": "name"; "required": false; "isSignal": true; }; }, { "mapClick": "mapClick"; "mapDblClick": "mapDblClick"; "mapSingleClick": "mapSingleClick"; "pointerDrag": "pointerDrag"; "pointerMove": "pointerMove"; "loadStart": "loadStart"; "loadEnd": "loadEnd"; "moveEnd": "moveEnd"; "moveStart": "moveStart"; }, ["olLayersDirective", "olOverlaysDirective", "olControlsDirective", "olInteractionsDirective"], never, true, [{ directive: typeof i1.OlViewDirective; inputs: { "projection": "projection"; "multiWorld": "multiWorld"; "showFullExtent": "showFullExtent"; "extent": "extent"; "smoothResolutionConstraint": "smoothResolutionConstraint"; "constrainRotation": "constrainRotation"; "constrainOnlyCenter": "constrainOnlyCenter"; "enableRotation": "enableRotation"; "zoomFactor": "zoomFactor"; "center": "center"; "zoom": "zoom"; "minZoom": "minZoom"; "maxZoom": "maxZoom"; "resolution": "resolution"; "rotation": "rotation"; }; outputs: { "centerChange": "centerChange"; "zoomChange": "zoomChange"; "minZoomChange": "minZoomChange"; "maxZoomChange": "maxZoomChange"; "resolutionChange": "resolutionChange"; "rotationChange": "rotationChange"; }; }]>;
}
//# sourceMappingURL=ol-map.component.d.ts.map