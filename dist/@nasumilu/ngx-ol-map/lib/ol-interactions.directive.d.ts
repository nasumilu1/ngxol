import { AfterContentInit, OnDestroy, OnInit, Signal } from '@angular/core';
import Interaction from "ol/interaction/Interaction";
import { EventsKey } from "ol/events";
import * as i0 from "@angular/core";
/**
 * OlInteraction is an abstract class representing a directive to manage and synchronize the
 * active state of an OpenLayers interaction with Angular's reactive state management.
 *
 * This class provides functionality to:
 * - Synchronize the active state of an OpenLayers interaction with an Angular reactive signal.
 * - Automatically handle event subscriptions during the lifecycle of the directive.
 *
 * To use this class, extend it and implement the abstract `interaction` property to return
 * an OpenLayers interaction object.
 *
 * @template T Defines the type of OpenLayers Interaction to be used.
 */
export declare abstract class OlInteraction<T extends Interaction> implements OnInit, OnDestroy {
    protected eventKeys: EventsKey[];
    readonly active: import("@angular/core").ModelSignal<boolean>;
    abstract readonly interaction: Signal<T>;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlInteraction<any>, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlInteraction<any>, never, never, { "active": { "alias": "active"; "required": false; "isSignal": true; }; }, { "active": "activeChange"; }, never, never, true, never>;
}
/**
 * The `OlInteractionsDirective` class is an Angular directive that manages a set of OpenLayers interactions.
 * This directive provides functionality to add or remove OpenLayers interactions and emits events when such actions occur.
 *
 * It works by maintaining an internal set of interactions and ensuring that interactions are added or removed consistently.
 *
 * Directive Selector:
 * - `ol-interactions`
 *
 * Implements:
 * - `AfterContentInit` lifecycle hook for initialization after content projection.
 *
 * Properties:
 * - `interactionAdded`: An event emitter that emits when a new interaction is added.
 * - `interactionRemoved`: An event emitter that emits when an interaction is removed.
 *
 * Methods:
 * - `addInteraction(interaction: OlInteraction<Interaction>)`: Adds a new interaction to the internal set if it is not already present, and emits the `interactionAdded` event.
 * - `removeInteraction(interaction: OlInteraction<Interaction>)`: Removes an interaction from the internal set if it is present, and emits the `interactionRemoved` event.
 */
export declare class OlInteractionsDirective implements AfterContentInit {
    #private;
    private readonly interactions;
    readonly interactionAdded: import("@angular/core").OutputEmitterRef<OlInteraction<Interaction>>;
    readonly interactionRemoved: import("@angular/core").OutputEmitterRef<OlInteraction<Interaction>>;
    addInteraction(interaction: OlInteraction<Interaction>): void;
    removeInteraction(interaction: OlInteraction<Interaction>): void;
    ngAfterContentInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlInteractionsDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlInteractionsDirective, "ol-interactions", never, {}, { "interactionAdded": "interactionAdded"; "interactionRemoved": "interactionRemoved"; }, ["interactions"], never, true, never>;
}
//# sourceMappingURL=ol-interactions.directive.d.ts.map