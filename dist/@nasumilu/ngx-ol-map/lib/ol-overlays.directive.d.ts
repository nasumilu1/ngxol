import { AfterContentInit, Signal } from '@angular/core';
import { Overlay } from 'ol';
import * as i0 from "@angular/core";
export declare abstract class OlOverlay {
    abstract overlay: Signal<Overlay>;
}
export declare class OlOverlaysDirective implements AfterContentInit {
    #private;
    private readonly ngxOlOverlays;
    readonly overlayAdded: import("@angular/core").OutputEmitterRef<OlOverlay>;
    readonly overlayRemoved: import("@angular/core").OutputEmitterRef<OlOverlay>;
    addOverlay(overlay: OlOverlay): void;
    removeOverlay(overlay: OlOverlay): void;
    ngAfterContentInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlOverlaysDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlOverlaysDirective, "ol-overlays", never, {}, { "overlayAdded": "overlayAdded"; "overlayRemoved": "overlayRemoved"; }, ["ngxOlOverlays"], never, true, never>;
}
//# sourceMappingURL=ol-overlays.directive.d.ts.map