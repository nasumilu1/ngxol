import { InjectionToken, Provider } from '@angular/core';
import { Observable } from "rxjs";
import { Projection, ProjectionLike } from "ol/proj";
export declare const EPSG_LOOKUP: InjectionToken<ProjectionLookup>;
export declare abstract class ProjectionLookup {
    abstract lookup(code: number): Observable<string>;
}
export declare class Proj4Service {
    #private;
    constructor();
    lookup(code: number): Promise<string>;
    getProjection(code: number | ProjectionLike): Observable<Projection>;
}
export type ProjectionLookupFactory = () => ProjectionLookup;
export declare function withSpatialReferenceOrg(): ProjectionLookupFactory;
export declare function withMapTilerApi(apiKey: string): ProjectionLookupFactory;
export type NamedProjection = {
    code: number | string;
    proj4: string;
};
export declare function provideProj4(provider: ProjectionLookupFactory, ...projections: (Projection | NamedProjection)[]): Provider[];
//# sourceMappingURL=ol-projection.service.d.ts.map