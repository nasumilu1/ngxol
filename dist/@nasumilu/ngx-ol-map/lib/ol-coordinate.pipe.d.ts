import { PipeTransform } from '@angular/core';
import { Coordinate } from "ol/coordinate";
import { ProjectionLike } from "ol/proj";
import { Observable } from "rxjs";
import * as i0 from "@angular/core";
export type CoordinateFormat = 'hdms' | 'format';
/**
 * A pipe for transforming OpenLayers `Coordinate` objects into string representations
 * based on the specified format and options.
 *
 * The `OlCoordinatePipe` can format a coordinate using a template, convert it into
 * HDMS (degrees, minutes, seconds) format, or represent it as an XY string.
 */
export declare class OlCoordinatePipe implements PipeTransform {
    transform(value: Coordinate | undefined | null, format: CoordinateFormat, fractionDigits?: number, tpl?: string): string | undefined | null;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlCoordinatePipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<OlCoordinatePipe, "coordinate", true>;
}
/**
 * Pipe that transforms geographical coordinates from a source projection to a destination projection.
 *
 * This pipe uses the Proj4Service to dynamically fetch projection definitions and performs
 * the transformation. If the Proj4Service is not available, it defaults to using the raw
 * source and destination projections provided.
 */
export declare class OlTransformPipe implements PipeTransform {
    #private;
    transform(coordinate: Coordinate | undefined | null, source: ProjectionLike, destination: ProjectionLike): Observable<Coordinate | undefined | null>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlTransformPipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<OlTransformPipe, "transform", true>;
}
//# sourceMappingURL=ol-coordinate.pipe.d.ts.map