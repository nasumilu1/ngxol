import { AfterContentInit, Signal } from '@angular/core';
import BaseLayer from 'ol/layer/Base';
import * as i0 from "@angular/core";
export declare abstract class OlLayer<T extends BaseLayer> {
    abstract readonly layer: Signal<T>;
}
export declare class OlLayersDirective implements AfterContentInit {
    #private;
    private readonly ngxOlLayers;
    readonly layerAdded: import("@angular/core").OutputEmitterRef<OlLayer<BaseLayer>>;
    readonly layerRemoved: import("@angular/core").OutputEmitterRef<OlLayer<BaseLayer>>;
    get layers(): OlLayer<BaseLayer>[];
    addLayer(layer: OlLayer<BaseLayer>): void;
    removeLayer(layer: OlLayer<BaseLayer>): void;
    ngAfterContentInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlLayersDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlLayersDirective, "ol-layers", never, {}, { "layerAdded": "layerAdded"; "layerRemoved": "layerRemoved"; }, ["ngxOlLayers"], never, true, never>;
}
//# sourceMappingURL=ol-layers.directive.d.ts.map