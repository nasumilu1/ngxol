import * as i0 from '@angular/core';
import { input, booleanAttribute, numberAttribute, computed, Directive, output, inject } from '@angular/core';
import { OlInteraction, OlInteractionsDirective } from '@nasumilu/ngx-ol-map';
import MouseWheelZoom from 'ol/interaction/MouseWheelZoom';
import DragPan from 'ol/interaction/DragPan';
import DblClickDragZoom from 'ol/interaction/DblClickDragZoom';
import KeyboardZoom from 'ol/interaction/KeyboardZoom';
import KeyboardPan from 'ol/interaction/KeyboardPan';
import DoubleClickZoom from 'ol/interaction/DoubleClickZoom';
import DragZoom from 'ol/interaction/DragZoom';
import DragRotate from 'ol/interaction/DragRotate';
import PinchRotate from 'ol/interaction/PinchRotate';
import PinchZoom from 'ol/interaction/PinchZoom';

/**
 * Directive to provide an integration with OpenLayers MouseWheelZoom interaction.
 * This directive enables configuring and controlling mouse wheel zoom behavior on an OpenLayers map.
 *
 * This directive extends the base OpenLayers interaction to provide additional configuration through
 * reactive inputs and computed behavior.
 *
 * Selector:
 * - ol-mouse-wheel-zoom: Apply this directive to automatically set up the MouseWheelZoom interaction.
 *
 * Inputs:
 * - onFocusOnly: A boolean input to determine whether zooming should only occur when the map has focus.
 * - duration: A number input specifying the animation duration for the zoom interaction in milliseconds.
 * - maxDelta: A number input defining the maximum zoom delta value.
 * - timeout: A number input setting the timeout duration in milliseconds to consider consecutive zoom actions as distinct.
 * - useAnchor: A boolean input specifying whether to use the mouse pointer location as an anchor for the zoom interaction.
 * - constrainResolution: A boolean input to determine whether zoom levels should be constrained to predefined resolutions.
 *
 * Property:
 * - interaction: A computed property that initializes and returns the MouseWheelZoom interaction based on current input values.
 */
class OlMouseWheelZoomDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.onFocusOnly = input(false, { transform: booleanAttribute });
        this.duration = input(250, { transform: numberAttribute });
        this.maxDelta = input(1, { transform: numberAttribute });
        this.timeout = input(80, { transform: numberAttribute });
        this.useAnchor = input(true, { transform: booleanAttribute });
        this.constrainResolution = input(false, { transform: booleanAttribute });
        this.interaction = computed(() => new MouseWheelZoom({
            onFocusOnly: this.onFocusOnly(),
            duration: this.duration(),
            maxDelta: this.maxDelta(),
            timeout: this.timeout(),
            useAnchor: this.useAnchor(),
            constrainResolution: this.constrainResolution()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlMouseWheelZoomDirective_BaseFactory; return function OlMouseWheelZoomDirective_Factory(__ngFactoryType__) { return (ɵOlMouseWheelZoomDirective_BaseFactory || (ɵOlMouseWheelZoomDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlMouseWheelZoomDirective)))(__ngFactoryType__ || OlMouseWheelZoomDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlMouseWheelZoomDirective, selectors: [["ol-mouse-wheel-zoom"]], inputs: { onFocusOnly: [1, "onFocusOnly"], duration: [1, "duration"], maxDelta: [1, "maxDelta"], timeout: [1, "timeout"], useAnchor: [1, "useAnchor"], constrainResolution: [1, "constrainResolution"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlMouseWheelZoomDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlMouseWheelZoomDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-mouse-wheel-zoom',
                providers: [{ provide: OlInteraction, useExisting: OlMouseWheelZoomDirective }]
            }]
    }], null, null); })();

/**
 * Directive that encapsulates the OpenLayers DragPan interaction, allowing
 * integration with an Angular application. The DragPan interaction enables
 * panning of the map by dragging with the mouse or finger on touch devices.
 *
 * It extends the base OlInteraction class, providing additional functionality
 * specific to the DragPan interaction.
 *
 * Selector:
 * - `ol-drag-pan`
 *
 * Providers:
 * - Provides `OlInteraction` using this directive as its implementation.
 *
 * Inputs:
 * - `kinetic` (optional): Kinetic inertia settings for the pan interaction.
 * - `onFocusOnly` (optional): A boolean attribute determining whether the
 *   interaction is active only when the map has focus.
 *
 * Computed Interaction:
 * - Constructs and returns a new OpenLayers `DragPan` instance based on the provided inputs.
 * - Uses reactive bindings for input changes.
 */
class OlDragPanDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.kinetic = input();
        this.onFocusOnly = input(false, { transform: booleanAttribute });
        this.interaction = computed(() => new DragPan({
            kinetic: this.kinetic(),
            onFocusOnly: this.onFocusOnly()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlDragPanDirective_BaseFactory; return function OlDragPanDirective_Factory(__ngFactoryType__) { return (ɵOlDragPanDirective_BaseFactory || (ɵOlDragPanDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlDragPanDirective)))(__ngFactoryType__ || OlDragPanDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlDragPanDirective, selectors: [["ol-drag-pan"]], inputs: { kinetic: [1, "kinetic"], onFocusOnly: [1, "onFocusOnly"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlDragPanDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlDragPanDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-drag-pan',
                providers: [{ provide: OlInteraction, useExisting: OlDragPanDirective }]
            }]
    }], null, null); })();

/**
 * Directive for creating an OpenLayers DblClickDragZoom interaction.
 * This directive can be used to configure and add a `DblClickDragZoom` interaction
 * to an OpenLayers map. It provides a way to set properties such as duration
 * and zoom delta for the interaction.
 *
 * The interaction allows the user to zoom the map by double-clicking and
 * dragging the mouse.
 *
 * Extends:
 *   OlInteraction<DblClickDragZoom>
 *
 * Inputs:
 * - `duration`: The duration of the zoom animation in milliseconds. Defaults to 400ms.
 * - `delta`: The zoom delta applied on each zoom. Defaults to 1.
 *
 * Computed Properties:
 * - `interaction`: Creates a new `DblClickDragZoom` interaction based on the configured
 *   duration and delta inputs.
 */
class OlDblClickDragZoomDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.duration = input(400, { transform: numberAttribute });
        this.delta = input(1, { transform: numberAttribute });
        this.interaction = computed(() => new DblClickDragZoom({
            duration: this.duration(),
            delta: this.delta()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlDblClickDragZoomDirective_BaseFactory; return function OlDblClickDragZoomDirective_Factory(__ngFactoryType__) { return (ɵOlDblClickDragZoomDirective_BaseFactory || (ɵOlDblClickDragZoomDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlDblClickDragZoomDirective)))(__ngFactoryType__ || OlDblClickDragZoomDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlDblClickDragZoomDirective, selectors: [["ol-dbl-click-drag-zoom"]], inputs: { duration: [1, "duration"], delta: [1, "delta"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlDblClickDragZoomDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlDblClickDragZoomDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-dbl-click-drag-zoom',
                providers: [{ provide: OlInteraction, useExisting: OlDblClickDragZoomDirective }]
            }]
    }], null, null); })();

/**
 * Directive for enabling keyboard-zoom interaction with OpenLayers maps.
 *
 * This directive extends the `OlInteraction` base class and utilizes the
 * `KeyboardZoom` interaction from OpenLayers. It provides properties to
 * configure the zoom behavior when triggered by specific keyboard events.
 *
 * Properties:
 * - `duration`: Configurable duration in milliseconds for the zoom animation.
 * - `delta`: Configurable zoom level increment or decrement value.
 *
 * The directive computes an instance of the `KeyboardZoom` interaction using
 * the defined properties.
 */
class OlKeyboardZoomDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.duration = input(100, { transform: numberAttribute });
        this.delta = input(1, { transform: numberAttribute });
        this.interaction = computed(() => new KeyboardZoom({
            duration: this.duration(),
            delta: this.delta()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlKeyboardZoomDirective_BaseFactory; return function OlKeyboardZoomDirective_Factory(__ngFactoryType__) { return (ɵOlKeyboardZoomDirective_BaseFactory || (ɵOlKeyboardZoomDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlKeyboardZoomDirective)))(__ngFactoryType__ || OlKeyboardZoomDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlKeyboardZoomDirective, selectors: [["ol-keyboard-zoom"]], inputs: { duration: [1, "duration"], delta: [1, "delta"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlKeyboardZoomDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlKeyboardZoomDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-keyboard-zoom',
                providers: [{ provide: OlInteraction, useExisting: OlKeyboardZoomDirective }],
            }]
    }], null, null); })();

/**
 * Directive that provides a customized OpenLayers KeyboardPan interaction.
 *
 * This directive allows users to pan the map using keyboard arrow keys. The duration
 * and pixel delta for the pan action can be configured through the provided inputs.
 *
 * Selector: 'ol-keyboard-pan'
 *
 * Extends:
 * - OlInteraction<KeyboardPan>
 *
 * Dependencies:
 * - OpenLayers `KeyboardPan` interaction
 *
 * Inputs:
 * - `duration`: Sets the duration of the pan animation in milliseconds.
 * - `pixelDelta`: Sets the number of pixels to move with each pan action.
 *
 * Computed Property:
 * - `interaction`: Returns an instance of the `KeyboardPan` interaction with
 *   the configured duration and pixel delta.
 */
class OlKeyboardPanDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.duration = input(100, { transform: numberAttribute });
        this.pixelDelta = input(128, { transform: numberAttribute });
        this.interaction = computed(() => new KeyboardPan({
            duration: this.duration(),
            pixelDelta: this.pixelDelta()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlKeyboardPanDirective_BaseFactory; return function OlKeyboardPanDirective_Factory(__ngFactoryType__) { return (ɵOlKeyboardPanDirective_BaseFactory || (ɵOlKeyboardPanDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlKeyboardPanDirective)))(__ngFactoryType__ || OlKeyboardPanDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlKeyboardPanDirective, selectors: [["ol-keyboard-pan"]], inputs: { duration: [1, "duration"], pixelDelta: [1, "pixelDelta"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlKeyboardPanDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlKeyboardPanDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-keyboard-pan',
                providers: [{ provide: OlInteraction, useExisting: OlKeyboardPanDirective }]
            }]
    }], null, null); })();

/**
 * Directive that encapsulates the OpenLayers DoubleClickZoom interaction for angular applications.
 * Provides configuration options for the interaction's behavior such as duration and zoom delta.
 * This directive extends OlInteraction with type parameter DoubleClickZoom.
 *
 * Selector: `ol-dbl-click-zoom`
 *
 * Dependencies: It provides `OlInteraction` using itself (`OlDblClickZoomDirective`) as an implementation.
 *
 * Properties:
 * - `duration`: Configures the animation duration for the double-click zoom interaction, defaulting to 250 milliseconds.
 * - `delta`: Sets the zoom delta, which specifies the magnitude of the zoom on double-click, defaulting to 1.
 *
 * Computed Properties:
 * - `interaction`: A computed property that initializes the `DoubleClickZoom` interaction based on `duration` and `delta` properties.
 */
class OlDblClickZoomDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.duration = input(250, { transform: numberAttribute });
        this.delta = input(1, { transform: numberAttribute });
        this.interaction = computed(() => new DoubleClickZoom({
            duration: this.duration(),
            delta: this.delta()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlDblClickZoomDirective_BaseFactory; return function OlDblClickZoomDirective_Factory(__ngFactoryType__) { return (ɵOlDblClickZoomDirective_BaseFactory || (ɵOlDblClickZoomDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlDblClickZoomDirective)))(__ngFactoryType__ || OlDblClickZoomDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlDblClickZoomDirective, selectors: [["ol-dbl-click-zoom"]], inputs: { duration: [1, "duration"], delta: [1, "delta"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlDblClickZoomDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlDblClickZoomDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-dbl-click-zoom',
                providers: [{ provide: OlInteraction, useExisting: OlDblClickZoomDirective }]
            }]
    }], null, null); })();

class DragBoxDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.boxEnded = output();
        this.boxStarted = output();
    }
    ngOnInit() {
        super.ngOnInit();
        const interaction = this.interaction();
        this.eventKeys.push(interaction.on('boxend', (evt) => this.boxEnded.emit(evt.coordinate)));
        this.eventKeys.push(interaction.on('boxstart', (evt) => this.boxStarted.emit(evt.coordinate)));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵDragBoxDirective_BaseFactory; return function DragBoxDirective_Factory(__ngFactoryType__) { return (ɵDragBoxDirective_BaseFactory || (ɵDragBoxDirective_BaseFactory = i0.ɵɵgetInheritedFactory(DragBoxDirective)))(__ngFactoryType__ || DragBoxDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: DragBoxDirective, outputs: { boxEnded: "boxEnded", boxStarted: "boxStarted" }, features: [i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DragBoxDirective, [{
        type: Directive
    }], null, null); })();
class OlDragZoomDirective extends DragBoxDirective {
    constructor() {
        super(...arguments);
        this.className = input();
        this.minArea = input(64, { transform: numberAttribute });
        this.out = input(false, { transform: booleanAttribute });
        this.duration = input(200, { transform: numberAttribute });
        this.interaction = computed(() => new DragZoom({
            className: this.className(),
            duration: this.duration(),
            out: this.out(),
            minArea: this.minArea()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlDragZoomDirective_BaseFactory; return function OlDragZoomDirective_Factory(__ngFactoryType__) { return (ɵOlDragZoomDirective_BaseFactory || (ɵOlDragZoomDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlDragZoomDirective)))(__ngFactoryType__ || OlDragZoomDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlDragZoomDirective, selectors: [["ol-drag-zoom"]], inputs: { className: [1, "className"], minArea: [1, "minArea"], out: [1, "out"], duration: [1, "duration"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlDragZoomDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlDragZoomDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-drag-zoom',
                providers: [{ provide: OlInteraction, useExisting: OlDragZoomDirective }]
            }]
    }], null, null); })();

/**
 * Directive to create and configure a DragRotate interaction for OpenLayers maps.
 *
 * This directive extends the `OlInteraction` base class and provides functionality to enable
 * drag-rotation of the map. It uses OpenLayers' `DragRotate` interaction internally.
 *
 * Properties:
 * - `duration`: A reactive input that sets the animation duration (in milliseconds)
 *   for the map rotation. The default duration is 250.
 *
 * Computed Properties:
 * - `interaction`: A computed property that initializes and returns an instance of
 *   the `DragRotate` interaction. It uses the specified `duration` for the rotation animation.
 */
class OlDragRotateDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.duration = input(250, { transform: numberAttribute });
        this.interaction = computed(() => new DragRotate({
            duration: this.duration()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlDragRotateDirective_BaseFactory; return function OlDragRotateDirective_Factory(__ngFactoryType__) { return (ɵOlDragRotateDirective_BaseFactory || (ɵOlDragRotateDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlDragRotateDirective)))(__ngFactoryType__ || OlDragRotateDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlDragRotateDirective, selectors: [["ol-drag-rotate"]], inputs: { duration: [1, "duration"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlDragRotateDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlDragRotateDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-drag-rotate',
                providers: [{ provide: OlInteraction, useExisting: OlDragRotateDirective }]
            }]
    }], null, null); })();

/**
 * Directive that provides a wrapper for the OpenLayers `PinchRotate` interaction.
 * This directive can be used to configure and attach the `PinchRotate` interaction
 * to an OpenLayers map. PinchRotate interaction allows users to rotate the map
 * by using a two-finger touch gesture.
 *
 * Inputs:
 * - `duration`: The duration of the interaction in milliseconds. Determines how
 *   quickly the rotation is applied after the gesture. Default value is `250`.
 * - `threshold`: The threshold for minimum rotation angle in radians before the
 *   interaction is triggered. Default value is `0.3`.
 *
 * Properties:
 * - `interaction`: A computed property that instantiates a `PinchRotate` interaction
 *   using the configured `duration` and `threshold` values.
 *
 * This directive internally provides an instance of `OlInteraction` for dependency
 * injection usage when attached to components.
 */
class OlPinchRotateDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.duration = input(250, { transform: numberAttribute });
        this.threshold = input(0.3, { transform: numberAttribute });
        this.interaction = computed(() => new PinchRotate({
            duration: this.duration(),
            threshold: this.threshold()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlPinchRotateDirective_BaseFactory; return function OlPinchRotateDirective_Factory(__ngFactoryType__) { return (ɵOlPinchRotateDirective_BaseFactory || (ɵOlPinchRotateDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlPinchRotateDirective)))(__ngFactoryType__ || OlPinchRotateDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlPinchRotateDirective, selectors: [["ol-pinch-rotate"]], inputs: { duration: [1, "duration"], threshold: [1, "threshold"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlPinchRotateDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlPinchRotateDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-pinch-rotate',
                providers: [{ provide: OlInteraction, useExisting: OlPinchRotateDirective }]
            }]
    }], null, null); })();

class OlPinchZoomDirective extends OlInteraction {
    constructor() {
        super(...arguments);
        this.duration = input(400, { transform: numberAttribute });
        this.interaction = computed(() => new PinchZoom({
            duration: this.duration()
        }));
    }
    static { this.ɵfac = /*@__PURE__*/ (() => { let ɵOlPinchZoomDirective_BaseFactory; return function OlPinchZoomDirective_Factory(__ngFactoryType__) { return (ɵOlPinchZoomDirective_BaseFactory || (ɵOlPinchZoomDirective_BaseFactory = i0.ɵɵgetInheritedFactory(OlPinchZoomDirective)))(__ngFactoryType__ || OlPinchZoomDirective); }; })(); }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlPinchZoomDirective, selectors: [["ol-pinch-rotate"]], inputs: { duration: [1, "duration"] }, features: [i0.ɵɵProvidersFeature([{ provide: OlInteraction, useExisting: OlPinchZoomDirective }]), i0.ɵɵInheritDefinitionFeature] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlPinchZoomDirective, [{
        type: Directive,
        args: [{
                selector: 'ol-pinch-rotate',
                providers: [{ provide: OlInteraction, useExisting: OlPinchZoomDirective }]
            }]
    }], null, null); })();

/**
 * Directive to enable a set of default interactions for an OpenLayers map.
 *
 * This directive binds several commonly used interaction handlers to the map,
 * such as drag rotation, double-click zoom, drag pan, keyboard navigation,
 * mouse wheel zoom, and pinch gestures. These interactions are enabled by
 * injecting their respective directives and adding them to the OpenLayers
 * interactions manager.
 *
 * Each supported interaction directive is automatically bound and initialized
 * when the `OlDefaultInteractionDirective` is instantiated and its lifecycle
 * `ngOnInit` is executed.
 *
 * The following interactions are included:
 * - Drag rotation (`OlDragRotateDirective`)
 * - Double-click zoom (`OlDblClickZoomDirective`)
 * - Drag panning (`OlDragPanDirective`)
 * - Keyboard panning (`OlKeyboardPanDirective`)
 * - Keyboard zooming (`OlKeyboardZoomDirective`)
 * - Mouse wheel zooming (`OlMouseWheelZoomDirective`)
 * - Drag zooming (`OlDragZoomDirective`)
 * - Pinch rotation (`OlPinchRotateDirective`)
 * - Pinch zooming (`OlPinchZoomDirective`)
 */
class OlDefaultInteractionDirective {
    #interactions = inject(OlInteractionsDirective);
    #dragRotate = inject(OlDragRotateDirective);
    #dblClickZoom = inject(OlDblClickZoomDirective);
    #dragPan = inject(OlDragPanDirective);
    #keyboardPan = inject(OlKeyboardPanDirective);
    #keyboardZoom = inject(OlKeyboardZoomDirective);
    #mouseWheelZoom = inject(OlMouseWheelZoomDirective);
    #dragZoom = inject(OlDragZoomDirective);
    #pinchRotate = inject(OlPinchRotateDirective);
    #pinchZoom = inject(OlPinchZoomDirective);
    ngOnInit() {
        this.#interactions.addInteraction(this.#dragRotate);
        this.#interactions.addInteraction(this.#dblClickZoom);
        this.#interactions.addInteraction(this.#dragPan);
        this.#interactions.addInteraction(this.#keyboardPan);
        this.#interactions.addInteraction(this.#keyboardZoom);
        this.#interactions.addInteraction(this.#mouseWheelZoom);
        this.#interactions.addInteraction(this.#dragZoom);
        this.#interactions.addInteraction(this.#pinchRotate);
        this.#interactions.addInteraction(this.#pinchZoom);
    }
    static { this.ɵfac = function OlDefaultInteractionDirective_Factory(__ngFactoryType__) { return new (__ngFactoryType__ || OlDefaultInteractionDirective)(); }; }
    static { this.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: OlDefaultInteractionDirective, selectors: [["", "defaultInteractions", ""]], features: [i0.ɵɵHostDirectivesFeature([OlDragRotateDirective, OlDblClickZoomDirective, OlDragPanDirective, OlKeyboardPanDirective, OlKeyboardZoomDirective, OlMouseWheelZoomDirective, OlDragZoomDirective, OlPinchRotateDirective, OlPinchZoomDirective])] }); }
}
(() => { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OlDefaultInteractionDirective, [{
        type: Directive,
        args: [{
                selector: '[defaultInteractions]',
                hostDirectives: [
                    OlDragRotateDirective,
                    OlDblClickZoomDirective,
                    OlDragPanDirective,
                    OlKeyboardPanDirective,
                    OlKeyboardZoomDirective,
                    OlMouseWheelZoomDirective,
                    OlDragZoomDirective,
                    OlPinchRotateDirective,
                    OlPinchZoomDirective
                ]
            }]
    }], null, null); })();

/*
 * Public API Surface of ngx-ol-interaction
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DragBoxDirective, OlDblClickDragZoomDirective, OlDblClickZoomDirective, OlDefaultInteractionDirective, OlDragPanDirective, OlDragRotateDirective, OlDragZoomDirective, OlKeyboardPanDirective, OlKeyboardZoomDirective, OlMouseWheelZoomDirective, OlPinchRotateDirective, OlPinchZoomDirective };
//# sourceMappingURL=nasumilu-ngx-ol-interaction.mjs.map
