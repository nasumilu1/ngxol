import PinchZoom from "ol/interaction/PinchZoom";
import { OlInteraction } from '@nasumilu/ngx-ol-map';
import * as i0 from "@angular/core";
export declare class OlPinchZoomDirective extends OlInteraction<PinchZoom> {
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<PinchZoom>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlPinchZoomDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlPinchZoomDirective, "ol-pinch-rotate", never, { "duration": { "alias": "duration"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-pinch-zoom.directive.d.ts.map