import { OlInteraction } from "@nasumilu/ngx-ol-map";
import KeyboardPan from "ol/interaction/KeyboardPan";
import * as i0 from "@angular/core";
/**
 * Directive that provides a customized OpenLayers KeyboardPan interaction.
 *
 * This directive allows users to pan the map using keyboard arrow keys. The duration
 * and pixel delta for the pan action can be configured through the provided inputs.
 *
 * Selector: 'ol-keyboard-pan'
 *
 * Extends:
 * - OlInteraction<KeyboardPan>
 *
 * Dependencies:
 * - OpenLayers `KeyboardPan` interaction
 *
 * Inputs:
 * - `duration`: Sets the duration of the pan animation in milliseconds.
 * - `pixelDelta`: Sets the number of pixels to move with each pan action.
 *
 * Computed Property:
 * - `interaction`: Returns an instance of the `KeyboardPan` interaction with
 *   the configured duration and pixel delta.
 */
export declare class OlKeyboardPanDirective extends OlInteraction<KeyboardPan> {
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly pixelDelta: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<KeyboardPan>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlKeyboardPanDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlKeyboardPanDirective, "ol-keyboard-pan", never, { "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "pixelDelta": { "alias": "pixelDelta"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-keyboard-pan.directive.d.ts.map