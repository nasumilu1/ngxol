import { OlInteraction } from "@nasumilu/ngx-ol-map";
import DoubleClickZoom from "ol/interaction/DoubleClickZoom";
import * as i0 from "@angular/core";
/**
 * Directive that encapsulates the OpenLayers DoubleClickZoom interaction for angular applications.
 * Provides configuration options for the interaction's behavior such as duration and zoom delta.
 * This directive extends OlInteraction with type parameter DoubleClickZoom.
 *
 * Selector: `ol-dbl-click-zoom`
 *
 * Dependencies: It provides `OlInteraction` using itself (`OlDblClickZoomDirective`) as an implementation.
 *
 * Properties:
 * - `duration`: Configures the animation duration for the double-click zoom interaction, defaulting to 250 milliseconds.
 * - `delta`: Sets the zoom delta, which specifies the magnitude of the zoom on double-click, defaulting to 1.
 *
 * Computed Properties:
 * - `interaction`: A computed property that initializes the `DoubleClickZoom` interaction based on `duration` and `delta` properties.
 */
export declare class OlDblClickZoomDirective extends OlInteraction<DoubleClickZoom> {
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly delta: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<DoubleClickZoom>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlDblClickZoomDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlDblClickZoomDirective, "ol-dbl-click-zoom", never, { "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "delta": { "alias": "delta"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-dbl-click-zoom.directive.d.ts.map