import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./ol-drag-rotate.directive";
import * as i2 from "./ol-dbl-click-zoom.directive";
import * as i3 from "./ol-drag-pan.directive";
import * as i4 from "./ol-keyboard-pan.directive";
import * as i5 from "./ol-keyboard-zoom.directive";
import * as i6 from "./ol-mouse-wheel-zoom.directive";
import * as i7 from "./ol-drag-zoom.directive";
import * as i8 from "./ol-pinch-rotate.directive";
import * as i9 from "./ol-pinch-zoom.directive";
/**
 * Directive to enable a set of default interactions for an OpenLayers map.
 *
 * This directive binds several commonly used interaction handlers to the map,
 * such as drag rotation, double-click zoom, drag pan, keyboard navigation,
 * mouse wheel zoom, and pinch gestures. These interactions are enabled by
 * injecting their respective directives and adding them to the OpenLayers
 * interactions manager.
 *
 * Each supported interaction directive is automatically bound and initialized
 * when the `OlDefaultInteractionDirective` is instantiated and its lifecycle
 * `ngOnInit` is executed.
 *
 * The following interactions are included:
 * - Drag rotation (`OlDragRotateDirective`)
 * - Double-click zoom (`OlDblClickZoomDirective`)
 * - Drag panning (`OlDragPanDirective`)
 * - Keyboard panning (`OlKeyboardPanDirective`)
 * - Keyboard zooming (`OlKeyboardZoomDirective`)
 * - Mouse wheel zooming (`OlMouseWheelZoomDirective`)
 * - Drag zooming (`OlDragZoomDirective`)
 * - Pinch rotation (`OlPinchRotateDirective`)
 * - Pinch zooming (`OlPinchZoomDirective`)
 */
export declare class OlDefaultInteractionDirective implements OnInit {
    #private;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlDefaultInteractionDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlDefaultInteractionDirective, "[defaultInteractions]", never, {}, {}, never, never, true, [{ directive: typeof i1.OlDragRotateDirective; inputs: {}; outputs: {}; }, { directive: typeof i2.OlDblClickZoomDirective; inputs: {}; outputs: {}; }, { directive: typeof i3.OlDragPanDirective; inputs: {}; outputs: {}; }, { directive: typeof i4.OlKeyboardPanDirective; inputs: {}; outputs: {}; }, { directive: typeof i5.OlKeyboardZoomDirective; inputs: {}; outputs: {}; }, { directive: typeof i6.OlMouseWheelZoomDirective; inputs: {}; outputs: {}; }, { directive: typeof i7.OlDragZoomDirective; inputs: {}; outputs: {}; }, { directive: typeof i8.OlPinchRotateDirective; inputs: {}; outputs: {}; }, { directive: typeof i9.OlPinchZoomDirective; inputs: {}; outputs: {}; }]>;
}
//# sourceMappingURL=ol-default-interaction.directive.d.ts.map