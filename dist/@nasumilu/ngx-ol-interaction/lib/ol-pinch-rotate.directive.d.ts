import PinchRotate from "ol/interaction/PinchRotate";
import { OlInteraction } from '@nasumilu/ngx-ol-map';
import * as i0 from "@angular/core";
/**
 * Directive that provides a wrapper for the OpenLayers `PinchRotate` interaction.
 * This directive can be used to configure and attach the `PinchRotate` interaction
 * to an OpenLayers map. PinchRotate interaction allows users to rotate the map
 * by using a two-finger touch gesture.
 *
 * Inputs:
 * - `duration`: The duration of the interaction in milliseconds. Determines how
 *   quickly the rotation is applied after the gesture. Default value is `250`.
 * - `threshold`: The threshold for minimum rotation angle in radians before the
 *   interaction is triggered. Default value is `0.3`.
 *
 * Properties:
 * - `interaction`: A computed property that instantiates a `PinchRotate` interaction
 *   using the configured `duration` and `threshold` values.
 *
 * This directive internally provides an instance of `OlInteraction` for dependency
 * injection usage when attached to components.
 */
export declare class OlPinchRotateDirective extends OlInteraction<PinchRotate> {
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly threshold: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<PinchRotate>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlPinchRotateDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlPinchRotateDirective, "ol-pinch-rotate", never, { "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "threshold": { "alias": "threshold"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-pinch-rotate.directive.d.ts.map