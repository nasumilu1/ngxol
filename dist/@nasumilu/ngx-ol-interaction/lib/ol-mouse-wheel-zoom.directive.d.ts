import { OlInteraction } from '@nasumilu/ngx-ol-map';
import MouseWheelZoom from "ol/interaction/MouseWheelZoom";
import * as i0 from "@angular/core";
/**
 * Directive to provide an integration with OpenLayers MouseWheelZoom interaction.
 * This directive enables configuring and controlling mouse wheel zoom behavior on an OpenLayers map.
 *
 * This directive extends the base OpenLayers interaction to provide additional configuration through
 * reactive inputs and computed behavior.
 *
 * Selector:
 * - ol-mouse-wheel-zoom: Apply this directive to automatically set up the MouseWheelZoom interaction.
 *
 * Inputs:
 * - onFocusOnly: A boolean input to determine whether zooming should only occur when the map has focus.
 * - duration: A number input specifying the animation duration for the zoom interaction in milliseconds.
 * - maxDelta: A number input defining the maximum zoom delta value.
 * - timeout: A number input setting the timeout duration in milliseconds to consider consecutive zoom actions as distinct.
 * - useAnchor: A boolean input specifying whether to use the mouse pointer location as an anchor for the zoom interaction.
 * - constrainResolution: A boolean input to determine whether zoom levels should be constrained to predefined resolutions.
 *
 * Property:
 * - interaction: A computed property that initializes and returns the MouseWheelZoom interaction based on current input values.
 */
export declare class OlMouseWheelZoomDirective extends OlInteraction<MouseWheelZoom> {
    readonly onFocusOnly: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly maxDelta: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly timeout: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly useAnchor: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly constrainResolution: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly interaction: import("@angular/core").Signal<MouseWheelZoom>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlMouseWheelZoomDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlMouseWheelZoomDirective, "ol-mouse-wheel-zoom", never, { "onFocusOnly": { "alias": "onFocusOnly"; "required": false; "isSignal": true; }; "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "maxDelta": { "alias": "maxDelta"; "required": false; "isSignal": true; }; "timeout": { "alias": "timeout"; "required": false; "isSignal": true; }; "useAnchor": { "alias": "useAnchor"; "required": false; "isSignal": true; }; "constrainResolution": { "alias": "constrainResolution"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-mouse-wheel-zoom.directive.d.ts.map