import { OlInteraction } from "@nasumilu/ngx-ol-map";
import DragPan from "ol/interaction/DragPan";
import { Kinetic } from "ol";
import * as i0 from "@angular/core";
/**
 * Directive that encapsulates the OpenLayers DragPan interaction, allowing
 * integration with an Angular application. The DragPan interaction enables
 * panning of the map by dragging with the mouse or finger on touch devices.
 *
 * It extends the base OlInteraction class, providing additional functionality
 * specific to the DragPan interaction.
 *
 * Selector:
 * - `ol-drag-pan`
 *
 * Providers:
 * - Provides `OlInteraction` using this directive as its implementation.
 *
 * Inputs:
 * - `kinetic` (optional): Kinetic inertia settings for the pan interaction.
 * - `onFocusOnly` (optional): A boolean attribute determining whether the
 *   interaction is active only when the map has focus.
 *
 * Computed Interaction:
 * - Constructs and returns a new OpenLayers `DragPan` instance based on the provided inputs.
 * - Uses reactive bindings for input changes.
 */
export declare class OlDragPanDirective extends OlInteraction<DragPan> {
    readonly kinetic: import("@angular/core").InputSignal<Kinetic | undefined>;
    readonly onFocusOnly: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly interaction: import("@angular/core").Signal<DragPan>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlDragPanDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlDragPanDirective, "ol-drag-pan", never, { "kinetic": { "alias": "kinetic"; "required": false; "isSignal": true; }; "onFocusOnly": { "alias": "onFocusOnly"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-drag-pan.directive.d.ts.map