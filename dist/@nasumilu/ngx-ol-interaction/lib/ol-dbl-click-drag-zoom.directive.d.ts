import { OlInteraction } from "@nasumilu/ngx-ol-map";
import DblClickDragZoom from "ol/interaction/DblClickDragZoom";
import * as i0 from "@angular/core";
/**
 * Directive for creating an OpenLayers DblClickDragZoom interaction.
 * This directive can be used to configure and add a `DblClickDragZoom` interaction
 * to an OpenLayers map. It provides a way to set properties such as duration
 * and zoom delta for the interaction.
 *
 * The interaction allows the user to zoom the map by double-clicking and
 * dragging the mouse.
 *
 * Extends:
 *   OlInteraction<DblClickDragZoom>
 *
 * Inputs:
 * - `duration`: The duration of the zoom animation in milliseconds. Defaults to 400ms.
 * - `delta`: The zoom delta applied on each zoom. Defaults to 1.
 *
 * Computed Properties:
 * - `interaction`: Creates a new `DblClickDragZoom` interaction based on the configured
 *   duration and delta inputs.
 */
export declare class OlDblClickDragZoomDirective extends OlInteraction<DblClickDragZoom> {
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly delta: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<DblClickDragZoom>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlDblClickDragZoomDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlDblClickDragZoomDirective, "ol-dbl-click-drag-zoom", never, { "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "delta": { "alias": "delta"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-dbl-click-drag-zoom.directive.d.ts.map