import { OnInit } from '@angular/core';
import { OlInteraction } from "@nasumilu/ngx-ol-map";
import DragZoom from 'ol/interaction/DragZoom';
import DragBox from "ol/interaction/DragBox";
import { Coordinate } from "ol/coordinate";
import * as i0 from "@angular/core";
export declare abstract class DragBoxDirective<T extends DragBox> extends OlInteraction<T> implements OnInit {
    readonly boxEnded: import("@angular/core").OutputEmitterRef<Coordinate>;
    readonly boxStarted: import("@angular/core").OutputEmitterRef<Coordinate>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DragBoxDirective<any>, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DragBoxDirective<any>, never, never, {}, { "boxEnded": "boxEnded"; "boxStarted": "boxStarted"; }, never, never, true, never>;
}
export declare class OlDragZoomDirective extends DragBoxDirective<DragZoom> {
    readonly className: import("@angular/core").InputSignal<string | undefined>;
    readonly minArea: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly out: import("@angular/core").InputSignalWithTransform<boolean, unknown>;
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<DragZoom>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlDragZoomDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlDragZoomDirective, "ol-drag-zoom", never, { "className": { "alias": "className"; "required": false; "isSignal": true; }; "minArea": { "alias": "minArea"; "required": false; "isSignal": true; }; "out": { "alias": "out"; "required": false; "isSignal": true; }; "duration": { "alias": "duration"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-drag-zoom.directive.d.ts.map