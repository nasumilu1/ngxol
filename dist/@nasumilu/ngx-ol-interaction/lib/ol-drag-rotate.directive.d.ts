import { OlInteraction } from "@nasumilu/ngx-ol-map";
import DragRotate from "ol/interaction/DragRotate";
import * as i0 from "@angular/core";
/**
 * Directive to create and configure a DragRotate interaction for OpenLayers maps.
 *
 * This directive extends the `OlInteraction` base class and provides functionality to enable
 * drag-rotation of the map. It uses OpenLayers' `DragRotate` interaction internally.
 *
 * Properties:
 * - `duration`: A reactive input that sets the animation duration (in milliseconds)
 *   for the map rotation. The default duration is 250.
 *
 * Computed Properties:
 * - `interaction`: A computed property that initializes and returns an instance of
 *   the `DragRotate` interaction. It uses the specified `duration` for the rotation animation.
 */
export declare class OlDragRotateDirective extends OlInteraction<DragRotate> {
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<DragRotate>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlDragRotateDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlDragRotateDirective, "ol-drag-rotate", never, { "duration": { "alias": "duration"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-drag-rotate.directive.d.ts.map