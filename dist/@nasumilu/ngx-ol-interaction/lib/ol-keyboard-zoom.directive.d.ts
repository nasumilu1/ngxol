import { OlInteraction } from "@nasumilu/ngx-ol-map";
import KeyboardZoom from "ol/interaction/KeyboardZoom";
import * as i0 from "@angular/core";
/**
 * Directive for enabling keyboard-zoom interaction with OpenLayers maps.
 *
 * This directive extends the `OlInteraction` base class and utilizes the
 * `KeyboardZoom` interaction from OpenLayers. It provides properties to
 * configure the zoom behavior when triggered by specific keyboard events.
 *
 * Properties:
 * - `duration`: Configurable duration in milliseconds for the zoom animation.
 * - `delta`: Configurable zoom level increment or decrement value.
 *
 * The directive computes an instance of the `KeyboardZoom` interaction using
 * the defined properties.
 */
export declare class OlKeyboardZoomDirective extends OlInteraction<KeyboardZoom> {
    readonly duration: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly delta: import("@angular/core").InputSignalWithTransform<number, unknown>;
    readonly interaction: import("@angular/core").Signal<KeyboardZoom>;
    static ɵfac: i0.ɵɵFactoryDeclaration<OlKeyboardZoomDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<OlKeyboardZoomDirective, "ol-keyboard-zoom", never, { "duration": { "alias": "duration"; "required": false; "isSignal": true; }; "delta": { "alias": "delta"; "required": false; "isSignal": true; }; }, {}, never, never, true, never>;
}
//# sourceMappingURL=ol-keyboard-zoom.directive.d.ts.map