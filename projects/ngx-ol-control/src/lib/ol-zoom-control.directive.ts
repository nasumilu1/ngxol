import {computed, Directive, input, numberAttribute} from '@angular/core';
import Zoom from "ol/control/Zoom";
import {OlControl} from '@nasumilu/ngx-ol-map';

/**
 * A directive that provides a configurable OpenLayers zoom control.
 *
 * This directive extends the base `OlControl` for OpenLayers and integrates
 * a zoom control, allowing customization of various parameters such as animation
 * duration, button class names, labels, and zoom delta values.
 *
 * Directive Selector:
 * - `ol-zoom-control`
 *
 * Host Directives:
 * - `ClassNameDirective`: Allows setting a custom CSS class name for the control.
 *
 * Providers:
 * - Provides `OlControl` token, using itself (`OlZoomControlDirective`) as the implementation.
 *
 * Inputs:
 * - `duration`: The duration (in milliseconds) for zoom animations. Default is `250`.
 * - `zoomInClassName`: Custom class name for the zoom-in button. Default is `'ol-zoom-in'`.
 * - `zoomOutClassName`: Custom class name for the zoom-out button. Default is `'ol-zoom-out'`.
 * - `zoomInLabel`: Label or content for the zoom-in button. Default is `'+'`.
 * - `zoomOutLabel`: Label or content for the zoom-out button. Default is `'-'`.
 * - `zoomInTipLabel`: Tooltip label for the zoom-in button. Default is `'Zoom in'`.
 * - `zoomOutTipLabel`: Tooltip label for the zoom-out button. Default is `'Zoom out'`.
 * - `delta`: The zoom delta value, determining the zoom level increment or decrement. Default is `1`.
 *
 * Computed Properties:
 * - `control`: Generates a new `Zoom` instance configured with the provided inputs, applying
 *   all specified properties like class names, labels, and animation duration.
 */
@Directive({
  selector: 'ol-zoom-control',
  providers: [{provide: OlControl, useExisting: OlZoomControlDirective}]
})
export class OlZoomControlDirective extends OlControl<Zoom> {

  public readonly className = input<string>();
  public readonly duration = input(250, {transform: numberAttribute});
  public readonly zoomInClassName = input('ol-zoom-in');
  public readonly zoomOutClassName = input('ol-zoom-out');
  public readonly zoomInLabel = input('+');
  public readonly zoomOutLabel = input('-');
  public readonly zoomInTipLabel = input('Zoom in');
  public readonly zoomOutTipLabel = input('Zoom out');
  public readonly delta = input(1, {transform: numberAttribute});


  public readonly control = computed(() => new Zoom({
      className: this.className(),
      duration: this.duration(),
      zoomInClassName: this.zoomInClassName(),
      zoomOutClassName: this.zoomOutClassName(),
      zoomInLabel: this.zoomInLabel(),
      zoomOutLabel: this.zoomOutLabel(),
      zoomInTipLabel: this.zoomInTipLabel(),
      zoomOutTipLabel: this.zoomOutTipLabel(),
      delta: this.delta()
    })
  );

}

