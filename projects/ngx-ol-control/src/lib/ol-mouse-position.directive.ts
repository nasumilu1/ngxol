import {
  booleanAttribute,
  computed,
  Directive,
  input,
  model,
  OnDestroy,
  OnInit
} from '@angular/core';
import {CoordinateFormat} from "ol/coordinate";
import {get, ProjectionLike} from "ol/proj";
import MousePosition from "ol/control/MousePosition";
import {OlControl} from '@nasumilu/ngx-ol-map';
import {EventsKey} from "ol/events";
import {unByKey} from "ol/Observable";

/**
 * A directive that wraps the OpenLayers MousePosition control, allowing integration with Angular templates and bindings.
 * This directive provides options to control the format and projection of mouse position display, as well as other configurations.
 *
 * It extends the OlControl base class and binds inputs and models to control properties.
 *
 * Inputs:
 * - `className`: String input to define the CSS class name applied to the control's container element.
 * - `coordinateFormat`: A model input to define the formatting function for coordinates.
 * - `projection`: A model input to specify the projection used to transform coordinates.
 * - `placeholder`: String input to specify the placeholder text displayed when no position is available.
 * - `wrapX`: Boolean input that determines whether the world is wrapped horizontally.
 *
 * Lifecycle Methods:
 * - `ngOnInit`: Initializes the MousePosition control, subscribes to model updates, and updates the control state whenever bound properties change.
 * - `ngOnDestroy`: Cleans up event listeners and other resources when the directive is destroyed.
 */
@Directive({
  selector: 'ol-mouse-position',
  providers: [{provide: OlControl, useExisting: OlMousePositionDirective}]
})
export class OlMousePositionDirective extends OlControl<MousePosition> implements OnInit, OnDestroy {

  public readonly className = input<string>();
  public readonly coordinateFormat = model<CoordinateFormat>();
  public readonly projection = model<ProjectionLike>();
  public readonly placeholder = input<string>();
  public readonly wrapX = input(true, {transform: booleanAttribute});

  public override readonly control = computed(() => new MousePosition({
      className: this.className(),
      coordinateFormat: this.coordinateFormat(),
      projection: this.projection(),
      placeholder: this.placeholder(),
      wrapX: this.wrapX()
    })
  );

  #eventKeys: EventsKey[] = [];

  ngOnInit() {
    const control = this.control();

    this.projection.subscribe(value => {
      const _value = get(value);
      const projection = control.getProjection();
      if (_value?.getCode() != projection?.getCode()) {
        control.setProjection(value);
      }
    });

    this.coordinateFormat.subscribe(value => {
      const format = control.getCoordinateFormat();
      if (value != undefined && format !== value) {
        control.setCoordinateFormat(value);
      }
    })

    this.#eventKeys.push(control.on('change:projection', () => {
      const projection = control.getProjection();
      if (get(this.projection())?.getCode() !== projection?.getCode()) {
        this.projection.set(projection);
      }
    }));

    this.#eventKeys.push(control.on('change:coordinateFormat', () => {
      const format = control.getCoordinateFormat();
      if (this.coordinateFormat() !== format) {
        this.coordinateFormat.set(format);
      }
    }));
  }

  ngOnDestroy() {
    unByKey(this.#eventKeys);
    this.#eventKeys = [];
  }
}
