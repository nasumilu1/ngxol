import {Directive, inject, OnInit} from '@angular/core';
import {OlRotateDirective} from "./ol-rotate.directive";
import {OlZoomControlDirective} from "./ol-zoom-control.directive";
import {OlAttributionDirective} from "./ol-attribution.directive";
import {OlControlsDirective} from "@nasumilu/ngx-ol-map";

/**
 * A directive that provides a set of default controls for OpenLayers maps.
 * This directive automatically adds the specified controls to the parent
 * `OlControlsDirective` when initialized.
 *
 * By default, the following controls are included:
 * - Zoom control
 * - Rotate control
 * - Attribution control
 *
 * The directive relies on the framework's dependency injection mechanism
 * to obtain references to the requisite control directives.
 *
 * This directive should be used as an attribute on a container element,
 * and it will automatically synchronize the default controls with the map.
 */
@Directive({
  selector: '[defaultControls]',
  hostDirectives: [
    OlZoomControlDirective,
    OlRotateDirective,
    OlAttributionDirective
  ]
})
export class OlDefaultControlDirective implements OnInit {

  readonly #controls = inject(OlControlsDirective);

  readonly #zoomControl = inject(OlZoomControlDirective);
  readonly #rotateControl = inject(OlRotateDirective);
  readonly #attributionControl = inject(OlAttributionDirective);

  ngOnInit(): void {
    this.#controls.addControl(this.#zoomControl);
    this.#controls.addControl(this.#rotateControl);
    this.#controls.addControl(this.#attributionControl);
  }

}
