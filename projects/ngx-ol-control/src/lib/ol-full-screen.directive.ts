import {
  booleanAttribute, computed, Directive, input, OnDestroy, OnInit, output
} from '@angular/core';
import {OlControl} from "@nasumilu/ngx-ol-map";
import FullScreen from "ol/control/FullScreen";
import {EventsKey} from "ol/events";
import {unByKey} from "ol/Observable";

/**
 * Directive for integrating OpenLayers FullScreen control.
 *
 * This directive provides a convenient way to add and configure the FullScreen control in an OpenLayers map.
 * It allows customization of control properties such as class names, labels, and key interactions for the fullscreen toggle functionality.
 *
 * Features:
 * - Configurable class names for active/inactive states.
 * - Customizable labels for the FullScreen button.
 * - Emits events on entering and leaving fullscreen mode.
 * - Optional keyboard key support for toggling fullscreen mode.
 *
 * Inputs:
 * - `className`: Defines the CSS class name applied to the FullScreen element.
 * - `label`: Text or content used as the label for the control (inactive state).
 * - `tipLabel`: Tooltip text for the control.
 * - `labelActive`: Text or content used as the label for the control (active state).
 * - `activeClassName`: CSS class applied when fullscreen mode is active.
 * - `inactiveClassName`: CSS class applied when fullscreen mode is inactive.
 * - `keys`: Boolean defining whether keyboard shortcuts should be enabled for toggling fullscreen.
 *
 * Outputs:
 * - `fullScreenChange`: Event emitted whenever the fullscreen mode changes.
 *   Emits `true` when entering fullscreen and `false` when leaving.
 *
 * Lifecycle Hooks:
 * - Initializes and sets up event listeners for FullScreen control on `ngOnInit`.
 * - Cleans up event listeners on `ngOnDestroy`.
 */
@Directive({
  selector: 'ol-full-screen',
  providers: [{provide: OlControl, useExisting: OlFullScreenDirective}]
})
export class OlFullScreenDirective extends OlControl<FullScreen> implements OnInit, OnDestroy {
  public readonly className = input<string>();
  public readonly label = input<string>();
  public readonly tipLabel = input<string>();
  public readonly labelActive = input<string | HTMLElement | Text>();
  public readonly activeClassName = input<string>();
  public readonly inactiveClassName = input<string>();
  public readonly keys = input(false, {transform: booleanAttribute});

  public readonly fullScreenChange = output<boolean>();

  readonly control = computed(() => new FullScreen({
      className: this.className(),
      label: this.label(),
      tipLabel: this.tipLabel(),
      labelActive: this.labelActive(),
      activeClassName: this.activeClassName(),
      inactiveClassName: this.inactiveClassName(),
      keys: this.keys(),
    })
  );

  #eventKeys: EventsKey[] = [];

  ngOnInit() {
    this.#eventKeys.push(this.control().on('enterfullscreen', () => this.fullScreenChange.emit(true)));
    this.#eventKeys.push(this.control().on('leavefullscreen', () => this.fullScreenChange.emit(false)));
  }

  ngOnDestroy() {
    unByKey(this.#eventKeys);
    this.#eventKeys = [];
  }

}
