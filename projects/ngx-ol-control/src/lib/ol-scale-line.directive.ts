import {
  booleanAttribute,
  computed,
  Directive,
  input,
  model,
  numberAttribute, OnDestroy,
  OnInit
} from '@angular/core';
import ScaleLine, {Units} from "ol/control/ScaleLine";
import { OlControl } from '@nasumilu/ngx-ol-map';
import {EventsKey} from "ol/events";
import {unByKey} from "ol/Observable";

/**
 * Directive for integrating OpenLayers' ScaleLine control in Angular applications.
 * This directive enables seamless configuration and interaction with the ScaleLine control,
 * which adds a scale bar to a map for representing distances.
 *
 * Extends:
 * - OlControl<ScaleLine>: Provides interaction with OpenLayers' ScaleLine control.
 * - Angular lifecycle hooks OnInit and OnDestroy.
 *
 * Selector:
 * - ol-scale-line: Use this selector in templates to add the scale line control to a map.
 *
 * Dependencies:
 * - OpenLayers' ScaleLine class.
 *
 * Inputs:
 * - className: A string representing the CSS class name for the control.
 * - minWidth: A number defining the minimum width of the scale line in pixels. Default is 64.
 * - maxWidth: An optional number defining the maximum width of the scale line in pixels.
 * - units: Sets the units for measurement (e.g., metric, imperial). Default is 'metric'.
 * - bar: A boolean indicating whether to use the bar representation of the scale. Default is false.
 * - steps: A number determining the number of steps in the bar scale. Default is 4.
 * - text: A boolean indicating whether to display a textual representation of the scale. Default is false.
 * - dpi: An optional number specifying the dots per inch (DPI) for rendering accuracy.
 *
 * Outputs:
 * - binds to `units` via model changes to synchronize units between the directive and the ScaleLine control.
 *
 * Lifecycle Methods:
 * - ngOnInit(): Initializes the OpenLayers ScaleLine control and binds the `units` property for bidirectional synchronization.
 * - ngOnDestroy(): Cleans up event listeners attached to the ScaleLine control's `change:units` event.
 *
 * Computed Properties:
 * - control: A reactive property that creates and updates the ScaleLine control instance in response to changes in input properties.
 *
 * Notes:
 * - The directive automatically synchronizes the `units` input and updates the ScaleLine control's units when necessary.
 * - The OpenLayers `unByKey` function is used during destruction to unregister events attached to the control.
 */
@Directive({
  selector: 'ol-scale-line',
  providers: [{provide: OlControl, useExisting: OlScaleLineDirective}]
})
export class OlScaleLineDirective extends OlControl<ScaleLine> implements OnInit, OnDestroy {

  #eventKey?: EventsKey;

  public readonly className = input<string>();
  public readonly minWidth = input(64, {transform: numberAttribute});
  public readonly maxWidth = input(undefined, {transform: numberAttribute});
  public readonly units = model<Units>('metric');
  public readonly bar = input(false, {transform: booleanAttribute});
  public readonly steps = input(4, {transform: numberAttribute});
  public readonly text = input(false, {transform: booleanAttribute});
  public readonly dpi = input(undefined, {transform: numberAttribute});

  public override readonly control = computed(() => new ScaleLine({
    className: this.className(),
    minWidth: this.minWidth(),
    maxWidth: this.maxWidth(),
    units: this.units(),
    bar: this.bar(),
    steps: this.steps(),
    text: this.text(),
    dpi: this.dpi()
    })
  );

  ngOnInit() {
    const control = this.control();

    this.units.subscribe(value => {
      const units = control.getUnits();
      if (units !== value) {
        control.setUnits(value);
      }
    });

    this.#eventKey = control.on('change:units', () => {
      const units = control.getUnits();
      if (this.units() !== units) {
        this.units.set(units);
      }
    })
  }

  ngOnDestroy() {
    if (this.#eventKey) {
      unByKey(this.#eventKey);
      this.#eventKey = undefined;
    }
  }

}
