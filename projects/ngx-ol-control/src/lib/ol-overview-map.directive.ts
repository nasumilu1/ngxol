import {
  booleanAttribute,
  computed,
  contentChild,
  Directive,
  input
} from '@angular/core';
import {OlControl, OlLayersDirective} from '@nasumilu/ngx-ol-map';
import OverviewMap from "ol/control/OverviewMap";

/**
 * Directive for integrating an OpenLayers OverviewMap control as an Angular component.
 * This directive allows you to configure and use the OverviewMap control within an Angular application.
 * The control provides a small overview map that shows the current view extent of the larger map.
 *
 * Extends:
 * - OlControl<OverviewMap>
 *
 * Implements:
 * - AfterContentInit
 *
 * Features:
 * - Configurable properties such as class name, labels, collapse behavior, and layering.
 * - Supports dynamic addition and removal of layers via `OlLayersDirective`.
 * - Automatically updates the control based on the provided inputs.
 *
 * Inputs:
 * - `className`: Sets a custom class name for the OverviewMap control's container.
 * - `label`: Sets the label for the expand button.
 * - `tipLabel`: Sets the tooltip label for the expand button.
 * - `collapsed`: Controls whether the overview map is collapsed by default (default is `true`).
 * - `collapsible`: Enables or disables the collapsibility of the overview map (default is `false`).
 * - `collapseLabel`: Defines the label for the collapse button (default is `›`).
 * - `rotateWithView`: Toggles whether the overview map should rotate with the main view (default is `false`).
 *
 * Outputs:
 * - Integrates with `OlLayersDirective` to handle layer additions and removals dynamically.
 *
 * Lifecycle:
 * - `ngAfterContentInit`: Subscribes to layer changes from `OlLayersDirective` to update the internal layer collection of the OverviewMap control.
 */
@Directive({
  selector: 'ol-overview-map',
  providers: [{provide: OlControl, useExisting: OlOverviewMapDirective}]
})
export class OlOverviewMapDirective extends OlControl<OverviewMap> {

  private readonly layerDirective = contentChild(OlLayersDirective);

  public readonly className = input<string>();
  public readonly label = input<string>();
  public readonly tipLabel = input<string>();
  public readonly collapsed = input(true, {transform: booleanAttribute});
  public readonly collapsible = input(false, {transform: booleanAttribute});
  public readonly collapseLabel = input<string | HTMLElement>('›');
  public readonly rotateWithView = input(false, {transform: booleanAttribute});

  public override readonly control = computed(() => new OverviewMap({
      className: this.className(),
      label: this.label(),
      tipLabel: this.tipLabel(),
      collapsible: this.collapsible(),
      collapsed: this.collapsed(),
      collapseLabel: this.collapseLabel(),
      layers: this.layerDirective()?.layers.map(olLayer => olLayer.layer()),
      rotateWithView: this.rotateWithView()
    })
  );
}
