import {computed, Directive, input} from '@angular/core';
import {OlControl} from "@nasumilu/ngx-ol-map";
import ZoomToExtent from "ol/control/ZoomToExtent";
import {Extent} from "ol/extent";

/**
 * Directive that integrates OpenLayers ZoomToExtent control into Angular components.
 * This directive provides functionality to create and manage a ZoomToExtent control in
 * an OpenLayers map within an Angular application.
 *
 * The ZoomToExtent control provides a button to zoom to a predefined extent.
 *
 * Inputs:
 * - `className`: The CSS class name to apply to the control's container.
 * - `label`: The label to display on the control's button.
 * - `tipLabel`: The tooltip text to display when hovering over the control.
 * - `extent`: The extent (bounding box) to zoom to when the control is activated.
 *
 * This directive extends the `OlControl` base class to provide integration with other OpenLayers controls.
 */
@Directive({
  selector: 'ol-zoom-to-extent',
  providers: [{provide: OlControl, useExisting: OlZoomToExtentDirective}]
})
export class OlZoomToExtentDirective extends OlControl<ZoomToExtent> {

  public readonly className = input<string>();
  public readonly label = input<string>();
  public readonly tipLabel = input<string>();
  public readonly extent = input<Extent>();

  readonly control = computed(() => new ZoomToExtent({
      className: this.className(),
      label: this.label(),
      tipLabel: this.tipLabel(),
      extent: this.extent()
    })
  );
}
