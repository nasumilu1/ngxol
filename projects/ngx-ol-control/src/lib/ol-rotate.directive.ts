import {booleanAttribute, computed, Directive, input, numberAttribute} from '@angular/core';
import {OlControl} from "@nasumilu/ngx-ol-map";
import Rotate from "ol/control/Rotate";

/**
 * Directive to integrate OpenLayers Rotate control into Angular components.
 *
 * Extends the OlControl base class to provide bindings for the Rotate control in OpenLayers.
 * This directive allows customizing various options for the Rotate control using Angular's
 * reactive input system.
 *
 * Selector: `ol-rotate`
 *
 * Inputs:
 * - `className` (string): CSS class name to set to the rotate control element.
 * - `label` (string): The label to display on the rotate control.
 * - `compassClassName` (string): CSS class name for the compass element within the control.
 * - `tipLabel` (string): Displayed tooltip text for the button.
 * - `duration` (number): The animation duration in milliseconds for the rotation reset.
 * - `autoHide` (boolean): If true, the control is hidden when the map rotation is 0.
 *
 * Control:
 * - Computed `control` property creates a new Rotate instance with the given configuration.
 */
@Directive({
  selector: 'ol-rotate',
  providers: [{provide: OlControl, useExisting: OlRotateDirective}]
})
export class OlRotateDirective extends OlControl<Rotate> {

  public readonly className = input<string>();
  public readonly label = input<string>();
  public readonly compassClassName = input<string>();
  public readonly tipLabel = input<string>();
  public readonly duration = input(250, {transform: numberAttribute});
  public readonly autoHide = input(true, {transform: booleanAttribute});

  readonly control = computed(() => new Rotate({
    className: this.className(),
    label: this.label(),
    tipLabel: this.tipLabel(),
    compassClassName: this.compassClassName(),
    duration: this.duration(),
    autoHide: this.autoHide()
    })
  );
}
