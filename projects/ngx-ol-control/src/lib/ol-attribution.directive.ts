import {
  booleanAttribute,
  computed,
  Directive,
  input
} from '@angular/core';
import Attribution from 'ol/control/Attribution';
import {OlControl} from '@nasumilu/ngx-ol-map';

/**
 * Directive representing an OpenLayers Attribution control for Angular applications.
 * This class extends the OlControl class, encapsulating the functionality of the OpenLayers Attribution control.
 *
 * The directive provides an interface to configure and manipulate the Attribution control with attributes for
 * class names, labels, collapsibility, and other related options.
 *
 * Selector:
 * - `ol-attribution`: Use this selector to include the directive in an Angular component's template.
 *
 * Attributes:
 * - `className`: Defines the CSS class name for the control. Accepts a string value.
 * - `label`: Specifies the label for the Attribution control. Accepts a string value.
 * - `tipLabel`: Provides the tooltip text displayed upon hovering over the attribution control. Accepts a string value.
 * - `collapsed`: Determines whether the attribution control should initially be rendered in a collapsed state. Accepts a boolean value.
 * - `collapsible`: Indicates if the attribution control is collapsible by the user. Accepts a boolean value.
 * - `collapseLabel`: Specifies the label shown in the control when it is collapsed. Accepts a string or HTMLElement value.
 * - `expandClassName`: The CSS class name applied when the control is expanded. Accepts a string value.
 * - `collapseClassName`: The CSS class name applied when the control is collapsed. Accepts a string value.
 *
 * Computed Properties:
 * - `control`: Represents the underlying OpenLayers Attribution control instance configured with the provided inputs.
 *
 * This directive is typically used to add Attribution control in OpenLayers-enabled Angular applications,
 * allowing users to easily toggle and customize map attribution information.
 */
@Directive({
  selector: 'ol-attribution',
  providers: [{provide: OlControl, useExisting: OlAttributionDirective}]
})
export class OlAttributionDirective extends OlControl<Attribution> {
  public readonly className = input<string>();
  public readonly label = input<string>();
  public readonly tipLabel = input<string>();
  public readonly collapsed = input(true, {transform: booleanAttribute});
  public readonly collapsible = input(false, {transform: booleanAttribute});
  public readonly collapseLabel = input<string | HTMLElement>('›');
  public readonly expandClassName = input('ol-attribution-expand');
  public readonly collapseClassName = input('ol-attribution-collapse');

  public override readonly control = computed(() => new Attribution({
      className: this.className(),
      label: this.label(),
      tipLabel: this.tipLabel(),
      collapsed: this.collapsed(),
      collapsible: this.collapsible(),
      collapseLabel: this.collapseLabel(),
      expandClassName: this.expandClassName(),
      collapseClassName: this.collapseClassName(),
    })
  );

}
