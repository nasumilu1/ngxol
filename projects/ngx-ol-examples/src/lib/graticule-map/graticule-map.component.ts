import {ChangeDetectionStrategy, Component, linkedSignal, viewChild} from '@angular/core';
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {
  OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent, OlViewDirective
} from '@nasumilu/ngx-ol-map';
import {
  MinMaxLayerZoomDirective, OlGraticleLayerDirective, OlTileLayerDirective, OlVectorLayerDirective, SourceDirective
} from '@nasumilu/ngx-ol-layer';
import {OSM} from "ol/source";
import {OlWFSSourceDirective} from "@nasumilu/ngx-ol-source";
import {Extent, getCenter} from "ol/extent";
import {FlatStyle} from "ol/style/flat";
import {MatSlider, MatSliderRangeThumb} from "@angular/material/slider";

@Component({
  selector: 'graticule-map',
  imports: [
    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlDefaultInteractionDirective,
    OlDefaultControlDirective, OlTileLayerDirective, OlGraticleLayerDirective, SourceDirective, OlWFSSourceDirective,
    OlVectorLayerDirective, MatSlider, MatSliderRangeThumb
  ],
  templateUrl: './graticule-map.component.html',
  styleUrl: './graticule-map.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GraticuleMapComponent {

  protected readonly minMaxZoomDirective = viewChild('riv_gauges_new', {read: MinMaxLayerZoomDirective});
  protected readonly viewDirective = viewChild(OlViewDirective);

  protected readonly minZoom = linkedSignal(() => this.minMaxZoomDirective()?.minZoom());
  protected readonly maxZoom = linkedSignal(() => this.minMaxZoomDirective()?.maxZoom());

  protected readonly extent: Extent = [-14229084,2643357,-5512723,6859294];
  protected readonly center = getCenter(this.extent);
  protected readonly style: FlatStyle = {
    "circle-radius": 8,
    "circle-fill-color": 'rgba(51,102,153,0.77)',
    "circle-stroke-color": "rgba(0,0,0,0.6)",
    "circle-stroke-width": 1.25,
    "text-value": ['get', 'size'],
    "text-fill-color": "rgb(255,255,255)"
  };

  protected readonly osmSource = new OSM();

  updateMinZoom(value: number) {
    this.minMaxZoomDirective()?.minZoom.set(value);
  }

  updateMaxZoom(value: number) {
    this.minMaxZoomDirective()?.maxZoom.set(value);
  }

}
