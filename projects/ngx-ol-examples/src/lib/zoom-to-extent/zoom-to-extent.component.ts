import { Component } from '@angular/core';
import {OlAttributionDirective, OlZoomControlDirective, OlZoomToExtentDirective} from "@nasumilu/ngx-ol-control";
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {OlMouseWheelZoomDirective} from "@nasumilu/ngx-ol-interaction";
import {OlTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {OSM} from "ol/source";

@Component({
  selector: 'zoom-to-extent',
  imports: [
    OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
    OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
    OlZoomToExtentDirective, SourceDirective
  ],
  templateUrl: './zoom-to-extent.component.html',
  styleUrl: './zoom-to-extent.component.css'
})
export class ZoomToExtentComponent {

  protected readonly source = new OSM();
  protected readonly extent = [-14815256,2011249,-6547035,6679622];

}
