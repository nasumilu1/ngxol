import {Component, inject, linkedSignal, viewChild} from '@angular/core';
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {
  OlVectorLayerDirective, OlWebGLTileLayerDirective, SourceDirective, ZIndexDirective
} from "@nasumilu/ngx-ol-layer";
import {OlGeoJsonSourceDirective, OlKmlSourceDirective} from "@nasumilu/ngx-ol-source";
import ImageTile from 'ol/source/ImageTile';
import {EXAMPLES_BASE_PATH, MAP_TILER_API_KEY} from "../configuration";
import {MatButtonToggle, MatButtonToggleChange, MatButtonToggleGroup} from "@angular/material/button-toggle";
import {MatSlider, MatSliderThumb} from "@angular/material/slider";
import {FlatStroke} from "ol/style/flat";

@Component({
  selector: 'webgl-layer-map',
  imports: [
    OlControlsDirective,
    OlDefaultControlDirective,
    OlDefaultInteractionDirective,
    OlInteractionsDirective,
    OlLayersDirective,
    OlMapComponent,
    OlWebGLTileLayerDirective,
    OlVectorLayerDirective,
    OlGeoJsonSourceDirective,
    OlKmlSourceDirective,
    SourceDirective,
    MatButtonToggleGroup,
    MatButtonToggle,
    MatSlider,
    MatSliderThumb
  ],
  templateUrl: './webgl-layer-map.component.html',
  styleUrl: './webgl-layer-map.component.css'
})
export class WebglLayerMapComponent {

  readonly #apiKey = inject(MAP_TILER_API_KEY);
  private readonly layer = viewChild(OlWebGLTileLayerDirective);

  private readonly zIndexDirective = viewChild('randomPoints', {read: ZIndexDirective});

  private readonly basePath = inject(EXAMPLES_BASE_PATH);

  readonly #attributions = [
    '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> ' +
    '<a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
  ];

  protected readonly randomPointUrl = `${this.basePath}/data/random-point.geojson`;
  protected readonly kmlUrl = `${this.basePath}/data/random-polygon.kml`;

  protected readonly polygonStyle: FlatStroke = {
    'stroke-width': 1.25,
    'stroke-color': '#FFF'
  };

  protected readonly source = new ImageTile({
    attributions: this.#attributions,
    url: `https://api.maptiler.com/maps/satellite/{z}/{x}/{y}.jpg?key=${this.#apiKey}`,
    tileSize: 512,
    maxZoom: 20,
  });

  protected zIndex = linkedSignal(() => this.zIndexDirective()?.zIndex() ?? 1);
  protected readonly contrast = linkedSignal(() => this.layer()?.contrast() ?? 0);
  protected readonly exposure = linkedSignal(() => this.layer()?.exposure() ?? 0);
  protected readonly saturation = linkedSignal(() => this.layer()?.saturation() ?? 0);
  protected readonly brightness = linkedSignal(() => this.layer()?.brightness() ?? 0);
  protected readonly gamma = linkedSignal(() => this.layer()?.gamma() ?? 1);

  updateZIndex(evt: MatButtonToggleChange) {
    this.zIndexDirective()?.zIndex.set(evt.value);
  }

  updateStyle(value: number, key: string) {
    const layer = this.layer();
    let styleSignal = layer?.contrast;
    switch (key) {
      case 'contrast':
        break;
      case 'exposure':
        styleSignal = layer?.exposure;
        break;
      case 'saturation':
        styleSignal = layer?.saturation;
        break;
      case 'brightness':
        styleSignal = layer?.brightness;
        break;
      case 'gamma':
        styleSignal = layer?.gamma;
        break;
    }
    styleSignal?.set(value);
  }

}
