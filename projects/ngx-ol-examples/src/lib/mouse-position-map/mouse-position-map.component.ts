import {AfterContentInit, Component, model, viewChild} from '@angular/core';
import {OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective} from '@nasumilu/ngx-ol-map';
import {OlMousePositionDirective} from "@nasumilu/ngx-ol-control";
import {Coordinate, CoordinateFormat, format as coordinateFormat, toStringHDMS} from "ol/coordinate";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatSelect} from "@angular/material/select";
import {MatOption} from "@angular/material/core";
import { OlMouseWheelZoomDirective } from '@nasumilu/ngx-ol-interaction';
import {OlTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {OSM} from "ol/source";

export type PositionModel = {
  srid: string;
  label: string;
  format: string | CoordinateFormat;
  fractionDigits?: number;
};

@Component({
  selector: 'mouse-position-map',
  imports: [
    OlMapComponent, OlLayersDirective, OlControlsDirective,
    OlTileLayerDirective, OlMousePositionDirective, OlMouseWheelZoomDirective,
    MatFormField, MatLabel, MatSelect, MatOption, OlInteractionsDirective, SourceDirective
  ],
  templateUrl: './mouse-position-map.component.html',
  styleUrl: './mouse-position-map.component.css'
})
export class MousePositionMapComponent implements AfterContentInit {

  protected readonly projections: PositionModel[] = [
    {
      srid: 'EPSG:4326',
      label: 'WGS 84',
      format: "Lat: {y}, Lng: {x}",
      fractionDigits: 6
    },
    {
      srid: 'EPSG:3857',
      label: 'WGS 84 / Pseudo-Mercator',
      format: 'x: {x}, y: {y}',
      fractionDigits: 1
    },
    {
      srid: 'EPSG:32716',
      label: 'WGS84 / UTM zone 16S',
      format: coordinate => coordinate ? toStringHDMS(coordinate, 2) : ''
    }
  ];
  protected readonly source = new OSM();
  protected readonly position = viewChild(OlMousePositionDirective);

  public readonly projection = model<PositionModel>(this.projections[0]);

  format(coordinate: Coordinate | undefined): string {
    const projection = this.projection();
    const format = projection.format;
    const precision = projection.fractionDigits;
    if (typeof format === 'string') {
      return coordinate ? coordinateFormat(coordinate, format, precision) : '';
    }
    return format ? format(coordinate) : '';
  }

  ngAfterContentInit() {
    this.projection.subscribe(projection => {
      const position = this.position();
      position?.projection.set(projection.srid);
    });
  }

}
