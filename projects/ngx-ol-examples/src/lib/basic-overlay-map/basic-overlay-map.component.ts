import {Component, computed, ElementRef, inject, signal, viewChild} from '@angular/core';
import {
  OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent, OlOverlay, OlOverlaysDirective,
} from '@nasumilu/ngx-ol-map';
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from '@nasumilu/ngx-ol-interaction';
import {OlTileLayerDirective, SourceDirective} from '@nasumilu/ngx-ol-layer';
import {Overlay} from "ol";
import {Coordinate, toStringHDMS} from "ol/coordinate";
import {OSM} from "ol/source";

// Angular version of https://openlayers.org/en/latest/examples/popup.html
@Component({
  selector: 'simple-popup',
  providers: [{provide: OlOverlay, useExisting: SimplePopupComponent}],
  template: `
    <div id="popup" class="ol-popup">
      <a href="#" id="popup-closer" (click)="close($event)" class="ol-popup-closer"></a>
      <div id="popup-content">
        {{ content() }}
      </div>
    </div>`,
  styles: `
    .ol-popup {
      position: absolute;
      background-color: white;
      box-shadow: 0 1px 4px rgba(0, 0, 0, 0.2);
      padding: 15px;
      border-radius: 10px;
      border: 1px solid #cccccc;
      bottom: 12px;
      left: -50px;
      min-width: 280px;
    }

    .ol-popup:after, .ol-popup:before {
      top: 100%;
      border: solid transparent;
      content: " ";
      height: 0;
      width: 0;
      position: absolute;
      pointer-events: none;
    }

    .ol-popup:after {
      border-top-color: white;
      border-width: 10px;
      left: 48px;
      margin-left: -10px;
    }

    .ol-popup:before {
      border-top-color: #cccccc;
      border-width: 11px;
      left: 48px;
      margin-left: -11px;
    }

    .ol-popup-closer {
      text-decoration: none;
      position: absolute;
      top: 2px;
      right: 8px;
    }

    .ol-popup-closer:after {
      content: "✖";
    }`
})
export class SimplePopupComponent extends OlOverlay {

  private readonly ele = inject(ElementRef);
  public override readonly overlay = computed(() => new Overlay({
    element: this.ele.nativeElement,
    autoPan: {
      animation: {
        duration: 250
      }
    }
  }));

  protected readonly content = signal<string>('Unknown');

  close(evt: MouseEvent): boolean {
    this.overlay().setPosition(undefined);
    (evt.target as HTMLElement).blur();
    return false;
  }

  show(coordinate: Coordinate): void {
    this.content.set(toStringHDMS(coordinate));
    this.overlay().setPosition(coordinate);
  }

}

// Map Component
@Component({
  selector: 'basic-overlay-map',
  imports: [
    OlControlsDirective, OlLayersDirective, OlMapComponent, OlInteractionsDirective, OlDefaultInteractionDirective,
    OlTileLayerDirective, OlOverlaysDirective, SimplePopupComponent, OlDefaultControlDirective, SourceDirective
  ],
  templateUrl: './basic-overlay-map.component.html',
  styleUrl: './basic-overlay-map.component.css'
})
export class BasicOverlayMapComponent {

  protected readonly source = new OSM();
  protected readonly map = viewChild(OlMapComponent);
  protected readonly popup = viewChild(SimplePopupComponent);

}
