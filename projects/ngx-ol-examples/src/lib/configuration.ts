import {InjectionToken} from "@angular/core";
import {provideHttpClient, withFetch} from "@angular/common/http";

export const MAP_TILER_API_KEY = new InjectionToken<string>('MapTilerAPIKey');
export const EXAMPLES_BASE_PATH = new InjectionToken<string>('ExamplesBasePath');

export function provideExamples(basePath: string) {
  return [
    {provide: EXAMPLES_BASE_PATH, useValue: basePath},
    provideHttpClient(withFetch())
  ];
}

export function provideMapTilerAPIKey(apiKey: string) {

  return {provide: MAP_TILER_API_KEY, useValue: apiKey};

}
