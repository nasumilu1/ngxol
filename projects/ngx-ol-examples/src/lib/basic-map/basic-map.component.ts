import {ChangeDetectionStrategy, Component, viewChild} from '@angular/core';
import {
  OlControlsDirective, OlInteractionsDirective,
  OlLayersDirective, OlMapComponent,
  OlViewDirective, OlCoordinatePipe
} from '@nasumilu/ngx-ol-map';
import {OlDefaultControlDirective} from '@nasumilu/ngx-ol-control';
import {OlDefaultInteractionDirective,} from '@nasumilu/ngx-ol-interaction';
import {OlTileLayerDirective, OlVectorLayerDirective, SourceDirective} from '@nasumilu/ngx-ol-layer';
import {OSM} from 'ol/source';
import { OlWktSourceDirective } from '@nasumilu/ngx-ol-source';

@Component({
  selector: 'basic-map',
  imports: [
    OlMapComponent, OlLayersDirective, OlControlsDirective, OlInteractionsDirective, OlTileLayerDirective,
    OlDefaultInteractionDirective, OlDefaultControlDirective, OlCoordinatePipe, SourceDirective,
    OlVectorLayerDirective, OlWktSourceDirective
  ],
  templateUrl: 'basic-map.component.html',
  styleUrl: 'basic-map.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasicMapComponent {

  protected readonly wkt = 'Polygon ((31.1 -30.1, 8.1 -26.2, -0.4 -44.8, 42.8 -46.8, 31.1 -30.1))'
  protected readonly source = new OSM();
  protected readonly viewDirective = viewChild(OlViewDirective);
}
