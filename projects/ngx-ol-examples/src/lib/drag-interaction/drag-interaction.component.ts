import {Component, linkedSignal, OnInit, signal, viewChild} from '@angular/core';
import {IsActivePipe} from "../keyboard-interaction/keyboard-interaction.component";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {
  OlAttributionDirective,
  OlZoomControlDirective,
  OlRotateDirective,
} from "@nasumilu/ngx-ol-control";
import {
  OlControlsDirective,
  OlInteractionsDirective,
  OlLayersDirective,
  OlMapComponent
} from "@nasumilu/ngx-ol-map";
import {
  OlDragRotateDirective,
  DragBoxDirective,
  OlDragPanDirective,
  OlDragZoomDirective,
  OlDefaultInteractionDirective
} from '@nasumilu/ngx-ol-interaction';
import {OlTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {Coordinate} from "ol/coordinate";
import {OSM} from "ol/source";

@Component({
  selector: 'drag-interaction',
  imports: [
    IsActivePipe, MatSlideToggle, OlAttributionDirective, OlControlsDirective,
    OlInteractionsDirective, OlLayersDirective, OlMapComponent, OlZoomControlDirective,
    OlTileLayerDirective, OlRotateDirective, OlDefaultInteractionDirective, SourceDirective
  ],
  templateUrl: './drag-interaction.component.html',
  styleUrl: './drag-interaction.component.css'
})
export class DragInteractionComponent implements OnInit {

  private readonly dragZoom = viewChild(OlDragZoomDirective);
  private readonly dragPan = viewChild(OlDragPanDirective);
  private readonly dragRotate = viewChild(OlDragRotateDirective);
  private readonly dragBox = viewChild(DragBoxDirective);

  protected readonly source = new OSM();

  protected readonly dragZoomActive = linkedSignal(() => this.dragZoom()?.active());
  protected readonly dragPanActive = linkedSignal(() => this.dragPan()?.active());
  protected readonly dragRotateActive = linkedSignal(() => this.dragRotate()?.active())

  protected readonly boxStart = signal<Coordinate|undefined>(undefined);
  protected readonly boxEnd = signal<Coordinate|undefined>(undefined);

  toggleDragZoom(): void {
    this.dragZoom()?.active.update(value => !value);
  }

  toggleDragPan(): void {
    this.dragPan()?.active.update(value => !value);
  }

  toggleDragRotate(): void {
    this.dragRotate()?.active.update(value => !value);
  }

  ngOnInit() {
    this.dragBox()?.boxStarted.subscribe(coordinate => {
      this.boxEnd.set(undefined);
      this.boxStart.set(coordinate);
    });
    this.dragBox()?.boxEnded.subscribe(coordinate => this.boxEnd.set(coordinate));
  }
}
