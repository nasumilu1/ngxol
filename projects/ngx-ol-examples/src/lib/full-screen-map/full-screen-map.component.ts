import { Component } from '@angular/core';
import {OlAttributionDirective, OlFullScreenDirective, OlZoomControlDirective} from "@nasumilu/ngx-ol-control";
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {OlMouseWheelZoomDirective} from "@nasumilu/ngx-ol-interaction";
import {OlTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {OSM} from "ol/source";

@Component({
  selector: 'full-screen-map',
  imports: [
    OlAttributionDirective, OlControlsDirective, OlInteractionsDirective, OlLayersDirective,
    OlMapComponent, OlMouseWheelZoomDirective, OlZoomControlDirective, OlTileLayerDirective,
    OlFullScreenDirective, SourceDirective
  ],
  templateUrl: './full-screen-map.component.html',
  styleUrl: './full-screen-map.component.css'
})
export class FullScreenMapComponent {

  protected readonly source = new OSM();

  handleFullScreenChange(isFullScreen: boolean) {
    alert(`Full screen changed to ${isFullScreen}`);
  }

}
