import { Component } from '@angular/core';
import {OlControlsDirective, OlInteractionsDirective, OlLayersDirective, OlMapComponent} from "@nasumilu/ngx-ol-map";
import {OlDefaultControlDirective} from "@nasumilu/ngx-ol-control";
import {OlDefaultInteractionDirective} from "@nasumilu/ngx-ol-interaction";
import {OlVectorTileLayerDirective, SourceDirective} from "@nasumilu/ngx-ol-layer";
import {OGCVectorTile} from "ol/source";
import {MVT} from "ol/format";

@Component({
  selector: 'vector-tile-map',
  imports: [
    OlControlsDirective,
    OlDefaultControlDirective,
    OlDefaultInteractionDirective,
    OlInteractionsDirective,
    OlLayersDirective,
    OlMapComponent,
    OlVectorTileLayerDirective,
    SourceDirective
  ],
  templateUrl: './vector-tile-map.component.html',
  styleUrl: './vector-tile-map.component.css'
})
export class VectorTileMapComponent {

  protected readonly source = new OGCVectorTile({
    url: 'https://maps.gnosis.earth/ogcapi/collections/NaturalEarth:cultural:ne_10m_admin_0_countries/tiles/WebMercatorQuad',
    format: new MVT(),
  })
}
