import { Directive } from '@angular/core';
import BaseImageLayer, {Options} from "ol/layer/BaseImage";
import {OlLayerDirective} from "./ol-base-layer.directive";
import ImageSource from "ol/source/Image";
import LayerRenderer from "ol/renderer/Layer";
import ImageLayer from "ol/layer/Image";
import {OlLayer} from "@nasumilu/ngx-ol-map";

/**
 * Represents an abstract base class for OpenLayers image layer directives.
 * This directive extends the functionality of `OlLayerDirective` and provides
 * a foundation for working with OpenLayers image layers in Angular applications.
 *
 * @template L Extends the OpenLayers `BaseImageLayer` which uses `ImageSource` and `LayerRenderer`.
 */
@Directive()
export abstract class OlBaseImageLayerDirective<L extends BaseImageLayer<ImageSource, LayerRenderer<any>>>
  extends OlLayerDirective<L> {

}

/**
 * A directive representing an OpenLayers Image Layer.
 *
 * The directive is associated with the 'ol-image-layer' selector and integrates
 * with Angular dependency injection by providing itself as an OpenLayers Layer
 * and Layer Directive.
 */
@Directive({
  selector: 'ol-image-layer',
  providers: [
    {provide: OlLayer, useExisting: OlImageLayerDirective},
    {provide: OlLayerDirective, useExisting: OlImageLayerDirective},
    {provide: OlBaseImageLayerDirective, useExisting: OlImageLayerDirective}
  ]
})
export class OlImageLayerDirective extends OlBaseImageLayerDirective<ImageLayer<ImageSource>> {

  protected initLayer(options: Options<ImageSource>): ImageLayer<ImageSource> {
    return new ImageLayer(options);
  }


}
