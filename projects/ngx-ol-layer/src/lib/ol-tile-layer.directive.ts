import {booleanAttribute, computed, Directive, input, model, numberAttribute, OnInit} from '@angular/core';
import TileLayer from "ol/layer/Tile";
import BaseTileLayer, {Options} from 'ol/layer/BaseTile';
import TileSource from "ol/source/Tile";
import {OlLayer} from "@nasumilu/ngx-ol-map";
import LayerRenderer from "ol/renderer/Layer";
import {OlLayerDirective} from "./ol-base-layer.directive";
import WebGLTileLayer, {Style, Options as WebGLTileLayerOptions} from "ol/layer/WebGLTile";

/**
 * Represents an abstract base directive for OpenLayers tile layers, providing
 * common properties and configuration options for tile-based map layers.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
@Directive()
export abstract class OlBaseTileLayerDirective<L extends BaseTileLayer<TileSource, LayerRenderer<any>>> extends OlLayerDirective<L> {

  public readonly className = input<string>();
  public readonly preload = input(0, {transform: numberAttribute});
  public readonly useInterimTilesOnError = input(true, {transform: booleanAttribute});
  public readonly cacheSize = input(512, {transform: numberAttribute});

  public override readonly layer = computed(() => this.initLayer({
      className: this.className(),
      preload: this.preload(),
      useInterimTilesOnError: this.useInterimTilesOnError(),
      cacheSize: this.cacheSize(),
      background: this.background()
    })
  );

}

/**
 * The OlTileLayerDirective is an Angular directive that represents an OpenLayers TileLayer.
 * The directive is registered with the selector 'ol-tile-layer' for use in component templates.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
@Directive({
  selector: 'ol-tile-layer',
  providers: [
    {provide: OlLayer, useExisting: OlTileLayerDirective},
    {provide: OlLayerDirective, useExisting: OlTileLayerDirective},
    {provide: OlBaseTileLayerDirective, useExisting: OlTileLayerDirective}
  ]
})
export class OlTileLayerDirective extends OlBaseTileLayerDirective<TileLayer> {

  protected initLayer(options: Options<TileSource>): TileLayer {
    return new TileLayer(options);
  }

}

/**
 * Directive used to integrate an OpenLayers WebGL tile layer in declarative setups.
 * This directive acts as a wrapper for the OpenLayers `WebGLTileLayer`, allowing it
 * to be configured and used within an Angular application.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Defines the CSS class name for the layer. Defaults to an empty string.
 * - `preload`: Specifies the number of tiles to preload. Defaults to 0. Uses a number attribute transformation.
 * - `useInterimTilesOnError`: Determines whether to use interim tiles on errors. Defaults to true. Uses a boolean attribute transformation.
 * - `cacheSize`: Sets the maximum cache size for the tile layer. Defaults to 512. Uses a number attribute transformation.
 */
@Directive({
  selector: 'ol-webgl-tile-layer',
  providers: [
    {provide: OlLayer, useExisting: OlWebGLTileLayerDirective},
    {provide: OlLayerDirective, useExisting: OlWebGLTileLayerDirective},
    {provide: OlBaseTileLayerDirective, useExisting: OlWebGLTileLayerDirective}
  ]
})
export class OlWebGLTileLayerDirective extends OlBaseTileLayerDirective<WebGLTileLayer>
  implements OnInit {

  public readonly exposure = model(0);
  public readonly contrast = model(0);
  public readonly saturation = model(0);
  public readonly brightness = model(0);
  public readonly gamma = model(1);

  readonly #variables = computed(() => ({
    exposure: this.exposure(),
    contrast: this.contrast(),
    saturation: this.saturation(),
    brightness: this.brightness(),
    gamma: this.gamma()
  }));

  readonly #style: Style = {
    exposure: ['var', 'exposure'],
    contrast: ['var', 'contrast'],
    saturation: ['var', 'saturation'],
    brightness: ['var', 'brightness'],
    gamma: ['var', 'gamma'],
    variables: this.#variables()
  };

  protected initLayer(options: WebGLTileLayerOptions): WebGLTileLayer {
    options.style = this.#style;
    return new WebGLTileLayer(options);
  }

  override ngOnInit() {
    super.ngOnInit();
    const layer = this.layer();

    this.exposure.subscribe(() => layer.updateStyleVariables(this.#variables()));
    this.contrast.subscribe(() => layer.updateStyleVariables(this.#variables()));
    this.saturation.subscribe(() => layer.updateStyleVariables(this.#variables()));
    this.brightness.subscribe(() => layer.updateStyleVariables(this.#variables()));
    this.gamma.subscribe(() => layer.updateStyleVariables(this.#variables()));

  }
}
