import {booleanAttribute, computed, Directive, input, model, numberAttribute, OnInit} from '@angular/core';
import {OlLayerDirective} from "./ol-base-layer.directive";
import VectorLayer, {Options} from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {FeatureLike} from "ol/Feature";
import {OlLayer} from "@nasumilu/ngx-ol-map";
import VectorTile from "ol/source/VectorTile";
import BaseVectorLayer from "ol/layer/BaseVector";
import Graticule, {Options as GraticuleOptions} from "ol/layer/Graticule";
import Text from 'ol/style/Text';
import Stroke from "ol/style/Stroke";
import CanvasVectorLayerRenderer from "ol/renderer/canvas/VectorLayer";
import CanvasVectorTileLayerRenderer from "ol/renderer/canvas/VectorTileLayer";
import CanvasVectorImageLayerRenderer from "ol/renderer/canvas/VectorImageLayer";
import WebGLPointsLayerRenderer from "ol/renderer/webgl/PointsLayer";
import VectorImageLayer, {Options as VectorImageOptions} from "ol/layer/VectorImage";
import Heatmap, {Options as HeatmapOptions} from "ol/layer/Heatmap";
import VectorTileLayer, {Options as VectorTitleOptions, VectorTileRenderType} from "ol/layer/VectorTile";
import {StyleLike} from "ol/style/Style";
import {FlatStyleLike} from "ol/style/flat";


export type VectorSourceType = VectorSource<FeatureLike> | VectorTile<FeatureLike>;
export type VectorRenderType =
  CanvasVectorLayerRenderer
  | CanvasVectorTileLayerRenderer
  | CanvasVectorImageLayerRenderer
  | WebGLPointsLayerRenderer;

/**
 * OlBaseVectorLayerDirective serves as a base class for creating directives that
 * manage OpenLayers vector layers.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `style`: The layer's StyleLike | FlatStyleLike
 *
 */
@Directive()
export abstract class OlBaseVectorLayerDirective<L extends BaseVectorLayer<FeatureLike, VectorSourceType, VectorRenderType>>
  extends OlLayerDirective<L> {

  public readonly className = input<string>();
  public readonly updateWhileAnimating = input(false, {transform: booleanAttribute});
  public readonly updateWhileInteracting = input(false, {transform: booleanAttribute});
  public readonly declutter = input<string | boolean>();
  public readonly style = model<StyleLike | FlatStyleLike>();

  public override readonly layer = computed(() => this.initLayer({
      className: this.className(),
      style: this.style(),
      updateWhileAnimating: this.updateWhileAnimating(),
      updateWhileInteracting: this.updateWhileInteracting(),
      declutter: this.declutter(),
      background: this.background()
    })
  );

  override ngOnInit() {
    super.ngOnInit();
    const layer = this.layer();
    this.style.subscribe(value => {
      layer.setStyle(value);
    });
  }

}

/**
 * Directive representing an OpenLayers vector layer.
 *
 * This directive is used to integrate vector layers with Angular applications, allowing
 * seamless interaction with the OpenLayers library. Through dependency injection, it provides
 * the base layer functionality and reuses `OlBaseVectorLayerDirective`.
 *
 * The directive is associated with the `ol-vector-layer` selector and provides itself
 * as both `OlLayer` and `OlBaseVectorLayerDirective` for other components or classes to
 * depend on.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 */
@Directive({
  selector: 'ol-vector-layer',
  providers: [
    {provide: OlLayer, useExisting: OlVectorLayerDirective},
    {provide: OlLayerDirective, useExisting: OlVectorLayerDirective},
    {provide: OlBaseVectorLayerDirective, useExisting: OlVectorLayerDirective}
  ]
})
export class OlVectorLayerDirective extends OlBaseVectorLayerDirective<VectorLayer<VectorSource<FeatureLike>, FeatureLike>> {

  protected initLayer(options: Options): VectorLayer<VectorSource<FeatureLike>, FeatureLike> {
    return new VectorLayer(options);
  }

}

/**
 * Converts a string of comma-separated numbers into an array of numbers
 * or returns the input array of numbers as is.
 *
 * @param {number[]|string} value - The input value, which can be either an
 * array of numbers or a string of comma-separated numbers.
 * @return {number[]} An array of numbers parsed from the input string or
 * the input array if already provided as numbers.
 */
export function intervalAttribute(value: number[] | string): number[] {
  if (typeof value === 'string') {
    return value.split(',').map(parseFloat);
  }
  return value;
}

/**
 * A directive that represents an OpenLayers graticule layer in an Angular application.
 * The graticule layer renders a grid representing meridians (longitude) and parallels (latitude)
 * on a map. This directive extends the base vector layer directive and provides additional
 * configuration options specific to graticule rendering.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - maxLines: Defines the maximum number of grid lines to render.
 * - targetSize: Determines the target size for grid spacing.
 * - showLabels: Toggles the visibility of the longitude and latitude labels.
 * - lonLabelFormatter: Custom function to format longitude labels.
 * - latLabelFormatter: Custom function to format latitude labels.
 * - lonlatPosition: Specifies the longitude label position relative to the grid lines.
 * - latlonPosition: Specifies the latitude label position relative to the grid lines.
 * - strokeStyle: Configures the styling of the grid lines using a `Stroke` object.
 * - lonLabelStyle: Specifies the text style for longitude labels using a `Text` object.
 * - latLabelStyle: Specifies the text style for latitude labels using a `Text` object.
 * - intervals: Array of intervals to determine the spacing of grid lines, defined in degrees.
 * - wrapX: Determines whether the grid wraps horizontally across the world.
 *
 * The directive automatically integrates with OpenLayers and Angular dependency injection
 * to act as a vector layer, allowing for interaction with additional layers and features.
 */
@Directive({
  selector: 'ol-graticle-layer',
  providers: [
    {provide: OlLayer, useExisting: OlGraticleLayerDirective},
    {provide: OlLayerDirective, useExisting: OlGraticleLayerDirective},
    {provide: OlBaseVectorLayerDirective, useExisting: OlGraticleLayerDirective}
  ]
})
export class OlGraticleLayerDirective extends OlBaseVectorLayerDirective<VectorLayer<VectorSource<FeatureLike>, FeatureLike>> {

  public readonly maxLines = input(100, {transform: numberAttribute});
  public readonly targetSize = input(100, {transform: numberAttribute});
  public readonly showLabels = input(true, {transform: booleanAttribute});
  public readonly lonLabelFormatter = input<((lon: number) => string) | undefined>();
  public readonly latLabelFormatter = input<((lat: number) => string) | undefined>();
  public readonly lonlatPosition = input(0, {transform: numberAttribute});
  public readonly latlonPosition = input(1, {transform: numberAttribute});
  public readonly strokeStyle = input<Stroke>();
  public readonly lonLabelStyle = input<Text>();
  public readonly latLabelStyle = input<Text>();
  public readonly intervals = input([
      90, 45, 30, 20, 10, 5, 2, 1, 30 / 60, 20 / 60, 10 / 60, 5 / 60, 2 / 60, 1 / 60, 30 / 3600, 20 / 3600, 10 / 3600, 5 / 3600, 2 / 3600, 1 / 3600],
    {transform: intervalAttribute});
  public readonly wrapX = input(true, {transform: booleanAttribute});


  protected initLayer(options: GraticuleOptions): VectorLayer<VectorSource<FeatureLike>, FeatureLike> {
    options = Object.assign(options, {
      maxLines: this.maxLines(),
      targetSize: this.targetSize(),
      showLabels: this.showLabels(),
      lonLabelFormatter: this.lonLabelFormatter(),
      latlonLabelFormatter: this.latLabelFormatter(),
      lonlatPosition: this.lonlatPosition(),
      latlonPosition: this.latlonPosition(),
      strokeStyle: this.strokeStyle(),
      lonLabelStyle: this.lonLabelStyle(),
      latLabelStyle: this.latLabelStyle(),
      intervals: this.intervals(),
      wrapX: this.wrapX()
    });
    return new Graticule(options) as VectorLayer<VectorSource<FeatureLike>, FeatureLike>;
  }
}

/**
 * Directive to configure and create an OpenLayers `VectorImageLayer` component.
 *
 * This class extends the `OlBaseVectorLayerDirective` to provide specific functionalities
 * for `VectorImageLayer` which supports rendering vector data with rasterization for better
 * performance in cases of large datasets or complex geometries.
 *
 * The `OlVectorImageLayerDirective` allows binding for specific configuration options
 * including the `imageRatio`, which determines the resolution of the raster images
 * used to draw vector data.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `imageRatio`: A readable input property defining the ratio between the physical and logical pixel size for the rasterization process.
 */
@Directive({
  selector: 'ol-vector-image-layer'
})
export class OlVectorImageLayerDirective extends OlBaseVectorLayerDirective<VectorImageLayer<VectorSource<FeatureLike>, FeatureLike>> {

  public readonly imageRatio = input(1, {transform: numberAttribute});

  protected initLayer(options: VectorImageOptions): VectorImageLayer<VectorSource<FeatureLike>, FeatureLike> {
    options = Object.assign(options, {imageRatio: this.imageRatio()});
    return new VectorImageLayer(options) as VectorImageLayer<VectorSource<FeatureLike>, FeatureLike>;
  }
}

/**
 * Directive to integrate OpenLayers Heatmap layer into Angular applications.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `blur`: Reactive property to manage the blur size of the heatmap layer.
 * - `radius`: Reactive property to control the size of the radius for each feature.
 * - `gradient`: Reactive property for setting the color gradient of the heatmap.
 *
 * Outputs:
 * - `blurChanged`:
 * - `radiusChanged`:
 * - `gradientChanged`:
 */
@Directive({
  selector: 'ol-heatmap-layer',
  providers: [
    {provide: OlLayer, useExisting: OlHeatmapLayerDirective},
    {provide: OlLayerDirective, useExisting: OlHeatmapLayerDirective},
    {provide: OlBaseVectorLayerDirective, useExisting: OlHeatmapLayerDirective}
  ]
})
export class OlHeatmapLayerDirective extends OlBaseVectorLayerDirective<Heatmap<FeatureLike, VectorSource<FeatureLike>>>
  implements OnInit {

  public readonly blur = model(15);
  public readonly radius = model(8);
  public readonly gradient = model(['#00f', '#0ff', '#0f0', '#ff0', '#f00']);

  protected initLayer(options: HeatmapOptions): Heatmap<FeatureLike, VectorSource<FeatureLike>> {
    options = Object.assign(options, {
      blur: this.blur(),
      radius: this.radius(),
      gradient: this.gradient()
    });
    return new Heatmap(options) as Heatmap<FeatureLike, VectorSource<FeatureLike>>;
  }

  override ngOnInit(): void {
    super.ngOnInit();
    const layer = this.layer();

    this.blur.subscribe(value => {
      const blur = layer.getBlur();
      if (blur !== value) {
        layer.setBlur(value);
      }
    })

    // @ts-ignore
    this.eventKeys.push(layer.on('change:blur', () => {
      const blur = layer.getBlur();
      if (this.blur() !== blur) {
        this.blur.set(blur);
      }
    }));

    this.radius.subscribe(value => {
      const radius = layer.getRadius();
      if (radius !== value) {
        layer.setRadius(value);
      }
    });

    // @ts-ignore
    this.eventKeys.push(layer.on('change:radius', () => {
      const radius = layer.getRadius();
      if (this.radius() !== radius) {
        this.radius.set(radius);
      }
    }));

    this.gradient.subscribe(value => {
      const gradient = layer.getGradient();
      if (gradient !== value) {
        layer.setGradient(value);
      }
    });

    // @ts-ignore
    this.eventKeys.push(layer.on('change:gradient', () => {
      const gradient = layer.getGradient();
      if (this.gradient() !== gradient) {
        this.gradient.set(gradient);
      }
    }))

  }

}

/**
 * Directive for creating and configuring an OpenLayers vector tile layer.
 *
 * Inputs:
 * - `background`: Background color for the layer. If not specified, no background will be rendered.
 * - `className`: Input binding for specifying the CSS class name to associate with the vector layer.
 * - `updateWhileAnimating`: Input that enables or disables updates to the vector layer while animations are running. Defaults to false.
 * - `updateWhileInteracting`: Input that determines whether to update the vector layer during user interactions such as dragging. Defaults to false.
 * - `declutter`: Input that controls decluttering of text and point symbols. Accepts a boolean or a string.
 * - `preload`: Sets the number of low-resolution parent tiles to preload. This helps improve perceived performance when the map is panned or zoomed.
 * - `renderMode`: Determines the rendering mode for vector tiles. Possible values are `'hybrid'`, `'vector'`, or `'image'`. The default value is `'hybrid'`.
 */
@Directive({
  selector: 'ol-vector-tile-layer',
  providers: [
    {provide: OlLayer, useExisting: OlVectorTileLayerDirective},
    {provide: OlLayerDirective, useExisting: OlVectorTileLayerDirective},
    {provide: OlBaseVectorLayerDirective, useExisting: OlVectorTileLayerDirective}
  ]
})
export class OlVectorTileLayerDirective extends OlBaseVectorLayerDirective<VectorTileLayer> {

  public readonly preload = input(0, {transform: numberAttribute});
  public readonly renderMode = input<VectorTileRenderType>('hybrid');

  protected initLayer(options: VectorTitleOptions): VectorTileLayer {
    options = Object.assign(options, {
      preload: this.preload(),
      renderMode: this.renderMode()
    });
    return new VectorTileLayer(options);
  }

}
