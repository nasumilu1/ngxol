import {
  AfterContentInit,
  computed, contentChildren,
  Directive,
  inject,
  input,
  model,
  OnDestroy,
  OnInit, output
} from '@angular/core';
import BaseLayer, {BackgroundColor, Options} from "ol/layer/Base";
import {OlLayer} from '@nasumilu/ngx-ol-map';
import {EventsKey} from "ol/events";
import {unByKey} from "ol/Observable";
import Layer from "ol/layer/Layer";
import LayerRenderer from 'ol/renderer/Layer';
import Source from "ol/source/Source";
import LayerGroup, {Options as LayerGroupOptions} from "ol/layer/Group";
import Collection from "ol/Collection";
import {Extent} from "ol/extent";
import {FrameState} from "ol/Map";

@Directive()
abstract class AbstractLayerDirective implements OnDestroy {

  protected readonly layerDirective = inject(OlLayer);
  protected eventKeys: EventsKey[] = [];

  ngOnDestroy() {
    unByKey(this.eventKeys);
    this.eventKeys = [];
  }

}

@Directive()
export class OpacityDirective extends AbstractLayerDirective implements OnInit {

  public readonly opacity = model(1.0);

  ngOnInit() {
    const layer = this.layerDirective.layer();

    layer.setOpacity(this.opacity());

    this.opacity.subscribe(value => {
      const opacity = layer.getOpacity();
      if (opacity !== value) {
        layer.setOpacity(value);
      }
    });

    this.eventKeys.push(layer.on('change:opacity', () => {
        const opacity = layer.getOpacity();
        if (this.opacity() !== opacity) {
          this.opacity.set(opacity);
        }
      })
    );
  }

}

@Directive()
export class VisibleDirective extends AbstractLayerDirective implements OnInit  {

  public readonly visible = model(true);

  ngOnInit() {
    const layer = this.layerDirective.layer();
    layer.setVisible(this.visible());
    this.visible.subscribe(value => {
      const visible = layer.getVisible();
      if (visible !== value) {
        layer.setVisible(value);
      }
    });

    this.eventKeys.push(layer.on('change:visible', () => {
        const visible = layer.getVisible();
        if (this.visible() !== visible) {
          this.visible.set(visible);
        }
      })
    );
  }
}

/**
 * The ExtentDirective is an Angular directive that manages the synchronization
 * of an OpenLayers layer's extent with a reactive model property.
 *
 * This directive is used to dynamically manage the extent of a map layer in response
 * to changes in an observable model property or changes within the map itself.
 *
 * The directive's primary responsibilities include:
 * - Setting the initial extent of the OpenLayers layer based on the reactive model.
 * - Subscribing to changes in the model property and updating the layer's extent accordingly.
 * - Listening for changes in the layer's extent and updating the reactive model property.
 *
 * Lifecycle Hooks:
 * - `ngOnInit`: Initializes the synchronization between the extent model property and
 *   the OpenLayers layer's extent.
 * - `ngOnDestroy`: Cleans up event listeners and other resources associated with the directive.
 */
@Directive()
export class ExtentDirective extends AbstractLayerDirective implements OnInit  {

  public readonly extent = model<Extent>();

  ngOnInit() {
    const layer = this.layerDirective.layer();
    layer.setExtent(this.extent());
    this.extent.subscribe(value => {
      const extent = layer.getExtent();
      if (extent !== value) {
        layer.setExtent(value);
      }
    });

    this.eventKeys.push(layer.on('change:extent', () => {
        const extent = layer.getExtent();
        if (this.extent() !== extent) {
          this.extent.set(extent);
        }
      })
    );
  }
}

@Directive()
export class ZIndexDirective extends AbstractLayerDirective implements OnInit  {

  public readonly zIndex = model<number>();

  ngOnInit(): void {
    const layer = this.layerDirective.layer();
    layer.setZIndex(this.zIndex());
    this.zIndex.subscribe(value => {
      const zIndex = layer.getZIndex();
      if (zIndex !== value) {
        layer.setZIndex(value);
      }
    });

    this.eventKeys.push(layer.on('change:zIndex', () => {
        const zIndex = layer.getZIndex();
        if (this.zIndex() !== zIndex) {
          this.zIndex.set(zIndex);
        }
      })
    );
  }
}


@Directive()
export class MinMaxLayerZoomDirective extends AbstractLayerDirective implements OnInit {

  public readonly minZoom = model<number>(-Infinity);
  public readonly maxZoom = model<number>(Infinity);


  ngOnInit() {
    const layer = this.layerDirective.layer();

    layer.setMinZoom(this.minZoom());
    layer.setMaxZoom(this.maxZoom());

    this.minZoom.subscribe(value => {
      const minZoom = layer.getMinZoom();
      if (minZoom !== value) {
        layer.setMinZoom(value);
      }
    });

    this.maxZoom.subscribe(value => {
      const maxZoom = layer.getMaxZoom();
      if (maxZoom !== value) {
        layer.setMaxZoom(value);
      }
    });

    this.eventKeys.push(layer.on('change:minZoom', () => {
        const minZoom = layer.getMinZoom();
        if (this.minZoom() !== minZoom) {
          this.minZoom.set(minZoom);
        }
      })
    );

    this.eventKeys.push(layer.on('change:maxZoom', () => {
        const maxZoom = layer.getMaxZoom();
        if (this.maxZoom() !== maxZoom) {
          this.maxZoom.set(maxZoom);
        }
      })
    );
  }
}

@Directive()
export class MinMaxResolutionDirective extends AbstractLayerDirective implements OnInit {

  public readonly minResolution = model<number>(0);
  public readonly maxResolution = model<number>(Infinity);


  ngOnInit() {
    const layer = this.layerDirective.layer();
    layer.setMinResolution(this.minResolution());
    layer.setMaxResolution(this.maxResolution());

    this.minResolution.subscribe(value => {
      const minResolution = layer.getMinResolution();
      if (minResolution !== value) {
        layer.setMinResolution(value);
      }
    });

    this.maxResolution.subscribe(value => {
      const maxResolution = layer.getMaxResolution();
      if (maxResolution !== value) {
        layer.setMaxResolution(value);
      }
    });

    this.eventKeys.push(layer.on('change:minResolution', () => {
        const minResolution = layer.getMinResolution();
        if (this.minResolution() !== minResolution) {
          this.minResolution.set(minResolution);
        }
      })
    );

    this.eventKeys.push(layer.on('change:maxResolution', () => {
        const maxResolution = layer.getMaxResolution();
        if (this.maxResolution() !== maxResolution) {
          this.maxResolution.set(maxResolution);
        }
      })
    );
  }
}

/**
 * Abstract base directive for OpenLayers layers, providing common functionality
 * for managing layer configuration and lifecycle. This class is designed to
 * simplify the integration of OpenLayers layers within an Angular application.
 *
 * @template L The type of the OpenLayers BaseLayer that this directive manages.
 */
@Directive({
  hostDirectives: [
    {directive: OpacityDirective, inputs: ['opacity'], outputs: ['opacityChange']},
    {directive: VisibleDirective, inputs: ['visible'], outputs: ['visibleChange']},
    {directive: ExtentDirective, inputs: ['extent'], outputs: ['extentChange']},
    {directive: ZIndexDirective, inputs: ['zIndex'], outputs: ['zIndexChange']},
    {directive: MinMaxLayerZoomDirective, inputs: ['minZoom', 'maxZoom'], outputs: ['minZoomChange', 'maxZoomChange']},
    {directive: MinMaxResolutionDirective, inputs: ['minResolution', 'maxResolution'], outputs: ['minResolutionChange', 'maxResolutionChange']},
  ]
})
export abstract class OlBaseLayerDirective<L extends BaseLayer> extends OlLayer<L> {

  public readonly background = input<BackgroundColor>();
  public readonly layer = computed(() => this.initLayer({background: this.background()}))

  protected abstract initLayer<O extends Options>(options: O): L;
}


/**
 * Directive to manage OpenLayers LayerGroup components in Angular applications.
 * Acts as a wrapper for LayerGroup functionalities and integrates with Angular's lifecycle hooks.
 *
 * This directive is used to create and manage a group of layers (LayerGroup) in an OpenLayers map,
 * and provides capabilities to synchronize Angular component layers within the group.
 * It supports interaction with the OlLayerDirective to aggregate individual layers into a Collection.
 *
 * - Automatically initializes a LayerGroup instance with the specified options.
 * - Collects individual layers provided in the template using content children and adds them to the group.
 * - Ensures synchronization of Angular component layers with the underlying OpenLayers LayerGroup.
 *
 * Implements the AfterContentInit interface to manage component's content initialization and layer aggregation.
 */
@Directive({
  selector: 'ol-group-layer',
  providers: [{provide: OlLayer, useExisting: OlGroupLayerDirective}]
})
export class OlGroupLayerDirective extends OlBaseLayerDirective<LayerGroup> implements OnInit, AfterContentInit, OnDestroy {

  readonly #olLayers = new Collection<OlLayer<BaseLayer>>();
  readonly #layers = new Collection<BaseLayer>();
  #eventKey: EventsKey[] = [];
  private readonly layers = contentChildren(OlLayer);

  public readonly layerAdded = output<OlLayer<BaseLayer>>();
  public readonly layerRemoved = output<OlLayer<BaseLayer>>();

  protected initLayer(options: LayerGroupOptions): LayerGroup {
    options.layers = this.#layers;
    return new LayerGroup(options);
  }

  getLayers(): OlLayer<BaseLayer>[] {
    return this.#olLayers.getArray();
  }

  find(predicate: (olLayer: OlLayer<BaseLayer>) => boolean): OlLayer<BaseLayer> | undefined {
    return this.getLayers().find(predicate);
  }

  filter(predicate: (olLayer: OlLayer<BaseLayer>) => boolean): OlLayer<BaseLayer>[] {
    return this.getLayers().filter(predicate);
  }

  addLayer<L extends BaseLayer>(layer: OlLayer<L>): void {
    this.#olLayers.push(layer);
  }

  removeLayer<L extends BaseLayer>(layer: OlLayer<L>): void {
    this.#olLayers.remove(layer);
  }

  ngOnInit(): void {
    this.#eventKey.push(this.#olLayers.on('add', evt => {
        this.#layers.push(evt.element.layer());
        this.layerAdded.emit(evt.element);
      })
    );

    this.#eventKey.push(this.#olLayers.on('remove', evt => {
        this.#layers.remove(evt.element.layer());
        this.layerRemoved.emit(evt.element);
      })
    );
  }

  ngAfterContentInit(): void {
    this.layers().forEach(OlGroupLayerDirective.prototype.addLayer.bind(this));
  }

  ngOnDestroy(): void {
    unByKey(this.#eventKey);
    this.#eventKey = [];
  }

}

/**
 * Abstract class representing a directive for an OpenLayers (OL) layer. This class provides base functionality
 * for managing an OpenLayers Layer instance, including its source and associated lifecycle events.
 * It extends the functionality of `OlBaseLayerDirective` and implements the `OnInit` and `OnDestroy` Angular lifecycle hooks.
 *
 * @template L The type of the OpenLayers layer, extending the base Layer class.
 */
@Directive()
export abstract class OlLayerDirective<L extends Layer<Source, LayerRenderer<any>>> extends OlBaseLayerDirective<L>
  implements OnInit, OnDestroy {

  protected eventKeys: EventsKey[] = [];

  public readonly postRender = output<FrameState>();
  public readonly preRender = output<FrameState>();

  ngOnInit() {
    const layer = this.layer();
    this.eventKeys.push(layer.on('postrender', (event) => {
      if (event.frameState) {
        this.postRender.emit(event.frameState);
      }
    }));

    this.eventKeys.push(layer.on('prerender', (event) => {
      if (event.frameState) {
        this.preRender.emit(event.frameState);
      }
    }));
  }

  ngOnDestroy() {
    unByKey(this.eventKeys);
    this.eventKeys = [];
  }

}

/**
 * The `SourceDirective` is an Angular directive that manages the OpenLayers source of a layer.
 * It binds the source of an OpenLayers layer to a reactive model, ensuring synchronization
 * between the layer's source and the underlying model. The directive is meant to be used with
 * the `OlLayerDirective`.
 *
 * Features:
 * - Initializes the layer's source based on the value of the reactive `source` model.
 * - Synchronizes changes between the layer's source and the `source` model.
 * - Cleans up any registered events during destruction.
 *
 * Lifecycle:
 * - In `ngOnInit`, binds the given source to the layer and listens for source changes.
 * - In `ngOnDestroy`, cleans up event listeners to prevent memory leaks.
 */
@Directive({
  selector: '[source]'
})
export class SourceDirective implements OnInit, OnDestroy {

  readonly #layerDirective = inject(OlLayerDirective);
  #eventKey?: EventsKey;
  public readonly source = model<Source | null>(null);

  ngOnInit() {
    const layer = this.#layerDirective.layer();
    layer.setSource(this.source() ?? null);
    this.source.subscribe(value => {
      const source = layer.getSource();
      if (source !== value) {
        layer.setSource(value);
      }
    });

    this.#eventKey = layer.on('change:source', () => {
      const source = layer.getSource();
      if (this.source() !== source) {
        this.source.set(source);
      }
    });
  }

  ngOnDestroy() {
    if (this.#eventKey) {
      unByKey(this.#eventKey);
    }
  }

}




