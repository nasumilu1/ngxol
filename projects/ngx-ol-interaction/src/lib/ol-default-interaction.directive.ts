import {Directive, inject, OnInit} from '@angular/core';
import {OlDragRotateDirective} from './ol-drag-rotate.directive';
import {OlDblClickZoomDirective} from "./ol-dbl-click-zoom.directive";
import {OlDragPanDirective} from "./ol-drag-pan.directive";
import {OlKeyboardPanDirective} from "./ol-keyboard-pan.directive";
import {OlKeyboardZoomDirective} from "./ol-keyboard-zoom.directive";
import {OlDragZoomDirective} from "./ol-drag-zoom.directive";
import { OlMouseWheelZoomDirective } from './ol-mouse-wheel-zoom.directive';
import {OlInteractionsDirective} from "@nasumilu/ngx-ol-map";
import {OlPinchRotateDirective} from "./ol-pinch-rotate.directive";
import {OlPinchZoomDirective} from "./ol-pinch-zoom.directive";

/**
 * Directive to enable a set of default interactions for an OpenLayers map.
 *
 * This directive binds several commonly used interaction handlers to the map,
 * such as drag rotation, double-click zoom, drag pan, keyboard navigation,
 * mouse wheel zoom, and pinch gestures. These interactions are enabled by
 * injecting their respective directives and adding them to the OpenLayers
 * interactions manager.
 *
 * Each supported interaction directive is automatically bound and initialized
 * when the `OlDefaultInteractionDirective` is instantiated and its lifecycle
 * `ngOnInit` is executed.
 *
 * The following interactions are included:
 * - Drag rotation (`OlDragRotateDirective`)
 * - Double-click zoom (`OlDblClickZoomDirective`)
 * - Drag panning (`OlDragPanDirective`)
 * - Keyboard panning (`OlKeyboardPanDirective`)
 * - Keyboard zooming (`OlKeyboardZoomDirective`)
 * - Mouse wheel zooming (`OlMouseWheelZoomDirective`)
 * - Drag zooming (`OlDragZoomDirective`)
 * - Pinch rotation (`OlPinchRotateDirective`)
 * - Pinch zooming (`OlPinchZoomDirective`)
 */
@Directive({
  selector: '[defaultInteractions]',
  hostDirectives: [
    OlDragRotateDirective,
    OlDblClickZoomDirective,
    OlDragPanDirective,
    OlKeyboardPanDirective,
    OlKeyboardZoomDirective,
    OlMouseWheelZoomDirective,
    OlDragZoomDirective,
    OlPinchRotateDirective,
    OlPinchZoomDirective
  ]
})
export class OlDefaultInteractionDirective implements OnInit {

  readonly #interactions = inject(OlInteractionsDirective);

  readonly #dragRotate = inject(OlDragRotateDirective);
  readonly #dblClickZoom = inject(OlDblClickZoomDirective);
  readonly #dragPan = inject(OlDragPanDirective);
  readonly #keyboardPan = inject(OlKeyboardPanDirective);
  readonly #keyboardZoom = inject(OlKeyboardZoomDirective);
  readonly #mouseWheelZoom = inject(OlMouseWheelZoomDirective);
  readonly #dragZoom = inject(OlDragZoomDirective);
  readonly #pinchRotate = inject(OlPinchRotateDirective);
  readonly #pinchZoom = inject(OlPinchZoomDirective);

  ngOnInit(): void {
    this.#interactions.addInteraction(this.#dragRotate);
    this.#interactions.addInteraction(this.#dblClickZoom);
    this.#interactions.addInteraction(this.#dragPan);
    this.#interactions.addInteraction(this.#keyboardPan);
    this.#interactions.addInteraction(this.#keyboardZoom);
    this.#interactions.addInteraction(this.#mouseWheelZoom);
    this.#interactions.addInteraction(this.#dragZoom);
    this.#interactions.addInteraction(this.#pinchRotate);
    this.#interactions.addInteraction(this.#pinchZoom);
  }

}
