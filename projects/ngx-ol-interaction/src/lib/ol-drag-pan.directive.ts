import {booleanAttribute, computed, Directive, input} from '@angular/core';
import {OlInteraction} from "@nasumilu/ngx-ol-map";
import DragPan from "ol/interaction/DragPan";
import {Kinetic} from "ol";

/**
 * Directive that encapsulates the OpenLayers DragPan interaction, allowing
 * integration with an Angular application. The DragPan interaction enables
 * panning of the map by dragging with the mouse or finger on touch devices.
 *
 * It extends the base OlInteraction class, providing additional functionality
 * specific to the DragPan interaction.
 *
 * Selector:
 * - `ol-drag-pan`
 *
 * Providers:
 * - Provides `OlInteraction` using this directive as its implementation.
 *
 * Inputs:
 * - `kinetic` (optional): Kinetic inertia settings for the pan interaction.
 * - `onFocusOnly` (optional): A boolean attribute determining whether the
 *   interaction is active only when the map has focus.
 *
 * Computed Interaction:
 * - Constructs and returns a new OpenLayers `DragPan` instance based on the provided inputs.
 * - Uses reactive bindings for input changes.
 */
@Directive({
  selector: 'ol-drag-pan',
  providers: [{provide: OlInteraction, useExisting: OlDragPanDirective}]
})
export class OlDragPanDirective extends OlInteraction<DragPan> {

  public readonly kinetic = input<Kinetic>();
  public readonly onFocusOnly = input(false, {transform: booleanAttribute});

  public readonly interaction = computed(() => new DragPan({
      kinetic: this.kinetic(),
      onFocusOnly: this.onFocusOnly()
    })
  );

}
