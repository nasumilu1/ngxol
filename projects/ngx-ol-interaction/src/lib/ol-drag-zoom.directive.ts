import {
  booleanAttribute,
  computed,
  Directive,
  input,
  numberAttribute,
  OnInit, output
} from '@angular/core';
import {OlInteraction} from "@nasumilu/ngx-ol-map";
import DragZoom from 'ol/interaction/DragZoom';
import DragBox, {DragBoxEvent} from "ol/interaction/DragBox";
import {Coordinate} from "ol/coordinate";

@Directive()
export abstract class DragBoxDirective<T extends DragBox> extends OlInteraction<T> implements OnInit {

  public readonly boxEnded = output<Coordinate>();
  public readonly boxStarted = output<Coordinate>();

  override ngOnInit() {
    super.ngOnInit();
    const interaction = this.interaction();
    this.eventKeys.push(interaction.on('boxend', (evt: DragBoxEvent) => this.boxEnded.emit(evt.coordinate)));
    this.eventKeys.push(interaction.on('boxstart', (evt: DragBoxEvent) => this.boxStarted.emit(evt.coordinate)));
  }

}


@Directive({
  selector: 'ol-drag-zoom',
  providers: [{provide: OlInteraction, useExisting: OlDragZoomDirective}]
})
export class OlDragZoomDirective extends DragBoxDirective<DragZoom> {

  public readonly className = input<string>();
  public readonly minArea = input(64, {transform: numberAttribute});
  public readonly out = input(false, {transform: booleanAttribute});
  public readonly duration = input(200, {transform: numberAttribute});
  public readonly interaction = computed(() => new DragZoom({
    className: this.className(),
    duration: this.duration(),
    out: this.out(),
    minArea: this.minArea()
  }));

}
