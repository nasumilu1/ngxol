import {booleanAttribute, computed, Directive, input, numberAttribute} from '@angular/core';
import {OlInteraction} from '@nasumilu/ngx-ol-map';
import MouseWheelZoom from "ol/interaction/MouseWheelZoom";

/**
 * Directive to provide an integration with OpenLayers MouseWheelZoom interaction.
 * This directive enables configuring and controlling mouse wheel zoom behavior on an OpenLayers map.
 *
 * This directive extends the base OpenLayers interaction to provide additional configuration through
 * reactive inputs and computed behavior.
 *
 * Selector:
 * - ol-mouse-wheel-zoom: Apply this directive to automatically set up the MouseWheelZoom interaction.
 *
 * Inputs:
 * - onFocusOnly: A boolean input to determine whether zooming should only occur when the map has focus.
 * - duration: A number input specifying the animation duration for the zoom interaction in milliseconds.
 * - maxDelta: A number input defining the maximum zoom delta value.
 * - timeout: A number input setting the timeout duration in milliseconds to consider consecutive zoom actions as distinct.
 * - useAnchor: A boolean input specifying whether to use the mouse pointer location as an anchor for the zoom interaction.
 * - constrainResolution: A boolean input to determine whether zoom levels should be constrained to predefined resolutions.
 *
 * Property:
 * - interaction: A computed property that initializes and returns the MouseWheelZoom interaction based on current input values.
 */
@Directive({
  selector: 'ol-mouse-wheel-zoom',
  providers: [{provide: OlInteraction, useExisting: OlMouseWheelZoomDirective}]
})
export class OlMouseWheelZoomDirective extends OlInteraction<MouseWheelZoom> {

  public readonly onFocusOnly = input(false, {transform: booleanAttribute});
  public readonly duration = input(250, {transform: numberAttribute});
  public readonly maxDelta = input(1, {transform: numberAttribute});
  public readonly timeout = input(80, {transform: numberAttribute});
  public readonly useAnchor = input(true, {transform: booleanAttribute});
  public readonly constrainResolution = input(false, {transform: booleanAttribute})

  public override readonly interaction = computed(() => new MouseWheelZoom({
      onFocusOnly: this.onFocusOnly(),
      duration: this.duration(),
      maxDelta: this.maxDelta(),
      timeout: this.timeout(),
      useAnchor: this.useAnchor(),
      constrainResolution: this.constrainResolution()
    })
  );

}
