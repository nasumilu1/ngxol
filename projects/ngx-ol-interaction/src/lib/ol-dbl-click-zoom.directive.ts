import {computed, Directive, input, numberAttribute} from '@angular/core';
import {OlInteraction} from "@nasumilu/ngx-ol-map";
import DoubleClickZoom from "ol/interaction/DoubleClickZoom";

/**
 * Directive that encapsulates the OpenLayers DoubleClickZoom interaction for angular applications.
 * Provides configuration options for the interaction's behavior such as duration and zoom delta.
 * This directive extends OlInteraction with type parameter DoubleClickZoom.
 *
 * Selector: `ol-dbl-click-zoom`
 *
 * Dependencies: It provides `OlInteraction` using itself (`OlDblClickZoomDirective`) as an implementation.
 *
 * Properties:
 * - `duration`: Configures the animation duration for the double-click zoom interaction, defaulting to 250 milliseconds.
 * - `delta`: Sets the zoom delta, which specifies the magnitude of the zoom on double-click, defaulting to 1.
 *
 * Computed Properties:
 * - `interaction`: A computed property that initializes the `DoubleClickZoom` interaction based on `duration` and `delta` properties.
 */
@Directive({
  selector: 'ol-dbl-click-zoom',
  providers: [{provide: OlInteraction, useExisting: OlDblClickZoomDirective}]
})
export class OlDblClickZoomDirective extends OlInteraction<DoubleClickZoom> {

  public readonly duration = input(250, {transform: numberAttribute});
  public readonly delta = input(1, {transform: numberAttribute});

  public readonly interaction = computed(() => new DoubleClickZoom({
      duration: this.duration(),
      delta: this.delta()
    })
  );

}
