import {computed, Directive, input, numberAttribute} from '@angular/core';
import PinchRotate from "ol/interaction/PinchRotate";
import { OlInteraction } from '@nasumilu/ngx-ol-map';

/**
 * Directive that provides a wrapper for the OpenLayers `PinchRotate` interaction.
 * This directive can be used to configure and attach the `PinchRotate` interaction
 * to an OpenLayers map. PinchRotate interaction allows users to rotate the map
 * by using a two-finger touch gesture.
 *
 * Inputs:
 * - `duration`: The duration of the interaction in milliseconds. Determines how
 *   quickly the rotation is applied after the gesture. Default value is `250`.
 * - `threshold`: The threshold for minimum rotation angle in radians before the
 *   interaction is triggered. Default value is `0.3`.
 *
 * Properties:
 * - `interaction`: A computed property that instantiates a `PinchRotate` interaction
 *   using the configured `duration` and `threshold` values.
 *
 * This directive internally provides an instance of `OlInteraction` for dependency
 * injection usage when attached to components.
 */
@Directive({
  selector: 'ol-pinch-rotate',
  providers:[{provide: OlInteraction, useExisting: OlPinchRotateDirective}]
})
export class OlPinchRotateDirective extends OlInteraction<PinchRotate>{

  public readonly duration = input(250, {transform: numberAttribute});
  public readonly threshold = input(0.3, {transform: numberAttribute});

  public readonly interaction = computed(() => new PinchRotate({
      duration: this.duration(),
      threshold: this.threshold()
    })
  );
}
