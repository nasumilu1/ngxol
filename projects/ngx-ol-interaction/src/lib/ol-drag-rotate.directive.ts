import {computed, Directive, input, numberAttribute} from '@angular/core';
import {OlInteraction} from "@nasumilu/ngx-ol-map";
import DragRotate from "ol/interaction/DragRotate";

/**
 * Directive to create and configure a DragRotate interaction for OpenLayers maps.
 *
 * This directive extends the `OlInteraction` base class and provides functionality to enable
 * drag-rotation of the map. It uses OpenLayers' `DragRotate` interaction internally.
 *
 * Properties:
 * - `duration`: A reactive input that sets the animation duration (in milliseconds)
 *   for the map rotation. The default duration is 250.
 *
 * Computed Properties:
 * - `interaction`: A computed property that initializes and returns an instance of
 *   the `DragRotate` interaction. It uses the specified `duration` for the rotation animation.
 */
@Directive({
  selector: 'ol-drag-rotate',
  providers: [{provide: OlInteraction, useExisting: OlDragRotateDirective}]
})
export class OlDragRotateDirective extends OlInteraction<DragRotate> {

  public readonly duration = input(250, {transform: numberAttribute});

  public readonly interaction = computed(() => new DragRotate({
      duration: this.duration()
    })
  );

}
