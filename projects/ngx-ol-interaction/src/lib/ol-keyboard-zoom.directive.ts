import {
  computed,
  Directive, input, numberAttribute,
} from '@angular/core';
import {OlInteraction} from "@nasumilu/ngx-ol-map";
import KeyboardZoom from "ol/interaction/KeyboardZoom";


/**
 * Directive for enabling keyboard-zoom interaction with OpenLayers maps.
 *
 * This directive extends the `OlInteraction` base class and utilizes the
 * `KeyboardZoom` interaction from OpenLayers. It provides properties to
 * configure the zoom behavior when triggered by specific keyboard events.
 *
 * Properties:
 * - `duration`: Configurable duration in milliseconds for the zoom animation.
 * - `delta`: Configurable zoom level increment or decrement value.
 *
 * The directive computes an instance of the `KeyboardZoom` interaction using
 * the defined properties.
 */
@Directive({
  selector: 'ol-keyboard-zoom',
  providers: [{provide: OlInteraction, useExisting: OlKeyboardZoomDirective}],
})
export class OlKeyboardZoomDirective extends OlInteraction<KeyboardZoom> {

  public readonly duration = input(100, {transform: numberAttribute});
  public readonly delta = input(1, {transform: numberAttribute});

  public readonly interaction = computed(() => new KeyboardZoom({
      duration: this.duration(),
      delta: this.delta()
    })
  );

}
