import {computed, Directive, input, numberAttribute} from '@angular/core';
import PinchZoom from "ol/interaction/PinchZoom";
import { OlInteraction } from '@nasumilu/ngx-ol-map';

@Directive({
  selector: 'ol-pinch-rotate',
  providers:[{provide: OlInteraction, useExisting: OlPinchZoomDirective}]
})
export class OlPinchZoomDirective extends OlInteraction<PinchZoom>{

  public readonly duration = input(400, {transform: numberAttribute});

  public readonly interaction = computed(() => new PinchZoom({
    duration: this.duration()
  }));


}
