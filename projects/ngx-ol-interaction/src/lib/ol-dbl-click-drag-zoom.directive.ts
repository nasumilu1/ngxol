import {computed, Directive, input, numberAttribute} from '@angular/core';
import {OlInteraction} from "@nasumilu/ngx-ol-map";
import DblClickDragZoom from "ol/interaction/DblClickDragZoom";

/**
 * Directive for creating an OpenLayers DblClickDragZoom interaction.
 * This directive can be used to configure and add a `DblClickDragZoom` interaction
 * to an OpenLayers map. It provides a way to set properties such as duration
 * and zoom delta for the interaction.
 *
 * The interaction allows the user to zoom the map by double-clicking and
 * dragging the mouse.
 *
 * Extends:
 *   OlInteraction<DblClickDragZoom>
 *
 * Inputs:
 * - `duration`: The duration of the zoom animation in milliseconds. Defaults to 400ms.
 * - `delta`: The zoom delta applied on each zoom. Defaults to 1.
 *
 * Computed Properties:
 * - `interaction`: Creates a new `DblClickDragZoom` interaction based on the configured
 *   duration and delta inputs.
 */
@Directive({
  selector: 'ol-dbl-click-drag-zoom',
  providers: [{provide: OlInteraction, useExisting: OlDblClickDragZoomDirective}]
})
export class OlDblClickDragZoomDirective extends OlInteraction<DblClickDragZoom> {

  public readonly duration = input(400, {transform: numberAttribute});
  public readonly delta = input(1, {transform: numberAttribute});

  readonly interaction = computed(() => new DblClickDragZoom({
      duration: this.duration(),
      delta: this.delta()
    })
  );

}
