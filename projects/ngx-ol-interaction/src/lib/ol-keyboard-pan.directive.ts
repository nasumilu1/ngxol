import {computed, Directive, input, numberAttribute} from '@angular/core';
import {OlInteraction} from "@nasumilu/ngx-ol-map";
import KeyboardPan from "ol/interaction/KeyboardPan";

/**
 * Directive that provides a customized OpenLayers KeyboardPan interaction.
 *
 * This directive allows users to pan the map using keyboard arrow keys. The duration
 * and pixel delta for the pan action can be configured through the provided inputs.
 *
 * Selector: 'ol-keyboard-pan'
 *
 * Extends:
 * - OlInteraction<KeyboardPan>
 *
 * Dependencies:
 * - OpenLayers `KeyboardPan` interaction
 *
 * Inputs:
 * - `duration`: Sets the duration of the pan animation in milliseconds.
 * - `pixelDelta`: Sets the number of pixels to move with each pan action.
 *
 * Computed Property:
 * - `interaction`: Returns an instance of the `KeyboardPan` interaction with
 *   the configured duration and pixel delta.
 */
@Directive({
  selector: 'ol-keyboard-pan',
  providers: [{provide: OlInteraction, useExisting: OlKeyboardPanDirective}]
})
export class OlKeyboardPanDirective extends OlInteraction<KeyboardPan> {

  public readonly duration = input(100, {transform: numberAttribute});
  public readonly pixelDelta = input(128, {transform: numberAttribute});

  public readonly interaction = computed(() => new KeyboardPan({
      duration: this.duration(),
      pixelDelta: this.pixelDelta()
    })
  );

}
