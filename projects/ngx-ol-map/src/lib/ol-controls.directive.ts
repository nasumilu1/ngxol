import {
  AfterContentInit,
  contentChildren,
  Directive,
  output,
  Signal
} from '@angular/core';
import Control from 'ol/control/Control';

@Directive()
export abstract class OlControl<T extends Control> {

  public readonly abstract control: Signal<T>;

}

@Directive({selector: 'ol-controls'})
export class OlControlsDirective implements AfterContentInit {

  readonly #controls = new Set<OlControl<Control>>();
  private readonly olControls = contentChildren(OlControl);

  public readonly controlAdded = output<OlControl<Control>>();
  public readonly controlRemoved = output<OlControl<Control>>();

  addControl(control: OlControl<Control>): void {
    if (!this.#controls.has(control)) {
      this.#controls.add(control);
      this.controlAdded.emit(control);
    }
  }

  removeControl(control: OlControl<Control>): void {
    if (this.#controls.delete(control)) {
      this.controlRemoved.emit(control);
    }
  }

  ngAfterContentInit() {
    this.olControls().forEach(OlControlsDirective.prototype.addControl.bind(this));
  }

}
