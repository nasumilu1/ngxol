import {
  AfterViewInit, ChangeDetectionStrategy,
  Component, computed, contentChild,
  ElementRef, inject, input, OnDestroy,
  OnInit, output, viewChild
} from '@angular/core';
import Map, {FrameState} from 'ol/Map';
import {unByKey} from "ol/Observable";
import {Coordinate} from "ol/coordinate";
import {EventsKey} from "ol/events";
import {Pixel} from "ol/pixel";
import {OlLayersDirective} from "./ol-layers.directive";
import {OlControlsDirective} from "./ol-controls.directive";
import {OlOverlaysDirective} from "./ol-overlays.directive";
import {OlInteractionsDirective} from "./ol-interactions.directive";
import {OlViewDirective} from "./ol-view.directive";

export interface MapCLickEvent {
  coordinate: Coordinate;
  pixel: Pixel;
}

@Component({
  selector: 'ol-map',
  changeDetection: ChangeDetectionStrategy.OnPush,
  hostDirectives: [
    {
      directive: OlViewDirective,
      inputs: [
        'projection',
        'multiWorld',
        'showFullExtent',
        'extent',
        'smoothResolutionConstraint',
        'constrainRotation',
        'constrainOnlyCenter',
        'enableRotation',
        'zoomFactor',
        'center',
        'zoom',
        'minZoom',
        'maxZoom',
        'resolution',
        'rotation',
      ],
      outputs: [
        'centerChange',
        'zoomChange',
        'minZoomChange',
        'maxZoomChange',
        'resolutionChange',
        'rotationChange'
      ]
    },
  ],
  template: `<div #map tabindex="0"></div>`,
  styles: `div {width: 100%;height: 100%;}`
})
export class OlMapComponent implements OnInit, AfterViewInit, OnDestroy {

  #eventKeys: EventsKey[] = [];

  private readonly olLayersDirective = contentChild(OlLayersDirective);
  private readonly olOverlaysDirective = contentChild(OlOverlaysDirective);
  private readonly olControlsDirective = contentChild(OlControlsDirective);
  private readonly olInteractionsDirective = contentChild(OlInteractionsDirective);

  readonly #viewDirective = inject(OlViewDirective);

  readonly #map = computed(() => new Map({
    layers: [],
    controls: [],
    overlays: [],
    interactions: []
  }));

  private readonly ele = viewChild('map', {read: ElementRef});

  public readonly name = input<string>();

  // events
  // MapBrowserEvent
  public readonly mapClick = output<MapCLickEvent>();
  public readonly mapDblClick = output<MapCLickEvent>();
  public readonly mapSingleClick = output<MapCLickEvent>();
  public readonly pointerDrag = output<MapCLickEvent>();
  public readonly pointerMove = output<MapCLickEvent>();

  // MapEvent
  public readonly loadStart = output<FrameState>();
  public readonly loadEnd = output<FrameState>();
  public readonly moveEnd = output<FrameState | null>();
  public readonly moveStart = output<FrameState | null>();

  ngOnInit() {
    const map = this.#map();

    this.olLayersDirective()?.layerAdded.subscribe(olLayer => map.addLayer(olLayer.layer()));
    this.olLayersDirective()?.layerRemoved.subscribe(olLayer => map.removeLayer(olLayer.layer()));

    this.olControlsDirective()?.controlAdded.subscribe(olControl => map.addControl(olControl.control()));
    this.olControlsDirective()?.controlRemoved.subscribe(olControl => map.removeControl(olControl.control()));

    this.olInteractionsDirective()?.interactionAdded.subscribe(olInteraction => map.addInteraction(olInteraction.interaction()));
    this.olInteractionsDirective()?.interactionRemoved.subscribe(olInteraction => map.removeInteraction(olInteraction.interaction()));

    this.olOverlaysDirective()?.overlayAdded.subscribe(olOverlay => map.addOverlay(olOverlay.overlay()));
    this.olOverlaysDirective()?.overlayRemoved.subscribe(olOverlay => map.removeOverlay(olOverlay.overlay()));

    this.#eventKeys.push(
      //MapBrowserEvents
      map.on('click', evt => this.mapClick.emit({coordinate: evt.coordinate, pixel: evt.pixel})),
      map.on('dblclick', evt => this.mapDblClick.emit({coordinate: evt.coordinate, pixel: evt.pixel})),
      map.on('singleclick', evt => this.mapSingleClick.emit({coordinate: evt.coordinate, pixel: evt.pixel})),
      map.on('pointerdrag', evt => this.pointerDrag.emit({coordinate: evt.coordinate, pixel: evt.pixel})),
      map.on('pointermove', evt => this.pointerMove.emit({coordinate: evt.coordinate, pixel: evt.pixel})),
      // MapEvents
      map.on('movestart', evt => this.moveStart.emit(evt.frameState)),
      map.on('moveend', evt => this.moveEnd.emit(evt.frameState))
    );

    if (this.name()) {
      map.set('name', this.name());
    }
  }

  ngAfterViewInit() {
    this.#viewDirective.view$.subscribe(view => {
      if (view != undefined) {
        this.#map().setView(view);
      }
    });
    this.#map().setTarget(this.ele()?.nativeElement);
  }

  ngOnDestroy() {
    unByKey(this.#eventKeys);
    this.#eventKeys = [];
  }

}
