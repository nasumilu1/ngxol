import {
  inject,
  InjectionToken,
  Provider
} from '@angular/core';
import {fromEPSGCode, register, setEPSGLookup} from "ol/proj/proj4";
import {HttpClient} from "@angular/common/http";
import {firstValueFrom, from, map, Observable, of, switchMap} from "rxjs";
import proj4 from "proj4";
import {addProjection, Projection, ProjectionLike} from "ol/proj";

export const EPSG_LOOKUP = new InjectionToken<ProjectionLookup>('ProjectionLookupToken');

export abstract class ProjectionLookup {
  public abstract lookup(code: number): Observable<string>;
}

class SpatialReferenceOrgLookup extends ProjectionLookup {

  readonly #client = inject(HttpClient);
  readonly #url = of('https://spatialreference.org/ref/epsg');

  override lookup(code: number): Observable<string> {
    return this.#url.pipe(
      switchMap(url => this.#client.get(`${url}/${code}/proj4.txt`, {responseType: 'text'}))
    );
  }
}

type MapTilerExports = {exports: {proj4: string}};
type MapTilerResults = {
  results: Array<MapTilerExports>
  total: number
}

class MapTilerLookup extends ProjectionLookup {

  readonly #client = inject(HttpClient);
  readonly #key: string;
  readonly #url = of(`https://api.maptiler.com/coordinates/search`);

  constructor(apiKey: string) {
    super();
    this.#key = apiKey;
  }


  override lookup(code: number): Observable<string> {
    return this.#url.pipe(
      switchMap(url => this.#client.get<MapTilerResults>(`${url}/${code}.json?transformations=true&exports=true&key=${this.#key}`)),
      map(response => {
        if (response.total === 0) {
          throw new Error(`No projection found for code ${code}`);
        }
        return response.results[0].exports.proj4;
      })
    );
  }
}

export class Proj4Service {

  readonly #epsgLookupFn = inject(EPSG_LOOKUP);

  constructor() {
    setEPSGLookup(Proj4Service.prototype.lookup.bind(this));
  }

  public async lookup(code: number): Promise<string> {
    return await firstValueFrom(this.#epsgLookupFn.lookup(code));
  }

  getProjection(code: number | ProjectionLike): Observable<Projection> {
    if (undefined == code) {
      code = 3857;
    }
    return code instanceof Projection ? of(code) : from(fromEPSGCode(code));
  }

}

export type ProjectionLookupFactory = () => ProjectionLookup;

export function withSpatialReferenceOrg(): ProjectionLookupFactory {
  return () => new SpatialReferenceOrgLookup();
}

export function withMapTilerApi(apiKey: string): ProjectionLookupFactory {
  return () => new MapTilerLookup(apiKey);
}

export type NamedProjection = {
  code: number|string;
  proj4: string;
};

export function provideProj4(provider: ProjectionLookupFactory, ...projections: (Projection|NamedProjection)[]): Provider[] {

  // register proj4
  projections.forEach(projection => {
    if (projection instanceof Projection) {
      addProjection(projection);
    } else {
      proj4.defs(`${projection.code}`, projection.proj4);
    }
  });
  register(proj4);

  return [
    {provide: Proj4Service, useClass: Proj4Service},
    {provide: EPSG_LOOKUP, useFactory: provider}
  ];
}
