import {inject, Pipe, PipeTransform} from '@angular/core';
import {Coordinate, toStringXY, format as coordinateFormat, toStringHDMS} from "ol/coordinate";
import {ProjectionLike} from "ol/proj";
import {Proj4Service} from "./ol-projection.service";
import {transform as transformFn} from 'ol/proj';
import {forkJoin, map, Observable, of} from "rxjs";

export type CoordinateFormat = 'hdms' | 'format';

/**
 * A pipe for transforming OpenLayers `Coordinate` objects into string representations
 * based on the specified format and options.
 *
 * The `OlCoordinatePipe` can format a coordinate using a template, convert it into
 * HDMS (degrees, minutes, seconds) format, or represent it as an XY string.
 */
@Pipe({
  name: 'coordinate'
})
export class OlCoordinatePipe implements PipeTransform {

  transform(value: Coordinate | undefined | null, format: CoordinateFormat, fractionDigits?: number, tpl?: string): string | undefined | null {

    if (undefined == value) {
      return value;
    }

    if (format === 'format' && tpl) {
      return coordinateFormat(value, tpl, fractionDigits);
    }

    if (format === 'hdms') {
      return toStringHDMS(value, fractionDigits);
    }

    return toStringXY(value, fractionDigits);

  }

}

/**
 * Pipe that transforms geographical coordinates from a source projection to a destination projection.
 *
 * This pipe uses the Proj4Service to dynamically fetch projection definitions and performs
 * the transformation. If the Proj4Service is not available, it defaults to using the raw
 * source and destination projections provided.
 */
@Pipe({
  name: 'transform'
})
export class OlTransformPipe implements PipeTransform {
  #projService = inject(Proj4Service, {optional: true});

  transform(coordinate: Coordinate | undefined | null, source: ProjectionLike, destination: ProjectionLike): Observable<Coordinate | undefined | null> {

    const source$ = this.#projService?.getProjection(source) ?? of(source);
    const destination$ = this.#projService?.getProjection(destination) ?? of(destination);

    return forkJoin([source$, destination$]).pipe(
      map(([source, destination]) => coordinate ? transformFn(coordinate, source, destination) : coordinate)
    );
  }

}
