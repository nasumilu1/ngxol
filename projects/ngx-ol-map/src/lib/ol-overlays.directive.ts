import {AfterContentInit, contentChildren, Directive, output, Signal} from '@angular/core';
import {Overlay} from 'ol';

export abstract class OlOverlay {

  public abstract overlay: Signal<Overlay>;

}

@Directive({
  selector: 'ol-overlays',
  standalone: true
})
export class OlOverlaysDirective implements AfterContentInit {

  readonly #overlays = new Set<OlOverlay>();
  private readonly ngxOlOverlays = contentChildren(OlOverlay);

  public readonly overlayAdded = output<OlOverlay>();
  public readonly overlayRemoved = output<OlOverlay>();

  public addOverlay(overlay: OlOverlay): void {
    if (!this.#overlays.has(overlay)) {
      this.#overlays.add(overlay);
      this.overlayAdded.emit(overlay);
    }
  }

  public removeOverlay(overlay: OlOverlay): void {
    if(this.#overlays.delete(overlay)) {
      this.overlayRemoved.emit(overlay);
    }
  }

  ngAfterContentInit(): void {
    this.ngxOlOverlays().forEach(OlOverlaysDirective.prototype.addOverlay.bind(this));
  }

}
