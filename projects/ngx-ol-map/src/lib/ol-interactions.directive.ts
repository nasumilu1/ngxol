import {
  AfterContentInit,
  contentChildren,
  Directive,
  model,
  OnDestroy,
  OnInit,
  output,
  Signal
} from '@angular/core';
import Interaction from "ol/interaction/Interaction";
import {EventsKey} from "ol/events";
import {unByKey} from "ol/Observable";

/**
 * OlInteraction is an abstract class representing a directive to manage and synchronize the
 * active state of an OpenLayers interaction with Angular's reactive state management.
 *
 * This class provides functionality to:
 * - Synchronize the active state of an OpenLayers interaction with an Angular reactive signal.
 * - Automatically handle event subscriptions during the lifecycle of the directive.
 *
 * To use this class, extend it and implement the abstract `interaction` property to return
 * an OpenLayers interaction object.
 *
 * @template T Defines the type of OpenLayers Interaction to be used.
 */
@Directive()
export abstract class OlInteraction<T extends Interaction> implements OnInit, OnDestroy {

  protected eventKeys: EventsKey[] = [];
  public readonly active = model(true);
  abstract readonly interaction: Signal<T>;

  ngOnInit() {
    const interaction = this.interaction();

    this.active.subscribe(value => {
      const active = interaction.getActive();
      if (value !== active) {
        interaction.setActive(value);
      }
    })

    this.eventKeys.push(this.interaction().on('change:active', () => {
      const active = interaction.getActive();
      if (this.active() !== active) {
        this.active.set(active);
      }
    }));
  }

  ngOnDestroy() {
    unByKey(this.eventKeys);
    this.eventKeys = [];
  }

}

/**
 * The `OlInteractionsDirective` class is an Angular directive that manages a set of OpenLayers interactions.
 * This directive provides functionality to add or remove OpenLayers interactions and emits events when such actions occur.
 *
 * It works by maintaining an internal set of interactions and ensuring that interactions are added or removed consistently.
 *
 * Directive Selector:
 * - `ol-interactions`
 *
 * Implements:
 * - `AfterContentInit` lifecycle hook for initialization after content projection.
 *
 * Properties:
 * - `interactionAdded`: An event emitter that emits when a new interaction is added.
 * - `interactionRemoved`: An event emitter that emits when an interaction is removed.
 *
 * Methods:
 * - `addInteraction(interaction: OlInteraction<Interaction>)`: Adds a new interaction to the internal set if it is not already present, and emits the `interactionAdded` event.
 * - `removeInteraction(interaction: OlInteraction<Interaction>)`: Removes an interaction from the internal set if it is present, and emits the `interactionRemoved` event.
 */
@Directive({selector: 'ol-interactions'})
export class OlInteractionsDirective implements AfterContentInit {

  readonly #interactions = new Set<OlInteraction<Interaction>>();
  private readonly interactions = contentChildren(OlInteraction);

  public readonly interactionAdded = output<OlInteraction<Interaction>>();
  public readonly interactionRemoved = output<OlInteraction<Interaction>>();

  addInteraction(interaction: OlInteraction<Interaction>): void {
    if (!this.#interactions.has(interaction)) {
      this.#interactions.add(interaction);
      this.interactionAdded.emit(interaction);
    }
  }

  removeInteraction(interaction: OlInteraction<Interaction>): void {
    if (this.#interactions.delete(interaction)) {
      this.interactionRemoved.emit(interaction);
    }
  }

  ngAfterContentInit() {
    this.interactions().forEach(OlInteractionsDirective.prototype.addInteraction.bind(this));
  }


}
