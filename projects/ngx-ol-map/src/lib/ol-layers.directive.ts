import {
  AfterContentInit,
  contentChildren,
  Directive,
  output,
  Signal
} from '@angular/core';
import BaseLayer from 'ol/layer/Base';

export abstract class OlLayer<T extends BaseLayer> {

  abstract readonly layer: Signal<T>;

}

@Directive({selector: 'ol-layers'})
export class OlLayersDirective implements AfterContentInit {

  readonly #layers = new Set<OlLayer<BaseLayer>>();
  private readonly ngxOlLayers = contentChildren(OlLayer);

  public readonly layerAdded = output<OlLayer<BaseLayer>>();
  public readonly layerRemoved = output<OlLayer<BaseLayer>>();

  get layers(): OlLayer<BaseLayer>[] {
    return Array.from(this.#layers.values());
  }

  addLayer(layer: OlLayer<BaseLayer>): void {
    if (!this.#layers.has(layer)) {
      this.#layers.add(layer);
      this.layerAdded.emit(layer);
    }
  }

  removeLayer(layer: OlLayer<BaseLayer>): void {
    if (this.#layers.delete(layer)) {
      this.layerRemoved.emit(layer);
    }
  }

  ngAfterContentInit(): void {
    this.ngxOlLayers().forEach(OlLayersDirective.prototype.addLayer.bind(this));
  }

}

