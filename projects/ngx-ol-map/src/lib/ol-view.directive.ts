import {
  booleanAttribute,
  Directive, inject,
  input,
  model, numberAttribute,
  OnDestroy,
  OnInit
} from '@angular/core';

import View from "ol/View";
import {EventsKey} from "ol/events";
import {Coordinate} from "ol/coordinate";
import {unByKey} from "ol/Observable";
import {ProjectionLike} from "ol/proj";
import {Extent} from "ol/extent";
import {Proj4Service} from "./ol-projection.service";
import {BehaviorSubject, of} from "rxjs";


function extentAttribute(value: Extent | string | undefined): Extent | undefined {
  if (typeof value === 'string') {
    value = value.split(',').map(parseFloat);
  }
  return value;
}

function constrainRotationAttribute(value: string | number | boolean): boolean | number {
  if (typeof value === 'number') {
    return value;
  }

  if (typeof value === 'string') {
    const _value = parseInt(value);
    if (isNaN(_value)) {
      return value === 'true';
    } else {
      return _value;
    }
  }
  return false;
}

@Directive()
export class OlViewDirective implements OnInit, OnDestroy {

  readonly #proj4Service = inject(Proj4Service, {optional: true});
  #view?: View;
  #eventKeys: EventsKey[] = [];

  // center
  public readonly center = model<Coordinate>([0, 0]);

  //zoom
  public readonly zoom = model(3);
  public readonly minZoom = model(0);
  public readonly maxZoom = model(28);

  // resolution
  public readonly resolution = model<number>();
  //rotation
  public readonly rotation = model(0);

  // projection
  public readonly projection = input<ProjectionLike>();

  public readonly multiWorld = input(false, {transform: booleanAttribute});
  public readonly showFullExtent = input(false, {transform: booleanAttribute});
  public readonly extent = input(undefined, {transform: extentAttribute});
  public readonly smoothResolutionConstraint = input(true, {transform: booleanAttribute});
  public readonly constrainRotation = input(true, {transform: constrainRotationAttribute});
  public readonly enableRotation = input(true, {transform: booleanAttribute});
  public readonly zoomFactor = input(2, {transform: numberAttribute});
  public readonly constrainOnlyCenter = input(false, {transform: booleanAttribute});


  public readonly view$ = new BehaviorSubject<View|undefined>(this.#view);

  ngOnInit() {
    const projection$ = (this.#proj4Service?.getProjection(this.projection()) ?? of(this.projection()));
    projection$.subscribe(projection => {
      this.#view = new View({
        projection: projection,
        multiWorld: this.multiWorld(),
        showFullExtent: this.showFullExtent(),
        smoothResolutionConstraint: this.smoothResolutionConstraint(),
        constrainRotation: this.constrainRotation(),
        constrainOnlyCenter: this.constrainOnlyCenter(),
        enableRotation: this.enableRotation(),
        zoomFactor: this.zoomFactor(),
        extent: this.extent(),
        center: this.center(),
        zoom: this.zoom(),
        minZoom: this.minZoom(),
        maxZoom: this.maxZoom(),
        resolution: this.resolution(),
        rotation: this.rotation(),
      });

      this.view$.next(this.#view);

      this.center.subscribe(value => {
        const center = this.#view?.getCenter();
        if (center !== value) {
          this.#view?.setCenter(center);
        }
      });
      this.#eventKeys.push(this.#view?.on('change:center', () => {
        const center = this.#view?.getCenter();
        if (undefined != center) {
          this.center.set(center);
        }
      }));

      // zoom
      this.zoom.subscribe(value => {
        const zoom = this.#view?.getZoom();
        if (value && value !== zoom) {
          this.#view?.setZoom(value);
        }
      });
      this.#eventKeys.push(this.#view?.on('change:resolution', () => {
        const zoom = this.#view?.getZoom();
        const resolution = this.#view?.getResolution();

        if (zoom != undefined && zoom != this.zoom()) {
          this.zoom.set(zoom);
        }
        if (resolution != this.resolution()) {
          this.resolution.set(resolution);
        }
      }));
      this.minZoom.subscribe(value => {
        const minZoom = this.#view?.getMinZoom();
        if (value !== minZoom) {
          this.#view?.setMinZoom(value);
        }
      });
      this.maxZoom.subscribe(value => {
        const maxZoom = this.#view?.getMaxZoom();
        if (value !== maxZoom) {
          this.#view?.setMaxZoom(value);
        }
      });

      //resolution
      this.resolution.subscribe(value => {
        const resolution = this.#view?.getResolution();
        if (value !== resolution) {
          this.#view?.setResolution(value);
        }
      });

      // rotation
      this.rotation.subscribe(value => {
        const rotation = this.#view?.getRotation();
        if (value !== rotation) {
          this.#view?.setRotation(value);
        }
      });
      this.#eventKeys.push(this.#view?.on('change:rotation', () => {
        const rotation = this.#view?.getRotation();
        if (rotation != this.rotation() && this.#view) {
          this.rotation.set(this.#view.getRotation());
        }
      }));

    });
  }

  ngOnDestroy() {
    unByKey(this.#eventKeys);
    this.#eventKeys = [];
  }


}
