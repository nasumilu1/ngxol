/*
 * Public API Surface of ngx-ol-source
 */


export * from './lib/ol-source.directive';
export * from './lib/ol-vector-source.directive';
