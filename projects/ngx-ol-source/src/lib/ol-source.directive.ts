import {booleanAttribute, Directive, inject, input, model} from '@angular/core';
import {AttributionLike, Options} from "ol/source/Source";
import {ProjectionLike} from "ol/proj";
import {Proj4Service} from "@nasumilu/ngx-ol-map";
import {map, Observable, of} from "rxjs";

@Directive()
export class OlSourceDirective {

  protected readonly proj4Service = inject(Proj4Service, {optional: true});

  public readonly attributions = model<AttributionLike>();
  public readonly attributionsCollapsible = input(true, {transform: booleanAttribute});
  public readonly wrapX = input(true, {transform: booleanAttribute});
  public readonly projection = input<ProjectionLike>();
  public readonly interpolate = input(false, {transform: booleanAttribute});

  readonly #options = (this.proj4Service?.getProjection(this.projection()) ?? of(this.projection()))
    .pipe(
      map(projection => ({
        projection,
        attributions: this.attributions(),
        attributionsCollapsible: this.attributionsCollapsible(),
        wrapX: this.wrapX(),
        interpolate: this.interpolate()
      }))
    );

  protected options$(): Observable<Options> {
    return this.#options;
  }

}

