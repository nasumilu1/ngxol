import {
  booleanAttribute, Directive, inject, InjectionToken, input, numberAttribute, OnDestroy, OnInit, output, Provider
} from '@angular/core';
import {OlBaseVectorLayerDirective} from "@nasumilu/ngx-ol-layer";
import Collection from "ol/Collection";
import {Feature} from "ol";
import {Geometry, Point} from "ol/geom";
import {OlSourceDirective} from "./ol-source.directive";
import VectorSource, {LoadingStrategy, Options} from "ol/source/Vector";
import {Projection, ProjectionLike} from "ol/proj";
import {FeatureLoader, FeatureUrlFunction} from "ol/featureloader";
import {catchError, first, forkJoin, map, Observable, of, switchMap, tap} from "rxjs";
import EsriJSON from "ol/format/EsriJSON";
import {GeoJSON, KML, TopoJSON, WFS, WKT} from "ol/format";
import GML2 from "ol/format/GML2";
import GML3 from "ol/format/GML3";
import GML32 from "ol/format/GML32";
import {EventsKey} from "ol/events";
import {unByKey} from "ol/Observable";
import {FeatureLike} from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import {Extent} from "ol/extent";
import FeatureFormat from "ol/format/Feature";
import {HttpClient} from "@angular/common/http";
import {Cluster} from "ol/source";
import {tile} from "ol/loadingstrategy";
import {createXYZ} from "ol/tilegrid";
import {toFixed} from "ol/math";

/**
 * Represents an event related to a vector source.
 */
export type VectorSourceEvent = {
  layer: OlBaseVectorLayerDirective<VectorLayer>;
  feature?: FeatureLike[]
}

/**
 * A type definition for a function that handles the successful loading of features.
 *
 * This function is invoked when a set of features with associated geometry is successfully loaded.
 */
export type LoadSuccessFn = (features: Feature<Geometry>[]) => void;
/**
 * Defines a function type used to handle load failures.
 *
 * This function is invoked when an error occurs during a loading process.
 * It takes a single argument, an Error object, which provides details about the failure.
 */
export type LoadFailureFn = () => void;
export type FeatureLoaderFactory = (url: string | FeatureUrlFunction, format: FeatureFormat) => FeatureLoader;

export type ClusterFn = (point: Point, features: Feature<Geometry>[]) => Feature<Point>;

export const OL_FEATURE_LOADER_FACTORY = new InjectionToken<FeatureLoaderFactory>('FeatureLoaderFactoryToken');

export function provideFeatureLoader(loaderFactory: (httpClient: HttpClient) => FeatureLoaderFactory): Provider {
  return {provide: OL_FEATURE_LOADER_FACTORY, useFactory: loaderFactory, deps: [HttpClient]};
}

export function withHttpClient(): (httpClient: HttpClient) => FeatureLoaderFactory {
  return function (httpClient: HttpClient) {
    return function (url: string | FeatureUrlFunction, format: FeatureFormat): FeatureLoader {
      return function (extent: Extent, resolution: number, projection: Projection,
                       success: LoadSuccessFn | undefined, failure: LoadFailureFn | undefined): void {
        const url_ = typeof url === 'function' ? url(extent, resolution, projection) : url;
        const responseType = format.getType() === 'arraybuffer' ? 'arraybuffer' : 'text';
        // @ts-ignore
        httpClient.get(url_, {responseType}).pipe(
          catchError(error => {
            // @ts-ignore
            this.removeLoadedExtent(extent);
            failure?.();
            return of(undefined);
          })
        ).subscribe(response => {
          if (response != undefined) {
            const features = format.readFeatures(response, {extent: extent, featureProjection: projection});
            // @ts-ignore
            this.addFeatures(features);
            success?.(features);
          }
        });
      }
    }
  }
}

const DEFAULT_CLUSTER_FN = (point: Point, features: Feature<Geometry>[]): Feature<Point> => {
  return new Feature({
    geometry: point,
    features: features,
    size: features.length > 1 ? `${features.length}` : ''
  })
}

@Directive()
export abstract class OlVectorSourceDirective extends OlSourceDirective implements OnInit, OnDestroy {

  readonly #loader = inject(OL_FEATURE_LOADER_FACTORY, {optional: true});

  #eventKeys: EventsKey[] = [];
  protected readonly olLayer = inject(OlBaseVectorLayerDirective);

  public readonly overlaps = input(true, {transform: booleanAttribute});
  public readonly useSpatialIndex = input(true, {transform: booleanAttribute});
  public readonly strategy = input<LoadingStrategy>();

  public readonly featureAdded = output<VectorSourceEvent>();
  public readonly featureRemoved = output<VectorSourceEvent>();
  public readonly featureLoadStarted = output<VectorSourceEvent>();
  public readonly featureLoadEnded = output<VectorSourceEvent>();
  public readonly featureLoadError = output<VectorSourceEvent>();

  // cluster
  public readonly cluster = input(false, {transform: booleanAttribute});
  public readonly clusterDistance = input(20, {transform: numberAttribute});
  public readonly minDistance = input(0, {transform: numberAttribute});
  public readonly createCluster = input<ClusterFn>(DEFAULT_CLUSTER_FN);

  protected override options$(): Observable<Options> {
    return super.options$().pipe(
      map(options => Object.assign(options, {
        overlaps: this.overlaps(),
        useSpatialIndex: this.useSpatialIndex(),
        strategy: this.strategy()
      }))
    );
  }

  #source$(): Observable<VectorSource<Feature<Geometry>>> {
    return this.options$().pipe(
      map(options => {
        if (options.url && options.format && this.#loader) {
          options = Object.assign(options, {loader: this.#loader(options.url, options.format)});
        }
        return options;
      }),
      map(options => new VectorSource(options)),
      map(source => {
        if (this.cluster()) {
          source = new Cluster({
            source,
            distance: this.clusterDistance(),
            minDistance: this.minDistance(),
            createCluster: this.createCluster()
          })
        }
        return source;
      })
    );
  }

  ngOnInit() {
    this.#source$()
      .pipe(
        first(),
        tap(source => {
          this.#eventKeys.push(
            source.on(
              'addfeature',
              evt => this.featureAdded.emit({
                layer: this.olLayer,
                feature: evt.feature ? [evt.feature] : undefined
              })
            ),
            source.on(
              'removefeature',
              evt => this.featureRemoved.emit({
                layer: this.olLayer,
                feature: evt.feature ? [evt.feature] : undefined
              })
            ),
            source.on(
              'featuresloadstart',
              evt => this.featureLoadStarted.emit({
                layer: this.olLayer,
                feature: evt.features
              })
            ),
            source.on(
              'featuresloadend',
              evt => this.featureLoadEnded.emit({
                layer: this.olLayer,
                feature: evt.features
              })
            ),
            source.on('featuresloaderror', evt => this.featureLoadError.emit({
              layer: this.olLayer
            }))
          );
        })
      ).subscribe(source => this.olLayer.layer().setSource(source));
  }

  ngOnDestroy() {
    unByKey(this.#eventKeys);
    this.#eventKeys = [];
  }
}

@Directive({
  selector: '[features]'
})
export class OlFeatureSourceDirective extends OlVectorSourceDirective {

  public readonly features = input.required<Collection<Feature<Geometry>> | Feature<Geometry>[]>();

  protected override options$(): Observable<Options> {
    let features = this.features();
    if (Array.isArray(features)) {
      features = new Collection(features);
    }
    return super.options$().pipe(
      map(options => Object.assign(options, {features: features}))
    );
  }
}

@Directive({
  selector: '[wkt]'
})
export class OlWktSourceDirective extends OlVectorSourceDirective {

  public readonly splitCollection = input(false, {transform: booleanAttribute});
  public readonly dataProjection = input<ProjectionLike>('EPSG:4326');
  public readonly featureProjection = input<ProjectionLike>('EPSG:3857');
  public readonly wkt = input.required<string>();

  protected override options$(): Observable<Options> {
    const format = new WKT({splitCollection: this.splitCollection()});

    const projections$ = forkJoin([
      (this.proj4Service?.getProjection(this.featureProjection()) ?? of(this.featureProjection())),
      (this.proj4Service?.getProjection(this.dataProjection()) ?? of(this.dataProjection())),
    ]).pipe(
      map(([featureProjection, dataProjection]) => ({dataProjection, featureProjection}))
    );

    return super.options$().pipe(
      switchMap(options => projections$.pipe(
        map(projection =>
          Object.assign(options, {features: new Collection<Feature<Geometry>>(format.readFeatures(this.wkt(), projection))})
        )
      ))
    );
  }
}

@Directive({
  selector: '[esrijson]'
})
export class OlEsriJsonSourceDirective extends OlVectorSourceDirective {

  public readonly geometryName = input<string>();
  public readonly esrijson = input.required<string | FeatureUrlFunction>();

  protected override options$(): Observable<Options> {
    return super.options$().pipe(
      map(options => Object.assign(options, {
        format: new EsriJSON({geometryName: this.geometryName()}),
        url: this.esrijson()
      }))
    );
  }
}

@Directive({
  selector: '[geojson]'
})
export class OlGeoJsonSourceDirective extends OlVectorSourceDirective {

  public readonly dataProjection = input<ProjectionLike>('EPSG:4326');
  public readonly geojson = input.required<string | FeatureUrlFunction>();

  protected override options$(): Observable<Options> {
    return super.options$().pipe(
      switchMap(options => (this.proj4Service?.getProjection(this.dataProjection()) ?? of(this.dataProjection()))
        .pipe(
          map(dataProjection => Object.assign(options, {
            url: this.geojson(),
            format: new GeoJSON({
              dataProjection: dataProjection
            })
          }))
        )
      )
    );
  }
}

@Directive({
  selector: '[kml]'
})
export class OlKmlSourceDirective extends OlVectorSourceDirective {

  public readonly showPointNames = input(true, {transform: booleanAttribute});
  public readonly extractStyles = input(true, {transform: booleanAttribute});
  public readonly kml = input.required<string | FeatureUrlFunction>();

  protected override options$(): Observable<Options> {
    return super.options$().pipe(
      map(options => Object.assign(options, {
        format: new KML({
          extractStyles: this.extractStyles(),
          showPointNames: this.showPointNames()
        }),
        url: this.kml()
      }))
    );
  }
}

@Directive({
  selector: '[topojson]'
})
export class OlTopoJsonSource extends OlVectorSourceDirective {

  public readonly dataProjection = input<ProjectionLike>('EPSG:4326');
  public readonly layerName = input<string>();
  public readonly layers = input<string[]>();

  public readonly topojson = input.required<string | FeatureUrlFunction>();

  protected override options$(): Observable<Options> {
    return super.options$().pipe(
      switchMap(options => (this.proj4Service?.getProjection(this.dataProjection()) ?? of(this.dataProjection()))
        .pipe(
          map(dataProjection => Object.assign(options, {
            url: this.topojson(),
            format: new TopoJSON({
              dataProjection: dataProjection,
              layerName: this.layerName(),
              layers: this.layers()
            })
          }))
        )
      )
    );
  }
}

export type GMLVersion = '2.0' | '3.0' | '3.2'
const GML_VERSION = {
  '1.0.0': GML2,
  '1.1.0': GML3,
  '2.0': GML2,
  '2.0.0': GML32,
  '3.0': GML3,
  '3.2': GML32
};

export type WFSVersion = '1.0.0' | '1.1.0' | '2.0.0'

@Directive({
  selector: '[wfs]'
})
export class OlWFSSourceDirective extends OlVectorSourceDirective {

  public readonly layer = input.required<string>();
  public readonly wfs = input.required<string>();
  public readonly gml = input<GMLVersion>();
  public readonly version = input<WFSVersion>('1.1.0');

  private url(extent: Extent, resolution: number, projection: Projection): string {
    const version = this.version();
    const params = {
      service: 'WFS',
      version,
      request: 'GetFeature',
      typeName: this.layer(),
      bbox: `${extent.map(value => toFixed(value, 6)).join(',')},${projection.getCode()}`,
      outputFormat: GML_VERSION[this.gml() ?? version].name,
      srsName: projection.getCode()
    };
    return `${this.wfs()}?${new URLSearchParams(params).toString()}`;
  }

  protected override options$(): Observable<Options> {
    return super.options$().pipe(
      map(options => Object.assign(options, {
        format: new WFS({version: this.version(), gmlFormat: new GML_VERSION[this.gml() ?? this.version()]()}),
        strategy: tile(createXYZ({tileSize: 512})),
        url: OlWFSSourceDirective.prototype.url.bind(this)
      }))
    );
  }
}
